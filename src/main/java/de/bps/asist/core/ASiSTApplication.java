package de.bps.asist.core;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import de.bps.asist.R;
import de.bps.asist.core.manager.ASiSTModuleManager;
import de.bps.asist.core.manager.environment.ASiSTCoreDataManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.manager.preferences.ASiSTPreferenceManager;
import de.bps.asist.core.manager.push.PushNotificationManager;
import de.bps.asist.core.manager.update.UpdateAlarmReceiver;
import de.bps.asist.core.manager.update.UpdateManager;
import de.bps.asist.module.AbstractAsistModule;

public class ASiSTApplication extends Application {

    private static final String ONLINE_STATUS = "online_status";
    private static final String TAG = ASiSTApplication.class.getSimpleName();
    private static final String ASIST_START_MODULE_NAME = "de.bps.asist.module.start.AsistStartModule";
    private static Context mContext;
    //Update alarm
    private PendingIntent pendingIntent;
    private AlarmManager manager;
    
    public ASiSTApplication() {
        super();
        ////Log.d(TAG, "Starting ASISTApplication now with mode "+ BuildConfig.BUILD_TYPE+" and debug state "+BuildConfig.DEBUG);
    }
    
    
    
    public static Context getContext() {
        return mContext;
    }
    
    private static AbstractAsistModule loadModule(String name) {
        try {
            Class<?> cl = Class.forName(name);
            if (AbstractAsistModule.class.isAssignableFrom(cl)) {
                return (AbstractAsistModule) cl.newInstance();
            }
        } catch (Exception e) {
            //Log.e(TAG, "could not load module " + name, e);
        }
        return null;
    }
    
    public static void startASiST(final Context context) {
        ASiSTCoreDataManager.getInstance().init(context);
        
        String[] moduleNames = context.getResources().getStringArray(R.array.asist_modules);
        ASiSTModuleManager.getInstance().resetModuleList();
        for (String moduleName : moduleNames) {
            loadAndRegisterModule(moduleName, true);
        }
        if (!Arrays.asList(moduleNames).contains(ASIST_START_MODULE_NAME)) {
            loadAndRegisterModule(ASIST_START_MODULE_NAME, false);
        }
        final EnvironmentManager em = EnvironmentManager.getInstance();
        em.setUpdateOngoing(context, false);

        final ASiSTPreferenceManager pm = ASiSTPreferenceManager.getInstance();
        pm.initPreferences(context);
        // context.registerReceiver(updateReceiver, new IntentFilter(Helper.BROADCAST_INTENT_UPDATE_NOTIFICATIONS));

        //timer task
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            if (isNetworkAvailable(context)) {
                                ////Log.i(TAG, "Network available");
                                // Wenn ja
                                em.setEnvironmentBoolean(context, ONLINE_STATUS, true);
                                final boolean fcmActivated = em.isFcmActivated(context, true);
                                if (fcmActivated) {
                                    //Try to register this device with fcm. Does nothing if already registered
                                    PushNotificationManager.getInstance().tryRegisterDevice(context);
                                }
                                final UpdateManager um = UpdateManager.getInstance();
                                um.updateAll(context);
                            } else {
                                ////Log.i(TAG, "Network not available");
                                // Wenn kein Netzwerk verfügbar
                                em.setEnvironmentBoolean(context, ONLINE_STATUS, false);
                            }
                        } catch (Exception e) {
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 600000); // every 10 minutes
        
    }

    private static void loadAndRegisterModule(String asistStartModuleName, boolean shown) {
        AbstractAsistModule module = loadModule(asistStartModuleName);
        if (module != null && module.isEnabled() && !module.isLoaded()) {
            ASiSTModuleManager.getInstance().register(module);
            module.setLoaded(true);
            module.setShown(shown);
        }
    }

    protected static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null, otherwise check
        // if we are connected
        return networkInfo != null && networkInfo.isConnected();
    }
    
    @Override
    public void onCreate() {
        super.onCreate();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .diskCacheFileCount(25)
                .diskCacheSize(50 * 1024 * 1024)
                .build();

        mContext = this;
        ImageLoader.getInstance().init(config);
        //Setup update alarm
        Intent alarmIntent = new Intent(this, UpdateAlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int interval = 3600000;
        
        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        //Toast.makeText(this, "Alarm Set", Toast.LENGTH_SHORT).show();

        PeriodicWorkRequest moduleDataUpdateWorkRequest = new PeriodicWorkRequest.Builder(ModuleDataUpdateWorker.class, 1, TimeUnit.HOURS)
                .addTag("module_data_update")
                .setConstraints(
                        new Constraints.Builder()
                                .setRequiresBatteryNotLow(true)
                                .setRequiredNetworkType(NetworkType.CONNECTED)
                                .build())
                .build();

        WorkManager.getInstance().enqueueUniquePeriodicWork("module_data_update", ExistingPeriodicWorkPolicy.KEEP, moduleDataUpdateWorkRequest);
    }
    
    
    //    private static class MyTUActivityBroadcastUpdateReceiver extends BroadcastReceiver {
    //
    //        @Override
    //        public void onReceive(final Context context, final Intent intent) {
    //            if (intent.getAction().equalsIgnoreCase(BROADCAST_INTENT_UPDATE_NOTIFICATIONS)) {
    //                Helper.updateAll(MyTUActivity.this);
    //            }
    //        }
    //
    //    }
    
}
