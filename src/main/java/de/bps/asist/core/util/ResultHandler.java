package de.bps.asist.core.util;

import com.loopj.android.http.ResponseHandlerInterface;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

import java.io.IOException;
import java.net.URI;

/**
* Created by litho on 26.11.14.
*/
public class ResultHandler implements ResponseHandlerInterface {

    private int responseCode;
    private HttpResponse response;

    /**
     * Returns data whether request completed successfully
     *
     * @param response HttpResponse object with data
     * @throws java.io.IOException if retrieving data from response fails
     */
    @Override
    public void sendResponseMessage(HttpResponse response) throws IOException {
        responseCode = response.getStatusLine().getStatusCode();
        this.response = response;
    }

    /**
     * Notifies callback, that request started execution
     */
    @Override
    public void sendStartMessage() {

    }

    /**
     * Notifies callback, that request was completed and is being removed from thread pool
     */
    @Override
    public void sendFinishMessage() {
    }

    /**
     * Notifies callback, that request (mainly uploading) has progressed
     *
     * @param bytesWritten number of written bytes
     * @param bytesTotal   number of total bytes to be written
     */
    @Override
    public void sendProgressMessage(int bytesWritten, int bytesTotal) {

    }

    /**
     * Notifies callback, that request was cancelled
     */
    @Override
    public void sendCancelMessage() {

    }

    /**
     * Notifies callback, that request was handled successfully
     *
     * @param statusCode   HTTP status code
     * @param headers      returned headers
     * @param responseBody returned data
     */
    @Override
    public void sendSuccessMessage(int statusCode, Header[] headers, byte[] responseBody) {
        this.responseCode = statusCode;
    }

    /**
     * Returns if request was completed with error code or failure of implementation
     *
     * @param statusCode   returned HTTP status code
     * @param headers      returned headers
     * @param responseBody returned data
     * @param error        cause of request failure
     */
    @Override
    public void sendFailureMessage(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        if (error != null) {
            ////Log.e(TAG, "error sending synchronous request", error);
        }
        this.responseCode = statusCode;
    }

    /**
     * Notifies callback of retrying request
     *
     * @param retryNo number of retry within one request
     */
    @Override
    public void sendRetryMessage(int retryNo) {

    }

    /**
     * Returns URI which was used to request
     *
     * @return uri of origin request
     */
    @Override
    public URI getRequestURI() {
        return null;
    }

    /**
     * Returns Header[] which were used to request
     *
     * @return headers from origin request
     */
    @Override
    public Header[] getRequestHeaders() {
        return new Header[0];
    }

    /**
     * Helper for handlers to receive Request URI info
     *
     * @param requestURI claimed request URI
     */
    @Override
    public void setRequestURI(URI requestURI) {

    }

    /**
     * Helper for handlers to receive Request Header[] info
     *
     * @param requestHeaders Headers, claimed to be from original request
     */
    @Override
    public void setRequestHeaders(Header[] requestHeaders) {

    }

    /**
     * Can set, whether the handler should be asynchronous or synchronous
     *
     * @param useSynchronousMode whether data should be handled on background Thread on UI Thread
     */
    @Override
    public void setUseSynchronousMode(boolean useSynchronousMode) {

    }

    /**
     * Returns whether the handler is asynchronous or synchronous
     *
     * @return boolean if the ResponseHandler is running in synchronous mode
     */
    @Override
    public boolean getUseSynchronousMode() {
        return true;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public HttpResponse getResponse() {
        return response;
    }
}
