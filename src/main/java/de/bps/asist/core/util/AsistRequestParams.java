package de.bps.asist.core.util;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.apache.http.HttpEntity;

import java.io.IOException;

/**
 * Created by litho on 26.11.14.
 * Request Params
 */
public class AsistRequestParams extends RequestParams {

    /**
     * Fake a null stream so a multipart entity is created
     * @param progressHandler
     * @return
     * @throws IOException
     */

    public HttpEntity createMultiPartEntity(ResponseHandlerInterface progressHandler) throws IOException {
        HttpEntity result;
        if(streamParams.isEmpty() && fileParams.isEmpty()) {
            StreamWrapper wrapper = new StreamWrapper(
                    null,
                    null,
                    APPLICATION_OCTET_STREAM,
                    true);
            streamParams.put("", wrapper);
            result = super.getEntity(progressHandler);
            streamParams.remove("");
        }else{
            result = super.getEntity(progressHandler);
        }
        return result;
    }
}
