package de.bps.asist.core.util;

import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by litho on 08.10.14.
 * Helper class for sending POST requests
 */
public class ASiSTPostSender extends AsyncTask<String, Void, Exception> {

    private static final String TAG = ASiSTPostSender.class.getSimpleName();

    private static final AsyncHttpClient client = new AsyncHttpClient();
    private static final SyncHttpClient syncClient = new SyncHttpClient();

    private File imageFile;

    public ASiSTPostSender() {

    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(url, params, responseHandler);
    }

    public static void post(String url, AsistRequestParams params, AsyncHttpResponseHandler responseHandler){
        try {
            HttpEntity entity = params.createMultiPartEntity(responseHandler);
            client.post(null, url, entity, null, responseHandler);
        } catch (IOException e) {
            //Log.e(TAG, "could not build entity", e);
        }
    }

    public static ResultHandler postSync(final String url, final RequestParams params) {
        final ResultHandler responseHandler = new ResultHandler();
        Thread thread = new Thread() {
            @Override
            public void run() {
                syncClient.post(url, params, responseHandler);
            }
        };
        try {
            thread.start();
            thread.join();
        } catch (InterruptedException e) {
            ////Log.e(TAG, "could not run post thread", e);
        }

        return responseHandler;
    }

    /**
     * @return an async client when calling from the main thread, otherwise a sync client.
     */
    private static AsyncHttpClient getClient() {
        // Return the synchronous HTTP client when the thread is not prepared
        if (Looper.myLooper() == null)
            return syncClient;
        return client;
    }

    @Override
    protected Exception doInBackground(String... params) {
        if(params.length < 1){
            return new NullPointerException("No url specified");
        }
        if(params.length < 2){
            return new NullPointerException("No deviceToken specified");
        }
        String url = params[0];
        String deviceToken = params[1];
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        try{
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", deviceToken));
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            if(response.getStatusLine().getStatusCode() != 200){
                //Log.e(TAG, "could not send post to url " + url + ", got statuscode "+response.getStatusLine().getStatusCode());
            } else {
                //Log.i(TAG, "Succesfull sended POST-REQUEST with url: " + url);
            }
            return null;
        }catch(Exception e){
            //Log.e(TAG, "Could not send post to url "+url, e);
            return e;
        }
    }


}
