/**
 * 
 */
package de.bps.asist.core.util;

/**
 * @author laeb
 */
public class StringHelper {

	/**
	 * Tests is the given string is null or empty
	 * 
	 * @param toTest
	 * @return True or false! What did you think?
	 */
	public static boolean isNullOrEmpty(final CharSequence toTest) {
		return toTest == null || toTest.length() < 1;
	}
}
