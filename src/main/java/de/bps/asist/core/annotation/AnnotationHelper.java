package de.bps.asist.core.annotation;

import android.annotation.SuppressLint;
import android.util.Log;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import de.bps.asist.core.model.ASiSTDetailDescription;

/**
 * Contains helper methods to handle annotation stuff.
 * 
 * @author bja, laeb
 */
public class AnnotationHelper {

	private static final String LOGGER_TAG = AnnotationHelper.class.getCanonicalName();

	private static final Map<String, String> getterCache = new TreeMap<>();

	/**
	 * @param annotated
	 *            The annotated object
	 * @param annotationClass
	 *            The annotations class (e.g. AsistTitle.class)
	 * @return value or null
	 */
	public static <T> T getValueOf(final Object annotated, final Class<? extends Annotation> annotationClass) {
		return getValueOf(annotated, annotationClass, null);
	}

	/**
	 * @param annotated
	 *            The annotated object
	 * @param annotationClass
	 *            The annotations class (e.g. AsistTitle.class)
	 * @param defaultValue
	 *            The value to return in case there is no such annotated value
	 * @return value or defaultValue
	 */
	public static <T> T getValueOf(final Object annotated, final Class<? extends Annotation> annotationClass, final T defaultValue) {
		if (annotated == null || annotationClass == null) {
			return null;
		}
		T value = defaultValue;
		for (final Field field : annotated.getClass().getDeclaredFields()) {
			if (field.getAnnotation(annotationClass) != null) {
				final String fieldName = field.getName();
				try {
					final String methodName = buildGetterFor(fieldName);
					final Method method;
					method = annotated.getClass().getMethod(methodName);
					@SuppressWarnings("unchecked")
					final T v = (T) method.invoke(annotated);
					value = v;
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e) {
				    ////Log.e(LOGGER_TAG, "Cannot get value of field '" + fieldName + "' and annotation class '" + annotationClass.getName() + "'.", e);
				} catch (final ClassCastException e) {
					////Log.e(LOGGER_TAG, "Invalid return type for field '" + fieldName + "' and annotation class '" + annotationClass.getName() + "'.", e);
				}
				break;
			}
		}
		return value;
	}

	public static Map<Integer, ASiSTDetailDescription> readASiSTDetails(final Object annotated) {
		final Map<Integer, ASiSTDetailDescription> result = new TreeMap<>();
		for (final Field field : annotated.getClass().getDeclaredFields()) {
			final ASiSTDetail detail = field.getAnnotation(ASiSTDetail.class);
			if (detail != null) {
				Object o;
				try {
					final boolean accBefore = field.isAccessible();
					field.setAccessible(true);
					o = field.get(annotated);
					field.setAccessible(accBefore);
                    String value = null;
                    if(o instanceof Collection){
                        StringBuilder sb = new StringBuilder();
                        for(Object item : (Collection) o){
                            if(sb.length() > 0){
                                sb.append("\n");
                            }
                            sb.append(item.toString());
                        }
                        value = sb.toString();
                    }else if (o != null) {
                        value = o.toString();
					}
                    if(value != null){
                        final ASiSTDetailDescription nvp = new ASiSTDetailDescription(detail, value);
                        result.put(detail.position(), nvp);
                    }
				} catch (IllegalAccessException | IllegalArgumentException e) {
					//////Log.e(LOGGER_TAG,
					//		"Could not get field value for field " + field.getName() + " for object class " + annotated.getClass() + ": " + e.getMessage());
				}
			}
		}
		return result;
	}

	/**
	 * Builds the name of the getter method.
	 * 
	 * @param fieldName the name of field to build getter for
	 * @return the designated getter
	 * @throws NoSuchMethodException
	 */
	@SuppressLint("DefaultLocale")
	private static String buildGetterFor(final String fieldName) throws NoSuchMethodException {
		if (fieldName == null || fieldName.length() < 1) {
			throw new NoSuchMethodException();
		}
		String getter = getterCache.get(fieldName);
		if (getter != null) {
			return getter;
		}
		final StringBuilder sb = new StringBuilder("get");
		sb.append(String.valueOf(fieldName.charAt(0)).toUpperCase());
		if (fieldName.length() > 1) {
			sb.append(fieldName.substring(1));
		}
		getter = sb.toString();
		synchronized (getterCache) {
			getterCache.put(fieldName, getter);
		}
		return getter;
	}

	public static Object getValueOf(final Object o, final Field field) {
		Object value = "";
		try {
			final String methodName = buildGetterFor(field.getName());
			final Method method = o.getClass().getMethod(methodName);
			value = method.invoke(o);
			////Log.i("AnnotationHelper", "Field: " + field.getName() + " methodName: " + methodName + " method: " + method.getModifiers() + " value: " + value);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException e) {
			////Log.e(AnnotationHelper.class.getName(),
			//		"Cannot get value of field '" + field.getName(), e);
		}

		return value;
	}

	/**
	 * Returns a boolean value for specified annotation.
	 * 
	 * @param o
	 * @param annotationElementName
	 * @param annotationType
	 * @return
	 */
	public static boolean getBooleanOf(final Object o, final String annotationElementName, final Class<? extends Annotation> annotationType) {
		if (o == null || annotationElementName == null || annotationType == null) {
			return false;
		}
		boolean returnValue = false;
		final Annotation annotation = o.getClass().getAnnotation(annotationType);
		if (annotation != null) {
			try {
				returnValue = (Boolean) annotation.getClass().getDeclaredMethod(annotationElementName).invoke(annotation);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e) {
				////Log.e(LOGGER_TAG, "Cannot get annotation value (" + annotationType.getName() + ", " + annotationElementName + ").");
			}

		}
		return returnValue;
	}

}
