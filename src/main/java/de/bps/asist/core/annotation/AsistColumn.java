/**
 *
 */
package de.bps.asist.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author thomasw
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
@Documented
@Inherited
public @interface AsistColumn {

	/**
	 * optional: position as int
	 */
	int position() default Integer.MAX_VALUE;

	/**
	 * optional: id of a R.String resource used as heading.
	 */
	String header() default "";
}
