package de.bps.asist.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface AsistItemConfiguration {
	boolean showMainLogo() default true;

	boolean showSubLogo() default false;

	boolean showDescription() default true;

	boolean useCallback() default true;
}
