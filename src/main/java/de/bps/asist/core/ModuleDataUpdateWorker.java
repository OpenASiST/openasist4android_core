package de.bps.asist.core;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import de.bps.asist.core.manager.ASiSTModuleManager;
import de.bps.asist.core.manager.update.UpdateManager;
import de.bps.asist.module.AbstractAsistModule;

public class ModuleDataUpdateWorker extends Worker {
    public ModuleDataUpdateWorker(@NonNull Context context, @NonNull WorkerParameters workerParameters) {
        super(context, workerParameters);
        Log.d("module_data_update", this.getClass().getName() + "initialized");
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            Log.d("module_data_update", this.getClass().getName() + " starts module update");
            for (final AbstractAsistModule module : ASiSTModuleManager.getInstance().getModules()) {
                Log.d("module_data_update", this.getClass().getName() + " updating module: " + module.getClass().getSimpleName() );
                module.updateData(this.getApplicationContext());
            }
            UpdateManager.getInstance().setUpdateDone(this.getApplicationContext());
            return Result.success();
        } catch (Exception exception) {
            Log.e("module_data_update", "Error while updating", exception);
            return Result.retry();
        }
    }
}
