package de.bps.asist.core.model;

import java.io.Serializable;
import java.util.List;

public interface IList extends Serializable {

	public List<? extends IListItem> getItems();

}
