package de.bps.asist.core.model;

import java.io.Serializable;

public interface IListItem extends Serializable {

	public Long getId();

}