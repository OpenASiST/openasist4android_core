package de.bps.asist.core.model.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by litho on 13.10.14.
 */
public class OnlyDateParser extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException {

        final String date = jp.getText();
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            return sdf.parse(date);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }

    }
}
