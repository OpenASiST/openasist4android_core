package de.bps.asist.core.model;

import android.util.Log;

import de.bps.asist.R;
import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.annotation.AsistDescription;

public class ASiSTDetailDescription implements IListItem {

	private static final long serialVersionUID = 7095206050829339288L;

    private static final String TAG = AsistDescription.class.getSimpleName();

	private int i18nKey;
	private String value;
	private int position;
	private String category;

	public ASiSTDetailDescription(final ASiSTDetail detail, final String val) {
		position = detail.position();
		category = detail.category();
		value = val;
        try {
            this.i18nKey = R.string.class.getField(detail.i18nKey()).getInt(null);
        }catch(Exception e){
            ////Log.e(TAG, "could not parse i18nkey "+detail.i18nKey());
        }
	}

    public ASiSTDetailDescription(int i18nKey, String value, int position, String category){
        this.i18nKey = i18nKey;
        this.value = value;
        this.position = position;
        this.category = category;
    }

	public int getI18nKey() {
		return i18nKey;
	}

	public void setI18nKey(final int i18nKey) {
		this.i18nKey = i18nKey;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(final int position) {
		this.position = position;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(final String category) {
		this.category = category;
	}

	@Override
	public Long getId() {
		return (long) position;
	}

}
