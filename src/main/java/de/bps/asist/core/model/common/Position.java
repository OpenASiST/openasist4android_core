package de.bps.asist.core.model.common;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import de.bps.asist.core.database.AbstractDatabaseObject;

@DatabaseTable
public class Position extends AbstractDatabaseObject {

	private static final long serialVersionUID = -5560383435431629112L;

	@DatabaseField
	private Float longitude;

	@DatabaseField
	private Float latitude;

	private String entityName;

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(final Float longitude) {
		this.longitude = longitude;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(final Float latitude) {
		this.latitude = latitude;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(final String entityName) {
		this.entityName = entityName;
	}

}
