package de.bps.asist.core.model.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PubdateDeserializer extends JsonDeserializer<Date> {

	// 2014-04-29 10:01

	@Override
	public Date deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException {

		final String date = jp.getText();
		try {
			final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm",
					Locale.getDefault());
			return sdf.parse(date);
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}

	}

}
