package de.bps.asist.core.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.util.JsonReader;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import androidx.core.app.NotificationCompat;
import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.manager.push.PushNotificationManager;
import de.bps.asist.module.canteen.CanteenActivity;
import de.bps.asist.module.feeds.SingleFeedActivity;
import de.bps.asist.module.feeds.model.FeedDescription;
import de.bps.asist.module.lostandfound.LostAndFoundActivity;
import de.bps.asist.module.olat.OlatActivity;
import de.bps.asist.module.feeds.FeedActivity;
import de.bps.asist.module.start.AsistStartActivity;
import de.bps.asist.module.start.AsistStartFragment;
import de.bps.asist.module.start.AsistStartModule;

import static de.bps.asist.gui.list.AbstractGenericListCallback.EXTRA_ITEM_SELECTED;

/**
 * Receives firebase notifications
 * Diese Klasse empfängt und behandelt Firebase Cloud Messaging Nachrichten
 *
 * @date 08.02.2017
 */
public class FirebaseReceiver extends FirebaseMessagingService {
    public static final int NOTIFICATION_ID_NEW_NOTIFICATION = 6953;
    /**
     * ID für die Nachrichtenanzeige in der oberen Leiste von Android
     */
    private static final int NOTIFY_ID = 1234;
    /**
     * Tag-String für die LogCat-Nachricht
     */
    private static final String TAG = "FirebaseReceiver";
    private static final String DATA_KEY_MESSAGE = "message";
    private static final String DATA_KEY_SOUND = "sound";
    private static PushNotificationManager pm = PushNotificationManager.getInstance();
    private static EnvironmentManager em = EnvironmentManager.getInstance();
    private Context context;
    
    
    @Override
    public void onCreate() {
        super.onCreate();
        
        context = getApplicationContext();
    }
    
    /**
     * @param remoteMessage Nachricht mit dem Nachrichteninhalt. Es wird eine Benachrichtigung
     *                      generiert und am oberen Bildschirmrand angezeigt. Ob die
     *                      Benachrichtigung laut oder diskret erfolgt, entscheidet der
     *                      Server.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (em.isPushDisabled(context, false)) {
            return;
        }
        
        Map<String, String> dataMap = remoteMessage.getData();
        //check, if all needed data fileds are present
        if (!dataMap.containsKey("message")) {
            //Log.e(TAG, "Firebase Message misses 'message' key in remoteMessage.getData()");
            return;
        }
        
        // Prüft ob myTU zur Zeit läuft. Wenn ja dann wird keine Nachricht
        // erzeugt sondern ein Update-Broadcast an die App gesendet
        context.sendBroadcast(new Intent(PushNotificationManager.BROADCAST_INTENT_UPDATE_NOTIFICATIONS));
        
        final String message = dataMap.get(DATA_KEY_MESSAGE);
        final String title = context.getResources().getString(R.string.app_name);

        // Creates an explicit intent for an Activity in your app
        String module = dataMap.get("module");
        String details = dataMap.get("details");
        Intent notifIntent;
        if (module == null) {
            notifIntent = new Intent(this, AsistStartFragment.class);
        } else {
            switch (module) {
                case "OlatModule":      notifIntent = new Intent(this, OlatActivity.class);
                    break;
                case "FeedModule":
                    if (details == null) {
                        notifIntent = new Intent(this, FeedActivity.class);
                    } else {
                        try {
                            JSONObject mainObject = new JSONObject(details);
                            Long feedId = mainObject.getLong("feedId");
                            notifIntent = new Intent(this, SingleFeedActivity.class);
                            final DatabaseManager<FeedDescription> db = new DatabaseManager<>(getBaseContext(), FeedDescription.class);
                            final List<FeedDescription> items = db.getAll();
                            for (FeedDescription feedDescription : items) {
                                if (feedDescription.getFeedid().equals(feedId)) {
                                    notifIntent.putExtra(EXTRA_ITEM_SELECTED, feedDescription);
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            notifIntent = new Intent(this, FeedActivity.class);
                        }
                    }
                    break;
                case "LostFoundModule": notifIntent = new Intent(this, LostAndFoundActivity.class);
                    break;
                case "CanteenModule":   notifIntent = new Intent(this, CanteenActivity.class);
                    break;
                default:                notifIntent = new Intent(this, AsistStartFragment.class);
                    break;
            }
        }



        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(AsistStartActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(notifIntent);
        PendingIntent contentIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        final Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_logo)
                .setContentText(message).setContentTitle(title).setContentIntent(contentIntent)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL | Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_ONLY_ALERT_ONCE;
        
        try {
            final String sound = dataMap.get(DATA_KEY_SOUND);
            if (sound == null || sound.equalsIgnoreCase("default")) {
                notification.defaults = Notification.DEFAULT_ALL;
            } else {
                notification.defaults = Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS;
            }
        } catch (final Exception e) {
            notification.defaults = Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS;
        }
        notificationManager.notify(NOTIFICATION_ID_NEW_NOTIFICATION, notification);
    }
    
}
