package de.bps.asist.core.database;

import com.j256.ormlite.field.DatabaseField;

import de.bps.asist.core.model.IListItem;

public abstract class AbstractDatabaseObject implements IListItem {

	private static final long serialVersionUID = 8303198068970289583L;

	@DatabaseField(generatedId = true)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

}
