package de.bps.asist.core.manager.update;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by epereira on 06.05.2016.
 */
public class DateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.i("DATE RECEIVER", "OK");
        final UpdateManager um = UpdateManager.getInstance();
        um.updateAll(context);
    }
}