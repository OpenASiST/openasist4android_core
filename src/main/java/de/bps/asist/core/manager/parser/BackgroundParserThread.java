package de.bps.asist.core.manager.parser;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;

public class BackgroundParserThread<T extends Serializable> extends Thread {

    private static final String TAG = BackgroundParserThread.class.getCanonicalName();

    private final String url;
    private final Class<T> resultClass;
    private final ParserCallback<T> callBack;
    private final ClientHttpRequestFactory clientHttpRequestFactory;

    public BackgroundParserThread(final String url, final Class<T> resultClass, final ParserCallback<T> cb) {
        this(url, resultClass, cb, null);
    }

    public BackgroundParserThread(final String url, final Class<T> resultClass, final ParserCallback<T> cb, ClientHttpRequestFactory clientHttpRequestFactory) {
        this.url = url;
        this.resultClass = resultClass;
        this.callBack = cb;
        this.clientHttpRequestFactory = clientHttpRequestFactory;
    }

    @Override
    public void run() {
        try {
            final RestTemplate restTemplate = new RestTemplate();

            // Override request factory if it is not null
            if( clientHttpRequestFactory != null ) {
                restTemplate.setRequestFactory(this.clientHttpRequestFactory);
            }

            // Add the String message converter
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            // Make the HTTP GET request, marshaling the response to a String
            final T result = restTemplate.getForObject(url, resultClass);
            if(result != null) {
                callBack.afterParse(result);
            }
        } catch (final Exception e) {
            //Log.e(TAG, "could not parseWithThread url " + url, e);
            callBack.onError(e);
        }finally {
            //Log.d(TAG, "finished parsing " + url);
        }
    }

}
