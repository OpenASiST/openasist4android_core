package de.bps.asist.core.manager.parser;

import java.io.Serializable;

import de.bps.asist.module.timetable.model.CourseList;

public interface ParserCallback<T extends Serializable> {
	
	public void afterParse(T parsed);

    public void onError(Exception e);

    public void onManagedError(T parsed);

}
