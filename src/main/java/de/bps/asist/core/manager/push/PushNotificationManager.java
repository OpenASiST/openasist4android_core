package de.bps.asist.core.manager.push;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.UUID;

import de.bps.asist.core.manager.environment.ASiSTCoreDataManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.util.ASiSTPostSender;

public class PushNotificationManager {
    
    public static final String KEY_C2DM_ID = "c2dm_id";
    public static final String C2DM_ID_NOT_SET = "not_set";
    public static final String BROADCAST_INTENT_UPDATE_NOTIFICATIONS = "de.bps.asist.UPDATE_NOTIFICATIONS";
    private static final String USERNAME = "tuifi2011@gmail.com";
    private static final String REGISTER_FOR_C2DM = "com.google.android.c2dm.intent.REGISTER";
    private static final PushNotificationManager INSTANCE = new PushNotificationManager();
    private static final String TAG = "PushNotificationManager";
    
    private PushNotificationManager() {
        //
    }
    
    public static PushNotificationManager getInstance() {
        return INSTANCE;
    }
    
    public void registerDevice(Context context, String token) {
        EnvironmentManager.getInstance().setDeviceToken(context, token);
        ASiSTCoreDataManager.getInstance().init(context);
        final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
        final StringBuilder url = new StringBuilder(rootUrl);
        url.append("/device/");
        url.append(ASiSTCoreDataManager.getInstance().getInstitutionName());
        url.append("/register/ANDROID");
        ASiSTPostSender sender = new ASiSTPostSender();
        sender.execute(url.toString(), token);
    }
    
    public void tryRegisterDevice(Context context) {
        String token = EnvironmentManager.getInstance().getDeviceToken(context);
        
        if (token.equalsIgnoreCase(C2DM_ID_NOT_SET)) {
            token = FirebaseInstanceId.getInstance().getToken();
        
            if (token == null) {
                //Log.w(TAG, "Firebase Device ID is not available. Creation is still running, FirebaseIDService.onTokenRefresh should be called later by the system.");
            } else {
                EnvironmentManager.getInstance().setDeviceToken(context, token);
                registerDevice(context, token);
            }
        }
        
        //do nothing if getDeviceToken is already a real token. If this happens, the token should
        //be already registered properly
    }


    public void registerPushUID(Context context, String pushUid){
        String token = EnvironmentManager.getInstance().getDeviceToken(context);

        if (token == null || token.equalsIgnoreCase(C2DM_ID_NOT_SET)) {
            tryRegisterDevice(context);
        } else {
            EnvironmentManager.getInstance().setDeviceToken(context, token);
            ASiSTCoreDataManager.getInstance().init(context);
            final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
            final StringBuilder url = new StringBuilder(rootUrl);
            url.append("/device/");
            url.append(ASiSTCoreDataManager.getInstance().getInstitutionName());
            url.append("/lms/");
            url.append(pushUid);
            ASiSTPostSender sender = new ASiSTPostSender();
            sender.execute(url.toString(), token);
        }
    }
}
