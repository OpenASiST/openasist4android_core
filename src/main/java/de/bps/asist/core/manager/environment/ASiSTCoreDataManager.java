package de.bps.asist.core.manager.environment;

import android.content.Context;

import de.bps.asist.R;

public class ASiSTCoreDataManager {

	private static final ASiSTCoreDataManager INSTANCE = new ASiSTCoreDataManager();

	private String rootUrl;
	private String institutionName;

	private ASiSTCoreDataManager() {
		//
	}

	public void init(final Context context) {
		rootUrl = context.getResources().getString(R.string.rootUrl);
		institutionName = context.getResources().getString(R.string.institutionName);
	}

	public String getRootUrl() {
		return rootUrl;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public static ASiSTCoreDataManager getInstance() {
		return INSTANCE;
	}

}
