package de.bps.asist.core.manager.update;

import android.content.Context;

import java.util.List;

import de.bps.asist.module.AbstractAsistModule;

public class UpdateThread extends Thread {

	private final List<AbstractAsistModule> modules;
	private final Context context;

	public UpdateThread(final Context context, final List<AbstractAsistModule> modules) {
		this.context = context;
		this.modules = modules;
	}

	@Override
	public void run() {
		for (final AbstractAsistModule module : modules) {
			module.updateData(context);
		}
		UpdateManager.getInstance().setUpdateDone(context);
	}

}
