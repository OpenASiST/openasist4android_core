package de.bps.asist.core.manager.push;

import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;


import de.bps.asist.core.manager.environment.EnvironmentManager;

/**
 * Created by EPereira on 06.02.2017.
 */

public class FirebaseIDService extends FirebaseMessagingService {
    
    private static final String TAG = "FirebaseIDService";
    private EnvironmentManager environment;
    private Context context;
    private PushNotificationManager pnm;
    
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        environment = EnvironmentManager.getInstance();
        pnm = PushNotificationManager.getInstance();
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        // Get updated InstanceID token.
        //String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log.d(TAG, "Refreshed token: " + refreshedToken);
        
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the

        sendRegistrationToServer(token);
    }
    private void sendRegistrationToServer(String token) {
        //this method is to send token to your app server.
        environment.setDeviceToken(context, token);
        pnm.registerDevice(context, token);
    }
    
}