package de.bps.asist.core.manager.parser;

import android.os.AsyncTask;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;

import de.bps.asist.module.timetable.model.CourseList;
import de.bps.asist.module.timetable.model.TimetableResponseErrorHandler;

/**
 * Created by litho on 09.10.14.
 * Background Parser for AsyncTask operations, e.g. for GUI refreshing
 */
public class BackgroundParserTask<T extends Serializable> extends AsyncTask<Void, Void, T>{

    private static final String TAG = BackgroundParserTask.class.getCanonicalName();

    private final String url;
    private final Class<T> resultClass;
    private final ParserCallback<T> callBack;
    private final TimetableResponseErrorHandler eHandler = new TimetableResponseErrorHandler();
    private final ClientHttpRequestFactory clientHttpRequestFactory;

    public BackgroundParserTask(final String url, final Class<T> resultClass, final ParserCallback<T> cb) {
        this(url, resultClass, cb, null);
    }

    public BackgroundParserTask(final String url, final Class<T> resultClass, final ParserCallback<T> cb, ClientHttpRequestFactory clientHttpRequestFactory) {
        this.url = url;
        this.resultClass = resultClass;
        this.callBack = cb;
        this.clientHttpRequestFactory = clientHttpRequestFactory;
    }

    @Override
    protected T doInBackground(final Void... params) {
        try {
            final RestTemplate restTemplate = new RestTemplate();

            // Override request factory if it is not null
            if( clientHttpRequestFactory != null ) {
                restTemplate.setRequestFactory(this.clientHttpRequestFactory);
            }

            // Add the String message converter
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            // Make the HTTP GET request, marshaling the response to a String

            restTemplate.setErrorHandler(eHandler);

            final T result = restTemplate.getForObject(url, resultClass);
            if(result.getClass().equals(CourseList.class)){
                //Log.d(TAG, "could not parseWithThread url " + url);
                callBack.onManagedError(result);
            }
            return result;
        } catch (final Exception e) {
            //Log.e(TAG, "could not parseWithThread url " + url, e);
            callBack.onError(e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(final T result) {
        super.onPostExecute(result);
        if(result != null) {
            callBack.afterParse(result);
        }
    }
}