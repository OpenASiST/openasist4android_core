package de.bps.asist.core.manager.update;

public class ASiSTNotificationManager {
	
	public static final int NOTIFICATION_ID_NEW_NOTIFICATION = 6953;
	public static final int NOTIFICATION_ID_SEND_LOST_AND_FOUND = 6952;

	// IDs für Notifications ins der Kopfzeile
	public static final int NOTIFICATION_ID_UPDATE = 6951;
	
	private static final ASiSTNotificationManager INSTANCE = new ASiSTNotificationManager();
	
	private ASiSTNotificationManager(){
		//
	}
	
	public static ASiSTNotificationManager getInstance() {
		return INSTANCE;
	}
	
	

}
