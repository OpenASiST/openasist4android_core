package de.bps.asist.core.manager.environment;

import android.content.Context;
import android.content.SharedPreferences;

import de.bps.asist.core.manager.push.PushNotificationManager;
import de.bps.asist.module.feedback.FeedbackModule;
import de.bps.asist.module.timetable.TimetableModule;

public class EnvironmentManager {

	public static final String SHAREDPREFERENCE_ENVIRONMENT = "environment";
	private static final String KEY_UPDATE_ONGOING = "key_update_ongoing";
	private static final String FCM_ACTIVATED = "c2dm_activated";
    private static final String KEY_DISABLE_PUSH_NOTIFICATION = "disable_push_notification";
	
	//Now Firebase messaging device id instead of c2dm
    public static final String KEY_C2DM_ID = "c2dm_id";
    public static final String KEY_UPDATE_ON_STARTUP = "update_on_start";
    public static final String KEY_NOTIFICATION_LIMIT = "notification_limit";
    public static final String KEY_FEED_LIMIT = "feed_limit";
    public static final String KEY_PARTICIPANTS_FILTER = "participants_filter";

	private static final EnvironmentManager INSTANCE = new EnvironmentManager();

	private EnvironmentManager() {
		//
	}

	public static EnvironmentManager getInstance() {
		return INSTANCE;
	}

	public boolean isUpdateOngoing(final Context context, final boolean defaultValue){
		return getEnvironmentBoolean(context, KEY_UPDATE_ONGOING, defaultValue);
	}

	public void setUpdateOngoing(final Context context, final boolean value){
		setEnvironmentBoolean(context, KEY_UPDATE_ONGOING, value);
	}

	public boolean isFcmActivated(final Context context, final boolean defaultValue){
		return getEnvironmentBoolean(context, FCM_ACTIVATED, defaultValue);
	}

    public boolean isPushDisabled(final Context context, final boolean defaultValue){
        return getEnvironmentBoolean(context, KEY_DISABLE_PUSH_NOTIFICATION, defaultValue);
    }

    public void setPushDisabled(final Context context, final boolean value){
        setEnvironmentBoolean(context, KEY_DISABLE_PUSH_NOTIFICATION, value);
    }

    public String getDeviceToken(Context context){
        return getEnvironmentString(context, KEY_C2DM_ID, PushNotificationManager.C2DM_ID_NOT_SET);
    }

    public void setDeviceToken(Context context, String token){
        setEnvironmentString(context, KEY_C2DM_ID, token);
    }

    public boolean isLecturerMode(Context context){
        return getEnvironmentBoolean(context, FeedbackModule.SHARED_PREFERENCES_FEEDBACK, false);
    }

    public void setLecturerMode(Context context, boolean isLecturerMode){
        setEnvironmentBoolean(context, FeedbackModule.SHARED_PREFERENCES_FEEDBACK, isLecturerMode);
    }

    public String getCourseShortCode(Context context){
        return getEnvironmentString(context, TimetableModule.PREF_KEY_SHORTCODE, null);
    }

    public void setCourseShortCode(Context context, String code){
        setEnvironmentString(context, TimetableModule.PREF_KEY_SHORTCODE, code);
    }

    public boolean containsCourseShortCode(Context context) {
		return containsEnvironmentKey(context, TimetableModule.PREF_KEY_SHORTCODE);
	}

    public static boolean updateOnStartUp(Context context){
        return getEnvironmentBoolean(context, KEY_UPDATE_ON_STARTUP, true);
    }

    public static void setUpdateOnStartUp(Context context, boolean value){
        setEnvironmentBoolean(context, KEY_UPDATE_ON_STARTUP, value);
    }

	/**
	 * Return if a environment variable key exists
	 *
	 * @param context The context
	 * @param key The key that is search
	 * @return Is the key available
	 */
	public static boolean containsEnvironmentKey(final Context context, final String key) {
		final SharedPreferences environment = context.getSharedPreferences(SHAREDPREFERENCE_ENVIRONMENT, Context.MODE_PRIVATE);
		return environment.contains(key);
	}

	/**
	 * Speichert einen String in den Umgebungsvariablen 'key'
	 * @param context
	 *            Applicationcontext
	 * @param key
	 *            Schlüssel des Strings in den Umgebungsvariablen
	 * @param value
	 *            String, der gespeichert werden soll
	 */
	public static void setEnvironmentString(final Context context, final String key, final String value) {
		final SharedPreferences environment = context.getSharedPreferences(SHAREDPREFERENCE_ENVIRONMENT, Context.MODE_PRIVATE);
		final SharedPreferences.Editor enviromentEditor = environment.edit();
		enviromentEditor.putString(key, value);
		enviromentEditor.apply();
	}

	/**
	 * @param context
	 *            Applicationcontext
	 * @param key
	 *            Schlüssel des boolschen Wertes
	 * @param defaultValue
	 *            Standardwert, der geladen wird, falls der Schlüssel nicht
	 *            gefunden wird
	 * @return String der unter 'key' gespeicherten Umgebungsvariable
	 */
	public static String getEnvironmentString(final Context context, final String key, final String defaultValue) {
		final SharedPreferences environment = context.getSharedPreferences(SHAREDPREFERENCE_ENVIRONMENT, Context.MODE_PRIVATE);
		return environment.getString(key, defaultValue);
	}

	/**
	 *
	 * @param context
	 *            Applicationcontext
	 * @param key
	 *            Schlüssel des boolschen Wertes in den Umgebungsvariablen
	 * @param value
	 *            boolscher Wert, der gespeichert werden soll
	 */
	public static void setEnvironmentBoolean(final Context context, final String key, final boolean value) {
		final SharedPreferences environment = context.getSharedPreferences(SHAREDPREFERENCE_ENVIRONMENT, Context.MODE_PRIVATE);
		final SharedPreferences.Editor enviromentEditor = environment.edit();
		enviromentEditor.putBoolean(key, value);
		enviromentEditor.apply();
	}

	/**
	 * @param context
	 *            Applicationcontext
	 * @param key
	 *            Schlüssel des boolschen Wertes
	 * @param defaultValue
	 *            Standardwert, der geladen wird, falls der Schlüssel nicht
	 *            gefunden wird
	 * @return boolscher Wert der unter 'key' gespeicherten Umgebungsvariable
	 */
	public static boolean getEnvironmentBoolean(final Context context, final String key, final boolean defaultValue) {
		final SharedPreferences environment = context.getSharedPreferences(SHAREDPREFERENCE_ENVIRONMENT, Context.MODE_PRIVATE);
		return environment.getBoolean(key, defaultValue);
	}

}
