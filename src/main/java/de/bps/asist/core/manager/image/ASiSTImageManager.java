package de.bps.asist.core.manager.image;

import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ASiSTImageManager {


	private static final int CACHE_IMAGE_SIZE = 10;
	private static final int DELAY_BEFORE_PURGE = 10 * 1000; // in milliseconds

	private static final ASiSTImageManager INSTANCE = new ASiSTImageManager();

	private ASiSTImageManager(){
		//
	}

	public static ASiSTImageManager getInstance() {
		return INSTANCE;
	}

	public void setImage(final String url, final ImageView view){
        if(url == null || url.isEmpty() || view == null){
            return;
        }
        DisplayImageOptions options = createDefaultDisplayImageOptions();
        ImageLoader.getInstance().displayImage(url, view, options);
	}

    public void setImage(String url, final ImageView view, final ImageLoadingListener listener){
        DisplayImageOptions options = createDefaultDisplayImageOptions();
        ImageLoader.getInstance().displayImage(url, view, options, listener);
    }


    private static DisplayImageOptions createDefaultDisplayImageOptions () {
		return new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.build();
	}
}
