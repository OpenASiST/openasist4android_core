package de.bps.asist.core.manager.database;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.core.database.AbstractDatabaseObject;

public class DatabaseManager<T extends AbstractDatabaseObject> {

    private static final String TAG = DatabaseManager.class.getSimpleName();
    private final Context context;
    private final Class<T> clazz;
    private OrmLiteSqliteOpenHelper helper;

    public DatabaseManager(final Context context, final Class<T> clazz) {
        this.context = context;
        this.clazz = clazz;
    }

    public void saveObject(final T object) {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);
            dao.createOrUpdate(object);
        } catch (final SQLException e) {
            Log.e(TAG, "could not save object" + object + " for " + clazz, e);
        }
    }

    public List<T> getAll() {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);
            return dao.queryForAll();
        } catch (final SQLException e) {
            ////Log.e(TAG, "could not get all objects for " + clazz, e);
        }
        return new ArrayList<>();
    }

    public List<T> getObjects(final Map<String, Object> keyValues) {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);
            return dao.queryForFieldValues(keyValues);
        } catch (final SQLException e) {
            ////Log.e(TAG, "could not get Objects ", e);
        }
        return new ArrayList<>();
    }

    public List<T> getObjects(final String key, final Object value) {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);
            final List<T> results;
            if (value != null) {
                results = dao.queryForEq(key, value);
            } else {
                QueryBuilder<T, ?> queryBuilder = dao.queryBuilder();
                Where<T, ?> where = queryBuilder.where();
                where = where.isNull(key);
                results = dao.query(where.prepare());
            }
            return results;
        } catch (final SQLException e) {
            //Log.e(TAG, "could not get Objects for key " + key + " and value " + value, e);
        }
        return new ArrayList<>();
    }

    public void release() {
        OpenHelperManager.releaseHelper();
        helper = null;
    }

    public T getObject(final String key, final Object value) {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);
            final List<T> results;
            if (value != null) {
                results = dao.queryForEq(key, value);
            } else {
                QueryBuilder<T, ?> queryBuilder = dao.queryBuilder();
                Where<T, ?> where = queryBuilder.where();
                where = where.isNull(key);
                results = dao.query(where.prepare());
            }
            if (results.size() > 0) {
                return results.get(0);
            }
        } catch (final SQLException e) {
            Log.e("could not find object for class " + clazz + " and request " + key + "=" + value, e.getMessage());
        }
        return null;
    }

    public T getObject(Map<String, Object> query) {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);

            int counter = 0;
            QueryBuilder<T, ?> queryBuilder = dao.queryBuilder();
            Where<T, ?> where = queryBuilder.where();
            final Map<SelectArg, Object> valueMap = new HashMap<>();
            for (String key : query.keySet()) {
                Object value = query.get(key);
                SelectArg selectArg = new SelectArg();
                if (counter > 0) {
                    where = where.and();
                }
                where = where.eq(key, selectArg);
                valueMap.put(selectArg, value);
                counter++;
            }

            for (SelectArg selectArg : valueMap.keySet()) {
                selectArg.setValue(valueMap.get(selectArg));
            }
            final List<T> results = dao.query(where.prepare());
            if (results.size() > 0) {
                return results.get(0);
            }
        } catch (final SQLException e) {
            ////Log.e(TAG, "could not find object for class " + clazz + " and requestMap " + query, e);
        }
        return null;
    }

    public T refresh(final T object) {
        return getObject("id", object.getId());
    }

    private OrmLiteSqliteOpenHelper getHelper(final Context context) {
        if (helper == null) {
            helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            helper.getWritableDatabase();
        }
        return helper;
    }

    public void deleteAll() {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);
            final List<T> elements = dao.queryForAll();
            if (elements != null) {
                final int size = elements.size();
                final int portion = 500;
                if (size > portion) {
                    final int rounds = size / portion;
                    final int rest = size % portion;
                    for (int i = 0; i < rounds; ++i) {
                        dao.delete(elements.subList(i * portion, i * portion + portion));
                    }
                    dao.delete(elements.subList(rounds * portion, rounds * portion + rest));
                } else {
                    dao.delete(elements);
                }
            }
        } catch (final SQLException e) {
            ////Log.e(TAG, "could not delete all objects: ", e);
        }
    }

    public void delete(T object) {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);
            dao.delete(object);
        } catch (final SQLException e) {
            ////Log.e(TAG, "could not delete object: ", e);
        }
    }

    public void delete(String key, Object value) {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);
            final List<T> elements = dao.queryForEq(key, value);
            if (elements != null) {
                final int size = elements.size();
                final int portion = 500;
                if (size > portion) {
                    final int rounds = size / portion;
                    final int rest = size % portion;
                    for (int i = 0; i < rounds; ++i) {
                        dao.delete(elements.subList(i * portion, i * portion + portion));
                    }
                    dao.delete(elements.subList(rounds * portion, rounds * portion + rest));
                } else {
                    dao.delete(elements);
                }
            }
        } catch (final SQLException e) {
            ////Log.e(TAG, "could not delete objects: ", e);
        }
    }

    public boolean hasItems() {
        try {
            final OrmLiteSqliteOpenHelper helper = getHelper(context);
            final Dao<T, ?> dao = helper.getDao(clazz);
            long count = dao.countOf();
            return count > 0;
        } catch (final SQLException e) {
            ////Log.e(TAG, "could not count items", e);
        }
        return false;
    }
}
