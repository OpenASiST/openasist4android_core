package de.bps.asist.core.manager.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.bps.asist.context.ASiSTConstants;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.ASiSTModuleManager;
import de.bps.asist.module.AbstractAsistModule;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	private final List<AbstractAsistModule> modules;

	public DatabaseHelper(final Context context) {
		super(context, ASiSTConstants.DB_NAME, null, ASiSTConstants.DB_VERSION);
		modules = ASiSTModuleManager.getInstance().getModules();
	}

	@Override
	public void onCreate(final SQLiteDatabase database, final ConnectionSource connectionSource) {
		final Set<Class<? extends AbstractDatabaseObject>> classes = new HashSet<>();
		for (final AbstractAsistModule module : modules) {
			final List<Class<? extends AbstractDatabaseObject>> databaseClasses = module.getDatabaseClasses();
			if (databaseClasses != null) {
				classes.addAll(databaseClasses);
			}
		}
		for (final Class<? extends AbstractDatabaseObject> cl : classes) {
			try {
				TableUtils.createTableIfNotExists(getConnectionSource(), cl);
			} catch (final Exception ex) {
				////Log.e("DatabaseHelper", "Cannot create table for the following class: " + cl.getSimpleName());
			}
		}
	}

	@Override
	public void onUpgrade(final SQLiteDatabase database, final ConnectionSource connectionSource, final int oldVersion, final int newVersion) {
		if (newVersion == 7) {
			final Set<Class<? extends AbstractDatabaseObject>> classes = new HashSet<>();
			for (final AbstractAsistModule module : modules) {
				final List<Class<? extends AbstractDatabaseObject>> databaseClasses = module.getDatabaseClasses();
				if (databaseClasses != null) {
					classes.addAll(databaseClasses);
				}
			}
			for (final Class<? extends AbstractDatabaseObject> cl : classes) {
				try {
					TableUtils.createTableIfNotExists(getConnectionSource(), cl);
				} catch (final Exception ex) {
					////Log.e("DatabaseHelper", "Cannot create table for the following class: " + cl.getSimpleName());
				}
			}
		} else if (oldVersion != newVersion && newVersion == 8) {
			try {
				// refactoring ptsmodule
				// delete old tables
				database.execSQL("DROP TABLE IF EXISTS ptsstation");
				database.execSQL("DROP TABLE IF EXISTS departure");
				// create new tables
				final Set<Class<? extends AbstractDatabaseObject>> classes = new HashSet<>();
				for (final AbstractAsistModule module : modules) {
					if (module.getClass().getName().contains("PTSModule")) {
						final List<Class<? extends AbstractDatabaseObject>> databaseClasses = module.getDatabaseClasses();
						if (databaseClasses != null) {
							classes.addAll(databaseClasses);
						}
					}
				}
				for (final Class<? extends AbstractDatabaseObject> cl : classes) {
					TableUtils.createTableIfNotExists(getConnectionSource(), cl);
				}
			} catch (Exception e) {
				// nothing to do
			}
		}
		if(oldVersion < 9)
		{
			database.execSQL("ALTER TABLE 'course' ADD COLUMN type INTEGER NOT NULL DEFAULT (0);");
		}
	}

}
