package de.bps.asist.core.manager.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.List;

import de.bps.asist.core.ASiSTApplication;
import de.bps.asist.core.manager.ASiSTModuleManager;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.start.AsistStartActivity;

public class ASiSTPreferenceManager {

	/** Konstante für den Key des Feed-Limits in den Umgebungsvariablen */
	public static final String KEY_FEED_LIMIT = "feed_limit";

	/** Konstante für den Key des Notification-Limits in den Umgebungsvariablen */
	public static final String KEY_NOTIFICATION_LIMIT = "notification_limit";

    public static final String PREFERENCE_PRICE = "module_canteen_price";

	private static final ASiSTPreferenceManager INSTANCE = new ASiSTPreferenceManager();

	//private ASiSTPreferenceManager() {
		//
	//}

	public static ASiSTPreferenceManager getInstance() {
		return INSTANCE;
	}

	public void initPreferences(final Context context) {
		setPrefFeedLimit(context);
	}

	/**
	 * Lädt das Feed-Limit aus den Umgebungsvariablen
	 * @param context
	 *            Apllicationcontext
	 */
	public void setPrefFeedLimit(final Context context) {
		final SharedPreferences preferences = getSharedPreferences(context);
		final Integer feedLimit = preferences.getInt(KEY_FEED_LIMIT, -1);
		if (feedLimit == -1) {
			// not yet initialized
			final Editor prefEditor = preferences.edit();
			prefEditor.putInt(KEY_FEED_LIMIT, 50);
			prefEditor.apply();
		}
	}

	public void setPrefNotificationLimit(final Context context) {
		final SharedPreferences preferences = getSharedPreferences(context);
		final int notiLimit = preferences.getInt(KEY_NOTIFICATION_LIMIT, -1);
		if (notiLimit == -1) {
			final Editor prefEditor = preferences.edit();
			prefEditor.putInt(KEY_NOTIFICATION_LIMIT, 30);
			prefEditor.apply();
		}
	}

    public void setCanteenPriceSetting(final Context context, int setting){
        final SharedPreferences preferences = getSharedPreferences(context);
            final Editor prefEditor = preferences.edit();
            prefEditor.putInt(PREFERENCE_PRICE, setting);
            prefEditor.apply();
    }

    public void setCanteenPriceSetting(final Context context, String setting){
        final SharedPreferences preferences = getSharedPreferences(context);
        final Editor prefEditor = preferences.edit();
        if(setting.equals("STUDENT")) {
            prefEditor.putInt(PREFERENCE_PRICE, 0);
            prefEditor.apply();
        }
        else if(setting.equals("EMPLOYEE")) {
            prefEditor.putInt(PREFERENCE_PRICE, 1);
            prefEditor.apply();
        }
        else if(setting.equals("GUEST")) {
            prefEditor.putInt(PREFERENCE_PRICE, 2);
            prefEditor.apply();
        }
        else{
            prefEditor.putInt(PREFERENCE_PRICE, 0);
            prefEditor.apply();
        }
    }

    public int getCanteenPriceSetting(final Context context) {
		int CurrentSetting = 0; //Default setting is Students.
        final SharedPreferences preferences = getSharedPreferences(context);
        CurrentSetting = preferences.getInt(PREFERENCE_PRICE, 0);
        return CurrentSetting;
    }

	private SharedPreferences getSharedPreferences(final Context context) {
		final String prefName = context.getPackageName() + "_preferences";
		final SharedPreferences preferences = context.getSharedPreferences(
				prefName, Context.MODE_PRIVATE);
		return preferences;
	}

	public boolean getSetting(final Context context, String key) {
		final SharedPreferences preferences = getSharedPreferences(context);
		List<AbstractAsistModule> modules = ASiSTModuleManager.getInstance().getModules();
		boolean defaultValueModuleStartTree = true;
		for (AbstractAsistModule module : modules) {
			if (AsistStartActivity.START_VIEW_TREE.concat(context.getResources().getString(module.getName())).equals(key)) {
				defaultValueModuleStartTree = module.getStartTreeConfig().isEnabledPerDefault();
			}
		}
		boolean currentSetting = preferences.getBoolean(key, defaultValueModuleStartTree);
        return currentSetting;
	}

	public void setSetting(final Context context, String key, boolean value) {
		// key example "startView_tree_Mensa" or for push settings "FeedModule"
		final SharedPreferences preferences = getSharedPreferences(context);
		final Editor prefEditor = preferences.edit();
		prefEditor.putBoolean(key, value);
		prefEditor.apply();
	}
}
