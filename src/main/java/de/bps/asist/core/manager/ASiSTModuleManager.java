package de.bps.asist.core.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.module.AbstractAsistModule;

public class ASiSTModuleManager {
	
	private static final ASiSTModuleManager INSTANCE = new ASiSTModuleManager();

	private Map<Class<? extends AbstractAsistModule>, AbstractAsistModule> modules = Collections.synchronizedMap(new LinkedHashMap<Class<? extends AbstractAsistModule>, AbstractAsistModule>());

	private ASiSTModuleManager(){
	}
	
	public static ASiSTModuleManager getInstance() {
		return INSTANCE;
	}

	/**
	 * @deprecated use {@link #resetModules} instead.
	 */
	@Deprecated
	public void resetModuleList() {
		this.modules.clear();
	}

	public void resetModules() {
		this.modules.clear();
	}

	public void register(AbstractAsistModule module) {
		this.modules.put(module.getClass(), module);
	}

	public <T extends AbstractAsistModule> T getModule(Class<T> moduleClass, T defaultValue) {
		T module = (T) this.modules.get(moduleClass);
		if (module == null) {
			module = defaultValue;
		}
		return module;
	}

	public <T extends AbstractAsistModule> T getModule( Class<T> moduleClass) {
		return this.getModule(moduleClass, null);
	}

	public List<AbstractAsistModule> getModules() {
		return new ArrayList<>(modules.values());
	}
}