package de.bps.asist.core.manager.update;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.List;

import androidx.core.app.NotificationCompat;
import de.bps.asist.R;
import de.bps.asist.context.ASiSTConstants;
import de.bps.asist.core.manager.ASiSTModuleManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.start.AsistStartActivity;

public class UpdateManager {

	private static final UpdateManager INSTANCE = new UpdateManager();

	private final List<AbstractAsistModule> modules;

	private UpdateManager() {
		modules = ASiSTModuleManager.getInstance().getModules();
	}

	public static UpdateManager getInstance() {
		return INSTANCE;
	}

	public void register(final AbstractAsistModule module) {
		modules.add(module);
	}

	/**
	 * @brief Erzeugt einen Ladebalken und ruft den Thread zum Update aller
	 *        Notifications und Datenbankeinträge
	 * @param context
	 */
	public void updateAll(final Context context) {
		final EnvironmentManager em = EnvironmentManager.getInstance();
		if (!em.isUpdateOngoing(context, false)) {
			// Threads starten zum Updaten der Datenbank
			em.setUpdateOngoing(context, true);
			//setUpdateNotification(context);
			final UpdateThread thread = new UpdateThread(context, modules);
			thread.start();
		}
	}

	private void setUpdateNotification(final Context context) {
		final String title = context.getResources().getString(R.string.app_name) + ": " + context.getResources().getString(R.string.txt_update_in_progress);
		final String contentTitle = context.getResources().getString(R.string.app_header);
		final String contextText = context.getResources().getString(R.string.txt_update_in_progress);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder = builder.setTicker(title).setSmallIcon(android.R.drawable.stat_notify_sync);
		builder = builder.setContentTitle(contentTitle);
		builder.setContentText(contextText);
		final Intent notifIntent = new Intent(context, AsistStartActivity.class);
		final PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notifIntent, 0);
		builder.setContentIntent(contentIntent);
		final int flags = Notification.FLAG_AUTO_CANCEL | Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_ONLY_ALERT_ONCE;
		final int defaults = Notification.DEFAULT_LIGHTS;
		builder.setDefaults(defaults);
		builder.setAutoCancel(true);
		builder.setOnlyAlertOnce(true);
		final Notification notification = builder.build();
		final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(ASiSTConstants.NOTIFICATION_ID_NEW_NOTIFICATION);
		notificationManager.notify(ASiSTConstants.NOTIFICATION_ID_UPDATE, notification);
	}

	public void setUpdateDone(final Context context) {
		final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(ASiSTConstants.NOTIFICATION_ID_UPDATE);
		final EnvironmentManager em = EnvironmentManager.getInstance();
		em.setUpdateOngoing(context, false);
	}

}
