package de.bps.asist.core.manager.parser;

import android.content.res.Resources;
import android.os.Build;
import android.util.Log;

import org.springframework.http.HttpMethod;
import org.springframework.http.client.AbstractClientHttpRequestFactoryWrapper;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class ASiSTParser {

    private static final ASiSTParser INSTANCE = new ASiSTParser();

    private ASiSTParser() {}

    public static ASiSTParser getInstance() {
        return INSTANCE;
    }

    public <T extends Serializable> void parseWithThread(final String url, final Class<T> cl, final ParserCallback<T> callback) {
        final BackgroundParserThread<T> parser = new BackgroundParserThread<>(url, cl, callback, createStandartClientHttpRequestFactory());
        parser.start();
    }

    public <T extends Serializable> void parse(final String url, final Class<T> cl, final ParserCallback<T> callback){
        final BackgroundParserThread<T> parser = new BackgroundParserThread<>(url, cl, callback, createStandartClientHttpRequestFactory());
        parser.run();
    }

    public <T extends Serializable> void parseWithTask(final String url, final Class<T> cl, final ParserCallback<T> callback){
        final BackgroundParserTask<T> parser = new BackgroundParserTask<>(url, cl, callback, createStandartClientHttpRequestFactory());
        parser.execute();

    }

    /**
     * Creates a standard {@link ClientHttpRequestFactory} which will add the system language as 'Accept-Language' header
     *
     * @return standard {@link ClientHttpRequestFactory}
     */
    private static ClientHttpRequestFactory createStandartClientHttpRequestFactory() {
        ClientHttpRequestFactory wrappingClientHttpRequestFactory = Build.VERSION.SDK_INT >= 9 ? new SimpleClientHttpRequestFactory() : new HttpComponentsClientHttpRequestFactory();
        Locale systemLocal = Resources.getSystem().getConfiguration().locale;

        return new AddLanguageHeaderClientHttpRequestFactory(wrappingClientHttpRequestFactory, systemLocal.getISO3Language());
    }

    /**
     * Add a 'Accept-Language' header to all manufactured requests
     */
    private static class AddLanguageHeaderClientHttpRequestFactory extends AbstractClientHttpRequestFactoryWrapper {

        private Set<String> languages;

        /**
         * Construct a factory which wrap a {@link ClientHttpRequestFactory} and add a 'Accept-Language' header to all manufactured requests
         *
         * @param wrappingRequestFactory wrapping {@link ClientHttpRequestFactory}
         * @param languages              Languages for the 'Accept-Language' header
         */
        public AddLanguageHeaderClientHttpRequestFactory(ClientHttpRequestFactory wrappingRequestFactory, Collection<String> languages) {
            super(wrappingRequestFactory);
            this.languages = new HashSet<>(languages);
        }

        /**
         * Construct a factory which wrap a {@link ClientHttpRequestFactory} and add a 'Accept-Language' header to all manufactured requests.
         *
         * @param wrappingRequestFactory wrapping {@link ClientHttpRequestFactory}
         * @param languages              Languages for the 'Accept-Language' header
         */
        public AddLanguageHeaderClientHttpRequestFactory(ClientHttpRequestFactory wrappingRequestFactory, String... languages) {
            this(wrappingRequestFactory, Arrays.asList(languages));
        }

        /**
         * Let the wrapped {@link ClientHttpRequestFactory} create the request and add 'Accept-Language' header
         *
         * @param uri
         * @param httpMethod
         * @param clientHttpRequestFactory
         * @return Create a new ClientHttpRequest for the specified URI and HTTP method.
         * @throws IOException
         */
        @Override
        protected ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod, ClientHttpRequestFactory clientHttpRequestFactory) throws IOException {
            ClientHttpRequest resultClientHttpRequest = clientHttpRequestFactory.createRequest(uri, httpMethod);

            if (this.languages != null) {
                resultClientHttpRequest.getHeaders().setAcceptLanguage(joinStrings(", ", this.languages));
            }

            return resultClientHttpRequest;
        }

        /**
         * Returns a new String composed of copies of the CharSequence elements joined together with a copy of the specified delimiter.
         * Note that if an individual element is null, then it will be ignored.
         *
         * @param delimiter a sequence of characters that is used to separate each of the elements in the resulting String
         * @param elements  an Iterable that will have its elements joined together.
         * @return a new String that is composed from the elements argument
         */
        private String joinStrings(CharSequence delimiter, Iterable<String> elements) {
            StringBuilder resultString = new StringBuilder();
            if (elements != null) {
                for (Iterator<String> stringsIterator = elements.iterator(); stringsIterator.hasNext(); ) {
                    CharSequence joiningString = stringsIterator.next();
                    if (joiningString != null) {
                        resultString.append(joiningString);
                        if (stringsIterator.hasNext()) {
                            resultString.append(delimiter);
                        }
                    }
                }
            }
            return resultString.toString();
        }
    }
}
