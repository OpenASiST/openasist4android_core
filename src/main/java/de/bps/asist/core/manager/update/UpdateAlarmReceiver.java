package de.bps.asist.core.manager.update;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import de.bps.asist.core.ASiSTApplication;

/**
 * Created by epereira on 18.08.2016.
 */
public class UpdateAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context arg0, Intent arg1) {
        final UpdateManager um = UpdateManager.getInstance();
        Context c = ASiSTApplication.getContext();
        um.updateAll(c);
        //Toast.makeText(arg0, "Database updated", Toast.LENGTH_SHORT).show();
    }
}
