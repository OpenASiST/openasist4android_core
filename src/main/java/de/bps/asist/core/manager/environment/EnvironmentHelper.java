package de.bps.asist.core.manager.environment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class EnvironmentHelper {

	private static final String ONLINE_STATUS = "online_status";

	protected static boolean isNetworkAvailable(final Context context) {
		final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		// if no network is available networkInfo will be null, otherwise check
		// if we are connected
        return networkInfo != null && networkInfo.isConnected();
    }
}
