package de.bps.asist.context;

import de.bps.asist.R;

public class ASiSTConstants {

    public static final String PREF_ROOTURL = "rooturl";

	public static final String BROADCAST_INTENT_NEW_NOTIFICATIONS = "de.tufreiberg.mytu.NEW_NOTIFICATIONS";
	public static final String BROADCAST_INTENT_REDRAW_BADGES = "de.tufreiberg.mytu.REDRAW_BADGES";
	public static final String BROADCAST_INTENT_UPDATE_NOTIFICATIONS = "de.tufreiberg.mytu.UPDATE_NOTIFICATIONS";

	public static final int NOTIFICATION_ID_NEW_NOTIFICATION = 6953;
	public static final int NOTIFICATION_ID_SEND_LOST_AND_FOUND = 6952;

	// IDs für Notifications ins der Kopfzeile
	public static final int NOTIFICATION_ID_UPDATE = 6951;

	public static final String DB_NAME = "ASiST.db";
	public static final int DB_VERSION = 9; // DB_Version 4 ab myTU Version

    public static final String FEEDBACK_LECTURER_MODE = "feedback.lecturer.mode";

    public static final int TABLE_ITEM_TYPE_HEADER = 0;
    public static final int TABLE_ITEM_TYPE_CELL = 1;
    public static final int TABLE_ITEM_TYPE_MAXCOUNT = 2;
    public static final int TABLE_ITEM_TYPE_FEEDBACK = 3;
    public static final int TABLE_ITEM_TYPE_MAP = 4;

    public static final int timetable_time_title = R.string.timetable_time_title;


}
