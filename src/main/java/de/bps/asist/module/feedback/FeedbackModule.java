/**
 * 
 */
package de.bps.asist.module.feedback;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.fragment.app.Fragment;
import de.bps.asist.BuildConfig;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.environment.ASiSTCoreDataManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.core.manager.push.PushNotificationManager;
import de.bps.asist.core.util.ASiSTPostSender;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.feedback.model.FeedbackList;

/**
 * @author thomasw
 * 
 */
public class FeedbackModule extends AbstractAsistModule {

    private static final String TAG = FeedbackModule.class.getSimpleName();

    public static final String SHARED_PREFERENCES_FEEDBACK = "feedback";

    public FeedbackModule(){
        setSettingsConfig(new FeedbackSettings());
    }

    /**
     * Amount of minutes before now since the feedbacks are shown.
     */
    private static final int TIMEFRAME_LECTURER_MINUTES = 10;

    /**
     * Time in minutes how long a student has to wait until it can send the next
     * question, speed or stop.
     */
    private static final int TIMEFRAME_STUDENT_MINUTES = 1;

    public static final int SPEED_SLOW = 0;
    public static final int SPEED_OK = 1;
    public static final int SPEED_FAST = 2;

    private static Date lastSpeed = null;
    private static Date lastQuestion = null;
    private static Date lastStop = null;

    private static Map<Date, String> removedQuestions = new HashMap<>();

    /**
	 * @see de.bps.asist.module.AbstractAsistModule#getName()
	 */
	@Override
	public int getName() {
		return R.string.module_feedback_name;
	}

	@Override
	public int getIcon() {
		return R.drawable.ic_menu_feedback;
	}

	/**
	 * @see de.bps.asist.module.AbstractAsistModule#getInitialFragment()
	 */
	@Override
	public Fragment getInitialFragment() {
		return new FeedbackFragment();
	}

	/**
	 * @see de.bps.asist.module.AbstractAsistModule#getDatabaseClasses()
	 */
	@Override
	public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
		return null;
	}

    public static void setLecturerMode(boolean lecturerMode, Context context) {
        EnvironmentManager.getInstance().setLecturerMode(context, lecturerMode);
    }

    public static boolean isLecturerMode(Context context) {
        return EnvironmentManager.getInstance().isLecturerMode(context);
    }

    /**
     * Generate dateString of now - 10 minutes
     * @return
     */
    private static String getDateString() {
        // date in Format dd-MM-yyyy_HH-mm-ss
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");

        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.MINUTE, -TIMEFRAME_LECTURER_MINUTES);
        return sdf.format(cal.getTime());
    }

    private static boolean checkLastSendDate(Date sendDate) {
       if (sendDate == null) {
           return true;
       }
        Date now = new Date();
        Calendar compareDate = Calendar.getInstance();
        compareDate.setTime(now);
        compareDate.add(Calendar.MINUTE, -TIMEFRAME_STUDENT_MINUTES);

        return sendDate.before(compareDate.getTime());
    }

    public static void addToRemovedQuestions(String question) {
        removedQuestions.put(new Date(), question);
    }

    public static boolean containsRemovedQuestion(String question) {
        clearRemovedQuestions();
        return removedQuestions.containsValue(question);
    }

    private static void clearRemovedQuestions() {
        // removed questions older than 10 minutes are removed

        Date now = new Date();
        Calendar compareDate = Calendar.getInstance();
        compareDate.setTime(now);
        compareDate.add(Calendar.MINUTE, -TIMEFRAME_LECTURER_MINUTES);
        Date nowMinus = compareDate.getTime();

        List<Date> toRemove = new ArrayList<>();
        for (Date d : removedQuestions.keySet()) {
            if (d.before(nowMinus)) {
                toRemove.add(d);
            }
        }

        for (Date d : toRemove) {
            removedQuestions.remove(d);
        }
    }

    public static void loadFeedbackStop(long courseId, Context context, ParserCallback callback) {
        final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
        final String url = rootUrl
                + "/feedback/result/stop"
                + "/" + courseId
                + "/" + getDateString();// + date in Format dd-MM-yyyy_HH-mm-ss
        ASiSTParser.getInstance().parseWithThread(url.toString(), FeedbackList.class, callback);
    }

    public static void loadFeedbackSpeed(long courseId, Context context, ParserCallback callback) {
        final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
        final String url = rootUrl
            + "/feedback/result/speed"
            + "/" + courseId
            + "/" + getDateString();// + date in Format dd-MM-yyyy_HH-mm-ss
        ASiSTParser.getInstance().parseWithThread(url.toString(), FeedbackList.class, callback);
    }

    public static void loadFeedbackQuestion(long courseId, Context context, ParserCallback callback) {
        final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
        final String url = rootUrl
                + "/feedback/result/question"
                + "/" + courseId
                + "/" + getDateString();// + date in Format dd-MM-yyyy_HH-mm-ss
        ASiSTParser.getInstance().parseWithThread(url.toString(), FeedbackList.class, callback);
    }

    public static void sendFeedbackSpeed(int speed, long courseId, Context context) {
        if (BuildConfig.DEBUG || checkLastSendDate(lastSpeed)) {
            String token = EnvironmentManager.getInstance().getEnvironmentString(context, PushNotificationManager.KEY_C2DM_ID, PushNotificationManager.C2DM_ID_NOT_SET);
            final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
            final String url = rootUrl
                    + "/feedback/speed"
                    + "/" + courseId
                    + "/" + EnvironmentManager.getInstance().getDeviceToken(context)
                    + "/" + speed;
            ASiSTPostSender sender = new ASiSTPostSender();
            sender.execute(url.toString(), token);
            lastSpeed = new Date();
            Toast.makeText(context, context.getText(R.string.feedback_send_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, context.getText(R.string.feedback_send_once_per_min), Toast.LENGTH_SHORT).show();
        }
    }

    public static void sendFeedbackStop(long courseId, Context context) {
        if (BuildConfig.DEBUG || checkLastSendDate(lastStop)) {
            String token = EnvironmentManager.getInstance().getEnvironmentString(context, PushNotificationManager.KEY_C2DM_ID, PushNotificationManager.C2DM_ID_NOT_SET);
            final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
            final String url = rootUrl
                    + "/feedback/stop"
                    + "/" + courseId
                    + "/" + EnvironmentManager.getInstance().getDeviceToken(context);
            ASiSTPostSender sender = new ASiSTPostSender();
            sender.execute(url.toString(), token);
            lastStop = new Date();
            Toast.makeText(context, context.getText(R.string.feedback_send_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, context.getText(R.string.feedback_send_once_per_min), Toast.LENGTH_SHORT).show();
        }
    }

    public static void sendFeedbackQuestion(final String question, long courseId, Context context) {
        if (BuildConfig.DEBUG || checkLastSendDate(lastQuestion)) {
            String token = EnvironmentManager.getInstance().getEnvironmentString(context, PushNotificationManager.KEY_C2DM_ID, PushNotificationManager.C2DM_ID_NOT_SET);
            final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
            final String url = rootUrl
                    + "/feedback/question"
                    + "/" + courseId
                    + "/" + EnvironmentManager.getInstance().getDeviceToken(context);
            //'question' as form paramter
            RequestParams params = new RequestParams();
            params.put("question", question);

            ASiSTPostSender.post(url.toString(), params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    //Log.i(TAG, "Successfully sended question: " + question);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    //Log.i(TAG, "Error while sending question: " + question);
                }
            });
            lastQuestion = new Date();
            Toast.makeText(context, context.getText(R.string.feedback_send_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, context.getText(R.string.feedback_send_once_per_min), Toast.LENGTH_SHORT).show();
        }
    }
}
