package de.bps.asist.module.feedback;

import de.bps.asist.R;
import de.bps.asist.module.settings.model.CheckboxSettingsItem;
import de.bps.asist.module.settings.model.SettingsConfig;
import de.bps.asist.module.settings.model.SettingsItem;
import de.bps.asist.module.settings.model.SettingsSection;

/**
 * Created by litho on 30.10.14.
 * Settings for Feedback
 */
public class FeedbackSettings extends SettingsConfig {

    public FeedbackSettings(){
        SettingsSection section = new SettingsSection(R.string.module_feedback_name);
        addSection(section);

        SettingsItem item = new CheckboxSettingsItem(R.string.feedback_settings_lecturer_mode);
        item.setKey(FeedbackModule.SHARED_PREFERENCES_FEEDBACK);
        item.setDefaultValue(false);
        section.addItem(item);
    }
}
