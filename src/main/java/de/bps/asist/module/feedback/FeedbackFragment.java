/**
 * 
 */
package de.bps.asist.module.feedback;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import de.bps.asist.R;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.module.timetable.TimetableModule;
import de.bps.asist.module.timetable.model.Course;

/**
 * @author thomasw
 * 
 */
public class FeedbackFragment extends AbstractASiSTFragment {

    @Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
		final View view = inflater.inflate(R.layout.generic_list_view, container, false);
        ListView listView = (ListView) view.findViewById(R.id.generic_list);
        List<Course> currentCourses = TimetableModule.findCurrentCourses(getActivity());
        final Class<? extends Activity> cl = FeedbackActivity.class;
        AbstractGenericListCallback cb = new AbstractGenericListCallback(getActivity(), cl);
        GenericListViewAdapter<Course> adapter = new GenericListViewAdapter<>(getActivity(), currentCourses, cb);
        listView.setAdapter(adapter);
		return view;
	}

}
