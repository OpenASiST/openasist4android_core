package de.bps.asist.module.feedback;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jjoe64.graphview.BarGraphView;
import com.jjoe64.graphview.CustomLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewDataInterface;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewStyle;
import com.jjoe64.graphview.ValueDependentColor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.bps.asist.R;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.gui.list.SwipeGestureListener;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.feedback.model.Feedback;
import de.bps.asist.module.feedback.model.FeedbackList;
import de.bps.asist.module.feedback.model.FeedbackListItem;
import de.bps.asist.module.feedback.model.StudentFeedbackList;
import de.bps.asist.module.timetable.model.Course;
import de.bps.asist.module.timetable.model.CourseDescription;

/**
 * Created by thomasw on 05.11.14.
 * Activity that shows feedback either for students or for the lecturer
 */
public class FeedbackActivity extends AsistModuleActivity<Course> {

    private static final String TAG = FeedbackActivity.class.getSimpleName();

    private boolean loadSpeedReady = false;
    private boolean loadStopReady = false;
    private boolean loadQuestionReady = false;

    private LinearLayout linearChart;

    private int slow = 0;
    private int ok = 0;
    private int fast = 0;

    private int stops = 0;

    private List<String> questions = new ArrayList<>();

    private ProgressDialog progressDialog;

    private CategoryListViewAdapter<FeedbackListItem> adapter;

    private boolean isLecturerMode;

    /**
     * updating the lecturer view periodically
     */
    private Timer autoUpdater;
    /**
     * Update period in milliseconds
     */
    private int updatePeriod = 30000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isLecturerMode = FeedbackModule.isLecturerMode(this);
        if (isLecturerMode) {

            progressDialog = new ProgressDialog(this);
            initLecturerView();
        } else {
            initStudentView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLecturerMode) {
            autoUpdater = new Timer();
            autoUpdater.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initLecturerView();
                        }
                    });
                }
            }, 0, updatePeriod);
        }
    }

    @Override
    protected void onPause() {
        if (autoUpdater != null) {
            autoUpdater.cancel();
        }
        super.onPause();
    }

    private void initStudentView() {
        setContentView(R.layout.module_feedback_student);
        ListView listView = (ListView) findViewById(R.id.feedback_list);
        StudentFeedbackList list = new StudentFeedbackList(this);

        CategoryListViewAdapter<FeedbackListItem> adapter = new CategoryListViewAdapter<>(this, list.getItems(), null);
        listView.setAdapter(adapter);
    }

    private void initLecturerView() {
        setContentView(R.layout.module_feedback_lecturer);

        progressDialog.setTitle(translate(R.string.module_course_add_wait_title));
        progressDialog.setMessage("");
        progressDialog.show();

        Course course = null;
        Object courseObject = getSelectedItem();
        if (courseObject instanceof Course) {
            course = (Course) courseObject;
        } else if (courseObject instanceof CourseDescription) {
            course = ((CourseDescription) courseObject).getCourse();
        }

        FeedbackModule.loadFeedbackSpeed(course.getCourseid(), this, new FeedbackSpeedCallback());
        FeedbackModule.loadFeedbackStop(course.getCourseid(), this, new FeedbackStopCallback());
        FeedbackModule.loadFeedbackQuestion(course.getCourseid(), this, new FeedbackQuestionCallback());

        int cnt = 0;

        while (!loadSpeedReady && !loadStopReady && !loadQuestionReady) {
            ++cnt;
            if (cnt > 10) {
                loadSpeedReady = true;
                loadStopReady = true;
                loadQuestionReady = true;
            }
            try {
                ////Log.i(TAG, "loading...");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        final ListView listView = (ListView) findViewById(R.id.module_feedback_lecturer_question_list);

        final Map<String, List<FeedbackListItem>> items = new LinkedHashMap<>();


        // stops:
        final List<FeedbackListItem> questionStopList = new ArrayList<>();
        if (stops > 0) {
            FeedbackListItem stopItem = new FeedbackListItem("STOP (x " + stops + ")", null);
            stopItem.setViewId(CategoryListViewAdapter.VIEW_ID_ALERT);
            questionStopList.add(stopItem);
        }

        // questions:
        if (!questions.isEmpty()) {
            //List<FeedbackListItem> questionList = new ArrayList<>();
            for (String question : questions) {
                FeedbackListItem questionItem1 = new FeedbackListItem(question, null);
                questionStopList.add(questionItem1);
            }
        } else {
            FeedbackListItem questionItem1 = new FeedbackListItem(translate(R.string.module_feedback_lecturer_no_question), null);
            questionStopList.add(questionItem1);
        }

        items.put(translate(R.string.module_feedback_question_stop), questionStopList);

        adapter = new CategoryListViewAdapter<>(this, items, null);
        listView.setAdapter(adapter);

        final GestureDetector gdt = new GestureDetector(this, new SwipeGestureListener(this) {
            @Override
            public void onSwipe(int x, int y, boolean right) {
                //get item
                Object item = adapter.getItem(listView.pointToPosition(x, y));
                if (item != null && item instanceof FeedbackListItem) {
                    if (((FeedbackListItem) item).getTitle().startsWith("STOP")) {
                        return;
                    }
                    FeedbackListItem listItem = (FeedbackListItem) item;
                    questionStopList.remove(listItem);
                    FeedbackModule.addToRemovedQuestions(listItem.getTitle());
                    items.put(translate(R.string.module_feedback_question_stop), questionStopList);
                    adapter = new CategoryListViewAdapter<>(FeedbackActivity.this, items, null);
                    listView.setAdapter(adapter);
                }
            }
        });
        listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gdt.onTouchEvent(event);
                return false;
            }
        });

        TextView chartHeader = (TextView) findViewById(R.id.chartHeader);
        chartHeader.setText(translate(R.string.module_feedback_speed));

        linearChart = (LinearLayout) findViewById(R.id.linearChart);
        drawChart(slow, ok, fast);
    }

    public void drawChart(int slow, int ok, int fast) {

        GraphViewSeries.GraphViewSeriesStyle seriesStyle = new GraphViewSeries.GraphViewSeriesStyle();
        seriesStyle.setValueDependentColor(new ValueDependentColor() {
            @Override
            public int get(GraphViewDataInterface data) {
                if (data.getX() == 0) {
                    return Color.YELLOW;
                }
                if (data.getX() == 1) {
                    return Color.GREEN;
                }
                if (data.getX() == 2) {
                    return Color.RED;
                }
                return Color.WHITE;
            }
        });

        // init example series data
        GraphViewSeries speedSeries = new GraphViewSeries("", seriesStyle, new GraphView.GraphViewData[] {
                new GraphView.GraphViewData(0, slow)
                , new GraphView.GraphViewData(1, ok)
                , new GraphView.GraphViewData(2, fast)
        });

        GraphView graphView = new BarGraphView(
                this, ""
        );

        // set data
        graphView.addSeries(speedSeries);

        //layout
        //graphView.setShowVerticalLabels(false);

        graphView.setHorizontalLabels(new String[] { getResources().getString(R.string.module_feedback_tooslow),
                getResources().getString(R.string.module_feedback_speedok),
                getResources().getString(R.string.module_feedback_toofast)});


        // set custom labels depending on the x and y values
        final List<String> yLabels = new ArrayList<>();
        graphView.setCustomLabelFormatter(new CustomLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    if (value == 0) {
                        return getResources().getString(R.string.module_feedback_tooslow);
                    } else if (value == 1) {
                        return getResources().getString(R.string.module_feedback_speedok);
                    } else if (value == 2) {
                        return getResources().getString(R.string.module_feedback_toofast);
                    }
                } else {
                    String yLabel = "" + ((int) value);
                    if (!yLabels.contains(yLabel)) {
                        yLabels.add(yLabel);
                        return yLabel;
                    } else {
                        return "";
                    }
                }
                return null;
            }
        });


        graphView.setViewPort(0,4);
        graphView.setManualMinY(true);
        graphView.setManualYMinBound(0);

        graphView.setShowLegend(false);
        graphView.getGraphViewStyle().setTextSize(getResources().getDimension(R.dimen.descTextSize));
        graphView.getGraphViewStyle().setGridStyle(GraphViewStyle.GridStyle.NONE);


        linearChart.addView(graphView);
    }

    private class FeedbackQuestionCallback implements ParserCallback<FeedbackList> {

        @Override
        public void afterParse(FeedbackList parsed) {
            questions.clear();

            if (progressDialog != null) {
                progressDialog.dismiss();
            }

            if (parsed != null) {
                ////Log.i(TAG, "received " + parsed.getItems().size() +  " question feedbacks");
                List<Feedback> feedbacks = parsed.getItems();
                for (Feedback feedback : feedbacks) {
                    if (!FeedbackModule.containsRemovedQuestion(feedback.getText())) {
                        questions.add(feedback.getText());
                    }
                }
            }

            loadQuestionReady = true;
        }

        @Override
        public void onError(Exception e) {
            loadQuestionReady = true;
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        @Override
        public void onManagedError(FeedbackList parsed) {
            //TODO
        }
    }

    private class FeedbackStopCallback implements ParserCallback<FeedbackList> {

        @Override
        public void afterParse(FeedbackList parsed) {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }

            if (parsed != null) {
                ////Log.i(TAG, "received " + parsed.getItems().size() +  " stop feedbacks");
                stops = parsed.getItems().size();
                //List<Feedback> feedbacks= (List<Feedback>) parsed.getItems();
            }

            loadStopReady = true;
        }

        @Override
        public void onError(Exception e) {
            loadStopReady = true;
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        @Override
        public void onManagedError(FeedbackList parsed) {
            //TODO
        }
    }

    private class FeedbackSpeedCallback implements ParserCallback<FeedbackList> {

        @Override
        public void afterParse(FeedbackList parsed) {
            slow = 0;
            ok = 0;
            fast = 0;

            if (progressDialog != null) {
                progressDialog.dismiss();
            }

            if (parsed != null) {
                ////Log.i(TAG, "received " + parsed.getItems().size() +  " speed feedbacks");

                List<Feedback> feedbacks = (List<Feedback>) parsed.getItems();


                for (Feedback feedback : feedbacks) {
                    if (FeedbackModule.SPEED_SLOW == feedback.getInteger_value()) {
                        ++slow;
                    } else if (FeedbackModule.SPEED_OK == feedback.getInteger_value()) {
                        ++ok;
                    } else if (FeedbackModule.SPEED_FAST == feedback.getInteger_value()) {
                        ++fast;
                    }
                }
            }
            loadSpeedReady = true;
        }

        @Override
        public void onError(Exception e) {
            loadSpeedReady = true;
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        @Override
        public void onManagedError(FeedbackList parsed) {
            //TODO
        }
    }


}
