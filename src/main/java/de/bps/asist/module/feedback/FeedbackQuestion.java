package de.bps.asist.module.feedback;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import de.bps.asist.R;
import de.bps.asist.module.AsistModuleActivity;

/**
 * Created by litho on 29.08.14.
 * feedback model class for questions
 */
public class FeedbackQuestion extends AsistModuleActivity{

    public static final String EXTRA_COURSE_ID = "course_id";

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.module_feedback_question);

        editText = (EditText) findViewById(R.id.feedback_editText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.send_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.action_send == item.getItemId()) {
            String question = editText.getText().toString();
            Long courseId = getIntent().getLongExtra(EXTRA_COURSE_ID, 0);
            FeedbackModule.sendFeedbackQuestion(question, courseId, this);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
