package de.bps.asist.module.feedback.model;

import android.content.Intent;
import android.graphics.drawable.Drawable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.feedback.FeedbackModule;
import de.bps.asist.module.feedback.FeedbackQuestion;
import de.bps.asist.module.timetable.model.Course;
import de.bps.asist.module.timetable.model.CourseDescription;

/**
 * Created by litho on 29.08.14.
 *
 */
public class StudentFeedbackList {

    private Map<String, List<FeedbackListItem>> items = new LinkedHashMap<>();
    private AsistModuleActivity context;
    private Course course;

    public StudentFeedbackList(AsistModuleActivity context){
        this.context = context;


        Object courseObject = context.getSelectedItem();
        if (courseObject instanceof Course) {
            course = (Course) courseObject;
        } else if (courseObject instanceof CourseDescription) {
            course = ((CourseDescription) courseObject).getCourse();
        }


        List<FeedbackListItem> speedList = new ArrayList<>();
        FeedbackListItem tooSlow = createItem(R.string.module_feedback_tooslow, R.drawable.ic_feedback_slow);
        FeedbackListItem speedOK = createItem(R.string.module_feedback_speedok, R.drawable.ic_feedback_ok);
        FeedbackListItem tooFast = createItem(R.string.module_feedback_toofast, R.drawable.ic_feedback_fast);
        tooSlow.setCallback(new SpeedSender(FeedbackModule.SPEED_SLOW));
        speedOK.setCallback(new SpeedSender(FeedbackModule.SPEED_OK));
        tooFast.setCallback(new SpeedSender(FeedbackModule.SPEED_FAST));
        speedList.add(tooSlow);
        speedList.add(speedOK);
        speedList.add(tooFast);
        items.put(translate(R.string.module_feedback_header_speed), speedList);
        List<FeedbackListItem> questionList = new ArrayList<>();
        FeedbackListItem questionItem = createItem(R.string.module_feedback_question, null);
        questionItem.setCallback(new QuestionCallback());
        questionList.add(questionItem);
        FeedbackListItem stopItem = new FeedbackListItem(translate(R.string.module_feedback_stop), null);
        stopItem.setCallback(new StopCallback());
        questionList.add(stopItem);
        items.put(translate(R.string.module_feedback_header_question), questionList);
    }

    private FeedbackListItem createItem(int string, Integer image){
        return new FeedbackListItem(translate(string), getImage(image));
    }

    private String translate(int id){
        return context.getResources().getString(id);
    }

    private Drawable getImage(Integer id){
        if(id == null){
            return null;
        }
        return context.getResources().getDrawable(id);
    }

    public Map<String, List<FeedbackListItem>> getItems() {
        return items;
    }

    private class SpeedSender implements IGenericListCallback<FeedbackListItem>{

        private int speed;

        public SpeedSender(int speed) {
            this.speed = speed;
        }

        @Override
        public void doAction(FeedbackListItem result) {
            FeedbackModule.sendFeedbackSpeed(speed, course.getCourseid(), context);
        }
    }

    private class QuestionCallback implements IGenericListCallback {

        @Override
        public void doAction(Serializable o) {
            Intent intent = new Intent(context, FeedbackQuestion.class);
            intent.putExtra(FeedbackQuestion.EXTRA_COURSE_ID, course.getCourseid());
            context.startActivity(intent);
        }
    }

    private class StopCallback implements IGenericListCallback {

        @Override
        public void doAction(Serializable o) {
            FeedbackModule.sendFeedbackStop(course.getCourseid(), context);
        }
    }
}
