package de.bps.asist.module.feedback;

import android.os.Bundle;
import android.view.View;
import android.widget.ToggleButton;

import de.bps.asist.R;
import de.bps.asist.module.AsistModuleActivity;

/**
 * Created by thomasw on 04.11.14.
 * Configuration activity for feedback view
 */
public class FeedbackConfigActivity extends AsistModuleActivity<String> {

    private boolean isLecturerMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isLecturerMode = FeedbackModule.isLecturerMode(this);

        setContentView(R.layout.feedback_config);

        ToggleButton lecturerButton = (ToggleButton) findViewById(R.id.lecturermode_button);
        lecturerButton.setChecked(isLecturerMode);

        lecturerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLecturerMode = !isLecturerMode;
                FeedbackModule.setLecturerMode(isLecturerMode, FeedbackConfigActivity.this);
            }
        });
    }
}
