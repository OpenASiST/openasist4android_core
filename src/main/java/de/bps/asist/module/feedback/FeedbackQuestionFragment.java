package de.bps.asist.module.feedback;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;
import de.bps.asist.R;

/**
 * Created by litho on 29.08.14.
 * Fragment for sending questions to the server
 */
public class FeedbackQuestionFragment extends DialogFragment {

    public FeedbackQuestionFragment(){
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.module_feedback_question, container, false);

        return view;
    }
}
