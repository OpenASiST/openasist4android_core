package de.bps.asist.module.feedback.model;

import de.bps.asist.core.model.IListItem;

/**
 * Created by thomasw on 05.11.14.
 * Feedback model class
 */
public class Feedback implements IListItem {

    int integer_value;
    String text;
    String lastModified;

    public void setInteger_value(int integer_value) {
        this.integer_value = integer_value;
    }

    public int getInteger_value() {
        return integer_value;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getLastModified() {
        return lastModified;
    }

    @Override
    public Long getId() {
        return null;
    }
}
