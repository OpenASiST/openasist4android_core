package de.bps.asist.module.feedback.model;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

import de.bps.asist.core.annotation.ASiSTCallback;
import de.bps.asist.core.annotation.ASiSTDrawable;
import de.bps.asist.core.annotation.ASiSTViewId;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.gui.list.IGenericListCallback;

/**
 * Created by litho on 29.08.14.
 * feedback model class
 */

@AsistItemConfiguration(showMainLogo = true)
public class FeedbackListItem implements Serializable{

    @AsistTitle
    private String title;

    @ASiSTDrawable
    private Drawable image;

    @ASiSTCallback
    private IGenericListCallback callback;

    @ASiSTViewId
    private int viewId;

    public int getViewId() {
        return viewId;
    }

    public void setViewId(int viewId) {
        this.viewId = viewId;
    }

    public FeedbackListItem(String title, Drawable drawable){
        this.title = title;
        this.image = drawable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public IGenericListCallback getCallback() {
        return callback;
    }

    public void setCallback(IGenericListCallback callback) {
        this.callback = callback;
    }
}
