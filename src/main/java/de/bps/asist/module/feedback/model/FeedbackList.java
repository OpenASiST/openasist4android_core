package de.bps.asist.module.feedback.model;

import java.util.List;

import de.bps.asist.core.model.IList;

/**
 * Created by thomasw on 05.11.14.
 * feedback root model class
 */
public class FeedbackList implements IList {

    private List<Feedback> questions;

    public List<Feedback> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Feedback> questions) {
        this.questions = questions;
    }

    @Override
    public List<Feedback> getItems() {
        return questions;
    }
}
