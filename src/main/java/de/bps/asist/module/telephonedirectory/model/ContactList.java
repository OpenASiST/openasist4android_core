package de.bps.asist.module.telephonedirectory.model;

import java.util.List;

import de.bps.asist.core.model.IList;

public class ContactList implements IList {

    private List<Contact> contacts;

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(final List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public List<Contact> getItems() {
        return contacts;
    }


}
