package de.bps.asist.module.telephonedirectory.model;

import de.bps.asist.core.model.IListItem;

/**
 * @author Martin Luschek
 */
public class TelephoneNumber implements IListItem {

    private String number;

    public TelephoneNumber() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(final String number) {
        this.number = number;
    }

    @Override
    public Long getId() {
        return 0L;
    }

}
