package de.bps.asist.module.telephonedirectory.model;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.model.IListItem;

/**
 * @author Martin Luschek
 */
public class Telephone implements IListItem {

    @ASiSTDetail(i18nKey = "module_telephone_contact_details_telephone", position = 6)
    private String number;

    public Telephone() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(final String number) {
        this.number = number;
    }

    @Override
    public Long getId() {
        return 0L;
    }

}
