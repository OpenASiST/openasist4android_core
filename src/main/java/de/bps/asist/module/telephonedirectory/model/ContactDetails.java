package de.bps.asist.module.telephonedirectory.model;

import com.j256.ormlite.field.DatabaseField;

import java.util.ArrayList;
import java.util.List;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.model.IListItem;

/**
 * Created by Martin Luschek on 04.11.2014.
 */
public class ContactDetails implements IListItem {


    @ASiSTDetail(i18nKey = "module_telephone_contact_details_name", position = 1)
    private String fullName;

    @DatabaseField
    @ASiSTDetail(i18nKey = "module_telephone_contact_details_department", position = 2)
    private String department;

    @DatabaseField
    @ASiSTDetail(i18nKey = "module_telephone_contact_details_email", position = 5)
    private String email;

    @DatabaseField
    @ASiSTDetail(i18nKey = "module_telephone_contact_details_room", position = 3)
    private String room;

    @DatabaseField
    @ASiSTDetail(i18nKey = "module_telephone_contact_details_building", position = 4)
    private String building;

    @ASiSTDetail(i18nKey = "module_telephone_contact_details_telephone", position = 6)
    private List<String> telephone = new ArrayList<>();

    public ContactDetails(Contact contact) {
        fullName = contact.getFullName();
        department = contact.getDepartment();
        if (contact.getRoom() != null) {
            room = contact.getRoom().getTitle();
        }
        if (contact.getBuilding() != null) {
            building = contact.getBuilding().getTitle();
        }
        email = contact.getEmail();
        if (!contact.getItems().isEmpty()) {
            for (Telephone tel : contact.getItems()) {
                telephone.add(tel.getNumber());
            }
        }
    }

    @Override
    public Long getId() {
        return 0l;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(final String department) {
        this.department = department;
    }

    public String getEmail() {
        return email.toString();
    }

    public void setEmail(final String email) {

        this.email = email;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(final String room) {
        this.room = room;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(final String building) {
        this.building = building;
    }

    public List<String> getTelephone() {
        return telephone;
    }

    public void setTelephone(final List<String> telephone) {
        this.telephone = telephone;
    }
}
