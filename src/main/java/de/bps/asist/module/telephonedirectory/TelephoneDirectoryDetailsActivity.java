package de.bps.asist.module.telephonedirectory;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.gui.detail.DetailedTableActivity;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.telephonedirectory.model.Contact;
import de.bps.asist.module.telephonedirectory.model.ContactDetails;

public class TelephoneDirectoryDetailsActivity extends DetailedTableActivity<Contact> {


    private ListView listView;
    private ContactDetails details;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void doCreate() {
        setContentView(R.layout.module_telephone_directory_details_activity);
        listView = getView(R.id.ContactDetailsListView);

        Contact selected = getSelectedItem();

        details = new ContactDetails(selected);

        Map<String, List<ASiSTDetailDescription>> detailsMap = createDetailsMap(details);
        final TelephoneAdapter adapter = new TelephoneAdapter(this, detailsMap, null);
        listView.setAdapter(adapter);
    }

    /**
     * Methode for cutting multiple telephone numbers down to one number.
     *
     * @param number telephonenumber, one or more
     * @return one telephone number
     */
    private String parseTelefoneNumber(String number) {
        if (number.length() <= 18) {
            return number;
        } else {
            number = number.substring(0, 18);
            return number;
        }
    }

    private class TelephoneAdapter extends CategoryListViewAdapter<ASiSTDetailDescription> {

        public TelephoneAdapter(final Context context, final Map<String, List<ASiSTDetailDescription>> objects, final IGenericListCallback callback) {
            super(context, objects, callback);
        }

        @Override
        protected LinearLayout addItemView(final int position, final LayoutInflater inflater) {
            final ASiSTDetailDescription item = (ASiSTDetailDescription) getItem(position);

            final LinearLayout row = (LinearLayout) inflater.inflate(R.layout.detail_list_item, null);

            final String title = getContext().getString(item.getI18nKey());
            if(title.equals("department")){
                final String retitle = "Abteilung";
                ((TextView) row.findViewById(R.id.detailItemTitle)).setText(retitle);
            } else if (title.equals("Email")) {
                final String retitle = "E-Mail";
                ((TextView) row.findViewById(R.id.detailItemTitle)).setText(retitle);
            } else {
                ((TextView) row.findViewById(R.id.detailItemTitle)).setText(title);
            }
            final SpannableStringBuilder sb = new SpannableStringBuilder();
            sb.append(item.getValue());
            final ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(R.color.textfieldActive));
            sb.setSpan(fcs, 0, item.getValue().length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);


            if (title.startsWith("Tele") && item.getValue().length() > 0) {
                ((TextView) row.findViewById(R.id.detailItemValue)).setAutoLinkMask(Linkify.PHONE_NUMBERS);
                ((TextView) row.findViewById(R.id.detailItemValue)).setText(sb);

                row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + parseTelefoneNumber(item.getValue())));
                        startActivity(call);
                    }
                });

            } else if (title.equals("Email")) {
                ((TextView) row.findViewById(R.id.detailItemValue)).setAutoLinkMask(Linkify.EMAIL_ADDRESSES);
                ((TextView) row.findViewById(R.id.detailItemValue)).setText(sb);

                row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent email = new Intent(Intent.ACTION_SENDTO);
                        email.setData(Uri.parse("mailto:" + item.getValue()));
                        startActivity(email);
                    }
                });

            } else {
                final String value = item.getValue();
                ((TextView) row.findViewById(R.id.detailItemValue)).setText(value);

            }
            addCallback(item, row, getCallback());
            return row;
        }
    }
}
