package de.bps.asist.module.telephonedirectory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistList;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IList;
import de.bps.asist.core.model.IListItem;

/**
 * @author Martin Luschek
 * @date 14.10.2014
 */
@AsistItemConfiguration(showDescription = true, useCallback = true, showMainLogo = true)
@DatabaseTable
public class Contact extends AbstractDatabaseObject implements IList, IListItem {

    private static final long serialVersionUID = 1L;

    @DatabaseField
    private String firstName;

    @DatabaseField
    private String lastName;

    @ASiSTDetail(i18nKey = "module_telephone_contact_details_name", position = 1)
    @AsistTitle
    private String fullName;

    @DatabaseField
    @ASiSTDetail(i18nKey = "module_telephone_contact_details_department", position = 2)
    @AsistDescription
    private String department;

    @DatabaseField
    @ASiSTDetail(i18nKey = "module_telephone_contact_details_email", position = 5)
    private String email;

    @DatabaseField
    @ASiSTDetail(i18nKey = "module_telephone_contact_details_room", position = 3)
    private POIDescription room;

    @DatabaseField
    @ASiSTDetail(i18nKey = "module_telephone_contact_details_building", position = 4)
    private POIDescription building;

    @JsonIgnore
    private long contactID;

    @AsistList
    @ASiSTDetail(i18nKey = "module_telephone_contact_details_telephone", position = 6)
    private List<Telephone> telephone;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        if (fullName == null) {
            setFullName();
            return fullName;
        } else {
            return fullName;
        }
    }

    private void setFullName() {
        this.fullName = this.lastName + ", " + this.firstName;
    }

    public void setContactID(long contactID) {
        this.contactID = contactID;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(final String department) {
        this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public List<Telephone> getTelephone() {
        return telephone;
    }

    public POIDescription getRoom() {
        return room;
    }

    public void setRoom(final POIDescription room) {
        this.room = room;
    }

    public POIDescription getBuilding() {
        return building;
    }

    public void setBuilding(POIDescription building) {
        this.building = building;
    }

    @Override
    public Long getId() {
        return contactID;
    }

    public List<Telephone> getItems() {
        return getTelephone();
    }

    public void setItems(final List<Telephone> telephone) {
        this.telephone = telephone;
    }
}
