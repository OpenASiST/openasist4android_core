/**
 *
 */
package de.bps.asist.module.telephonedirectory;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;

/**
 * @author thomasw
 */
public class TelephoneDirectoryModule extends AbstractAsistModule {

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getName()
     */
    @Override
    public int getName() {
        return R.string.module_telephonedirectory_name;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_user;
    }

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getInitialFragment()
     */
    @Override
    public Fragment getInitialFragment() {
        return new TelephoneDirectoryFragment();
    }

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getDatabaseClasses()
     */
    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        return new ArrayList<>();
    }

}
