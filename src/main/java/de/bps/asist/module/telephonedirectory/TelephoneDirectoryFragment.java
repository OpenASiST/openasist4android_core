/**
 *
 */
package de.bps.asist.module.telephonedirectory;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import de.bps.asist.R;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.module.telephonedirectory.model.Contact;
import de.bps.asist.module.telephonedirectory.model.ContactList;

/**
 * @author thomasw
 */
public class TelephoneDirectoryFragment extends AbstractASiSTFragment {

    private Button btnSearch;
    private TextView tvResultsCount;
    private ListView lvResults;

    private EditText etSearch;

    private GenericListViewAdapter adapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.module_telephone_directory_fragment, container, false);
        btnSearch = getView(view, R.id.moduleTelephoneDirectoryActivitySearchButton);
        etSearch = getView(view, R.id.moduleTelephoneDirectoryActivitySearchInputField);
        tvResultsCount = getView(view, R.id.moduleTelephoneDirectoryActivityResultsCount);
        lvResults = getView(view, R.id.moduleTelephoneDirectoryActivityListView);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            btnSearch.setVisibility(View.GONE);
        } else {
            btnSearch.setOnClickListener(new OnSearchButtonClickHandler());
        }

        etSearch.setOnEditorActionListener(new OnSearchButtonEnterHandler());

    }

    private void search(final String query) {
        if(query.length() < 3){
            Toast.makeText(getActivity(), getString(R.string.minimum_search_length),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        lvResults.setVisibility(View.GONE);

        tvResultsCount.setText(translate(R.string.module_telephone_result_loading));

        final String url = translate(R.string.rootUrl);
        final String university = translate(R.string.institutionName);
        String qs = query;

        try {
            qs = URLEncoder.encode(query, "utf8");
        } catch (final UnsupportedEncodingException e) {
            ////Log.wtf(TelephoneDirectoryFragment.class.getSimpleName(), "No UTF-8 support?", e);
        }
        final StringBuilder api = new StringBuilder(url);
        api.append("/telephone/");
        api.append(university);
        api.append("/search/");
        api.append(qs);

        final ContactCallback cb = new ContactCallback(getActivity());

        ASiSTParser.getInstance().parseWithTask(api.toString(), ContactList.class, cb);
    }

    private final class OnSearchButtonClickHandler implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            String qs = "";
            qs = etSearch.getText().toString().trim();
            search(qs);
        }
    }

    private final class OnSearchButtonEnterHandler implements OnEditorActionListener {

        @Override
        public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
            String qs = "";
            qs = etSearch.getText().toString().trim();
            search(qs);
            return false;
        }
    }

    private class ContactCallback implements ParserCallback<ContactList> {

        private final Context context;

        public ContactCallback(final Context context) {
            this.context = context;
        }

        @Override
        public void onError(Exception e) {
            //TODO
        }

        @Override
        public void onManagedError(ContactList parsed) {
            //TODO
        }

        @Override
        public void afterParse(final ContactList parsed) {
            final List<Contact> items = new ArrayList<>();
            if (parsed != null) {
                final List<Contact> list = parsed.getItems();
                if (list != null) {
                    items.addAll(list);
                }
            }

            final String res;
            if (items.size() == 0) {
                res = translate(R.string.module_telephone_no_results);
            } else {
                res = translate(R.string.module_telephone_result_results, items.size());
            }
            tvResultsCount.setText(res);

            adapter = new GenericListViewAdapter<>(context, items,
                    new AbstractGenericListCallback(
                            getActivity(), TelephoneDirectoryDetailsActivity.class));
            lvResults.setAdapter(adapter);
            lvResults.setVisibility(View.VISIBLE);
        }
    }
}
