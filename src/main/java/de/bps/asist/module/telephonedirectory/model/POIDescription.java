package de.bps.asist.module.telephonedirectory.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import de.bps.asist.core.database.AbstractDatabaseObject;

/**
 * Created by Martin Luschek on 19.10.2014.
 */
@DatabaseTable
public class POIDescription extends AbstractDatabaseObject {

    private static final long serialVersionUID = 3L;

    @DatabaseField
    private String title;

    @DatabaseField
    private int poiid;

    @DatabaseField
    private int reference;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public int getPoiid() {
        return poiid;
    }

    public void setPoiid(final int poiid) {
        this.poiid = poiid;
    }

    public int getReference() {
        return reference;
    }

    public void setReference(final int reference) {
        this.reference = reference;
    }
}
