package de.bps.asist.module.library;

import android.os.Bundle;

import de.bps.asist.R;

/**
 * Library favorites activity
 * 
 * @author laeb
 */
public class LibraryFavoritesActivity extends AbstractLibraryActivity {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.module_library_favorites_activity);
	}
}
