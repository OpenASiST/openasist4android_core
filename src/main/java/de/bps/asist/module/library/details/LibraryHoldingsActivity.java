package de.bps.asist.module.library.details;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.annotation.AnnotationHelper;
import de.bps.asist.core.manager.image.ASiSTImageManager;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.gui.detail.DetailTableAdapter;
import de.bps.asist.gui.detail.DetailedTableActivity;
import de.bps.asist.module.library.model.LibraryDetailsHoldings;

/**
 * Created by litho on 29.10.14.
 * Library model class for holdings of a book
 */
public class LibraryHoldingsActivity extends DetailedTableActivity<LibraryDetails> {

    private ImageView imageView;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.library_details);
        LinearLayout layout = getView(R.id.availability_layout);
        layout.setVisibility(View.GONE);
        listView = getView(R.id.bookDetails);

        imageView = getView(R.id.bookImage);
        LibraryDetails details = getSelectedItem();
        ASiSTImageManager.getInstance().setImage(details.getImageUrl(), imageView);
        TextView titleView = getView(R.id.bookTitle);
        titleView.setText(details.getTitle());

        Map<String, List<ASiSTDetailDescription>> detailsMap = createDetailsMap(details);
        final DetailTableAdapter adapter = new DetailTableAdapter(this, detailsMap, null);
        listView.setAdapter(adapter);
    }

    @Override
    protected Map<String, List<ASiSTDetailDescription>> createDetailsMap(IListItem item) {
        Map<String, List<ASiSTDetailDescription>> result = new LinkedHashMap<>();
        List<LibraryDetailsHoldings> holdings =  getSelectedItem().getHoldings();
        if(holdings != null){
            int counter = 1;
            for(LibraryDetailsHoldings holding: holdings){
                String header = translate(R.string.library_holding_header, counter++);
                List<ASiSTDetailDescription> detailList = parseDetails(holding);
                result.put(header, detailList);
            }
        }
        return result;
    }

    private List<ASiSTDetailDescription> parseDetails(LibraryDetailsHoldings holding){
        List<ASiSTDetailDescription> result = new ArrayList<>();
        Map<Integer, ASiSTDetailDescription> detailMap = AnnotationHelper.readASiSTDetails(holding);
        for(Integer row: detailMap.keySet()){
            result.add(detailMap.get(row));
        }
        return result;
    }


}
