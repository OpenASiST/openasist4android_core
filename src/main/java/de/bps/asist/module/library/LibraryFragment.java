package de.bps.asist.module.library;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.module.library.details.LibraryDetailsActivity;
import de.bps.asist.module.library.model.LibraryBook;
import de.bps.asist.module.library.model.LibraryBookList;
import de.bps.asist.module.library.model.LibraryResultErrorEvent;

import static de.bps.asist.module.library.LibraryExtendedSearchActivity.EXTRA_AUTHOR;
import static de.bps.asist.module.library.LibraryExtendedSearchActivity.EXTRA_ISBN;
import static de.bps.asist.module.library.LibraryExtendedSearchActivity.EXTRA_TITLE;
import static de.bps.asist.module.library.LibraryExtendedSearchActivity.EXTRA_YEAR;
import static de.bps.asist.module.library.LibraryModule.EXTENDED_SEARCH_RESULT;

/**
 * Initial library fragment (home)
 *
 * @author laeb
 */
public class LibraryFragment extends AbstractASiSTFragment {

    private static final String TAG = LibraryFragment.class.getSimpleName();

    private EditText searchField;
    private Button searchButton;
    private TextView resultCount;
    private ListView resultList;
    private boolean errorOnSearch;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.module_library_home_activity, container, false);
        searchField = (EditText) view.findViewById(R.id.moduleLibraryHomeSearchInputField);
        searchButton = (Button) view.findViewById(R.id.moduleLibraryHomeSearchButton);
        resultList = getView(view, R.id.library_search_result);
        resultCount = getView(view, R.id.library_result_count);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            searchButton.setVisibility(View.GONE);
        } else {
            searchButton.setOnClickListener(new OnSearchButtonClickHandler());
        }
        searchField.setOnEditorActionListener(new OnSearchButtonEnterHandler());
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private final class OnSearchButtonClickHandler implements OnClickListener {
        @Override
        public void onClick(final View v) {
            search(searchField.getText().toString(), false);
        }
    }

    @Override
    public Integer getMenuLayout() {
        return R.menu.library;
    }

    @Override
    public boolean handleMenuItemClicked(MenuItem item) {
        if (item.getItemId() == R.id.action_library_barcode) {
            final Intent intent = new Intent(getActivity(), LibraryBarcodeActivity.class);
            startActivityForResult(intent, LibraryModule.BARCODE_RESULT);
            return true;
        } else if (R.id.action_library_search == item.getItemId()) {
            final Intent mIntent = new Intent(getActivity(), LibraryExtendedSearchActivity.class);
            startActivityForResult(mIntent, LibraryModule.EXTENDED_SEARCH_RESULT);
            return true;
        }
        return super.handleMenuItemClicked(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LibraryModule.BARCODE_RESULT && resultCode == LibraryModule.BARCODE_RESULT) {
            String barcode = data.getStringExtra(LibraryModule.BARCODE_VALUE);
            searchField.setText(barcode);
            search(barcode, false);
        } else if (requestCode == EXTENDED_SEARCH_RESULT && resultCode == EXTENDED_SEARCH_RESULT) {
            Map<String, String> queryMap = new HashMap<>();
            queryMap.put(EXTRA_TITLE, data.getStringExtra(EXTRA_TITLE));
            queryMap.put(EXTRA_AUTHOR, data.getStringExtra(EXTRA_AUTHOR));
            queryMap.put(EXTRA_ISBN, data.getStringExtra(EXTRA_ISBN));
            queryMap.put(EXTRA_YEAR, data.getStringExtra(EXTRA_YEAR));
            StringBuilder sb = new StringBuilder();
            for (String key : queryMap.keySet()) {
                String value = queryMap.get(key);
                if (sb.length() > 0) {
                    sb.append("&");
                } else {
                    sb.append("?");
                }
                if (value != null && !value.isEmpty()) {
                    sb.append(key);
                    sb.append("=").append(value);
                }
            }
            search(sb.toString(), true);
        }
    }

    private final class OnSearchButtonEnterHandler implements OnEditorActionListener {

        @Override
        public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
            if (searchField == v && (EditorInfo.IME_ACTION_DONE == actionId || EditorInfo.IME_ACTION_NEXT == actionId)) {
                search(searchField.getText().toString(), false);
            }
            return false;
        }

    }

    private void search(final String query, final boolean extended) {
        if(query.length() < 3 || query.equals("?&&&")){
            Toast.makeText(getActivity(), getString(R.string.minimum_search_length),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        resultList.setVisibility(View.GONE);
        ////Log.i(TAG, "Starting search...");
        resultCount.setText(translate(R.string.module_library_results_loading));
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final String url = translate(R.string.rootUrl);

        final String university = translate(R.string.institutionName);
        final StringBuilder api = new StringBuilder(url);
        api.append("/library/").append(university);
        if (extended) {
            api.append("/findExtended");
            api.append(query);
        } else {
            String qs = query;
            try {
                qs = URLEncoder.encode(query, "utf8");
            } catch (final UnsupportedEncodingException e) {
                ////Log.wtf(TAG, "No UTF-8 support?", e);
            }
            api.append("/find/");
            api.append(qs);
        }
        final BookCallback cb = new BookCallback(getActivity());
        ASiSTParser.getInstance().parseWithTask(api.toString(), LibraryBookList.class, cb);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LibraryResultErrorEvent e){
        String res = translate(R.string.module_library_results_no_results);
        resultCount.setText(res);
    }

    private class BookCallback implements ParserCallback<LibraryBookList> {

        private final Context context;

        public BookCallback(final Context context) {
            this.context = context;
        }

        @Override
        public void onError(Exception e) {
            //TODO
            EventBus.getDefault().post(new LibraryResultErrorEvent());
        }

        @Override
        public void onManagedError(LibraryBookList parsed) {
            //TODO
        }

        @Override
        public void afterParse(final LibraryBookList parsed) {
            final List<LibraryBook> items = new ArrayList<>();
            if (parsed != null) {
                final List<LibraryBook> list = parsed.getItems();
                if (list != null) {

                    items.addAll(list);
                }
            }

            ////Log.i(TAG, "Finished search with " + items.size() + " results.");

            final String res;
            if (items.size() == 0) {
                res = translate(R.string.module_library_results_no_results);
            } else {
                res = translate(R.string.module_library_results_results, items.size());
            }
            resultCount.setText(res);
            final LibraryResultAdapter adapter = new LibraryResultAdapter(context, items, new AbstractGenericListCallback(
                    getActivity(), LibraryDetailsActivity.class));
            resultList.setAdapter(adapter);
            resultList.setVisibility(View.VISIBLE);
        }
    }
}
