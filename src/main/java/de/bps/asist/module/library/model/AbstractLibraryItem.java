package de.bps.asist.module.library.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.bps.asist.core.model.IList;
import de.bps.asist.core.model.IListItem;

abstract class AbstractLibraryItem implements IListItem, IList {
	private static final long serialVersionUID = -5265268011714016401L;

	private String bookId;

	@Override
	public Long getId() {
		Long id = 0l;
		try {
			id = Long.parseLong(getBookId());
		} catch (final NumberFormatException e) {
			////Log.w(LibraryDetailsHoldings.class.getSimpleName(), "Could not parseWithThread book ID " + getBookId());
		}
		return id;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(final String bookId) {
		this.bookId = bookId;
	}

	@Override
	public List<? extends IListItem> getItems() {
		return new ArrayList<>();
	}
}
