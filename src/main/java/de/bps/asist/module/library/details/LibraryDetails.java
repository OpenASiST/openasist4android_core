package de.bps.asist.module.library.details;

import android.text.Html;

import java.util.List;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.module.library.model.LibraryBook;
import de.bps.asist.module.library.model.LibraryDetailsBook;
import de.bps.asist.module.library.model.LibraryDetailsHoldings;

/**
 * Created by litho on 29.10.14.
 * Convenience class for interacting with the server
 */
public class LibraryDetails implements IListItem {

    private String bookId;

    private String title;

    @ASiSTDetail(i18nKey = "module_library_details_book_author", position = 1)
    private String author;

    @ASiSTDetail(i18nKey = "module_library_details_book_format", position = 2)
    private String format;

    @ASiSTDetail(i18nKey = "module_library_details_book_year", position = 3)
    private String year;

    @ASiSTDetail(i18nKey = "library_details_publisher", position = 4)
    private String publisher;

    @ASiSTDetail(i18nKey = "module_library_details_book_isbn", position = 5)
    private String isbn;

    @ASiSTDetail(i18nKey = "module_library_details_book_language", position = 6)
    private String language;

    @ASiSTDetail(i18nKey = "library_details_edition", position = 7)
    private String edition;

    @ASiSTDetail(i18nKey = "library_details_buzzwords", position = 8)
    private List<String> buzzwords;

    private String imageUrl;

    private int available;

    private List<LibraryDetailsHoldings> holdings;

    public LibraryDetails(LibraryBook book){
        bookId = book.getBookId();
        title = book.getTitle();
        author = book.getAuthor();
        format = book.getFormat();
        year = book.getPubdate();
        available = book.getAvailable();
        isbn = book.getIsbn();
    }

    public void addDetails(LibraryDetailsBook details){
        publisher = details.getPublisher();
        language = details.getLanguage();
        imageUrl = details.getImageUrl();
        edition = details.getEdition();
        buzzwords = details.getBuzzwords();
        holdings = details.getHoldings();
    }

    @Override
    public Long getId() {
        return 0l;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        try{return Html.fromHtml(title).toString();} catch (Exception e) {return title;}
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        try{return Html.fromHtml(author).toString();} catch (Exception e) {return author;}
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getFormat() {
        try{return Html.fromHtml(format).toString();} catch (Exception e) {return format;}
        }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getLanguage() {
        try{return Html.fromHtml(language).toString();} catch (Exception e) {return language;}
        }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getBuzzwords() {
        return buzzwords;
    }

    public void setBuzzwords(List<String> buzzwords) {
        this.buzzwords = buzzwords;
    }

    public List<LibraryDetailsHoldings> getHoldings() {
        return holdings;
    }

    public void setHoldings(List<LibraryDetailsHoldings> holdings) {
        this.holdings = holdings;
    }
}
