package de.bps.asist.module.library.model;

import java.util.List;

import de.bps.asist.core.model.IList;

public class LibraryBookList implements IList {

	private List<LibraryBook> books;

	public List<LibraryBook> getBooks() {
		return books;
	}

	public void setBooks(final List<LibraryBook> books) {
		this.books = books;
	}

	@Override
	public List<LibraryBook> getItems() {
		return books;
	}
}
