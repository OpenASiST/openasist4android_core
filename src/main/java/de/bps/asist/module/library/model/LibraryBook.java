package de.bps.asist.module.library.model;

import android.text.Html;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;

import static de.bps.asist.core.util.StringHelper.isNullOrEmpty;

@AsistItemConfiguration(showDescription = false, showMainLogo = true)
public class LibraryBook extends AbstractLibraryItem {
	private static final long serialVersionUID = -8216570388057582892L;

	private String title;

	@ASiSTDetail(i18nKey = "module_library_details_book_author", position = 1)
	private String author;

	@ASiSTDetail(i18nKey = "module_library_details_book_format", position = 2)
	private String format;
	private String formatIcon;

	@AsistTitle
	private String extendedTitle = null;
	@AsistDescription
	private String desc;
	@ASiSTDetail(i18nKey = "module_library_details_book_isbn", position = 3)
	private String isbn;

	@ASiSTDetail(i18nKey = "module_library_details_book_year", position = 4)
	private String pubdate;
	private Integer available;

    private Integer bookType;

	public LibraryBook() {
		//
	}

	public String getTitle() {
		try{return Html.fromHtml(title).toString();} catch (Exception e) {return title;}
	}

	public void setTitle(final String title) {
		extendedTitle = null;
		this.title = title;
	}

	public String getAuthor() {
		try{return Html.fromHtml(author).toString();} catch (Exception e) {return author;}
	}

	public void setAuthor(final String author) {
		this.author = author;
	}

	public String getDesc() {
		try{return Html.fromHtml(desc).toString();} catch (Exception e) {return desc;}
	}

	public void setDesc(final String description) {
		this.desc = description;
	}

	public String getIsbn() {
		try{return Html.fromHtml(isbn).toString();} catch (Exception e) {return isbn;}
	}

	public void setIsbn(final String isbn) {
		this.isbn = isbn;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(final Integer available) {
		this.available = available;
	}

	public String getPubdate() {
		try{return Html.fromHtml(pubdate).toString();} catch (Exception e) {return pubdate;}
	}

	public void setPubdate(final String pubdate) {
		extendedTitle = null;
		this.pubdate = pubdate;
	}

	/**
	 * "Dummy" property
	 * 
	 * @return getTitle() + " " + getPubdate()
	 */
	public String getExtendedTitle() {
		if (extendedTitle == null) {
			final StringBuilder sb = new StringBuilder(getTitle());
			if (isNullOrEmpty(getPubdate())) {
				sb.append(" (").append(getPubdate()).append(")");
			}
			extendedTitle = sb.toString();
		}
		try{return Html.fromHtml(extendedTitle).toString();} catch (Exception e) {return extendedTitle;}
	}

	public void setExtendedTitle(final String extendedTitle) {
		this.extendedTitle = extendedTitle;
	}

	public String getFormat() {
		try{return Html.fromHtml(format).toString();} catch (Exception e) {return format;}
	}

	public void setFormat(final String format) {
		this.format = format;
	}

	public String getFormatIcon() {
		return formatIcon;
	}

	public void setFormatIcon(final String formatIcon) {
		this.formatIcon = formatIcon;
	}

    public Integer getBookType() {
        return bookType;
    }

    public void setBookType(Integer bookType) {
        this.bookType = bookType;
    }
}
