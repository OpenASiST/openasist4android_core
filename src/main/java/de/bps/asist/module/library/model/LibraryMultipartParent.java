package de.bps.asist.module.library.model;

import android.text.Html;

/**
 * Created by litho on 29.10.14.
 * Library Model class for parent of multipart
 */
public class LibraryMultipartParent {

    private String id;
    private String title;
    private String titlePart;
    private String part;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitlePart() {
        try{return Html.fromHtml(titlePart).toString();} catch (Exception e) {return titlePart;}
        }

    public void setTitlePart(String titlePart) {
        this.titlePart = titlePart;
    }

    public String getPart() {
        try{return Html.fromHtml(part).toString();} catch (Exception e) {return part;}
    }

    public void setPart(String part) {
        this.part = part;
    }
}
