package de.bps.asist.module.library.model;

import android.text.Html;

import java.util.List;

import de.bps.asist.core.annotation.AsistItemConfiguration;

@AsistItemConfiguration(showDescription = false)
public class LibraryDetailsBook extends AbstractLibraryItem {
	private static final long serialVersionUID = -2536156687405435376L;

    private String title;

    private String publisher;

    private String format;

    private String imageUrl;

    private String pubDate;

    private String language;

    private String imageUrlThumb;

    private String edition;

    private List<String> buzzwords;

    private List<String> tags;

	private List<LibraryDetailsHoldings> holdings;

    private String signature;

    private List<String> notes;

    private List<String> sectors;

    private LibraryMultipartParent parent;

	public LibraryDetailsBook() {
		//
	}

	public String getPublisher() {
        try{return Html.fromHtml(publisher).toString();} catch (Exception e) {return publisher;}
	}

	public void setPublisher(final String publisher) {
		this.publisher = publisher;
	}

	public List<LibraryDetailsHoldings> getHoldings() {
		return holdings;
	}

	public void setHoldings(final List<LibraryDetailsHoldings> holdings) {
		this.holdings = holdings;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(final String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageUrlThumb() {
		return imageUrlThumb;
	}

	public void setImageUrlThumb(final String imageUrlThumb) {
		this.imageUrlThumb = imageUrlThumb;
	}

    public String getTitle() {
        try{return Html.fromHtml(title).toString();} catch (Exception e) {return title;}
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguage() {
        try{return Html.fromHtml(language).toString();} catch (Exception e) {return language;}
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getFormat() {
        try{return Html.fromHtml(format).toString();} catch (Exception e) {return format;}
        }

    public void setFormat(String format) {
        this.format = format;
    }


    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }


    public String getEdition() {
        try{return Html.fromHtml(edition).toString();} catch (Exception e) {return edition;}
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public List<String> getBuzzwords() {
        return buzzwords;
    }

    public void setBuzzwords(List<String> buzzwords) {
        this.buzzwords = buzzwords;
    }

    public String getSignature() {
        try{return Html.fromHtml(signature).toString();} catch (Exception e) {return signature;}
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public List<String> getSectors() {
        return sectors;
    }

    public void setSectors(List<String> sectors) {
        this.sectors = sectors;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }

    public LibraryMultipartParent getParent() {
        return parent;
    }

    public void setParent(LibraryMultipartParent parent) {
        this.parent = parent;
    }
}
