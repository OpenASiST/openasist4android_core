package de.bps.asist.module.library.details;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.manager.image.ASiSTImageManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.gui.detail.DetailTableAdapter;
import de.bps.asist.gui.detail.DetailedTableActivity;
import de.bps.asist.module.library.model.LibraryBook;
import de.bps.asist.module.library.model.LibraryDetailsBook;
import de.bps.asist.module.library.model.LibraryDetailsHoldings;

import static de.bps.asist.gui.list.AbstractGenericListCallback.EXTRA_ITEM_SELECTED;

/**
 * Created by litho on 29.10.14.
 * Details view for a library book
 */
public class LibraryDetailsActivity extends DetailedTableActivity<LibraryBook>{

    private static final String STATUS_AVAILABLE = "Verfügbar";

    private ImageView imageView;
    private ListView listView;
    private LibraryDetails details;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void doCreate() {
        setContentView(R.layout.library_details);
        listView = getView(R.id.bookDetails);

        imageView = getView(R.id.bookImage);
        LibraryBook selected = getSelectedItem();
        details = new LibraryDetails(selected);
        TextView titleView = getView(R.id.bookTitle);
        titleView.setText(details.getTitle());

        Map<String, List<ASiSTDetailDescription>> detailsMap = createDetailsMap(details);
        final DetailTableAdapter adapter = new DetailTableAdapter(this, detailsMap, null);
        listView.setAdapter(adapter);
        getDetails(details.getBookId());
    }

    private void getDetails(String bookId){
        StringBuilder sb = new StringBuilder();
        sb.append(translate(R.string.rootUrl)).append("/library/");
        sb.append(translate(R.string.institutionName));
        sb.append("/details/").append(bookId);
        ASiSTParser.getInstance().parseWithTask(sb.toString(), LibraryDetailsBook.class, new BookDetailParser());
    }

    private class BookDetailParser implements ParserCallback<LibraryDetailsBook> {

        @Override
        public void afterParse(LibraryDetailsBook bookDetails) {
            if(bookDetails != null) {
                ASiSTImageManager.getInstance().setImage(bookDetails.getImageUrl(), imageView);
                details.addDetails(bookDetails);
                Map<String, List<ASiSTDetailDescription>> detailsMap = createDetailsMap(details);
                final DetailTableAdapter adapter = new DetailTableAdapter(LibraryDetailsActivity.this, detailsMap, null);
                listView.setAdapter(adapter);
                TextView textView = getView(R.id.availableStatus);
                TextView actionView = getView(R.id.availableBranch);
                boolean hasAction = parseHoldings(textView, actionView);
                if(hasAction){
                    LinearLayout row = getView(R.id.availability_layout);
                    row.setOnClickListener(new HoldingsClickListener());
                }
            }
        }

        @Override
        public void onError(Exception e) {

        }

        @Override
        public void onManagedError(LibraryDetailsBook parsed) {
            //TODO
        }
    }

    private boolean parseHoldings(TextView textView, TextView actionView){
        List<LibraryDetailsHoldings> holdings = details.getHoldings();
        if(holdings != null){
            textView.setVisibility(View.VISIBLE);
            actionView.setVisibility(View.VISIBLE);
            boolean hasAction = false;
            for(LibraryDetailsHoldings holding: holdings){
                if(STATUS_AVAILABLE.equals(holding.getStatus())){
                    textView.setText(R.string.module_library_details_book_availability_available);
                    textView.setTextColor(getResources().getColor(R.color.green));
                    if(holding.getBranch() != null){
                        actionView.setText(holding.getBranch());
                    }
                    hasAction = true;
                    break;
                } else if(holding.getUrl() != null){
                    textView.setText("");
                    actionView.setText(R.string.library_available_online);
                    hasAction = true;
                }
            }
            return hasAction;

        } else {
            textView.setVisibility(View.GONE);
            actionView.setVisibility(View.GONE);
        }
        return false;
    }

    private class HoldingsClickListener implements View.OnClickListener {

            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(LibraryDetailsActivity.this, LibraryHoldingsActivity.class);
                final Bundle bundle = new Bundle();
                bundle.putSerializable(EXTRA_ITEM_SELECTED, details);
                intent.putExtra(EXTRA_ITEM_SELECTED, details);
                startActivity(intent);
            }
    }
}
