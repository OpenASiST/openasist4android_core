package de.bps.asist.module.library;

import android.content.Intent;
import android.os.Bundle;

import com.google.zxing.Result;

import de.bps.asist.module.AsistModuleActivity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static de.bps.asist.module.library.LibraryModule.BARCODE_RESULT;
import static de.bps.asist.module.library.LibraryModule.BARCODE_VALUE;

/**
 * Library scan barcode activity
 * 
 * @author laeb
 */
public class LibraryBarcodeActivity extends AsistModuleActivity<String> implements ZXingScannerView.ResultHandler {

    private static final String TAG = LibraryBarcodeActivity.class.getSimpleName();

    private ZXingScannerView mScannerView;
    private String barCode;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        barCode = rawResult.getText();
        ////Log.v(TAG, rawResult.getText()); // Prints scan results
        ////Log.v(TAG, rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
        Intent resultData = new Intent();
        resultData.putExtra(BARCODE_VALUE, barCode);
        setResult(BARCODE_RESULT, resultData);
        finish();
    }

    public String getBarCode() {
        return barCode;
    }
}
