package de.bps.asist.module.library.model;

import android.text.Html;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.annotation.AsistItemConfiguration;

@AsistItemConfiguration(showDescription = false)
public class LibraryDetailsHoldings extends AbstractLibraryItem {
	private static final long serialVersionUID = -6463586101737444599L;

    @ASiSTDetail(i18nKey = "module_library_details_availability_status", position = 1)
	private String status;

    @ASiSTDetail(i18nKey = "module_library_details_availability_type", position = 2)
	private String type;

    @ASiSTDetail(i18nKey = "module_library_details_availability_branch", position = 3)
	private String branch;

    @ASiSTDetail(i18nKey = "module_library_details_availability_shelf", position = 4)
	private String shelf;

    @ASiSTDetail(i18nKey = "module_library_details_availability_url", position = 4)
    private String url;

    @JsonProperty("desc")
    private String description;

    @ASiSTDetail(i18nKey = "module_library_details_availability_note", position = 5)
    private String note;


	LibraryDetailsHoldings() {
		//
	}

	public String getStatus() {
		try{return Html.fromHtml(status).toString();} catch (Exception e) {return status;}
		}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getBranch() {
		try{return Html.fromHtml(branch).toString();} catch (Exception e) {return branch;}
		}

	public void setBranch(final String branch) {
		this.branch = branch;
	}

	public String getShelf() {
		return shelf;
	}

	public void setShelf(final String shelf) {
		this.shelf = shelf;
	}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        try{return Html.fromHtml(description).toString();} catch (Exception e) {return description;}
		}

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
		try{return Html.fromHtml(note).toString();} catch (Exception e) {return note;}
		}

    public void setNote(String note) {
        this.note = note;
    }
}
