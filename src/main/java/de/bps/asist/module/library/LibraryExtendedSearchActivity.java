package de.bps.asist.module.library;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import de.bps.asist.R;

import static de.bps.asist.module.library.LibraryModule.EXTENDED_SEARCH_RESULT;

public class LibraryExtendedSearchActivity extends AbstractLibraryActivity {

    public static final String EXTRA_TITLE = "Title";
    public static final String EXTRA_AUTHOR = "Author";
    public static final String EXTRA_ISBN = "ISN";
    public static final String EXTRA_YEAR = "year";

	private EditText etTitle;
	private EditText etAuthor;
	private EditText etIsbn;
	private EditText etYear;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.module_library_extended_search_activity);

        etTitle = (EditText) findViewById(R.id.moduleLibraryExtendedSearchTitleInputField);
        etAuthor = (EditText) findViewById(R.id.moduleLibraryExtendedSearchAuthorInputField);
        etIsbn = (EditText) findViewById(R.id.moduleLibraryExtendedSearchIsbnInputField);
        etYear = (EditText) findViewById(R.id.moduleLibraryExtendedSearchYearInputField);

		//change icon color
		//final Drawable myIcon = getResources().getDrawable(R.drawable.ic_search);
		//final ColorFilter filter = new LightingColorFilter(Color.BLACK, Color.BLACK);
		//myIcon.setColorFilter(filter);
	}

	private void doSearch() {
        Intent resultData = new Intent();
        resultData.putExtra(EXTRA_TITLE, get(etTitle));
        resultData.putExtra(EXTRA_AUTHOR, get(etAuthor));
        resultData.putExtra(EXTRA_ISBN, get(etIsbn));
        resultData.putExtra(EXTRA_YEAR, get(etYear));
        setResult(EXTENDED_SEARCH_RESULT, resultData);
        finish();
	}

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.library_extended_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(R.id.action_library_extended_search == item.getItemId()){
            doSearch();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String get(final EditText field) {
		final Editable editable = field.getEditableText();
		final String string = editable != null ? editable.toString().trim() : null;
		return string;
	}
}
