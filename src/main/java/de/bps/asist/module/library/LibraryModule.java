/**
 * 
 */
package de.bps.asist.module.library;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;

/**
 * @author laeb
 */
public class LibraryModule extends AbstractAsistModule {

    public static final int EXTENDED_SEARCH_RESULT = 15879;
    public static final int BARCODE_RESULT = 11335;
    public static final String BARCODE_VALUE = "barcode-value";

	@Override
	public int getName() {
		return R.string.module_library_name;
	}

	@Override
	public int getIcon() {
		return R.drawable.ic_menu_library;
	}

	@Override
	public Fragment getInitialFragment() {
		return new LibraryFragment();
	}

	@Override
	public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
		return new ArrayList<>();
	}
}
