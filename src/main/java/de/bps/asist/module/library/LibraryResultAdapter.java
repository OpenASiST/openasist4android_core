package de.bps.asist.module.library;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collection;

import de.bps.asist.R;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.library.model.LibraryBook;

/**
 * Created by litho on 20.10.14.
 * Adapter for Library Result View
 */
public class LibraryResultAdapter extends GenericListViewAdapter<LibraryBook> {

    public LibraryResultAdapter(Context context, Collection<LibraryBook> objects, IGenericListCallback callback) {
        super(context, objects, callback);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView titleText = (TextView) view.findViewById(R.id.listItemTitle);
        Drawable borderLeft = getContext().getResources().getDrawable(R.drawable.border_left);
        ColorFilter filter = new LightingColorFilter(Color.WHITE, Color.GREEN);
        borderLeft.mutate().setColorFilter(filter);
        //noinspection deprecation
        titleText.setBackgroundDrawable(borderLeft);
        titleText.setPadding(10, 0, 0, 0);

        ImageView image = (ImageView) view.findViewById(R.id.listItemMainLogo);
        Drawable bookType = null;
        LibraryBook book = getItem(position);

        TextView descText = (TextView) view.findViewById(R.id.listItemDescription);
        final String author = book.getAuthor();
        final String year = book.getPubdate();
        if(author == null)
            descText.setText(year);
        else
            descText.setText(author + " | " + year);
        descText.setVisibility(View.VISIBLE);
        view.findViewById(R.id.listItemMainLogoWrapper).setVisibility(View.VISIBLE);
        switch (book.getBookType()){
            case 1:
                image.setImageResource(R.drawable.ic_media_book);
                image.setColorFilter(R.color.defaultColor, PorterDuff.Mode.MULTIPLY);
                break;
            case 2:
                image.setImageResource(R.drawable.ic_media_ebook);
                image.setColorFilter(R.color.defaultColor, PorterDuff.Mode.MULTIPLY);
                break;
            case 3:
                image.setImageResource(R.drawable.ic_media_magazine);
                image.setColorFilter(R.color.defaultColor, PorterDuff.Mode.MULTIPLY);
                break;
            case 4:
                image.setImageResource(R.drawable.ic_media_series);
                image.setColorFilter(R.color.defaultColor, PorterDuff.Mode.MULTIPLY);
                break;
            default:
                image.setImageResource(R.drawable.media_globe);
                image.setColorFilter(R.color.defaultColor, PorterDuff.Mode.MULTIPLY);
                break;
        }
        return view;
    }
}
