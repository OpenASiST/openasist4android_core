package de.bps.asist.module.survey;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.BarGraphView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewStyle;
import com.loopj.android.http.RequestParams;

import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.core.util.ASiSTPostSender;
import de.bps.asist.core.util.ResultHandler;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.survey.model.Survey;
import de.bps.asist.module.survey.model.SurveyDescription;
import de.bps.asist.module.survey.model.SurveyOption;
import de.bps.asist.module.survey.model.SurveyResult;
import de.bps.asist.module.survey.model.SurveyResultList;

/**
 * Created by litho on 05.12.14.
 * Evaluation of a survey
 */
public class SurveyEvaluationActivity extends AsistModuleActivity<SurveyDescription> {

    private static final String TAG = SurveyEvaluationActivity.class.getSimpleName();

    private SurveyDescription survey;
    private TextView emptyView;
    private GraphView graphView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.survey_evaluation);

        emptyView = getView(R.id.survey_empty_message);

        survey = getSelectedItem();
        updateData();
        drawGraph();
    }

    private void updateData(){
        StringBuilder sb = new StringBuilder();
        sb.append(translate(R.string.rootUrl));
        sb.append(SurveyModule.URL_SUB_PATH);
        sb.append(translate(R.string.institutionName));
        sb.append("/results/");
        sb.append(String.valueOf(survey.getSurveyCode()));
        ASiSTParser.getInstance().parseWithTask(sb.toString(), SurveyResultList.class, new ResultParserCallback());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(survey.isOwn() && !survey.isClosed()) {
            getMenuInflater().inflate(R.menu.survey_close_menu, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(R.id.action_close == item.getItemId()){
            closeSurvey();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void drawGraph(){

        Map<SurveyOption, List<SurveyResult>> results = survey.getResults();
        GraphView.GraphViewData[] allData = new GraphView.GraphViewData[survey.getOptions().size()];
        String[] labels = new String[survey.getOptions().size()];
        int count = 0;
        int maxCount = 0;
        for(SurveyOption option: survey.getOptions()){
            List<SurveyResult> resultList = results.get(option);
            int resultCount = resultList == null ? 0 : resultList.size();
            if(resultCount > maxCount){
                maxCount = resultCount;
            }
            GraphView.GraphViewData data = new GraphView.GraphViewData(count, resultCount);
            allData[count] = data;
            labels[count] = option.getTitle();
            count++;
        }

        LinearLayout layout = getView(R.id.survey_evaluation_graph);

        if(graphView != null){
            layout.removeView(graphView);
        }

        GraphViewSeries exampleSeries = new GraphViewSeries(allData);

        graphView = new BarGraphView(
                this // context
                , survey.getTitle()
        );
        graphView.addSeries(exampleSeries); // data

        String[] verticalLabels = new String[maxCount + 1];
        for(int i=0;i<=maxCount;i++){
            String value = String.valueOf(maxCount - i);
            verticalLabels[i] = value;
        }
        graphView.setVerticalLabels(verticalLabels);

        graphView.setHorizontalLabels(labels);

        graphView.setViewPort(0,4);
        graphView.setManualMinY(true);
        graphView.setManualYMinBound(0);

        graphView.setShowLegend(false);
        graphView.getGraphViewStyle().setTextSize(getResources().getDimension(R.dimen.descTextSize));
        graphView.getGraphViewStyle().setGridStyle(GraphViewStyle.GridStyle.NONE);


        layout.addView(graphView);

        if(labels.length == 0){
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(R.string.survey_no_results);
            layout.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            layout.setVisibility(View.VISIBLE);
        }
    }

    private class ResultParserCallback implements ParserCallback<SurveyResultList> {

        @Override
        public void afterParse(SurveyResultList parsed) {
            survey.setResults(parsed.getResults());
            drawGraph();
        }

        @Override
        public void onError(Exception e) {

        }

        @Override
        public void onManagedError(SurveyResultList parsed) {
            //TODO
        }
    }

    private void closeSurvey() {
            StringBuilder sb = new StringBuilder();
            sb.append(translate(R.string.rootUrl));
            sb.append(SurveyModule.URL_SUB_PATH);
            sb.append(translate(R.string.institutionName));
            sb.append("/close/").append(survey.getSurveyCode());

            RequestParams params = new RequestParams();
            params.put("deviceid", EnvironmentManager.getInstance().getDeviceToken(SurveyEvaluationActivity.this));
            ResultHandler result = ASiSTPostSender.postSync(sb.toString(), params);
            if (result.getResponseCode() != 200) {
                Toast.makeText(SurveyEvaluationActivity.this, translate(R.string.survey_close_error), Toast.LENGTH_LONG).show();
                //Log.e(TAG, "could not save attendance; Got response code " + result.getResponseCode());
            } else {
                Toast.makeText(SurveyEvaluationActivity.this, translate(R.string.survey_close_success), Toast.LENGTH_SHORT).show();
                DatabaseManager<Survey> surveyDB = new DatabaseManager<>(SurveyEvaluationActivity.this, Survey.class);
                Survey realSurvey = surveyDB.getObject("code", survey.getSurveyCode());
                realSurvey.setClosed(true);
                surveyDB.saveObject(realSurvey);
                finish();
            }
    }
}
