package de.bps.asist.module.survey;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import androidx.fragment.app.Fragment;
import de.bps.asist.BuildConfig;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.gui.list.OnRefreshFinishedListener;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.survey.model.Survey;
import de.bps.asist.module.survey.model.SurveyList;
import de.bps.asist.module.survey.model.SurveyOption;
import de.bps.asist.module.timetable.model.CourseDescription;

/**
 * Created by litho on 05.11.14.
 * Module for surveys
 */
public class SurveyModule extends AbstractAsistModule {

    public static final String PREFERENCES_NAME = "surveyPreferences";
    public static final String URL_SUB_PATH = "/survey/";

    private static final String TAG = SurveyModule.class.getSimpleName();
    private static final Lock lock = new ReentrantLock();

    public SurveyModule() {
        boolean debug = BuildConfig.DEBUG;
        //setEnabled(debug);
    }

    @Override
    public int getName() {
        return R.string.survey_name;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_feedback;
    }

    @Override
    public Fragment getInitialFragment() {
        return new SurveyFragment();
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        List<Class<? extends AbstractDatabaseObject>> result = new ArrayList<>();
        result.add(Survey.class);
        result.add(SurveyOption.class);
        return result;
    }

    @Override
    public void updateData(Context context) {
        super.updateData(context);
        updateSurveys(context, null, null);
    }

    public static void updateSurveys(Context context, OnRefreshFinishedListener listener, CourseDescription course) {
        if (context == null) {
            throw new IllegalArgumentException("context cannot be null");
        }
        StringBuilder url = new StringBuilder();
        url.append(context.getResources().getString(R.string.rootUrl));
        url.append(SurveyModule.URL_SUB_PATH).append(context.getResources().getString(R.string.institutionName));
        url.append("/surveys/");
        if(course == null){
            url.append("0");
        } else {
            url.append(String.valueOf(course.getCourse().getCourseid()));
        }
        url.append("/").append(EnvironmentManager.getInstance().getDeviceToken(context));
        ParserCallback<SurveyList> callback = new SurveyParser(context, listener);
        if(listener != null){
            ASiSTParser.getInstance().parseWithTask(url.toString(), SurveyList.class, callback);
        }else {
            ASiSTParser.getInstance().parse(url.toString(), SurveyList.class, callback);
        }
    }

    public static List<Survey> getAllSurveys(Context context) {
        List<Survey> result = new ArrayList<>();
        lock.lock();
        try {
            DatabaseManager<Survey> surveyDB = new DatabaseManager<>(context, Survey.class);
            result.addAll(surveyDB.getAll());
            surveyDB.release();
        } catch (Exception e) {
            //Log.e(TAG, "could not load surveys", e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    private static class SurveyParser implements ParserCallback<SurveyList> {

        private Context context;
        private OnRefreshFinishedListener listener;

        public SurveyParser(Context context, OnRefreshFinishedListener listener) {
            this.context = context;
            this.listener = listener;
        }

        @Override
        public void afterParse(SurveyList parsed) {
            if (parsed == null) {
                //Log.e(TAG, "could not parse surveys, result is null");
                return;
            }
            lock.lock();
            try {
                DatabaseManager<Survey> surveyDB = new DatabaseManager<>(context, Survey.class);
                DatabaseManager<SurveyOption> optionDB = new DatabaseManager<>(context, SurveyOption.class);
                surveyDB.deleteAll();
                optionDB.deleteAll();
                for (Survey survey : parsed.getSurveys()) {
                    surveyDB.saveObject(survey);
                    if(survey.getOptions() != null) {
                        for (SurveyOption option : survey.getOptions()) {
                            option.setSurvey(survey);
                            optionDB.saveObject(option);
                        }
                    }
                }
                if(listener != null){
                    listener.refreshFinished();
                }
            } catch (Exception e) {
                //Log.e(TAG, "could not update surveys", e);
            } finally {
                lock.unlock();
            }
        }

        @Override
        public void onError(Exception e) {
            if(listener != null){
                listener.refreshFinished();
            }
        }

        @Override
        public void onManagedError(SurveyList parsed) {
            //TODO
        }
    }
}
