package de.bps.asist.module.survey;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.FragmentActivity;
import de.bps.asist.R;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.util.ASiSTPostSender;
import de.bps.asist.core.util.ResultHandler;
import de.bps.asist.gui.dialog.ASiSTDialog;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.settings.SettingsFragment;
import de.bps.asist.module.settings.model.SettingsConfig;
import de.bps.asist.module.start.SettingsChangedCallback;
import de.bps.asist.module.survey.model.SurveyContext;
import de.bps.asist.module.survey.model.SurveyDescription;
import de.bps.asist.module.survey.model.SurveyOption;
import de.bps.asist.module.timetable.model.CourseDescription;

import static de.bps.asist.module.survey.model.SurveyContext.SurveyType;

/**
 * Created by litho on 27.11.14.
 * Activity for creating a new survey
 */
public class CreateSurveyActivity extends AsistModuleActivity<CourseDescription> implements SettingsChangedCallback {

    public static final int EDIT_SURVEY_CODE = 158;
    public static final String EXTRA_REQUEST_CODE = "requestCode";

    private static final String TAG = CreateSurveyActivity.class.getSimpleName();

    private SettingsFragment fragment;
    private Button submitButton;
    private CreateSurveySettingsConfig config;
    private SurveyDescription survey;
    private CourseDescription course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        course = getSelectedItem();

        setContentView(R.layout.create_survey);
        fragment = new SettingsFragment();
        List<SettingsConfig> configList = new ArrayList<>();
        config = new CreateSurveySettingsConfig(this);
        configList.add(config);
        fragment.setSettingsConfigList(configList);
        fragment.setPreferencesName(SurveyModule.PREFERENCES_NAME);
        getSupportFragmentManager().beginTransaction().replace(R.id.survey_settings, fragment).commit();
        submitButton = (Button) getView(R.id.create_survey_button);
        submitButton.setOnClickListener(new SendButtonListener());

        Button editButton = (Button) getView(R.id.create_survey_edit_button);
        editButton.setOnClickListener(new EditButtonListener());
    }

    @Override
    public void onValueChanged(FragmentActivity context, SharedPreferences preferences, String key) {
        if(CreateSurveySettingsConfig.KEY_SURVEY_TYPE.equals(key)){
            String value = preferences.getString(key, null);
            if(SurveyType.OPTIONS.toString().equals(value)){
                fragment.showSetting(CreateSurveySettingsConfig.KEY_OPTIONS_TYPE);
                fragment.showSetting(CreateSurveySettingsConfig.KEY_OPTIONS_COUNT);
            } else {
                fragment.hideSetting(CreateSurveySettingsConfig.KEY_OPTIONS_TYPE);
                fragment.hideSetting(CreateSurveySettingsConfig.KEY_OPTIONS_COUNT);
            }
        }
    }


    private class EditButtonListener implements View.OnClickListener {

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            survey = new SurveyDescription(config);
            final Intent intent = new Intent(CreateSurveyActivity.this, SurveyEditOptionsActivity.class);
            intent.putExtra(AbstractGenericListCallback.EXTRA_ITEM_SELECTED, survey);
            intent.putExtra(EXTRA_REQUEST_CODE, EDIT_SURVEY_CODE);
            startActivityForResult(intent, EDIT_SURVEY_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == EDIT_SURVEY_CODE && resultCode == EDIT_SURVEY_CODE){
            survey = (SurveyDescription) data.getSerializableExtra(SurveyEditOptionsActivity.DATA_SURVEY);
        }
    }

    private class SendButtonListener implements View.OnClickListener {

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            if(v == submitButton){
                if(survey == null){
                    survey = new SurveyDescription(config);
                }
                SurveyType surveyType = survey.getSurveyType();
                SurveyContext.SurveyOptionsType optionsType = survey.getSurveyOptionsType();
                SurveyContext.SurveySecurityType securityType = config.getSecurityType();
                int optionsCount = survey.getOptions().size();

                StringBuilder url;
                url = new StringBuilder();
                url.append(translate(R.string.rootUrl));
                url.append(SurveyModule.URL_SUB_PATH);
                url.append(translate(R.string.institutionName));
                url.append("/create/");
                if(course == null || securityType != SurveyContext.SurveySecurityType.WITHIN_COURSE) {
                    url.append("0");
                } else  {
                    url.append(course.getCourse().getCourseid());
                }
                RequestParams params = new RequestParams();
                params.put("surveytype", surveyType.getNumericValue());
                params.put("optionstype", optionsType.getNumericValue());
                params.put("optionscount", optionsCount);
                for(int i=0;i<survey.getOptions().size();i++){
                    SurveyOption option = survey.getOptions().get(i);
                    String name = "option" + (i+1);
                    params.put(name, option.getTitle());
                }

                String title = survey.getTitle();
                params.put("title", title);
                params.put("securitytype", securityType.getNumericValue());
                params.put("deviceid", EnvironmentManager.getInstance().getDeviceToken(CreateSurveyActivity.this));
                ResultHandler resultHandler = ASiSTPostSender.postSync(url.toString(), params);
                if(resultHandler.getResponseCode() == HttpStatus.SC_OK){
                    HttpEntity entity = resultHandler.getResponse().getEntity();
                    EntityReader reader = new EntityReader();
                    reader.execute(entity);
                }else{
                    //Log.e(TAG, "could not create survey, status code = "+resultHandler.getResponseCode());
                    Toast.makeText(CreateSurveyActivity.this, R.string.survey_create_error, Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    private class EntityReader extends AsyncTask<HttpEntity, Void, Void> {

        private String result;

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Void doInBackground(HttpEntity... params) {
            try {
                SurveyModule.updateSurveys(CreateSurveyActivity.this, null, null);
                result = parseContent(params[0]);
                String desc = translate(R.string.survey_result_code_text, result);
                ASiSTDialog dialog = new CodeDialog(translate(R.string.survey_result_code_title), desc);
                dialog.show(getSupportFragmentManager(), TAG);

            }catch(Exception e){
                //Log.e(TAG, "could not parse content", e);
            }
            return null;
        }

        private String parseContent(HttpEntity entity) throws IOException {
            InputStreamReader is = new InputStreamReader(entity.getContent());
            StringBuilder sb=new StringBuilder();
            BufferedReader br = new BufferedReader(is);
            String read = br.readLine();

            while(read != null) {
                sb.append(read);
                read =br.readLine();

            }
            return sb.toString();
        }

        public String getResult() {
            return result;
        }
    }

    private class CodeDialog extends ASiSTDialog {

        public CodeDialog(String title, String message){
            super(title, message);
            setEnableCancel(false);
        }

        @Override
        public void onOkClicked(DialogInterface dialog, int which) {
            CreateSurveyActivity.this.finish();
        }
    }
}
