package de.bps.asist.module.survey.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.parser.PubdateDeserializer;

/**
 * Created by litho on 24.11.14.
 * Model class for surveys
 */

@DatabaseTable
public class Survey extends AbstractDatabaseObject implements Comparable<Survey>{

    @DatabaseField
    private String title;

    @DatabaseField
    private String code;

    @DatabaseField
    private int type;

    @DatabaseField
    private int optionsCount;

    @DatabaseField
    private int optionsType;

    @ForeignCollectionField(eager = true)
    private Collection<SurveyOption> options = new ArrayList<>();

    @DatabaseField
    private int securityType;

    @DatabaseField
    private boolean own;

    @DatabaseField
    private boolean attended;

    @DatabaseField
    private Long courseId;

    @DatabaseField
    private boolean closed;

    @JsonDeserialize(using = PubdateDeserializer.class)
    @DatabaseField
    private Date creationDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOptionsCount() {
        return optionsCount;
    }

    public void setOptionsCount(int optionsCount) {
        this.optionsCount = optionsCount;
    }

    public Collection<SurveyOption> getOptions() {
        return options;
    }

    public void setOptions(List<SurveyOption> options) {
        this.options = options;
    }

    public int getSecurityType() {
        return securityType;
    }

    public void setSecurityType(int securityType) {
        this.securityType = securityType;
    }

    public boolean isAttended() {
        return attended;
    }

    public void setAttended(boolean attended) {
        this.attended = attended;
    }

    public int getOptionsType() {
        return optionsType;
    }

    public void setOptionsType(int optionsType) {
        this.optionsType = optionsType;
    }

    public void setOptions(Collection<SurveyOption> options) {
        this.options = options;
    }

    public boolean isOwn() {
        return own;
    }

    public void setOwn(boolean own) {
        this.own = own;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public int compareTo(Survey another) {
        if(creationDate != null && another.creationDate != null){
            return another.creationDate.compareTo(creationDate);
        }
        if(creationDate == null && another.creationDate == null){
            return 0;
        }
        if(another.creationDate == null){
            return -1;
        }
        return 1;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Survey)){
            return false;
        }
        Survey s = (Survey) o;
        return s.code == code;
    }
}
