package de.bps.asist.module.survey;

import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.util.ASiSTPostSender;
import de.bps.asist.core.util.ResultHandler;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.survey.model.Survey;
import de.bps.asist.module.survey.model.SurveyContext;
import de.bps.asist.module.survey.model.SurveyDescription;

/**
 * Created by litho on 10.12.14.
 * activity for answering a survey of type freeText or numeric
 */
public class SurveyAnswerFreeTextActivity extends AsistModuleActivity<SurveyDescription> {

    private static final String TAG = SurveyAnswerFreeTextActivity.class.getSimpleName();

    private SurveyDescription survey;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.survey_freetext);
        survey = getSelectedItem();


        TextView titleView = getView(R.id.survey_answer_title);
        titleView.setText(survey.getTitle());

        editText = getView(R.id.survey_answer_edit);
        if(survey.getSurveyType() == SurveyContext.SurveyType.NUMERIC) {
            editText.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_send) {
            send();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void send() {
        StringBuilder sb = new StringBuilder();
        sb.append(translate(R.string.rootUrl));
        sb.append(SurveyModule.URL_SUB_PATH);
        sb.append(translate(R.string.institutionName));
        sb.append("/attend/").append(survey.getSurveyCode());

        Editable title = editText.getText();

        RequestParams params = new RequestParams();
        params.put("text", title);
        params.put("deviceid", EnvironmentManager.getInstance().getDeviceToken(SurveyAnswerFreeTextActivity.this));
        ResultHandler result = ASiSTPostSender.postSync(sb.toString(), params);
        if (result.getResponseCode() != 200) {
            Toast.makeText(SurveyAnswerFreeTextActivity.this, translate(R.string.survey_create_error), Toast.LENGTH_LONG).show();
            //Log.e(TAG, "could not save attendance; Got response code " + result.getResponseCode());
        } else {
            Toast.makeText(SurveyAnswerFreeTextActivity.this, translate(R.string.dialog_ok), Toast.LENGTH_SHORT).show();
            DatabaseManager<Survey> surveyDB = new DatabaseManager<>(SurveyAnswerFreeTextActivity.this, Survey.class);
            Survey realSurvey = surveyDB.getObject("code", survey.getSurveyCode());
            realSurvey.setAttended(true);
            surveyDB.saveObject(realSurvey);
            finish();
        }
    }
}
