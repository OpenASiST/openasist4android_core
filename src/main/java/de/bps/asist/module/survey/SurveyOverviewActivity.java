package de.bps.asist.module.survey;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.fragment.app.FragmentManager;
import de.bps.asist.R;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.timetable.model.CourseDescription;

/**
 * Created by litho on 08.12.14.
 * activity for displaying the SurveyFragment
 */
public class SurveyOverviewActivity extends AsistModuleActivity<CourseDescription> {

    private SurveyFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generic_fragment_activity);
        fragment = new SurveyFragment();
        fragment.setCourse(getSelectedItem());
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.generic_fragment_container, fragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Integer layout = fragment.getMenuLayout();
        if(layout != null){
            getMenuInflater().inflate(layout, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(final MenuItem item) {
        if(fragment.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
