package de.bps.asist.module.survey;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.survey.model.SurveyDescription;
import de.bps.asist.module.survey.model.SurveyOption;

/**
 * Created by litho on 05.12.14.
 * View for Survey
 */
public class SurveyEditOptionsActivity extends AsistModuleActivity<SurveyDescription> {

    public static final String DATA_SURVEY = "data.survey";

    private SurveyDescription survey;
    private EditSurveyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.survey_view);
        ListView answerView = getView(R.id.survey_answer_list);
        survey = getSelectedItem();

        Map<String, List<SurveyOption>> map = new HashMap<>();
        map.put(survey.getTitle(), survey.getOptions());
        adapter = new EditSurveyAdapter(this, map);
        answerView.setFocusable(false);
        answerView.setAdapter(adapter);
        answerView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_send) {
            survey.setOptions(adapter.getOptions());
            survey.setTitle(adapter.getTitle());
            Intent data = new Intent();
            data.putExtra(DATA_SURVEY, survey);
            setResult(CreateSurveyActivity.EDIT_SURVEY_CODE, data);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
