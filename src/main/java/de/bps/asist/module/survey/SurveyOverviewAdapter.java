package de.bps.asist.module.survey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.survey.model.SurveyDescription;

/**
 * Created by litho on 05.12.14.
 * Adapter for survey overview
 */
public class SurveyOverviewAdapter extends CategoryListViewAdapter<SurveyDescription> {

    public SurveyOverviewAdapter(final Context context, final Map<String, List<SurveyDescription>> objects) {
        super(context, objects, null);
    }

    @Override
    protected LinearLayout addItemView(final int position, final LayoutInflater inflater) {
        final SurveyDescription item = (SurveyDescription) getItem(position);
        final LinearLayout row;
        row = (LinearLayout) inflater.inflate(R.layout.detail_list_item, null);

        final String title = item.getTitle();
        ((TextView) row.findViewById(R.id.detailItemTitle)).setText(title);

        final String value = item.getSurveyCode();
        ((TextView) row.findViewById(R.id.detailItemValue)).setText(value);
        addCallback(item, row);
        return row;
    }

    protected void addCallback(SurveyDescription item, View row) {
        Class<? extends AsistModuleActivity<SurveyDescription>> target;
        switch (item.getSurveyType()) {
            case OPTIONS:
                if(item.isAttended() && item.isOwn() || item.isClosed()){
                    target = SurveyEvaluationActivity.class;
                }else {
                    target = SurveyAnswerOptionsActivity.class;
                }
                break;
            case FREETEXT:
            case NUMERIC:
                if (item.isAttended() && item.isOwn() || item.isClosed()) {
                    target = SurveyFreetextActivity.class;
                } else {
                    target = SurveyAnswerFreeTextActivity.class;
                }
                break;
            default:
                target = null;
        }
        if (target != null) {
            AbstractGenericListCallback callback = new AbstractGenericListCallback(getContext(), target);
            super.addCallback(item, row, callback);
        }
    }
}
