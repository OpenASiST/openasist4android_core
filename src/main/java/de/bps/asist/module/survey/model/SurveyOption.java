package de.bps.asist.module.survey.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;

/**
 * Created by litho on 02.12.14.
 * model class for survey options
 */

@AsistItemConfiguration(useCallback = true)
@DatabaseTable
public class SurveyOption extends AbstractDatabaseObject {

    @AsistTitle
    @DatabaseField
    private String title;
    private int index;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Survey survey;

    public SurveyOption(){
        //
    }

    public SurveyOption(String value){
        title = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    @Override
    public String toString() {
        return title;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
