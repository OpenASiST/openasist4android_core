package de.bps.asist.module.survey;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.bps.asist.R;
import de.bps.asist.module.settings.model.ListSettingsItem;
import de.bps.asist.module.settings.model.NumberSettingsItem;
import de.bps.asist.module.settings.model.SettingsConfig;
import de.bps.asist.module.settings.model.SettingsSection;
import de.bps.asist.module.survey.model.SurveyContext;
import de.bps.asist.module.survey.model.SurveyOption;

import static de.bps.asist.module.survey.model.SurveyContext.SurveyOptionsType;
import static de.bps.asist.module.survey.model.SurveyContext.SurveySecurityType;
import static de.bps.asist.module.survey.model.SurveyContext.SurveyType;

/**
 * Created by litho on 02.12.14.
 * Settings config for create survey settings
 */
public class CreateSurveySettingsConfig extends SettingsConfig {

    public static final String PREFERENCE_KEY_COUNT = "survey_count";

    public static final String KEY_SURVEY_TYPE = "surveyType";
    public static final String KEY_OPTIONS_TYPE = "optionsType";
    public static final String KEY_SECURITY_TYPE = "securityType";
    public static final String KEY_OPTIONS_COUNT = "optionsCount";

    private static final int[] surveyTypes = {R.string.survey_type_options, R.string.survey_type_freetext, R.string.survey_type_numeric};
    private static final String[] surveyTypeKeys = {SurveyType.OPTIONS.toString(), SurveyType.FREETEXT.toString(), SurveyType.NUMERIC.toString()};

    private static final int[] optionsType = {R.string.survey_options_character, R.string.survey_options_numeric};
    private static final String[] optionsTypeKeys = {SurveyOptionsType.CHARACTER.toString(), SurveyOptionsType.NUMERIC.toString()};

    private static final int[] securityType = {R.string.survey_security_course, R.string.survey_security_private, R.string.survey_security_public};
    private static final String[] securityTypeKeys = {SurveySecurityType.WITHIN_COURSE.toString(), SurveySecurityType.PRIVATE.toString(), SurveySecurityType.PUBLIC.toString()};

    private final CreateSurveyActivity context;
    SharedPreferences preferences;
    private String title;

    public CreateSurveySettingsConfig(CreateSurveyActivity context){
        this.context = context;
        preferences = context.getSharedPreferences(SurveyModule.PREFERENCES_NAME, Context.MODE_PRIVATE);

        SettingsSection section = new SettingsSection(R.string.survey_create_button_title);
        ListSettingsItem typeItem = createTypeItem();
        section.addItem(typeItem);

        ListSettingsItem optionsItem = createOptionsItem();
        section.addItem(optionsItem);

        NumberSettingsItem countItem = createNumberOptionsItem();
        section.addItem(countItem);

        ListSettingsItem securityItem = createSecurityItem();
        section.addItem(securityItem);

        addSection(section);

        DateFormat df = new SimpleDateFormat("HH:mm");
        title = context.getResources().getString(R.string.survey_create_title, df.format(new Date()));
    }

    private ListSettingsItem createTypeItem() {
        ListSettingsItem typeItem = new ListSettingsItem(R.string.survey_type_title);
        typeItem.setEntryValues(surveyTypes);
        typeItem.setKeys(surveyTypeKeys);
        typeItem.setDefaultValue(surveyTypeKeys[0]);
        typeItem.setKey(KEY_SURVEY_TYPE);
        typeItem.setCallback(context);
        return typeItem;
    }

    private NumberSettingsItem createNumberOptionsItem(){
        NumberSettingsItem item = new NumberSettingsItem(R.string.survey_options_count);
        item.setKey(KEY_OPTIONS_COUNT);
        item.setDefaultValue(4);
        item.setCallback(context);
        return item;
    }

    private ListSettingsItem createOptionsItem() {
        ListSettingsItem typeItem = new ListSettingsItem(R.string.survey_options_title);
        typeItem.setEntryValues(optionsType);
        typeItem.setKeys(optionsTypeKeys);
        typeItem.setDefaultValue(optionsTypeKeys[0]);
        typeItem.setKey(KEY_OPTIONS_TYPE);
        typeItem.setCallback(context);
        return typeItem;
    }

    private ListSettingsItem createSecurityItem() {
        ListSettingsItem typeItem = new ListSettingsItem(R.string.survey_security_title);
        typeItem.setEntryValues(securityType);
        typeItem.setKeys(securityTypeKeys);
        typeItem.setDefaultValue(securityTypeKeys[0]);
        typeItem.setKey(KEY_SECURITY_TYPE);
        typeItem.setCallback(context);
        return typeItem;
    }

    private NumberSettingsItem createCountOptionsItem(){
        NumberSettingsItem item = new NumberSettingsItem(R.string.survey_options_count);
        item.setKey(PREFERENCE_KEY_COUNT);
        return item;
    }

    public SurveyType getSurveyType(){
        String value = preferences.getString(KEY_SURVEY_TYPE, surveyTypeKeys[0]);
        SurveyType result = SurveyType.valueOf(value);
        return result;
    }

    public SurveyOptionsType getOptionsType(){
        String value = preferences.getString(KEY_OPTIONS_TYPE, optionsTypeKeys[0]);
        SurveyOptionsType optionsType = SurveyOptionsType.valueOf(value);
        return optionsType;
    }

    public int getOptionsCount(){
        Integer value = preferences.getInt(KEY_OPTIONS_COUNT, 4);
        return value;
    }

    public SurveySecurityType getSecurityType(){
        String value = preferences.getString(KEY_SECURITY_TYPE, securityTypeKeys[0]);
        SurveySecurityType result = SurveySecurityType.valueOf(value);
        return result;
    }

    public List<SurveyOption> getOptionsList(){
        List<SurveyOption> result = new ArrayList<>();
        SurveyOptionsType optionsType = getOptionsType();

        for(int i=1;i <= getOptionsCount();i++){
            String value = createOption(optionsType, i);
            SurveyOption option = new SurveyOption(value);
            result.add(option);
        }
        return result;
    }

    public List<String> getOptions(){
        List<String> result = new ArrayList<>();

        SurveyOptionsType optionsType = getOptionsType();

        for(int i=1;i <= getOptionsCount();i++){
            result.add(createOption(optionsType, i));
        }
        return result;
    }

    private String createOption(SurveyContext.SurveyOptionsType optionsType, int counter){
        switch (optionsType){
            case CHARACTER:
                char c = (char) (64 + counter);
                return "" + c;
            case NUMERIC:
                return String.valueOf(counter);
            default:
                return "";
        }
    }

    public String getTitle() {
        return title;
    }
}
