package de.bps.asist.module.survey.model;

import java.util.List;

import de.bps.asist.core.model.IList;

/**
 * Created by litho on 05.12.14.
 * Model Object that holds the results
 */
public class SurveyResultList implements IList{

    private List<SurveyResult> results;

    public List<SurveyResult> getResults() {
        return results;
    }

    public void setResults(List<SurveyResult> results) {
        this.results = results;
    }

    @Override
    public List<SurveyResult> getItems() {
        return results;
    }
}
