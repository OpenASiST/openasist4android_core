package de.bps.asist.module.survey;

import android.content.Context;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.annotation.AnnotationHelper;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.annotation.AsistTitlePrefixKey;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.survey.model.SurveyOption;

/**
 * Created by litho on 12.12.14.
 * Adapter for survey edit view
 */
public class EditSurveyAdapter extends CategoryListViewAdapter<SurveyOption> {

    private EditText titleEdit;
    private Map<String, EditText> optionEdits = new HashMap<>();

    public EditSurveyAdapter(Context context, Map<String, List<SurveyOption>> objects) {
        super(context, objects, null);
        setRowLayout(R.layout.survey_edit_list_item);
        setHeaderLayout(R.layout.survey_edit_header);
    }

    @Override
    protected void addDescription(SurveyOption item, View row) {
        // do nothing
    }

    protected void addTitle(final SurveyOption item, final View row) {
        // title
        final Integer titlePrefixKey = AnnotationHelper.getValueOf(item, AsistTitlePrefixKey.class);
        String titlePrefix = "";
        if (titlePrefixKey != null) {
            titlePrefix = translate(titlePrefixKey);
            titlePrefix += " ";
        }
        final String title = titlePrefix + String.valueOf(AnnotationHelper.getValueOf(item,AsistTitle.class));
        EditText titleView = (EditText) row.findViewById(R.id.survey_edit);
        titleView.setText(title);
        optionEdits.put(title, titleView);
    }

    protected void addHeaderView(View row, String header){
        titleEdit = getView(row, R.id.edit_headerTitle);
        titleEdit.setText(header);
    }

    public List<SurveyOption> getOptions(){
        List<SurveyOption> result = new ArrayList<>();
        for(EditText edit: optionEdits.values()){
            String text = edit.getText().toString();
            SurveyOption option = new SurveyOption(text);
            result.add(option);
        }
        return result;
    }

    public String getTitle(){
        return titleEdit.getText().toString();
    }
}
