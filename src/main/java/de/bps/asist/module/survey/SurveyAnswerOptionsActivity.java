package de.bps.asist.module.survey;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.util.ASiSTPostSender;
import de.bps.asist.core.util.ResultHandler;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.survey.model.Survey;
import de.bps.asist.module.survey.model.SurveyDescription;
import de.bps.asist.module.survey.model.SurveyOption;

/**
 * Created by litho on 05.12.14.
 * View for Survey
 */
public class SurveyAnswerOptionsActivity extends AsistModuleActivity<SurveyDescription> {

    private static final String TAG = SurveyAnswerOptionsActivity.class.getSimpleName();

    private SurveyDescription survey;
    private boolean editMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        int requestCode = intent.getIntExtra(CreateSurveyActivity.EXTRA_REQUEST_CODE, -1);
        if (requestCode > 0) {
            editMode = true;
        }

        setContentView(R.layout.survey_view);
        ListView answerView = getView(R.id.survey_answer_list);
        survey = getSelectedItem();

        TextView attendedView = getView(R.id.survey_answer_attended);
        if (survey.isAttended()) {
            answerView.setVisibility(View.GONE);
            attendedView.setVisibility(View.VISIBLE);
            attendedView.setText(translate(R.string.survey_already_attended));
        } else {
            Map<String, List<SurveyOption>> map = new HashMap<>();
            map.put(survey.getTitle(), survey.getOptions());
            CategoryListViewAdapter<SurveyOption> adapter;
            if (requestCode > 0) {
                adapter = new EditSurveyAdapter(this, map);
                answerView.setFocusable(false);
            } else {
                adapter = new CategoryListViewAdapter<>(this, map, new AttendSurvey());
            }
            answerView.setAdapter(adapter);
            answerView.setVisibility(View.VISIBLE);
            attendedView.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (survey.isOwn() && !editMode) {
            getMenuInflater().inflate(R.menu.generic_statistic, menu);
            return true;
        } else if(editMode){
            getMenuInflater().inflate(R.menu.send_menu, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.generic_statistic) {
            forwardWithObject(SurveyEvaluationActivity.class, survey);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class AttendSurvey implements IGenericListCallback<SurveyOption> {

        @Override
        public void doAction(SurveyOption surveyOption) {
            StringBuilder sb = new StringBuilder();
            sb.append(translate(R.string.rootUrl));
            sb.append(SurveyModule.URL_SUB_PATH);
            sb.append(translate(R.string.institutionName));
            sb.append("/attend/").append(survey.getSurveyCode());

            RequestParams params = new RequestParams();
            params.put("text", surveyOption.getTitle());
            params.put("deviceid", EnvironmentManager.getInstance().getDeviceToken(SurveyAnswerOptionsActivity.this));
            ResultHandler result = ASiSTPostSender.postSync(sb.toString(), params);
            if (result.getResponseCode() != 200) {
                Toast.makeText(SurveyAnswerOptionsActivity.this, translate(R.string.survey_create_error), Toast.LENGTH_LONG).show();
                //Log.e(TAG, "could not save attendance; Got response code " + result.getResponseCode());
            } else {
                Toast.makeText(SurveyAnswerOptionsActivity.this, translate(R.string.survey_attend_success), Toast.LENGTH_SHORT).show();
                DatabaseManager<Survey> surveyDB = new DatabaseManager<>(SurveyAnswerOptionsActivity.this, Survey.class);
                surveyOption.getSurvey().setAttended(true);
                surveyDB.saveObject(surveyOption.getSurvey());
                finish();
            }
        }
    }
}
