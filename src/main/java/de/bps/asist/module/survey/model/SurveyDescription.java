package de.bps.asist.module.survey.model;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.module.survey.CreateSurveySettingsConfig;

import static de.bps.asist.module.survey.model.SurveyContext.SurveyOptionsType;
import static de.bps.asist.module.survey.model.SurveyContext.SurveyType;

/**
 * Created by litho on 02.12.14.
 * Model class for survey items. Contains more detailed information than the survey class
 */

@AsistItemConfiguration(useCallback = true)
public class SurveyDescription implements Serializable {

    @AsistTitle
    private String title;

    private int courseCode = 0;

    private SurveyType surveyType;

    private SurveyOptionsType surveyOptionsType;

    private List<SurveyOption> options;

    private boolean isOwn;

    private boolean attended;

    private boolean closed;

    private Map<SurveyOption, List<SurveyResult>> results = new HashMap<>();

    private Map<String, SurveyOption> optionMap = new HashMap<>();

    @AsistDescription
    private String surveyCode;

    public SurveyDescription(Survey survey, Context context){
        this.title = survey.getTitle();
        surveyType = SurveyContext.SurveyType.valueOf(survey.getType());
        surveyOptionsType = SurveyContext.SurveyOptionsType.valueOf(survey.getOptionsType());
        this.attended = survey.isAttended();
        setOptions(survey.getOptions());
        isOwn = survey.isOwn();
        closed = survey.isClosed();
        surveyCode = survey.getCode();
    }

    public SurveyDescription(CreateSurveySettingsConfig config){
        this.title = config.getTitle();
        surveyType = config.getSurveyType();
        surveyOptionsType = config.getOptionsType();
        setOptions(config.getOptionsList());
        attended = false;
        isOwn = true;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SurveyContext.SurveyType getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(SurveyContext.SurveyType surveyType) {
        this.surveyType = surveyType;
    }

    public SurveyContext.SurveyOptionsType getSurveyOptionsType() {
        return surveyOptionsType;
    }

    public void setSurveyOptionsType(SurveyContext.SurveyOptionsType surveyOptionsType) {
        this.surveyOptionsType = surveyOptionsType;
    }

    public String getSurveyCode() {
        return surveyCode;
    }

    public void setSurveyCode(String surveyCode) {
        this.surveyCode = surveyCode;
    }

    public List<SurveyOption> getOptions() {
        return options;
    }

    public void setOptions(Collection<SurveyOption> options) {
        this.options = new ArrayList<>(options);
        optionMap.clear();
        for(SurveyOption option: options){
            optionMap.put(option.getTitle(), option);
        }
    }

    public Map<SurveyOption, List<SurveyResult>> getResults() {
        return results;
    }

    public void setResults(List<SurveyResult> results) {
        switch (surveyType){
            case OPTIONS:
                switch (surveyOptionsType){
                    case CHARACTER:
                        createTextMap(results);
                        break;
                    case NUMERIC:
                        createIntegerMap(results);
                        break;
                    default:
                        throw new IllegalArgumentException("surveyOptionsType is valid: "+surveyOptionsType);
                }
                break;
            case NUMERIC:
                createNumericText(results);
                break;
            case FREETEXT:
                createFreeText(results);
                break;
            default:
                throw new IllegalArgumentException("surveyType is not valid: "+surveyType);

        }
    }

    private void createFreeText(List<SurveyResult> results){
        for(SurveyResult result: results){
            String title = result.getText();
            SurveyOption option = optionMap.get(title);
            if(option == null){
                option = new SurveyOption();
                option.setTitle(result.getText());
                optionMap.put(result.getText(), option);
            }
            List<SurveyResult> resultList = this.results.get(option);
            if(resultList == null){
                resultList = new ArrayList<>();
                this.results.put(option, resultList);
            }
            resultList.add(result);
        }
    }

    private void createNumericText(List<SurveyResult> results){
        for(SurveyResult result: results){
            String title = String.valueOf(result.getIntValue());
            SurveyOption option = optionMap.get(title);
            if(option == null){
                option = new SurveyOption();
                option.setTitle(result.getText());
                optionMap.put(result.getText(), option);
            }
            List<SurveyResult> resultList = this.results.get(option);
            if(resultList == null){
                resultList = new ArrayList<>();
                this.results.put(option, resultList);
            }
            resultList.add(result);
        }
    }

    private void createIntegerMap(List<SurveyResult> results){
        for(SurveyResult result: results){
            String title = String.valueOf(result.getIntValue());
            SurveyOption option = optionMap.get(title);
            if(option == null){
                throw new IllegalArgumentException("could not find survey option for title "+title);
            }
            List<SurveyResult> resultList = this.results.get(option);
            if(resultList == null){
                resultList = new ArrayList<>();
                this.results.put(option, resultList);
            }
            resultList.add(result);
        }
    }

    private void createTextMap(List<SurveyResult> currentResults){
        for(SurveyResult result: currentResults){
            String title = result.getText();
            SurveyOption option = optionMap.get(title);
            if(option == null){
                throw new IllegalArgumentException("could not find survey option for title "+title);
            }
            List<SurveyResult> resultList = this.results.get(option);
            if(resultList == null){
                resultList = new ArrayList<>();
                this.results.put(option, resultList);
            }
            resultList.add(result);
        }
    }

    public int getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(int courseCode) {
        this.courseCode = courseCode;
    }

    public boolean isOwn() {
        return isOwn;
    }

    public void setOwn(boolean isOwn) {
        this.isOwn = isOwn;
    }

    public boolean isAttended() {
        return attended;
    }

    public void setAttended(boolean attended) {
        this.attended = attended;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }
}
