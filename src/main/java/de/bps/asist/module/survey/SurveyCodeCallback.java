package de.bps.asist.module.survey;

import android.content.Context;
import android.content.DialogInterface;

import de.bps.asist.R;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.gui.code.CodeInputCallback;
import de.bps.asist.gui.code.GenericAddActivity;
import de.bps.asist.gui.dialog.ASiSTDialog;
import de.bps.asist.module.survey.model.Survey;
import de.bps.asist.module.survey.model.SurveyDescription;
import de.bps.asist.module.survey.model.SurveyList;

/**
 * Created by litho on 10.12.14
 * Callback for when survey code was entered.
 */
public class SurveyCodeCallback implements CodeInputCallback {

    @Override
    public void onCodeInputFinished(GenericAddActivity activity, String code) {
        StringBuilder url = new StringBuilder();
        url.append(translate(R.string.rootUrl, activity));
        url.append(SurveyModule.URL_SUB_PATH);
        url.append(translate(R.string.institutionName, activity));
        url.append("/single/");
        url.append(code).append("/").append(EnvironmentManager.getInstance().getDeviceToken(activity));
        SurveyParserCallback callback = new SurveyParserCallback(activity);
        ASiSTParser.getInstance().parseWithTask(url.toString(), SurveyList.class, callback);
    }


    private class SurveyParserCallback implements ParserCallback<SurveyList> {

        private GenericAddActivity activity;

        public SurveyParserCallback(GenericAddActivity activity){
            this.activity = activity;
        }

        @Override
        public void afterParse(SurveyList parsed) {
            if(parsed.getItems().size() > 0){
                Survey survey = parsed.getItems().get(0);
                SurveyDescription description = new SurveyDescription(survey, activity);
                activity.forwardWithObject(SurveyAnswerOptionsActivity.class, description);
            } else {
                onError(null);
            }
        }

        @Override
        public void onError(Exception e) {
            ASiSTDialog dialog = new ASiSTDialog(translate(R.string.error, activity), translate(R.string.survey_code_not_found, activity), true, false);
            dialog.show(activity.getSupportFragmentManager(), "");
        }

        @Override
        public void onManagedError(SurveyList parsed) {
            //TODO
        }
    }

    private String translate(int id, Context context){
        return context.getResources().getString(id);
    }
}
