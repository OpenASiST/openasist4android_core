package de.bps.asist.module.survey;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.module.survey.model.Survey;
import de.bps.asist.module.survey.model.SurveyContext;
import de.bps.asist.module.survey.model.SurveyDescription;
import de.bps.asist.module.survey.model.SurveyList;
import de.bps.asist.module.timetable.model.CourseDescription;

/**
 * Created by litho on 24.11.14.
 * Manager class for interacting with surveys
 */
public class SurveyManager {

    private static final String TAG = SurveyManager.class.getSimpleName();

    private Long courseId;
    private final Context context;

    private List<Survey> surveys = new ArrayList<>();

    public SurveyManager(Context context) {
        this(context, null);
    }

    public SurveyManager(Context context, CourseDescription course) {
        this.context = context;
        if (course != null) {
            this.courseId = course.getCourse().getCourseid();
        }
        surveys = SurveyModule.getAllSurveys(context);
        Collections.sort(surveys);
    }

    public List<SurveyDescription> getSurveysOfCourse() {
        List<SurveyDescription> result = new ArrayList<>();
        for (Survey survey : surveys) {
            Long surveyCourseId = survey.getCourseId();
            if (courseId != null && surveyCourseId != null && !survey.isClosed()) {
                if (survey.getSecurityType() == SurveyContext.SurveySecurityType.WITHIN_COURSE.getNumericValue() && surveyCourseId == courseId) {
                    SurveyDescription desc = new SurveyDescription(survey, context);
                    result.add(desc);
                }
            }
        }
        return result;
    }

    public List<SurveyDescription> getSurveysOfSecurityType(SurveyContext.SurveySecurityType type) {
        List<SurveyDescription> result = new ArrayList<>();
        for (Survey survey : surveys) {
            if (survey.getSecurityType() == type.getNumericValue() && !survey.isAttended() && !survey.isClosed()) {
                SurveyDescription desc = new SurveyDescription(survey, context);
                result.add(desc);
            }
        }
        return result;
    }

    public List<SurveyDescription> findAttendedSurveys() {
        List<SurveyDescription> result = new ArrayList<>();
        for (Survey survey : surveys) {
            if (survey.isAttended() && !survey.isClosed()) {
                SurveyDescription desc = new SurveyDescription(survey, context);
                result.add(desc);
            }
        }
        return result;
    }

    public List<SurveyDescription> findClosedSurveys(){
        List<SurveyDescription> result = new ArrayList<>();
        for (Survey survey : surveys) {
            if (survey.isClosed()) {
                SurveyDescription desc = new SurveyDescription(survey, context);
                result.add(desc);
            }
        }
        return result;
    }

    private class CourseSurveyCallback implements ParserCallback<SurveyList> {


        @Override
        public void afterParse(SurveyList parsed) {

        }

        @Override
        public void onError(Exception e) {

        }

        @Override
        public void onManagedError(SurveyList parsed) {
            //TODO
        }
    }
}
