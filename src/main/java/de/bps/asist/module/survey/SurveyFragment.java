package de.bps.asist.module.survey;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.bps.asist.R;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.code.GenericAddActivity;
import de.bps.asist.gui.list.OnRefreshFinishedListener;
import de.bps.asist.module.survey.model.SurveyContext;
import de.bps.asist.module.survey.model.SurveyDescription;
import de.bps.asist.module.timetable.TimetableModule;
import de.bps.asist.module.timetable.model.CourseDescription;

/**
 * Created by litho on 05.11.14.
 * Initial Fragment for surveys
 */
public class SurveyFragment extends AbstractASiSTFragment implements SwipeRefreshLayout.OnRefreshListener, OnRefreshFinishedListener, AdapterView.OnItemSelectedListener  {

    private static final String TAG = SurveyFragment.class.getSimpleName();

    private SwipeRefreshLayout swipeLayout;
    private SurveyManager manager;
    private ListView listView;
    private CourseDescription course;
    private SurveyOverviewAdapter adapter;
    private SurveyCourseChooseAdapter courseChooseAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.survey_refresh_listview, null);
        swipeLayout = getView(view, R.id.generic_list_swipe);
        swipeLayout.setOnRefreshListener(this);
        listView = getView(view, R.id.generic_list);
        manager = new SurveyManager(getActivity(), course);
        Map<String, List<SurveyDescription>> map = createMap();
        adapter = new SurveyOverviewAdapter(getActivity(), map);
        listView.setAdapter(adapter);
        Spinner spinner = getView(view, R.id.survey_course_spinner);
        if(TimetableModule.findCurrentCourses(getActivity()).size() > 0){
            courseChooseAdapter = new SurveyCourseChooseAdapter(getActivity());
            spinner.setAdapter(courseChooseAdapter);
            spinner.setOnItemSelectedListener(this);
        } else {
            spinner.setVisibility(View.GONE);
        }

        return view;
    }

    private Map<String, List<SurveyDescription>> createMap(){
        Map<String, List<SurveyDescription>> result = new LinkedHashMap<>();
        List<SurveyDescription> inCourseList = manager.getSurveysOfSecurityType(SurveyContext.SurveySecurityType.WITHIN_COURSE);
        List<SurveyDescription> globalList = manager.getSurveysOfSecurityType(SurveyContext.SurveySecurityType.PUBLIC);
        globalList.addAll(manager.getSurveysOfSecurityType(SurveyContext.SurveySecurityType.PRIVATE));
        List<SurveyDescription> attendedList = manager.findAttendedSurveys();
        List<SurveyDescription> closedList = manager.findClosedSurveys();

        result.put(translate(R.string.surveys_in_course), inCourseList);
        result.put(translate(R.string.surveys_global), globalList);
        result.put(translate(R.string.surveys_attended), attendedList);
        result.put(translate(R.string.surveys_closed), closedList);
        return result;
    }

    @Override
    public Integer getMenuLayout() {
        return R.menu.survey_start;
    }

    public boolean onOptionsItemSelected(final MenuItem item) {
        if(R.id.action_survey_add == item.getItemId()){
            forwardWithObject(CreateSurveyActivity.class, course);
            return true;
        } else if(R.id.action_survey_code == item.getItemId()){
            SurveyCodeCallback callback = new SurveyCodeCallback();
            forwardWithObject(GenericAddActivity.class, callback);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void refreshFinished() {
        manager = new SurveyManager(getActivity(), course);
        Map<String, List<SurveyDescription>> map = createMap();
        adapter = new SurveyOverviewAdapter(getActivity(), map);
        listView.setAdapter(adapter);
        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        swipeLayout.setRefreshing(true);
        SurveyModule.updateSurveys(getActivity(), this, course);
    }

    public CourseDescription getCourse() {
        return course;
    }

    public void setCourse(CourseDescription course) {
        this.course = course;
    }

    /**
     * <p>Callback method to be invoked when an item in this view has been
     * selected. This callback is invoked only when the newly selected
     * position is different from the previously selected position or if
     * there was no selected item.</p>
     * <p/>
     * Impelmenters can call getItemAtPosition(position) if they need to access the
     * data associated with the selected item.
     *
     * @param parent   The AdapterView where the selection happened
     * @param view     The view within the AdapterView that was clicked
     * @param position The position of the view in the adapter
     * @param id       The row id of the item that is selected
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(courseChooseAdapter != null) {
            //Log.d(TAG, "selected position " + position);
            course = courseChooseAdapter.getItem(position);
            onRefresh();
        }
    }

    /**
     * Callback method to be invoked when the selection disappears from this
     * view. The selection can disappear for instance when touch is activated
     * or when the adapter becomes empty.
     *
     * @param parent The AdapterView that now contains no selected item.
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
