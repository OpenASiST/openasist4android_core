package de.bps.asist.module.survey.model;

import java.util.List;

import de.bps.asist.core.model.IList;

/**
 * Created by litho on 24.11.14.
 * Model class for survey root element
 */
public class SurveyList implements IList {

    private List<Survey> surveys;

    public List<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        this.surveys = surveys;
    }

    @Override
    public List<Survey> getItems() {
        return surveys;
    }
}
