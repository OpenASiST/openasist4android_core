package de.bps.asist.module.survey;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.core.util.ASiSTPostSender;
import de.bps.asist.core.util.ResultHandler;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.survey.model.Survey;
import de.bps.asist.module.survey.model.SurveyDescription;
import de.bps.asist.module.survey.model.SurveyFreeTextDescription;
import de.bps.asist.module.survey.model.SurveyOption;
import de.bps.asist.module.survey.model.SurveyResult;
import de.bps.asist.module.survey.model.SurveyResultList;

/**
 * Created by litho on 10.12.14.
 * activity for freetext surveys
 */
public class SurveyFreetextActivity extends AsistModuleActivity<SurveyDescription> {

    private static final String TAG = SurveyFreetextActivity.class.getSimpleName();

    private SurveyDescription survey;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.generic_list_view);

        listView = getView(R.id.generic_list);

        survey = getSelectedItem();

        updateData();
    }

    private void updateView(){
        Map<SurveyOption, List<SurveyResult>> results = survey.getResults();
        List<SurveyFreeTextDescription> list = new ArrayList<>();
        for(SurveyOption option: results.keySet()){
            List<SurveyResult> allResults = results.get(option);
            SurveyFreeTextDescription description = new SurveyFreeTextDescription(option.getTitle(), allResults.size());
            list.add(description);
        }

        GenericListViewAdapter<SurveyFreeTextDescription> adapter = new GenericListViewAdapter<>(this, list, null);
        listView.setAdapter(adapter);
    }

    private void updateData(){
        StringBuilder sb = new StringBuilder();
        sb.append(translate(R.string.rootUrl));
        sb.append(SurveyModule.URL_SUB_PATH);
        sb.append(translate(R.string.institutionName));
        sb.append("/results/");
        sb.append(String.valueOf(survey.getSurveyCode()));
        ASiSTParser.getInstance().parseWithTask(sb.toString(), SurveyResultList.class, new ResultParserCallback());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(survey.isOwn() && !survey.isClosed()) {
            getMenuInflater().inflate(R.menu.survey_close_menu, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(R.id.action_close == item.getItemId()){
            closeSurvey();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class ResultParserCallback implements ParserCallback<SurveyResultList> {

        @Override
        public void afterParse(SurveyResultList parsed) {
            survey.setResults(parsed.getResults());
            updateView();
        }

        @Override
        public void onError(Exception e) {

        }

        @Override
        public void onManagedError(SurveyResultList parsed) {
            //TODO
        }
    }

    private void closeSurvey() {
        StringBuilder sb = new StringBuilder();
        sb.append(translate(R.string.rootUrl));
        sb.append(SurveyModule.URL_SUB_PATH);
        sb.append(translate(R.string.institutionName));
        sb.append("/close/").append(survey.getSurveyCode());

        RequestParams params = new RequestParams();
        params.put("deviceid", EnvironmentManager.getInstance().getDeviceToken(this));
        ResultHandler result = ASiSTPostSender.postSync(sb.toString(), params);
        if (result.getResponseCode() != 200) {
            Toast.makeText(this, translate(R.string.survey_close_error), Toast.LENGTH_LONG).show();
            //Log.e(TAG, "could not save attendance; Got response code " + result.getResponseCode());
        } else {
            Toast.makeText(this, translate(R.string.survey_close_success), Toast.LENGTH_SHORT).show();
            DatabaseManager<Survey> surveyDB = new DatabaseManager<>(this, Survey.class);
            Survey realSurvey = surveyDB.getObject("code", survey.getSurveyCode());
            realSurvey.setClosed(true);
            surveyDB.saveObject(realSurvey);
            finish();
        }
    }
}
