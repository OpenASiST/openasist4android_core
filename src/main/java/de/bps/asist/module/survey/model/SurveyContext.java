/**
 * 
 * BPS Bildungsportal Sachsen GmbH<br>
 * Bahnhofstrasse 6<br>
 * 09111 Chemnitz<br>
 * Germany<br>
 * 
 * Copyright (c) 2005-2014 by BPS Bildungsportal Sachsen GmbH<br>
 * http://www.bps-system.de<br>
 * 
 * All rights reserved.
 */
package de.bps.asist.module.survey.model;

import de.bps.asist.R;

/**
 * @author litho
 * 
 * @date 06.11.2014
 * 
 */
public class SurveyContext {

	public enum SurveyType {

		OPTIONS(R.string.survey_type_options, 1), FREETEXT(R.string.survey_type_freetext, 2), NUMERIC(R.string.survey_type_numeric, 3);

		private final int numericValue;
        private final int i18n;

		private SurveyType(final int i18n, final int value) {
			numericValue = value;
            this.i18n = i18n;
		}

		/**
		 * @return the numericValue
		 */
		public int getNumericValue() {
			return numericValue;
		}

        public int getI18n() {
            return i18n;
        }

        public static SurveyType valueOf(final int value) {
			for (final SurveyType type : values()) {
				if (type.getNumericValue() == value) {
					return type;
				}
			}
			return null;
		}

	}

	public enum SurveyOptionsType {
		CHARACTER(R.string.survey_options_character, 1), NUMERIC(R.string.survey_options_numeric, 2);

		private final int numericValue;
        private final int i18;

		private SurveyOptionsType(final int i18n, final int value) {
			numericValue = value;
            this.i18 = i18n;
		}

		/**
		 * @return the numericValue
		 */
		public int getNumericValue() {
			return numericValue;
		}

        public int getI18() {
            return i18;
        }

        public static SurveyOptionsType valueOf(final int value) {
			for (final SurveyOptionsType type : values()) {
				if (type.getNumericValue() == value) {
					return type;
				}
			}
			return null;
		}
	}

	public enum SurveySecurityType {
		WITHIN_COURSE(R.string.survey_security_course, 1), PRIVATE(R.string.survey_security_private, 2), PUBLIC(R.string.survey_security_public, 3);

		private final int numericValue;
        private final int i18n;

		private SurveySecurityType(final int i18n, final int value) {
			numericValue = value;
            this.i18n = i18n;
		}

		/**
		 * @return the numericValue
		 */
		public int getNumericValue() {
			return numericValue;
		}

        public int getI18n() {
            return i18n;
        }

        public static SurveySecurityType parse(final int value) {
			for (final SurveySecurityType type : values()) {
				if (type.getNumericValue() == value) {
					return type;
				}
			}
			return null;
		}
	}

}
