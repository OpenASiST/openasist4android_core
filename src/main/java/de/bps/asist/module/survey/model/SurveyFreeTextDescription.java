package de.bps.asist.module.survey.model;

import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.model.IListItem;

/**
 * Created by litho on 10.12.14.
 * model class for survey result of type freetext or numeric
 */
public class SurveyFreeTextDescription implements IListItem{

    @AsistTitle
    private String option;

    @AsistDescription
    private String count;

    public SurveyFreeTextDescription(String option, Integer count){
        this.option = option;
        this.count = String.valueOf(count);
    }

    @Override
    public Long getId() {
        return 0l;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
