package de.bps.asist.module.survey.model;

import de.bps.asist.core.model.IListItem;

/**
 * Created by litho on 05.12.14.
 * Results for a specific survey
 */
public class SurveyResult implements IListItem{

    private String text;
    private int intValue;

    @Override
    public Long getId() {
        return 0L;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getIntValue() {
        return intValue;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }
}
