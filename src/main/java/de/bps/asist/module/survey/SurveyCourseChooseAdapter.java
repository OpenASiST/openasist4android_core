package de.bps.asist.module.survey;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.bps.asist.module.timetable.TimetableModule;
import de.bps.asist.module.timetable.model.Course;
import de.bps.asist.module.timetable.model.CourseDescription;

/**
 * Created by litho on 16.12.14.
 * adapter for choosing course within surveys
 */
public class SurveyCourseChooseAdapter  extends ArrayAdapter<CourseDescription> {

    private Context context;
    private List<CourseDescription> courses = new ArrayList<>();


    /**
     * Constructor
     *
     * @param context  The current context.
     */
    public SurveyCourseChooseAdapter(Context context) {
        super(context, android.R.layout.simple_spinner_item);
        this.context = context;
        List<Course> allCourses = TimetableModule.findCurrentCourses(context);
        for(Course course: allCourses){
            CourseDescription desc = new CourseDescription(context, course);
            courses.add(desc);
        }

    }

    @Override
    public int getCount() {
        return courses.size();
    }

    @Override
    public long getItemId(int position) {
        CourseDescription item = getItem(position);
        return item.getCourse().getCourseid();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        CourseDescription item = getItem(position);
        view.setText(item.getCourse().getTitle());
        return view;
    }

    @Override
    public CourseDescription getItem(int position) {
        return courses.get(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        CourseDescription item = getItem(position);
        view.setText(item.getCourse().getTitle());
        return view;
    }


}
