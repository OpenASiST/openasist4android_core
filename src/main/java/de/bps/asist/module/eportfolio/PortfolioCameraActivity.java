package de.bps.asist.module.eportfolio;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import de.bps.asist.R;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.lostandfound.ASiSTCameraUtil;

/**
 * Created by litho on 24.11.14.
 * camera activity for portfolio module
 */
public class PortfolioCameraActivity extends AsistModuleActivity {

    private static final String TAG = PortfolioCameraActivity.class.getSimpleName();


    public static final String KEY_FILENAME = "fileName";

    private Uri imageUri;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String desc = ((Object)this).toString();
        //Log.d(TAG + desc, "onViewCreated");
        if (savedInstanceState != null) {
            imageUri = savedInstanceState.getParcelable(KEY_FILENAME);
            //Log.d(TAG + desc, "have imageUri "+imageUri);
        } else {
            //Log.d(TAG + desc, "I do not have an imageUri, so I will dispatch to camera");
            imageUri = ASiSTCameraUtil.dispatchTakePictureIntent(this);
        }
        setContentView(R.layout.portfolio_fragment);
        imageView = (ImageView) getView(R.id.portfolio_image_view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String desc = ((Object)this).toString();
        //Log.d(TAG + desc, "onActivityResult");
        Bitmap bm = ASiSTCameraUtil.findBitMap(data, imageUri, imageView);
        imageView.setImageBitmap(bm);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable(KEY_FILENAME, imageUri);
    }
}
