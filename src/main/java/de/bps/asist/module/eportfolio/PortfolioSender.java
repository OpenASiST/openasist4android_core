package de.bps.asist.module.eportfolio;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import de.bps.asist.R;
import de.bps.asist.gui.dialog.ResponseHandler;
import de.bps.asist.module.olat.OlatModule;

/**
 * Created by litho on 26.11.14.
 * class to send data to olat portfolio
 */
public class PortfolioSender extends AsyncTask<RequestParams, Void, Void>{

    private static final String TAG = PortfolioSender.class.getSimpleName();

    private PortfolioFragment context;
    private static final SyncHttpClient syncClient = new SyncHttpClient();
    private ProgressDialog dialog;

    public PortfolioSender(PortfolioFragment context, ProgressDialog dialog){
        this.context = context;
        this.dialog = dialog;
    }

    private void login(){
        StringBuilder url = new StringBuilder();
        url.append(context.getResources().getString(R.string.portfolio_login));
        String header = context.getResources().getString(R.string.appHeaderName);
        String headerValue = context.getResources().getString(R.string.appHeaderValue);
        String loginHeader = context.getResources().getString(R.string.olat_login_token);
        String loginToken = OlatModule.getOlatToken(context.getActivity());
        try {
            AbstractHttpClient client = new DefaultHttpClient();
            String getURL = url.toString();
            HttpGet get = new HttpGet(getURL);
            get.setHeader(header, headerValue);
            get.setHeader(loginHeader, loginToken);
            HttpResponse responseGet = client.execute(get);
            HttpEntity resEntityGet = responseGet.getEntity();
            if (resEntityGet != null) {
                //do something with the response
                //Log.i("GET ", EntityUtils.toString(resEntityGet));
            }
            syncClient.setCookieStore(client.getCookieStore());
        } catch (Exception e) {
            //Log.e(TAG, "could not login at restapi", e);
        }
    }

    @Override
    protected Void doInBackground(RequestParams... params) {
        final RequestParams allParams = params[0];
        login();
        final StringBuilder url = new StringBuilder();
        url.append(context.getResources().getString(R.string.portfolio_send));
        //final ResponseHandler responseHandler = new ResponseHandler(dialog, context);
        //syncClient.post(url.toString(), allParams, responseHandler);

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(dialog != null){
            dialog.hide();
        }
    }
}
