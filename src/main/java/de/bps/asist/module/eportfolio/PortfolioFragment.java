package de.bps.asist.module.eportfolio;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesClient;
//import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.loopj.android.http.RequestParams;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import de.bps.asist.R;
import de.bps.asist.core.util.CameraPreview;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.dialog.ProgressFinishedCallback;
import de.bps.asist.module.lostandfound.ASiSTCameraUtil;
import de.bps.asist.module.olat.OlatModule;
import de.bps.asist.module.poi.PoiModule;
import de.bps.asist.module.poi.model.PoiItem;
import de.bps.asist.module.timetable.TimetableModule;
import de.bps.asist.module.timetable.model.Course;

/**
 * Created by litho on 21.11.14.
 * Initial Fragment for ePortfolio Module
 */
public class PortfolioFragment extends AbstractASiSTFragment //implements
        //GooglePlayServicesClient.ConnectionCallbacks,
        //GooglePlayServicesClient.OnConnectionFailedListener,
        /*LocationListener, ProgressFinishedCallback*/ {

//    private static final String TAG = PortfolioFragment.class.getSimpleName();
//
//    private LocationClient mLocationClient;
//    private Location mCurrentLocation;
//
//    private ImageView imageView;
//    private CameraPreview cameraPreview;
//    private ScrollView editLayout;
//    private EditText titleEdit;
//    private EditText descEdit;
//    private StringBuilder desc;
//    private Bitmap imageBm;
//    private ProgressDialog progressDialog;
//
//    private static final DateFormat df = DateFormat.getDateTimeInstance();
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        //Log.d(TAG, "creating view");
//        View view = inflater.inflate(R.layout.portfolio_fragment, null);
//        cameraPreview = getView(view, R.id.portfolio_camera_preview);
//        editLayout = getView(view, R.id.portfolio_edit_layout);
//        String olatLoginToken = OlatModule.getOlatToken(getActivity());
//        if(olatLoginToken == null){
//            TextView noOlatView = getView(view, R.id.portfolio_nologin);
//            noOlatView.setText(R.string.noOpal);
//            editLayout.setVisibility(View.GONE);
//            cameraPreview.setVisibility(View.GONE);
//        } else {
//            cameraPreview.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    cameraPreview.getCamera().autoFocus(null);
//                    cameraPreview.getCamera().takePicture(null, null, new PictureTakenCallback());
//                }
//            });
//
//            imageView = getView(view, R.id.portfolio_image_view);
//            editLayout.setVisibility(View.GONE);
//            titleEdit = getView(view, R.id.portfolio_title);
//            String title = translate(R.string.app_header) + " "+df.format(new Date());
//            titleEdit.setText(title);
//            List<Course> currentCourse = TimetableModule.findCurrentCourses(getActivity());
//            desc = new StringBuilder();
//            desc.append(df.format(new Date())).append("\n");
//            if (currentCourse.size() > 0) {
//                desc.append(currentCourse.get(0).getTitle());
//                desc.append("\n").append(currentCourse.get(0).getRoom());
//            }
//            descEdit = getView(view, R.id.portfolio_description);
//            descEdit.setText(desc.toString());
//            ImageButton sendButton = getView(view, R.id.portfolio_send_button);
//            sendButton.setOnClickListener(new SendButtonListener());
//            mLocationClient = new LocationClient(getActivity(), this, this);
//            mLocationClient.connect();
//        }
//        return view;
//    }
//
//    @Override
//    public void onStop() {
//        // Disconnecting the client invalidates it.
//        if(mLocationClient != null) {
//            mLocationClient.disconnect();
//        }
//        if(cameraPreview != null && cameraPreview.getCamera() != null) {
//            cameraPreview.release();
//        }
//        super.onStop();
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if(R.id.action_generic_add == item.getItemId()){
//            Intent intent = new Intent(getActivity(), PortfolioCameraActivity.class);
//            startActivity(intent);
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public void onConnected(Bundle bundle) {
//        mCurrentLocation = mLocationClient.getLastLocation();
//        if(mCurrentLocation != null) {
//            PoiItem nearest = PoiModule.findNearestItem(getActivity(), mCurrentLocation);
//            if(nearest != null) {
//                desc.append(nearest.getTitle());
//                descEdit.setText(desc.toString());
//            }
//        }
//    }
//
//    @Override
//    public void onDisconnected() {
//
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {
//
//    }
//
//    @Override
//    public void onLocationChanged(Location location) {
//        mCurrentLocation = mLocationClient.getLastLocation();
//        if(mCurrentLocation != null) {
//            PoiItem nearest = PoiModule.findNearestItem(getActivity(), mCurrentLocation);
//            desc.append(nearest.getTitle());
//            descEdit.setText(desc.toString());
//        }
//    }
//
//    @Override
//    public void onFinish() {
//
//    }
//
//    @Override
//    public void onError(Throwable e) {
//
//    }
//
//    private class PictureTakenCallback implements Camera.PictureCallback{
//        @Override
//        public void onPictureTaken(byte[] data, Camera camera) {
//
//            editLayout.setVisibility(View.VISIBLE);
//            editLayout.requestLayout();
//            cameraPreview.setVisibility(View.GONE);
//            cameraPreview.getCamera().stopPreview();
//            imageBm = ASiSTCameraUtil.setBitMap(data, imageView);
//        }
//    }
//
//    private class SendButtonListener implements View.OnClickListener {
//
//        @Override
//        public void onClick(View v) {
//            StringBuilder url = new StringBuilder();
//            url.append(translate(R.string.olat_url));
//            url.append("/restapi/auth/app/header");
//            final RequestParams params = new RequestParams();
//            params.put("title", titleEdit.getText());
//            params.put("description", descEdit.getText());
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            imageBm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
//            byte[] data = bos.toByteArray();
//            ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
//            params.put("file", inputStream);
//            String filename = titleEdit.getText().toString().replaceAll("[^a-zA-Z0-9-_\\.]", "_");
//            params.put("filename", filename + ".jpg");
//
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            progressDialog.show();
//
//            PortfolioSender sender = new PortfolioSender(PortfolioFragment.this, progressDialog);
//            sender.execute(params);
//        }
//    }
//
//

}
