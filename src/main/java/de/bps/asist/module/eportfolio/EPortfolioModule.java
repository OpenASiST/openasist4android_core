package de.bps.asist.module.eportfolio;

import java.util.ArrayList;
import java.util.List;

import de.bps.asist.BuildConfig;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;

/**
 * Created by litho on 05.11.14.
 * Module for e-Portfolio
 */
public class EPortfolioModule extends AbstractAsistModule {

    private PortfolioFragment fragment;

    public EPortfolioModule(){
        boolean debug = BuildConfig.DEBUG;
        //setEnabled(debug);
    }

    @Override
    public int getName() {
        return R.string.portfolio_name;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_opal;
    }

    @Override
    public PortfolioFragment getInitialFragment() {
        fragment = new PortfolioFragment();
        return fragment;
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        return new ArrayList<>();
    }
}
