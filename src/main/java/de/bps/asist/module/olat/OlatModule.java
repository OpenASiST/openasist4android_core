/**
 * 
 */
package de.bps.asist.module.olat;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.start.AsistStartModule;

/**
 * @author thomasw
 * 
 */
public class OlatModule extends AbstractAsistModule {

    private static final String SHARED_PREFERENCES = "prefs_olat";
    public static final String KEY_LOGIN = "login";
    private static final String KEY_PUSH_UID = "push-uid";

	public OlatModule(){
		AsistStartModule.registerForPush(getName(), "OlatModule");
	}

	/**
	 * @see de.bps.asist.module.AbstractAsistModule#getName()
	 */
	@Override
	public int getName() {
		return R.string.module_olat_name;
	}

	@Override
	public int getIcon() {
		return R.drawable.ic_menu_opal;
	}

	/**
	 * @see de.bps.asist.module.AbstractAsistModule#getInitialFragment()
	 */
	@Override
	public Fragment getInitialFragment() {
		return new OlatFragment();
	}

	/**
	 * @see de.bps.asist.module.AbstractAsistModule#getDatabaseClasses()
	 */
	@Override
	public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
		return null;
	}

    public static String getOlatToken(Context context){
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String login = preferences.getString(KEY_LOGIN, null);
        return login;
    }

}
