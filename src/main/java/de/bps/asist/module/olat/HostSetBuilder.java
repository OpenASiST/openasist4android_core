package de.bps.asist.module.olat;

import android.content.Context;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class HostSetBuilder {
    private HashSet<String> hosts;

    private void addHost(String addingHost) {
        if (addingHost != null) {
            if (!addingHost.isEmpty()) {
                if (this.hosts == null) {
                    this.hosts = new HashSet<>();
                }
                this.hosts.add(addingHost);
            }
        }
    }

    public HostSetBuilder extractHost(URL url) {
        throwExceptionIfNull(url, "url");
        this.addHost(url.getHost());
        return this;
    }

    public HostSetBuilder host(String host) {
        throwExceptionIfNull(host, "host");
        this.addHost(host);
        return this;
    }

    public HostSetBuilder host(int resId, Context context) {
        this.host(context.getString(resId));
        return this;
    }

    public HostSetBuilder hosts(int resId, Context context) {
        String[] addingUrls = context.getResources().getStringArray(resId);
        for (String url : addingUrls) {
            this.host(url);
        }
        return this;
    }

    public Set<String> build() {
        if (this.hosts == null) {
            return new HashSet<>();
        } else {
            return new HashSet<>(this.hosts);
        }
    }

    private static void throwExceptionIfNull(Object object, String paramterName) {
        if (object == null) {
            throw new NullPointerException(paramterName + "should not be null.");
        }
    }
}
