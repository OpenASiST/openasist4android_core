/**
 *
 */
package de.bps.asist.module.olat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import de.bps.asist.R;
import de.bps.asist.core.manager.push.PushNotificationManager;
import de.bps.asist.gui.AbstractASiSTFragment;

import static android.app.Activity.RESULT_OK;

/**
 * @author thomasw
 */
public class OlatFragment extends AbstractASiSTFragment {

    private static final String SHARED_PREFERENCES = "prefs_olat";
    public static final String KEY_LOGIN = "login";
    private static final String KEY_PUSH_UID = "push-uid";

    private static final String TAG = OlatFragment.class.getSimpleName();
    WebView webView;

    ValueCallback<Uri> mUploadMessage;
    ValueCallback<Uri[]> mUploadMessageArray;

    private final static int FILECHOOSER_RESULTCODE=17709;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.olat_fragment, container, false);
        webView = (WebView) view.findViewById(R.id.olat_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        Map<String, String> headers = new HashMap<>();

        String login = OlatModule.getOlatToken(getActivity());
        if(login != null){
            headers.put(translate(R.string.olat_login_token), login);
        }else {
            CookieSyncManager.createInstance(getActivity());
        }
        headers.put(translate(R.string.appHeaderName), translate(R.string.appHeaderValue));
        webView.setWebViewClient(new MyBrowser(getActivity(), headers));
        CookieSyncManager.getInstance().sync();
        webView.loadUrl(translate(R.string.olat_url), headers);
        webView.setOnKeyListener(new View.OnKeyListener(){

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == MotionEvent.ACTION_UP
                        && webView.canGoBack()) {
                    handler.sendEmptyMessage(1);
                    return true;
                }

                return false;
            }

        });
        webView.setWebChromeClient(new WebChromeClient(){
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType){
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                startActivityForResult(Intent.createChooser(i, "File Browser"), FILECHOOSER_RESULTCODE);
            }

            public boolean onShowFileChooser(
                    WebView webView, ValueCallback<Uri[]> filePathCallback,
                    WebChromeClient.FileChooserParams fileChooserParams){
                if(mUploadMessageArray != null){
                    mUploadMessageArray.onReceiveValue(null);
                }
                mUploadMessageArray = filePathCallback;
                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                contentSelectionIntent.setType("*/*");

                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "File Chooser");
                startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
                return true;
            }
        });

        Set<String> allowedHosts = new HostSetBuilder()
                .host(R.string.olat_url, getContext())
                .hosts(R.array.olat_allowed_download_hosts, getContext())
                .build();

        webView.setDownloadListener(new OpalDownloadListener(this.getContext().getApplicationContext(), allowedHosts));

        return view;
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:{
                    webViewGoBack();
                }break;
            }
        }
    };

    private void webViewGoBack(){
        webView.goBack();
    }

    private class MyBrowser extends WebViewClient {

        private Map<String, String> headers;
        private Context context;

        public MyBrowser(Context context, Map<String, String> headers) {
            this.headers = headers;
            this.context = context;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url, headers);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            String currentUrl = view.getUrl();
            if (currentUrl != null) {
                CookieManager cookieManager = CookieManager.getInstance();
                String loginToken = getCookie(view.getUrl(), "token");
                String pushUid = getCookie(view.getUrl(), "push-uid");
                if (loginToken != null || pushUid != null) {
                    SharedPreferences shared = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = shared.edit();
                    if (loginToken != null) {
                        editor.putString(KEY_LOGIN, loginToken);
                    }
                    if (pushUid != null) {
                        //if(shared.getString(KEY_PUSH_UID, "").equals(""))
                        PushNotificationManager.getInstance().registerPushUID(getActivity(), pushUid);
                        editor.putString(KEY_PUSH_UID, pushUid);
                    }
                    editor.apply();
                    ////Log.i(TAG, "found cookie loginToken and pushUid: " + loginToken + ", " + pushUid);
                }
            }
        }

        public String getCookie(String siteName, String CookieName) {
            String cookieValue = null;

            CookieManager cookieManager = CookieManager.getInstance();
            String cookies = cookieManager.getCookie(siteName);
            if(cookies == null){
                return null;
            }
            String[] temp = cookies.split("[;]");
            for (String ar1 : temp) {
                if (ar1.contains(CookieName)) {
                    String[] temp1 = ar1.split("[=]");
                    cookieValue = temp1[1];
                }
            }
            return cookieValue;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage) {
                if(mUploadMessageArray == null)
                    return;
                Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
                if(result == null)
                    return;
                Uri[] resultArray = new Uri[]{result};
                mUploadMessageArray.onReceiveValue(resultArray);
                mUploadMessageArray = null;
            } else {
                Uri result = data == null || resultCode != RESULT_OK ? null
                        : data.getData();
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private File createImageFile() throws IOException{
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "img_"+timeStamp+"_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName,".jpg",storageDir);
    }

    /**
     * Download listener for opal web view. This download listner will block every download which is not in the given hosts.
     */
    private class OpalDownloadListener implements DownloadListener {

        private final static String USER_AGENT_HEADER = "User-Agent";
        private final static String COOKIE_HEADER = "Cookie";

        private Context context;
        private Set<String> allowedHosts;

        /**
         * Contructs a new OpalDownloadListener instance.
         *
         * @param context - context wich will be used for system calls
         */
        OpalDownloadListener(Context context) {
            this.context = context;
            this.allowedHosts = null;
        }

        /**
         * Contructs a new OpalDownloadListener instance.
         *
         * @param context      - context which will be used for system calls
         * @param allowedHosts - allowed hosts for downloading
         */
        OpalDownloadListener(Context context, Collection<String> allowedHosts) {
            this.context = context;
            this.allowedHosts = new HashSet<>(allowedHosts);
        }

        /**
         * Contructs a new OpalDownloadListener instance.
         *
         * @param context      - context which will be used for system calls
         * @param allowedHost - allowed host for downloading
         */
        OpalDownloadListener(Context context, String allowedHost) {
            this(context, Arrays.asList(allowedHost));
        }

        /**
         * Contructs a new OpalDownloadListener instance.
         *
         * @param context      - context which will be used for system calls
         * @param allowedHost - allowed host for downloading
         */
        OpalDownloadListener(Context context, Uri allowedHost) {
            this(context, allowedHost.getHost());
        }

        @Override
        public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
            Uri downloadUri = Uri.parse(url);
            if (this.isHostAllowed(downloadUri)) {
                try {
                    String downloadFileName = guessFileName(url, contentDisposition, mimetype);

                    DownloadManager.Request downloadRequest = new DownloadManager.Request(Uri.parse(url));
                    downloadRequest.setMimeType(mimetype);
                    downloadRequest.setTitle(downloadFileName);
                    downloadRequest.setDescription(this.context.getString(R.string.module_olat_download_description) + " " + downloadFileName + ".");

                    downloadRequest.addRequestHeader(COOKIE_HEADER, CookieManager.getInstance().getCookie(url));
                    downloadRequest.addRequestHeader(USER_AGENT_HEADER, userAgent);

                    downloadRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, downloadFileName);

                    downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE | DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    downloadRequest.allowScanningByMediaScanner();

                    DownloadManager downloadManager = (DownloadManager) this.context.getSystemService(Context.DOWNLOAD_SERVICE);
                    downloadManager.enqueue(downloadRequest);
                } catch (SecurityException exception) {
                    Toast.makeText(this.context, getString(R.string.module_olat_download_security_error), Toast.LENGTH_LONG).show();
                    Log.e("olat", exception.getClass().getSimpleName() + " while Download", exception);
                } catch (Throwable throwable) {
                    Log.e("olat", "Unexpected error while downloading " + downloadUri.toString() + ".", throwable);
                }
            } else {
                Toast.makeText(this.context, getString(R.string.module_olat_download_blocking, downloadUri.getHost()), Toast.LENGTH_LONG).show();
            }
        }

        /**
         * Check wether the included uri host is permitted in downloading files.
         *
         * @param checkingUri  - Uri wich will be checked
         * @param allowedHosts - List of allowed hosts
         * @return true if host is allowed or false if not
         */
        private boolean isHostAllowed(Uri checkingUri, Set<String> allowedHosts) {
            boolean isHostAllowed = true;
            if (allowedHosts != null) {
                isHostAllowed = allowedHosts.contains(checkingUri.getHost());
            }
            return isHostAllowed;
        }

        /**
         * Check wether the included uri host is permitted in downloading files.
         *
         * @param checkingUri - Uri wich will be checked
         * @return true if host is allowed or false if not
         */
        private boolean isHostAllowed(Uri checkingUri) {
            return this.isHostAllowed(checkingUri, this.allowedHosts);
        }

        /**
         * Guesses a filename that a download would have, using url and contentDisposition. Is no file extension spicified the mime type is used as fallback.
         *
         * @param url
         * @param contentDisposition
         * @param mimetype
         * @return
         */
        private String guessFileName(String url, String contentDisposition, String mimetype) {
            String guessingFileName = URLUtil.guessFileName(url, contentDisposition, mimetype);

            String contentDispositionContentParts[] = contentDisposition.split(";");
            for (String contentDispositionContentPart : contentDispositionContentParts) {
                contentDispositionContentPart = contentDispositionContentPart.trim();
                if (contentDispositionContentPart.startsWith("filename=")) {
                    guessingFileName = contentDispositionContentPart.substring(contentDispositionContentPart.indexOf('=') + 1);
                    if (guessingFileName.startsWith("\"") && guessingFileName.endsWith("\"")) {
                        guessingFileName = guessingFileName.substring(1, guessingFileName.length() - 1);
                    }
                    break;
                }
            }

            return guessingFileName;
        }
    }
}
