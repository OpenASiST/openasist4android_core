package de.bps.asist.module.impressum;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import java.util.Locale;

import de.bps.asist.R;
import de.bps.asist.gui.AbstractASiSTFragment;

/**
 * Created by epereira on 31.08.2016.
 */
public class ImpressumFragment extends AbstractASiSTFragment {
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.impressum_fragment, container, false);
        WebView tvImpressum = (WebView) view.findViewById(R.id.impressumTextView);
        tvImpressum.loadData(getString(R.string.impressumText), "text/html; charset=utf-8", "UTF-8");
        tvImpressum.getSettings().setJavaScriptEnabled(true);
        return view;
    }
}
