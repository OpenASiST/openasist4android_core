package de.bps.asist.module.impressum;

import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;

/**
 * Created by epereira on 31.08.2016.
 */
public class ImpressumModule extends AbstractAsistModule {

    @Override
    public int getName() {
        return R.string.module_impressum_name;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_impressum;
    }


    @Override
    public Fragment getInitialFragment() {
        return new ImpressumFragment();
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        return null;
    }
}
