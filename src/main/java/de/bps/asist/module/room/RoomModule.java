package de.bps.asist.module.room;

import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;

/**
 * Room module
 */
public class RoomModule extends AbstractAsistModule {
    @Override
    public int getName() {
        return R.string.module_room_name;
    }

    @Override
    public int getIcon() {
        //TODO: Set proper icon
        return R.drawable.module_room_icon;
    }

    @Override
    public Fragment getInitialFragment() {
        return new RoomFragment();
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        return null;
    }
}
