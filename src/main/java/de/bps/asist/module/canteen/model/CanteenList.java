package de.bps.asist.module.canteen.model;

import java.util.List;

import de.bps.asist.core.model.IList;

/**
 * 
 * @author bja
 * 
 * @date 24.04.2014
 * 
 */
public class CanteenList implements IList {

	private List<Canteen> canteens;

	@Override
	public List<Canteen> getItems() {
		return getCanteens();
	}

	public List<Canteen> getCanteens() {
		return canteens;
	}

	public void setCanteens(final List<Canteen> canteens) {
		this.canteens = canteens;
	}

}
