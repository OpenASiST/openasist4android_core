package de.bps.asist.module.canteen;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.bps.asist.R;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.module.canteen.model.Canteen;
import de.bps.asist.module.canteen.model.CanteenMenu;


public class ExpandableMensaList extends AbstractASiSTFragment {



    private ExpandableListView listView;
    Calendar calendar = Calendar.getInstance();
    ExpandableMensaListAdapter expandableMensaListAdapter;
    List<String> daten; //Saves the dates for the header of the ExpandableListView
    HashMap<String, List<CanteenMenu>> menus; //HashMap for the ExpandableListView

    Canteen selectedCanteen;

    public ExpandableMensaList() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ExpandableMensaList(Canteen canteen) {
        // Required empty public constructor
        selectedCanteen = canteen;
    }


    private static final String DATE_Format = "dd.MM.yyyy";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreateView(inflater,container,savedInstanceState);
        View view = inflater.inflate(R.layout.expandablelistview, container, false);

        listView = getView(view,R.id.listView_id);

        prepareListData();

        expandableMensaListAdapter = new ExpandableMensaListAdapter(getContext(),daten, menus);
        try{
            listView.setAdapter(expandableMensaListAdapter);
            listView.expandGroup(0);
        } catch (Exception e){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(de.bps.asist.R.string.serverErrorText).setTitle(de.bps.asist.R.string.serverError);
            builder.setPositiveButton("OK",null);
            builder.create();
            builder.show();
        }

        //Child Click!
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                Intent intent = new Intent(getContext(), MenuDetailsActivity.class);
                //TODO change name
                intent.putExtra("1",menus.get(daten.get(i)).get(i1));
                startActivity(intent);
                return false;
            }
        });
        return view;

    }

    //Prepares the ListData to sort the menus to it's dates
    private void prepareListData(){
        daten = new ArrayList<String>();
        menus = new HashMap<String, List<CanteenMenu>>();

        List<CanteenMenu> firstMenuList = new ArrayList<CanteenMenu>();
        firstMenuList.addAll(selectedCanteen.getMenuItems());

        Date maxDate = Calendar.getInstance().getTime();
        for(final CanteenMenu menu : firstMenuList){
            Date maxDateTemp = menu.getDate();
            if(maxDateTemp.compareTo(maxDate) > 0){
                maxDate = maxDateTemp;
            }
        }

        //Adding Daten
        for(int i = 0;i < 5;i++){
            if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
                add(calendar);
                add(calendar);
            }
            if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                add(calendar);
            }
            daten.add(cleanDate(calendar));

            //Sorts for Categorys (Menu 1 should be on top)
            Collections.sort(firstMenuList, new Comparator<CanteenMenu>() {
                @Override
                public int compare(CanteenMenu canteenMenu1, CanteenMenu canteenMenu2) {
                    if(canteenMenu1 == null || canteenMenu2 == null || canteenMenu1.getCategory() == null){
                        return 0;
                    }
                    return canteenMenu1.getCategory().compareTo(canteenMenu2.getCategory());
                }
            });

            List<CanteenMenu> menuListWithMatchingDates = new ArrayList<CanteenMenu>();
            //Compares all menus with the dates of the current Day to provide a List with matching dates and menus
            Calendar cal = Calendar.getInstance();
            for(final CanteenMenu menu : firstMenuList){
                cal.setTime(menu.getDate());
                if(cleanDate(cal).equals(cleanDate(calendar))){
                    menuListWithMatchingDates.add(menu);
                }
            }
            add(calendar);
            menus.put(daten.get(i), menuListWithMatchingDates);
            String temp;
            temp = cleanDate(calendar);
            if(temp.compareTo(cleanDate(maxDate)) > 0) break;
        }
        firstMenuList.clear();
    }

    //Calendar goes one Day ahead
    private void add(Calendar cal) {
        int toAdd = 1;
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            toAdd = 3;
        }
        cal.add(Calendar.DAY_OF_WEEK, toAdd);
    }

    //Calendar goes to the previous day
    private void reduce(Calendar cal) {
        int toAdd = -1;
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
            toAdd = -3;
        }
        cal.add(Calendar.DAY_OF_WEEK, toAdd);
    }

    //Provides a clean Date String for the Headers
    private String cleanDate(Calendar cal) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_Format);
        return format.format(cal.getTime());
    }

    private String cleanDate(Date date){
        SimpleDateFormat format = new SimpleDateFormat(DATE_Format);
        return format.format(date.getTime());
    }

    @Override
    public void onResume() {
        super.onResume();
        //Changes prices if settings change and gone back to MensaFragment
        expandableMensaListAdapter.notifyDataSetChanged();
    }
}
