package de.bps.asist.module.canteen;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.bps.asist.module.canteen.model.CanteenMenu;
import de.bps.asist.module.start.tree.ASiSTTreeItem;
import de.bps.asist.module.start.tree.StartTreeConfig;

/**
 * Created by litho on 30.10.14.
 * Configuration of StartTreeView for Canteen Module
 */
public class CanteenStartViewConfig extends StartTreeConfig<CanteenModule> {

    private List<ASiSTTreeItem> treeItems;

    public CanteenStartViewConfig(CanteenModule module){
        super(module);
    }

    @Override
    public ASiSTTreeItem getRootNode(Context context) {
        String title = context.getResources().getString(getModule().getName());
        String description = context.getResources().getString(getModule().getDescription());
        ASiSTTreeItem rootItem = new ASiSTTreeItem(title, description);
        treeItems = new ArrayList<>();
        createList(context);
        rootItem.setChildren(treeItems);
        return rootItem;
    }

    private void createList(Context context) {
        treeItems.clear();
        List<CanteenMenu> allMenus = CanteenDatabaseManager.getInstance().getAllMenusToday(context);
        final CanteenTreeItemLayouter layouter = new CanteenTreeItemLayouter(context, allMenus);
        Map<String, List<CanteenMenu>> objects = layouter.getObjects();
        for(String header: objects.keySet()) {
            ASiSTTreeItem headerItem = new ASiSTTreeItem(header, null);
            headerItem.setLayouter(layouter);
            treeItems.add(headerItem);
            for (CanteenMenu menu : objects.get(header)) {
                ASiSTTreeItem<CanteenMenu> item = new ASiSTTreeItem<>(menu.getTitle(), menu.getDescription());
                item.setModelItem(menu);
                item.setLayouter(layouter);
                treeItems.add(item);
            }
        }
    }

    public void update(Context context){
        createList(context);
    }

    @Override
    public boolean isEnabledPerDefault() {
        return CanteenModule.isStartTreeEnabledPerDefault();
    }
}
