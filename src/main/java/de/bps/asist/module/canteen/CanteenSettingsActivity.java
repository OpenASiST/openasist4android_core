package de.bps.asist.module.canteen;

import android.os.Bundle;

import de.bps.asist.R;
import de.bps.asist.module.AsistModuleActivity;

public class CanteenSettingsActivity extends AsistModuleActivity {

    //Activity for new CanteenSettings
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canteen_settings);
        getSupportActionBar().setTitle(R.string.module_canteen_settings);
        getFragmentManager().beginTransaction().replace(R.id.list_container, new CanteenSettingsFragment()).commit();
    }
}
