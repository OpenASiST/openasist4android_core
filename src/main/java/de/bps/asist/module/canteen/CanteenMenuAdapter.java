package de.bps.asist.module.canteen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.manager.preferences.ASiSTPreferenceManager;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.canteen.model.CanteenMenu;
import de.bps.asist.module.canteen.model.MenuPriceDescription;

/**
 * Created by litho on 22.10.14.
 * Adapter for Canteen Menu Items
 */
public class CanteenMenuAdapter extends CategoryListViewAdapter<CanteenMenu> {

    Context acontext;

    public CanteenMenuAdapter(Context context, Collection<CanteenMenu> menus) {
        super(context, parseMenus(menus), new AbstractGenericListCallback(context, MenuDetailActivity.class));
        acontext = context;
        setRowLayout(R.layout.menu_list_item);
    }

    private static Map<String, List<CanteenMenu>> parseMenus(Collection<CanteenMenu> menuCollection) {
        List<CanteenMenu> menuList = new ArrayList<>(menuCollection);
        Collections.sort(menuList, new MenuCategoryComparator());
        final Map<String, List<CanteenMenu>> menus = new LinkedHashMap<>();
        for (final CanteenMenu men : menuList) {
            List<CanteenMenu> list = menus.get(men.getCategory());
            if (list == null) {
                list = new ArrayList<>();
                menus.put(men.getCategory(), list);
            }
            list.add(men);
        }
        return menus;
    }

    @Override
    protected View addItemView(int position, LayoutInflater inflater) {
        View layout = super.addItemView(position, inflater);
        final CanteenMenu item = (CanteenMenu) getItem(position);
        // Here we need to check current price config and show only corresponding price.
        ASiSTPreferenceManager APM = new ASiSTPreferenceManager();
        int PriceSetting = APM.getCanteenPriceSetting(acontext);

        TextView prices = getView(layout, R.id.canteen_prices);
        List<MenuPriceDescription> mpg = new ArrayList<>(item.getPrices());

        if (mpg.isEmpty()) {
            return layout;
        }

        TextView canteenName = getView(layout, R.id.listItemDescription);
        String cName = mpg.get(0).getCanteenMenu().getCanteen().getTitle();
        canteenName.setText(cName);

        Collections.sort(mpg, new MenuPriceComparator());
        StringBuilder sb = new StringBuilder();
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.GERMANY);
        if(mpg.size() > 0) {
            MenuPriceDescription mp = mpg.get(PriceSetting);
            if (sb.length() > 0) {
                sb.append(" / ");
            }

            if(mp.getPrice() > 0.00f) {
                sb.append(nf.format(mp.getPrice()));
            }
            else{
                sb.append(""); //Kein Preis vorhanden.
            }
        }
        prices.setText(sb.toString());
        return layout;
    }

    private static class MenuCategoryComparator implements Comparator<CanteenMenu> {
        @Override
        public int compare(CanteenMenu lhs, CanteenMenu rhs) {
            if(lhs.getCategory() != null && rhs.getCategory() != null){
                return lhs.getCategory().compareTo(rhs.getCategory());
            }
            return 0;
        }
    }

}
