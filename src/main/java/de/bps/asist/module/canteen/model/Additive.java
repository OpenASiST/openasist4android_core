package de.bps.asist.module.canteen.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IListItem;

@DatabaseTable
public class Additive extends AbstractDatabaseObject implements IListItem {

	private static final long serialVersionUID = 1197216928552872377L;

	@AsistTitle
	@DatabaseField
	private String title;

	@DatabaseField
	private String imageurl;

	@DatabaseField
	@JsonProperty(value = "desc")
	private String description;

	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private CanteenMenu menu;

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(final String imageurl) {
		this.imageurl = imageurl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public CanteenMenu getMenu() {
		return menu;
	}

	public void setMenu(final CanteenMenu menu) {
		this.menu = menu;
	}

}
