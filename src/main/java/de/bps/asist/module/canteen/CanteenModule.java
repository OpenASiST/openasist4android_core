package de.bps.asist.module.canteen;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.ASiSTApplication;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.canteen.model.Additive;
import de.bps.asist.module.canteen.model.Canteen;
import de.bps.asist.module.canteen.model.CanteenList;
import de.bps.asist.module.canteen.model.CanteenMenu;
import de.bps.asist.module.canteen.model.MenuPriceDescription;
import de.bps.asist.module.start.AsistStartModule;

public class CanteenModule extends AbstractAsistModule {

    private static final String TAG = CanteenModule.class.getSimpleName();

    public static final String PRICE_PUPIL = "P";
    public static final String PRICE_STUDENT = "S";
    public static final String PRICE_EMPLOYEE = "E";
    public static final String PRICE_GUEST = "G";

    public CanteenModule(){
        /*Auskommentiert um Mensa auf Startseite und Einstellungen zu deaktivieren
        setStartTreeConfig(new CanteenStartViewConfig(this));
        */
        setSettingsConfig(new CanteenSettings());
        AsistStartModule.registerForPush(getName(), "CanteenModule");
    }

    public static boolean isStartTreeEnabledPerDefault() {
        return ASiSTApplication.getContext().getResources().getBoolean(R.bool.moduleCanteenStartTreeEnabledPerDefault);
    }

    @Override
    public int getName() {
        return R.string.module_canteen_name;
    }

    public int getDescription(){
        return R.string.module_canteen_description;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_mensa;
    }

    @Override
    public Fragment getInitialFragment() {
        return new MensaFragment();
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        final List<Class<? extends AbstractDatabaseObject>> result = new ArrayList<>();
        result.add(Canteen.class);
        result.add(CanteenMenu.class);
        result.add(MenuPriceDescription.class);
        result.add(Additive.class);
        return result;
    }

    @Override
    public void updateData(final Context context) {
        super.updateData(context);
        String institutionName = context.getResources().getString(R.string.institutionName);
        final String url = context.getResources().getString(R.string.rootUrl) + "/canteen/" + institutionName + "/all";
        final CanteenCallBack cb = new CanteenCallBack(context);
        ASiSTParser.getInstance().parse(url, CanteenList.class, cb);
    }

    private class CanteenCallBack implements ParserCallback<CanteenList> {

        private final Context context;

        public CanteenCallBack(final Context context) {
            this.context = context;
        }

        @Override
        public void afterParse(final CanteenList parsed) {
            CanteenDatabaseManager.getInstance().getAllMenus(parsed, context, ((CanteenStartViewConfig)getStartTreeConfig()));
        }

        @Override
        public void onError(Exception e) {
            //
        }

        @Override
        public void onManagedError(CanteenList parsed) {
            //
        }
    }

}
