package de.bps.asist.module.canteen;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import de.bps.asist.R;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.module.canteen.model.Canteen;

/**
 * A simple {@link androidx.fragment.app.Fragment} subclass.
 */
public class MensaFragment extends AbstractASiSTFragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    private List<Canteen> canteens;

    public MensaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mensa,container,false);

        tabLayout = (TabLayout)view.findViewById(R.id.tabLayout_id);
        viewPager = (ViewPager)view.findViewById(R.id.viewpager_id);
        adapter = new ViewPagerAdapter(getChildFragmentManager());

        //get Canteens
        canteens = CanteenDatabaseManager.getInstance().getAllCanteens(getActivity());
        if(canteens.size() == 0){
            if(canteens.size() == 0){
                Toast.makeText(getContext(),getText(R.string.module_canteen_noCanteen_found_toast),Toast.LENGTH_LONG).show();
            }
        }

        //Sorts canteens like "Mensa, Cafeteria, Mensa, Cafeteria" (but the ugly way)
        List<Canteen> canteens2 = CanteenDatabaseManager.getInstance().getAllCanteens(getActivity());
        String reichenhainer = "Reichenhainer";
        String StraNa = "Straße der Nationen";
        Collections.sort(canteens, new Comparator<Canteen>() {
            @Override
            public int compare(Canteen canteen2, Canteen canteen1) {
                if (canteen1 == null || canteen2 == null  || canteen1.getTitle() == null) {
                    return 0;
                }
                return canteen1.getTitle().compareTo(canteen2.getTitle());
            }
        });
        canteens2.clear();
        for(Canteen canteen : canteens){
            if(canteen.getTitle().contains(reichenhainer)){
                canteens2.add(canteen);
            }
        }

        for(Canteen canteen : canteens){
            if(canteen.getTitle().contains(StraNa)){
                canteens2.add(canteen);
            }
        }

        canteens.clear();
        canteens = canteens2;

        /*
        //sort the old way
        Collections.sort(canteens, new Comparator<Canteen>() {
            @Override
            public int compare(Canteen canteen2, Canteen canteen1) {
                if (canteen1 == null || canteen2 == null  || canteen1.getTitle() == null) {
                    return 0;
                }
                return canteen2.getTitle().compareTo(canteen1.getTitle());
            }
        });
        */


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //TODO replace Key
        String preferredMensaName = sharedPreferences.getString("canteens", null);
        int preferredMensaTabIndex = 0;

        for(int i = 0; i < canteens.size();i++){
            Canteen addingCanteen = canteens.get(i);
            adapter.AddFragment(new ExpandableMensaList(addingCanteen), addingCanteen.getTitle());
            if (addingCanteen.getTitle().equals(preferredMensaName)) {
                preferredMensaTabIndex = i;
            }
        }

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        //Makes use of custom_Tabs
        for(int i = 0;i < adapter.getCount();i++){
            View customView = adapter.getCustomView(getActivity(),i);
            tabLayout.getTabAt(i).setCustomView(customView);
        }

        //Prevent the reloading of Menus everytime the tab is changed
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
        viewPager.setOffscreenPageLimit(limit);

        if (savedInstanceState == null) {
            viewPager.setCurrentItem(preferredMensaTabIndex);
        } else {
            viewPager.setCurrentItem(savedInstanceState.getInt("selected_canteen_tab", preferredMensaTabIndex));

        }


        return view;
        }

    @Override
    public Integer getMenuLayout() {
        return R.menu.mensa;
    }

    @Override
    public boolean handleMenuItemClicked(MenuItem menuItem){
        if(menuItem.getItemId() == R.id.action_mensa_settings){
            final Intent intent = new Intent(getActivity(),CanteenSettingsActivity.class);
            startActivity(intent);
        }
        return super.handleMenuItemClicked(menuItem);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("selected_canteen_tab", viewPager.getCurrentItem());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            viewPager.setCurrentItem(savedInstanceState.getInt("selected_canteen_tab", 0));
        }
    }
}
