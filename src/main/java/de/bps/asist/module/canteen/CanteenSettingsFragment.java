package de.bps.asist.module.canteen;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.Nullable;
import de.bps.asist.R;
import de.bps.asist.module.canteen.model.Canteen;

public class CanteenSettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    ListPreference listPreference;
    ListPreference listPreference1;

    //PreferenceFragment for new Canteen Settings

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_canteen);
        listPreference = (ListPreference)findPreference("canteenPrices");
        listPreference1 = (ListPreference)findPreference("canteens");
        load_ListPreferences();
        setSummary();
    }

    //Makes ListPreferences dynamic loads it with all canteens
    private void load_ListPreferences(){
        ListPreference listPreference = (ListPreference)findPreference("canteens");
        //get Canteens
        List<Canteen> canteens = CanteenDatabaseManager.getInstance().getAllCanteens(getActivity());

        //Sorts canteens like "Mensa, Cafeteria, Mensa, Cafeteria" (but the ugly way)
        List<Canteen> canteens2 = CanteenDatabaseManager.getInstance().getAllCanteens(getActivity());
        String reichenhainer = "Reichenhainer";
        String StraNa = "Straße der Nationen";
        Collections.sort(canteens, new Comparator<Canteen>() {
            @Override
            public int compare(Canteen canteen2, Canteen canteen1) {
                if (canteen1 == null || canteen2 == null  || canteen1.getTitle() == null) {
                    return 0;
                }
                return canteen1.getTitle().compareTo(canteen2.getTitle());
            }
        });
        canteens2.clear();
        for(Canteen canteen : canteens){
            if(canteen.getTitle().contains(reichenhainer)){
                canteens2.add(canteen);
            }
        }

        for(Canteen canteen : canteens){
            if(canteen.getTitle().contains(StraNa)){
                canteens2.add(canteen);
            }
        }

        canteens.clear();
        canteens = canteens2;

        CharSequence canteenTitels[] = new String[canteens.size()];
        CharSequence entryValues[] = new String[canteens.size()];
        int i = 0;
        for(Canteen canteen : canteens){
            canteenTitels[i] = canteen.getTitle();
            entryValues[i] = Integer.toString(i);
            i++;
        }

        listPreference.setEntries(canteenTitels);
        listPreference.setEntryValues(canteenTitels);
    }

    private void setSummary(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //TODO change Strings with R.string
        listPreference.setSummary(sharedPreferences.getString("canteenPrices",null));

        SharedPreferences sharedPreferences1 = PreferenceManager.getDefaultSharedPreferences(getActivity());
        listPreference1.setSummary(sharedPreferences1.getString("canteens",null));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        listPreference.setSummary(listPreference.getEntry().toString());
        listPreference1.setSummary(listPreference1.getEntry().toString());

    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
}
