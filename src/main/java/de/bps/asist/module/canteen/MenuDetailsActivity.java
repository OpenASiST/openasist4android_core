package de.bps.asist.module.canteen;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.core.util.DownloadImageTask;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.canteen.model.CanteenMenu;

public class MenuDetailsActivity extends AsistModuleActivity<CanteenMenu> {

    ListView listView;
    CanteenMenu canteenMenu;

    //Creates Canteen Activity with Details for food
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(de.bps.asist.R.layout.generic_list_view);

        listView = getView(R.id.generic_list);

        final Intent intent = getIntent();
        if(intent != null){
            final Bundle extras = intent.getExtras();
            if(extras != null){
                canteenMenu = (CanteenMenu)extras.getSerializable("1");
            }
        }
        getSupportActionBar().setTitle(canteenMenu.getTitle());
        final Map<String, List<IListItem>> objects = CanteenDatabaseManager.getInstance().refreshCanteenMenu(this, canteenMenu);
        final CategoryListViewAdapter<?extends IListItem> categoryListViewAdapter = new CategoryListViewAdapter<>(this,objects,null);
        listView.setAdapter(categoryListViewAdapter);
        LinearLayout placeholderLayout = (LinearLayout) findViewById(R.id.generic_listLayout4Images);
        getLayoutInflater().inflate(R.layout.module_canteen_dishimage, placeholderLayout);

        if(canteenMenu.getImgUrlBig() != null && !canteenMenu.getImgUrlBig().isEmpty()){
            new DownloadImageTask((ImageView) findViewById(R.id.canteenDishImageView)).execute(getString(R.string.pictureProxyUrl) + canteenMenu.getImgUrlBig() + "&size=500");
        }
    }
}
