package de.bps.asist.module.canteen;

import java.util.Comparator;

import de.bps.asist.module.canteen.model.MenuPriceDescription;

/**
 * Created by litho on 03.11.14.
 */
public class MenuPriceComparator implements Comparator<MenuPriceDescription> {

    @Override
    public int compare(MenuPriceDescription lhs, MenuPriceDescription rhs) {
        CANTEEN_PRICE p1 = CANTEEN_PRICE.parse(lhs);
        CANTEEN_PRICE p2 = CANTEEN_PRICE.parse(rhs);
        return p1.getPosition().compareTo(p2.getPosition());
    }
}
