package de.bps.asist.module.canteen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.bps.asist.R;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.module.canteen.model.Canteen;

/**
 * Not longer in use
 */

public class CanteenFragment extends AbstractASiSTFragment {

    private boolean started = false;

    private ListView menus;

    /*
     * @see androidx.core.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.generic_list_view, container, false);
        menus = (ListView) view.findViewById(R.id.generic_list);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        // canteens
        final List<Canteen> canteens = CanteenDatabaseManager.getInstance().getAllCanteens(getActivity());
        // sort
        Collections.sort(canteens, new Comparator<Canteen>() {
            @Override
            public int compare(Canteen canteen1, Canteen canteen2) {
                if (canteen1 == null || canteen2 == null  || canteen1.getTitle() == null) {
                    return 0;
                }
                return canteen1.getTitle().compareTo(canteen2.getTitle());
            }
        });
        AbstractGenericListCallback cb = new AbstractGenericListCallback(getActivity(), SingleCanteenActivity.class);
        if (!started && canteens.size() == 1) {
            cb.doAction(canteens.get(0));
        } else if (!canteens.isEmpty()) {
            // get configured canteen
            final GenericListViewAdapter<Canteen> adapter = new GenericListViewAdapter<>(getActivity(), canteens, cb);
            this.menus.setAdapter(adapter);
        }
        started = true;
    }

}
