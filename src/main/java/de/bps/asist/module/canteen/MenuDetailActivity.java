package de.bps.asist.module.canteen;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.core.util.DownloadImageTask;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.canteen.model.CanteenMenu;

public class MenuDetailActivity extends AsistModuleActivity<CanteenMenu> {

    private ListView menus;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generic_list_view);

        menus = getView(R.id.generic_list);

        final CanteenMenu selected = getSelectedItem();
        getSupportActionBar().setTitle(selected.getTitle());
        final Map<String, List<IListItem>> objects = CanteenDatabaseManager.getInstance().refreshCanteenMenu(this, selected);
        final CategoryListViewAdapter<? extends IListItem> clva = new CategoryListViewAdapter<>(
                this, objects, null);
        menus.setAdapter(clva);
        LinearLayout placeholderLayout = (LinearLayout) findViewById(R.id.generic_listLayout4Images);
        getLayoutInflater().inflate(R.layout.module_canteen_dishimage, placeholderLayout);

        if (selected.getImgUrlBig() != null && !selected.getImgUrlBig().isEmpty()) {
            new DownloadImageTask((ImageView) findViewById(R.id.canteenDishImageView)).execute(getString(R.string.pictureProxyUrl) + selected.getImgUrlBig() + "&size=500");
        }
    }
}
