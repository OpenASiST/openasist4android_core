package de.bps.asist.module.canteen.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistList;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IListItem;

/**
 * 
 * @author bja
 * 
 * @date 24.04.2014
 * 
 */
@DatabaseTable
@AsistItemConfiguration(showDescription = false)
public class Canteen extends AbstractDatabaseObject implements IListItem {

	private static final long serialVersionUID = 6682458085432748523L;

	@DatabaseField
	@JsonProperty(value = "id")
	private long canteenId;

	@DatabaseField
	@AsistTitle
	private String title;

	@ForeignCollectionField(eager = true)
	@AsistList
	private Collection<CanteenMenu> menuItems;

	public long getCanteenId() {
		return canteenId;
	}

	public void setCanteenId(final long canteenId) {
		this.canteenId = canteenId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public Collection<CanteenMenu> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(final Collection<CanteenMenu> menuItems) {
		this.menuItems = menuItems;
	}

}
