package de.bps.asist.module.canteen;

import de.bps.asist.R;
import de.bps.asist.core.ASiSTApplication;
import de.bps.asist.module.settings.model.ListSettingsItem;
import de.bps.asist.module.settings.model.SettingsConfig;
import de.bps.asist.module.settings.model.SettingsItem;
import de.bps.asist.module.settings.model.SettingsSection;

/**
 * Created by litho on 30.10.14.
 * Settings for Canteen Module, containing price information as well as preferred canteen information
 * Not longer used
 */
public class CanteenSettings extends SettingsConfig {

    public static final String PREFERENCE_PRICE = "module_canteen_price";
    public static final String PREFERENCE_CANTEEN = "module_canteen_canteen";

    public CanteenSettings(){
        Integer numberOfPriceCategories = ASiSTApplication.getContext().getResources().getInteger(R.integer.number_of_price_categories);
        SettingsSection section = new SettingsSection(R.string.module_canteen_name);
        addSection(section);
        ListSettingsItem priceItem = new ListSettingsItem(R.string.module_canteen_prices);
        priceItem.setKey(PREFERENCE_PRICE);
        int[] values = new int[numberOfPriceCategories.intValue()];
        String[] entries = new String[numberOfPriceCategories.intValue()];
        int counter = 0;
        for(CANTEEN_PRICE price: CANTEEN_PRICE.values()){
            if (counter == numberOfPriceCategories) {
                break;
            }
            entries[counter] = price.toString();
            values[counter] = price.getI18n();
            counter++;
        }
        priceItem.setKeys(entries);
        priceItem.setEntryValues(values);
        if (entries.length > 0) {
            priceItem.setDefaultValue(entries[0]);
        }
        section.addItem(priceItem);

        SettingsItem canteenItem = new ListSettingsItem(R.string.canteen_settings_canteens);
        canteenItem.setKey(PREFERENCE_CANTEEN);
//        section.addItem(canteenItem);
    }

}
