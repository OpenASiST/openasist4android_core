package de.bps.asist.module.canteen;

import android.content.Context;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.preferences.ASiSTPreferenceManager;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.module.canteen.model.Additive;
import de.bps.asist.module.canteen.model.Canteen;
import de.bps.asist.module.canteen.model.CanteenList;
import de.bps.asist.module.canteen.model.CanteenMenu;
import de.bps.asist.module.canteen.model.MenuPriceDescription;
import de.bps.asist.module.feeds.model.FeedItem;
import de.bps.asist.module.start.AsistStartActivity;
import de.bps.asist.module.start.AsistStartModule;
import de.bps.asist.module.start.tree.ASiSTTreeItem;

import static de.bps.asist.module.canteen.CanteenModule.PRICE_EMPLOYEE;
import static de.bps.asist.module.canteen.CanteenModule.PRICE_GUEST;
import static de.bps.asist.module.canteen.CanteenModule.PRICE_PUPIL;
import static de.bps.asist.module.canteen.CanteenModule.PRICE_STUDENT;

/**
 * Created by bja on 22.01.18.
 */

public class CanteenDatabaseManager {
    private static final CanteenDatabaseManager ourInstance = new CanteenDatabaseManager();

    private final ReentrantLock lock = new ReentrantLock();

    public static CanteenDatabaseManager getInstance() {
        return ourInstance;
    }

    private CanteenDatabaseManager() {
    }

    public List<CanteenMenu> getAllMenusToday(Context context){
        List<CanteenMenu> result = new ArrayList<>();
        lock.lock();
        try {
            DatabaseManager<Canteen> canteenDB = new DatabaseManager<>(context, Canteen.class);
            List<Canteen> allCanteens = canteenDB.getAll();
            for(Canteen canteen: allCanteens){
                final Collection<CanteenMenu> menuItems = canteen.getMenuItems();
                for(CanteenMenu menu: menuItems){
                    if(isToday(menu)){
                        result.add(menu);
                    }
                }
            }
        } finally {
            lock.unlock();
        }
        return result;
    }

    private boolean isToday(CanteenMenu menu) {
        Calendar mCal = Calendar.getInstance();
        mCal.setTime(menu.getDate());
        Calendar nCal = Calendar.getInstance();
        return mCal.get(Calendar.YEAR) == nCal.get(Calendar.YEAR) && mCal.get(Calendar.DAY_OF_YEAR) == nCal.get(Calendar.DAY_OF_YEAR);
    }

    public Map<String, List<IListItem>> refreshCanteenMenu(Context context, CanteenMenu selected) {
        final Map<String, List<IListItem>> objects;
        lock.lock();
        try {
            final DatabaseManager<Canteen> daoCanteen = new DatabaseManager<>(context, Canteen.class);
            Canteen canteen = daoCanteen.getObject("canteenId", selected.getCanteen().getCanteenId());
            Collection<CanteenMenu> menus = canteen.getMenuItems();
            CanteenMenu menu = null;
            String hash2 = selected.getHashId();
            for (CanteenMenu canteenMenu : menus) {
                String hash1 = canteenMenu.getHashId();
                if (hash1.equals(hash2)) {
                    menu = canteenMenu;
                    break;
                }
            }
            if (menu == null) {
                objects = createMap(selected, context);
            } else {
                objects = createMap(menu, context);
            }
        } finally {
            lock.unlock();
        }
        return objects;
    }

    private Map<String, List<IListItem>> createMap(final CanteenMenu menu, Context context) {
        final Map<String, List<IListItem>> result = new HashMap<>();

        Collection<Additive> additives = menu.getAdditives();
        final List<IListItem> addList = new ArrayList<IListItem>(additives);
        result.put(context.getResources().getString(R.string.module_canteen_additives), addList);

        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.GERMANY);
        final List<IListItem> mpgList = new ArrayList<>();
        int counter = 0;
        List<MenuPriceDescription> mpg = new ArrayList<>(menu.getPrices());
        Collections.sort(mpg, new MenuPriceComparator());
        for (MenuPriceDescription shortName : mpg) {
            int title = findPrice(shortName);
            String price = nf.format(shortName.getPrice());
            if (shortName.getPrice().equals(0.0f)) {
                price = "";
            }
            ASiSTDetailDescription desc = new ASiSTDetailDescription(title, price, counter, context.getResources().getString(R.string.module_canteen_prices));
            mpgList.add(desc);
            counter++;
        }
        result.put(context.getResources().getString(R.string.module_canteen_prices), mpgList);

        return result;
    }

    private int findPrice(MenuPriceDescription mpd) {
        String shortName = mpd.getTitle();
        switch (shortName) {
            case PRICE_PUPIL:
                return R.string.module_canteen_price_pupil;
            case PRICE_STUDENT:
                return R.string.module_canteen_price_student;
            case PRICE_EMPLOYEE:
                return R.string.module_canteen_price_employee;
            case PRICE_GUEST:
                return R.string.module_canteen_price_guest;
            default:
                return -1;
        }
    }

    public List<Canteen> getAllCanteens(Context context) {
        final List<Canteen> canteens;
        final DatabaseManager<Canteen> db = new DatabaseManager<>(context, Canteen.class);
        canteens = db.getAll();
        return canteens;
    }

    public void getAllMenus(CanteenList parsed, Context context, CanteenStartViewConfig startTreeConfig) {
        lock.lock();
        try {
            final List<Canteen> items = new ArrayList<>();
            if (parsed != null) {
                items.addAll(parsed.getItems());
            }
            // canteen
            final DatabaseManager<Canteen> db = new DatabaseManager<>(context, Canteen.class);
            final DatabaseManager<CanteenMenu> dbm = new DatabaseManager<>(context, CanteenMenu.class);
            final DatabaseManager<MenuPriceDescription> dbp = new DatabaseManager<>(context, MenuPriceDescription.class);
            final DatabaseManager<Additive> adb = new DatabaseManager<>(context, Additive.class);

            // delete all menus
            dbm.deleteAll();

            for (final Canteen canteen : items) {
                if (canteen != null && canteen.getMenuItems() == null) {
                    canteen.setMenuItems(new ArrayList<CanteenMenu>());
                }
                Canteen reference;
                Canteen saved = db.getObject("canteenId", canteen.getCanteenId());
                if(saved != null) {
                    db.delete(saved);
                }
                db.saveObject(canteen);
                reference = canteen;
                for (final CanteenMenu menu : canteen.getMenuItems()) {
                    if (menu.getTitle() == null || "".equals(menu.getTitle())) {
                        ////Log.w(TAG, "could not save canteen menuItem because title is empty or null: "+menu);
                        continue;
                    }
                    //CanteenMenu fromDB = findMenu(dbm, menu, reference);
                    //if(fromDB != null){
                    ////Log.d(TAG, "do not save canteen item since it is already in database: "+fromDB);
                    //    continue;
                    //}
                    menu.setCanteen(reference);
                    dbm.saveObject(menu);
                    for (final MenuPriceDescription price : menu.getPrices()) {
                        price.setCanteenMenu(menu);
                        dbp.saveObject(price);
                    }
                    for (final Additive a : menu.getAdditives()) {
                        a.setMenu(menu);
                        adb.saveObject(a);
                    }
                }
            }
            db.release();
            dbm.release();
            dbp.release();
            adb.release();
            startTreeConfig.update(context);
            ASiSTTreeItem<FeedItem> root = startTreeConfig.getRootNode(context);
            String moduleName = context.getString(R.string.module_canteen_name);
            ASiSTPreferenceManager apm = new ASiSTPreferenceManager();
            boolean enabled = apm.getSetting(context, AsistStartActivity.START_VIEW_TREE + moduleName);
            root.setEnabled(enabled);
            AsistStartModule.update(root);
        } finally {
            lock.unlock();
        }
    }
}
