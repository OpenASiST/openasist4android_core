package de.bps.asist.module.canteen.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IListItem;

/**
 * 
 * @author bja
 * 
 * @date 24.04.2014
 * 
 */
@DatabaseTable
@AsistItemConfiguration
public class MenuPriceDescription extends AbstractDatabaseObject implements IListItem {

	private static final long serialVersionUID = -7062028688585928323L;

	@DatabaseField
	private String portion;
	@DatabaseField
	private Float price;
	@DatabaseField

	private String title;
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private CanteenMenu canteenMenu;
    @AsistTitle
    private String internalTitle;

	public String getPortion() {
		return portion;
	}

	public void setPortion(final String portion) {
		this.portion = portion;
	}

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public CanteenMenu getCanteenMenu() {
		return canteenMenu;
	}

	public void setCanteenMenu(final CanteenMenu canteenMenu) {
		this.canteenMenu = canteenMenu;
	}

    public String getInternalTitle() {
        return internalTitle;
    }

    public void setInternalTitle(String internalTitle) {
        this.internalTitle = internalTitle;
    }
}
