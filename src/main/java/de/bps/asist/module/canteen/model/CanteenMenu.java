package de.bps.asist.module.canteen.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;
import java.util.Date;

import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistImage;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.core.model.parser.OnlyDateParser;

/**
 * @author bja
 * @date 24.04.2014
 */
@DatabaseTable
@AsistItemConfiguration
public class CanteenMenu extends AbstractDatabaseObject implements IListItem {

    private static final long serialVersionUID = 7418784513768973765L;

    @DatabaseField
    @AsistTitle
    private String title;
    @DatabaseField
    private String category;
    @DatabaseField
    @AsistDescription
    @JsonProperty(value = "desc")
    private String description;
    @ForeignCollectionField(eager = true)
    private Collection<MenuPriceDescription> prices;
    @ForeignCollectionField(eager = true)
    private Collection<Additive> additives;
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Canteen canteen;

    @JsonDeserialize(using = OnlyDateParser.class)
    @DatabaseField
    private Date date;

    @DatabaseField
    @AsistImage
    private String imgUrlBig;

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Collection<MenuPriceDescription> getPrices() {
        return prices;
    }

    public void setPrices(final Collection<MenuPriceDescription> prices) {
        this.prices = prices;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Canteen getCanteen() {
        return canteen;
    }

    public void setCanteen(final Canteen canteen) {
        this.canteen = canteen;
    }

    public Collection<Additive> getAdditives() {
        return additives;
    }

    public void setAdditives(final Collection<Additive> additives) {
        this.additives = additives;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public String getImgUrlBig() {
        return imgUrlBig;
    }

    public void setImgUrlBig(String imgUrlBig) {
        this.imgUrlBig = imgUrlBig;
    }

    @Override
    public String toString() {
        return title + " at date " + date;
    }

    public String getHashId() {
        return "hashId-"+getDate().getTime()+"-"+getCanteen().getCanteenId()+toString();
    }
}
