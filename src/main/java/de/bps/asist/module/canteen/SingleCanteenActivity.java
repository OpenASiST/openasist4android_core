package de.bps.asist.module.canteen;

import android.app.AlertDialog;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import androidx.appcompat.app.ActionBar;
import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.canteen.model.Canteen;
import de.bps.asist.module.canteen.model.CanteenMenu;

/**
 * Created by litho on 04.11.14.
 * View for a single Canteen
 * No longer needed
 */
public class SingleCanteenActivity extends AsistModuleActivity<Canteen> {

    private static final String TAG = SingleCanteenActivity.class.getSimpleName();

    private static final String DATE_FORMAT = "ddMMyyyy";

    private ListView menus;
    private ImageButton prevButton;
    private ImageButton nextButton;
    private TextView titleView;
    private Calendar currentCal = Calendar.getInstance();
    private Map<String, List<CanteenMenu>> menuMap = new TreeMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.canteen_activity);
        menus = getView(R.id.menus);
        ColorFilter cf = new LightingColorFilter(R.color.defaultColor, R.color.defaultColor);



        prevButton = getView(R.id.button_back);
        prevButton.setOnClickListener(new NextModeListener());
        prevButton.setEnabled(false);
        prevButton.getBackground().setColorFilter(cf);

        nextButton = getView(R.id.button_next);
        nextButton.setOnClickListener(new NextModeListener());
        nextButton.getBackground().setColorFilter(cf);

        titleView = getView(R.id.week_title);
        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
        String date = df.format(currentCal.getTime());
        titleView.setText(date);
        final Canteen selectedCanteen = getSelectedItem();
        String canteenName = selectedCanteen.getTitle();
        ActionBar ab = getSupportActionBar();
        ab.setTitle(getString(R.string.module_canteen_name) + " - " + canteenName);
        DatabaseManager<Canteen> canteenDB = new DatabaseManager<>(this, Canteen.class);

        int size = canteenDB.getAll().size();
        //Log.d(TAG, "got " + size + " canteens");

        final Canteen reloadedCanteen = canteenDB.refresh(selectedCanteen);
        if (reloadedCanteen == null) {
            return;
        }
        // get menus
        final Collection<CanteenMenu> menuItems = reloadedCanteen.getMenuItems();
        Calendar cal = Calendar.getInstance();
        for (final CanteenMenu menuItem : menuItems) {
            cal.setTime(menuItem.getDate());
            String clean = cleanDate(cal);
            List<CanteenMenu> list = menuMap.get(clean);
            if (list == null) {
                list = new ArrayList<>();
                menuMap.put(clean, list);
            }
            list.add(menuItem);
        }
        try{
            setAdapter();
        } catch (Exception e){
            AlertDialog.Builder builder = new AlertDialog.Builder(SingleCanteenActivity.this);
            builder.setMessage(R.string.serverErrorText)
                    .setTitle(R.string.serverError);
            builder.setPositiveButton("OK",null);
            builder.create();
            builder.show();
        }
    }

    private void setAdapter() {
        List<CanteenMenu> todayMenus = menuMap.get(cleanDate(currentCal));
        if (todayMenus == null || todayMenus.isEmpty()) {
            ////Log.w(TAG, "could not find any menu entries for date " + currentCal.getTime());
            todayMenus = new ArrayList<>();
        }
        final CategoryListViewAdapter<CanteenMenu> adapter = new CanteenMenuAdapter(this, todayMenus);
        this.menus.setAdapter(adapter);
    }


    private String cleanDate(Calendar cal) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        return format.format(cal.getTime());
    }


    private class NextModeListener implements View.OnClickListener {

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
            if (nextButton == v) {
                prevButton.setEnabled(true);
                add(currentCal);
                Calendar ref = Calendar.getInstance();
                ref.setTime(currentCal.getTime());
                add(ref);
                if (menuMap.get(cleanDate(ref)) == null) {
                    nextButton.setEnabled(false);
                }
            } else if (prevButton == v) {
                reduce(currentCal);
                if (isToday()) {
                    prevButton.setEnabled(false);
                }
                nextButton.setEnabled(true);
            }
            String date = df.format(currentCal.getTime());
            titleView.setText(date);
            setAdapter();
        }

        private boolean isToday() {
            Calendar today = Calendar.getInstance();
            return today.get(Calendar.YEAR) == currentCal.get(Calendar.YEAR) && today.get(Calendar.DAY_OF_YEAR) == currentCal.get(Calendar.DAY_OF_YEAR);
        }

        private void add(Calendar cal) {
            int toAdd = 1;
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
                toAdd = 3;
            }
            cal.add(Calendar.DAY_OF_WEEK, toAdd);
        }

        private void reduce(Calendar cal) {
            int toAdd = -1;
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                toAdd = -3;
            }
            cal.add(Calendar.DAY_OF_WEEK, toAdd);
        }
    }
}
