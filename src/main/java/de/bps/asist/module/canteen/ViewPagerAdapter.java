package de.bps.asist.module.canteen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import de.bps.asist.R;

/**
 * Adapter for TabLayout in MensaFragment
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> FragmentList = new ArrayList<>();
    private final List<String> TitelList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentList.get(position);
    }

    @Override
    public int getCount() {
        return TitelList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TitelList.get(position);
    }

    public void AddFragment(Fragment fragment,String titel){
        FragmentList.add(fragment);
        TitelList.add(titel);
    }

    public View getCustomView(Context context,int pos){
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tab,null,false);
        TextView textView1 = (TextView)view.findViewById(R.id.first_tab_text_id);
        TextView textView2 = (TextView)view.findViewById(R.id.second_tab_text_id);
        String text1 = TitelList.get(pos);

        textView1.setText(text1.substring(0,text1.indexOf(' ')));
        textView2.setText(text1.substring(text1.indexOf(' ')+1));

        return view;

    }
}
