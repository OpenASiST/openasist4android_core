package de.bps.asist.module.canteen;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.fragment.app.FragmentManager;
import de.bps.asist.R;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.start.AsistStartActivity;
import de.bps.asist.module.timetable.model.CourseDescription;

/**
 * Created by bja on 27.06.17.
 */

public class CanteenActivity extends AsistModuleActivity<CourseDescription> {

    private CanteenFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generic_fragment_activity);
        fragment = new CanteenFragment();
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.generic_fragment_container, fragment).commit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Integer layout = fragment.getMenuLayout();
        if(layout != null){
            getMenuInflater().inflate(layout, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(final MenuItem item) {
        if(fragment.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AsistStartActivity.class);
        startActivity(intent);
    }
}