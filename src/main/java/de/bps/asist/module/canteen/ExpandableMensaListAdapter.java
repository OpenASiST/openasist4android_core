package de.bps.asist.module.canteen;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.bps.asist.R;
import de.bps.asist.core.manager.image.ASiSTImageManager;
import de.bps.asist.core.util.DownloadImageTask;
import de.bps.asist.module.canteen.model.CanteenMenu;
import de.bps.asist.module.canteen.model.MenuPriceDescription;

public class ExpandableMensaListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> dataHeader; //Tittel der ExpandableList (Datum)
    private HashMap<String, List<CanteenMenu>> listDataMenus;

    public ExpandableMensaListAdapter(Context context, List<String> dataHeader, HashMap<String, List<CanteenMenu>> listDataMenus) {
        this.context = context;
        this.dataHeader = dataHeader;
        this.listDataMenus = listDataMenus;
    }

    @Override
    public int getGroupCount() {
        return this.dataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataMenus.get(this.dataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.dataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.listDataMenus.get(this.dataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitel = (String) getGroup(groupPosition);
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.generic_treeview,null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.treeViewTitle);
        TextView descView = (TextView) convertView.findViewById(de.bps.asist.R.id.generic_treeDescription);
        descView.setVisibility(View.GONE);

        textView.setText(headerTitel);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        CanteenMenu canteenMenu = (CanteenMenu) getChild(groupPosition, childPosition);
        String childText = canteenMenu.getTitle();
        String image = canteenMenu.getImgUrlBig();
        String descript = canteenMenu.getCategory();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        //TODO replace with R.string
        String price = sharedPreferences.getString("canteenPrices",null);

        List<MenuPriceDescription> menuPriceDescriptions = new ArrayList<>(canteenMenu.getPrices());

        int PriceSetting = 0;

        if(price != null){
            if(price.equals(context.getResources().getString(R.string.module_canteen_price_student))){
                PriceSetting = 0;
            }
            else if(price.equals(context.getResources().getString(R.string.module_canteen_price_employee))){
                PriceSetting = 1;
            }
            else if(price.equals(context.getResources().getString(R.string.module_canteen_price_guest))){
                PriceSetting = 2;
            }
        }

        Collections.sort(menuPriceDescriptions, new MenuPriceComparator());
        StringBuilder sb = new StringBuilder();
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.GERMANY);
        if(menuPriceDescriptions.size() > 0){
            MenuPriceDescription mp = menuPriceDescriptions.get(PriceSetting);
            if(sb.length() > 0){
                sb.append(" / ");
            }
            if(mp.getPrice() > 0.00f){
                sb.append(nf.format((mp.getPrice())));
            }
            else{
                sb.append(""); //Kein Preis vorhanden
            }
        }

        ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.menu_list_item,null);
            viewHolder = new ViewHolder();
            viewHolder.titel = (TextView)convertView.findViewById(R.id.listItemTitle);
            viewHolder.descText = (TextView)convertView.findViewById(R.id.listItemDescription);
            viewHolder.prices = (TextView)convertView.findViewById(R.id.canteen_prices);
            viewHolder.imageView = (ImageView)convertView.findViewById(R.id.listItemMainLogo);
            viewHolder.linearLayout = (LinearLayout)convertView.findViewById(R.id.listItemMainLogoWrapper);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.titel.setText(childText);
        viewHolder.descText.setText(descript);
        if(sb.toString() == null || sb.toString().isEmpty())
        {
            viewHolder.prices.setText(R.string.module_canteen_noPriceAvailable);
        }
        else{
            viewHolder.prices.setText(sb.toString());
        }

        if(image != null && !image.isEmpty()){
            //new DownloadImageTask(viewHolder.imageView).execute(canteenMenu.getImgUrlBig());
            viewHolder.linearLayout.setVisibility(View.VISIBLE);
            ASiSTImageManager.getInstance().setImage(image, viewHolder.imageView);
        }
        else{
            //new DownloadImageTask(viewHolder.imageView).execute(image);
            viewHolder.linearLayout.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    static class ViewHolder{
        TextView titel;
        TextView descText;
        TextView prices;
        ImageView imageView;
        LinearLayout linearLayout;
    }

}
