package de.bps.asist.module.canteen;

import android.content.Context;

import java.util.Collection;

import de.bps.asist.module.canteen.model.CanteenMenu;

/**
 * Created by litho on 03.11.14.
 * layouter for Menu Tree Items for Canteen Module
 */
public class CanteenTreeItemLayouter extends CanteenMenuAdapter {

    public CanteenTreeItemLayouter(Context context, Collection<CanteenMenu> objects) {
        super(context, objects);
    }
}
