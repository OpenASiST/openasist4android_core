package de.bps.asist.module.canteen;

import de.bps.asist.R;
import de.bps.asist.module.canteen.model.MenuPriceDescription;

/**
 * Created by litho on 30.10.14.
 * Configuration of Canteen Prices
 */
public enum CANTEEN_PRICE {
    STUDENT("S", 0, R.string.module_canteen_price_student), //
    EMPLOYEE("E", 1, R.string.module_canteen_price_employee), //
    GUEST("G", 2, R.string.module_canteen_price_guest);

    private String id;
    private int position;
    private int i18n;

    private CANTEEN_PRICE(String id, int position, int i18n){
        this.id = id;
        this.position = position;
        this.i18n = i18n;
    }

    public Integer getPosition() {
        return position;
    }

    public String getId() {
        return id;
    }

    public int getI18n() {
        return i18n;
    }

    public static int[] i18nValues(){
        int[] result = new int[values().length];
        int counter = 0;
        for(CANTEEN_PRICE price: values()){
            result[counter] = price.getI18n();
            counter++;
        }
        return result;
    }

    public static CANTEEN_PRICE parse(MenuPriceDescription mpg){
        for(CANTEEN_PRICE cp: values()){
            if(cp.getId().equals(mpg.getTitle())){
                return cp;
            }
        }
        return null;
    }
}
