package de.bps.asist.module.occupancy;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.gui.detail.DetailTableAdapter;
import de.bps.asist.gui.detail.DetailedTableActivity;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.occupancy.model.OccupancyDescription;
import de.bps.asist.module.occupancy.model.OccupancyDetails;
import de.bps.asist.module.occupancy.model.OccupancyTimeSpan;
import de.bps.asist.module.occupancy.model.TimeSpanDetails;

/**
 * Created by Martin Luschek.
 */
public class OccupancyDetailActivity extends DetailedTableActivity<OccupancyDescription> implements IGenericListCallback<OccupancyDescription> {

    private ListView poilistView;
    private ListView timesListView;
    private OccupancyDetails details;
    private Map<String, List<TimeSpanDetails>> timeMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void doCreate() {
        setContentView(R.layout.module_occupancy_detail_activity);
        poilistView = getView(R.id.module_occupancy_detail_poi_list);
        timesListView = getView(R.id.module_occupancy_detail_time_list);

        OccupancyDescription occupancy = getSelectedItem();

//        //Log.i(OccupancyDetails.class.getSimpleName(), "times.size " + occupancy.getTimes().size());
        details = new OccupancyDetails(occupancy, this);

        Map<String, List<ASiSTDetailDescription>> detailMap = createDetailsMap(details);
        final DetailTableAdapter adapter = new DetailTableAdapter(this, detailMap, null);
        poilistView.setAdapter(adapter);

        Collection<OccupancyTimeSpan> timeSpan = occupancy.getTimes();
        for (final OccupancyTimeSpan tsd : timeSpan) {
            TimeSpanDetails times = new TimeSpanDetails(tsd);
            List<TimeSpanDetails> list = timeMap.get(times.getDayOfWeek());
            if (list == null) {
                list = new ArrayList<>();
                timeMap.put(times.getDayOfWeek(), list);
            }
            list.add(times);
        }

        final CategoryListViewAdapter<TimeSpanDetails> timeAdapter = new CategoryListViewAdapter<>(this, timeMap, null);
        timesListView.setAdapter(timeAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_occupany_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void doAction(OccupancyDescription o) {

    }
}
