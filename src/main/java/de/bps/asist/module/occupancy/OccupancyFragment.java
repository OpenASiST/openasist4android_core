package de.bps.asist.module.occupancy;


import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.module.occupancy.model.OccupancyDescription;


public class OccupancyFragment extends AbstractASiSTFragment {

    private static final String TAG = OccupancyFragment.class.getSimpleName();

    private ListView occupancyList;
    private Button searchButton;
    private TextView searchTextView;
    private TextView resultCount;

    private DatabaseManager<OccupancyDescription> db = new DatabaseManager<>(getActivity(), OccupancyDescription.class);
    private List<OccupancyDescription> items;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.module_occupancy_fragment, container, false);
        occupancyList = (ListView) view.findViewById(R.id.moduleOccupancyFragmentListView);
        searchButton = (Button) view.findViewById(R.id.moduleOccupancySearchButton);
        searchTextView = (TextView) view.findViewById(R.id.moduleOccupancySearchInputField);
        resultCount = (TextView) view.findViewById(R.id.moduleOccupancySearchResultsCount);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            searchButton.setVisibility(View.GONE);
        } else {
            searchButton.setOnClickListener(new OnSearchButtonClickHandler());
        }

        searchTextView.setOnEditorActionListener(new OnSearchButtonEnterHandler());
    }

    private void search(final String query) {
        occupancyList.setVisibility(View.GONE);
        resultCount.setText(translate(R.string.module_telephone_result_loading));
        List<OccupancyDescription> searchOccu = new ArrayList<>();
        items = db.getAll();
        db.release();
        String res;

        for (OccupancyDescription occu : items) {
            if (occu.getTitle().equalsIgnoreCase(query)) {
                searchOccu.add(occu);
            } else if (occu.getTitle().contains(query.toUpperCase())) {
                if (!searchOccu.contains(occu)) {
                    searchOccu.add(occu);
                }
            }
        }
        if (searchOccu.size() == 0) {
            res = translate(R.string.module_telephone_no_results);
        } else {
            res = translate(R.string.module_telephone_result_results, searchOccu.size());
        }
        resultCount.setText(res);
        Calendar cal = Calendar.getInstance();
        Integer today = cal.get(Calendar.DAY_OF_WEEK);

        final OccupancyAdapter adapter = new OccupancyAdapter(getActivity(), searchOccu, today - 1);

        occupancyList.setAdapter(adapter);
        occupancyList.setVisibility(View.VISIBLE);
    }

    private final class OnSearchButtonClickHandler implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            String qs = "";
            qs = searchTextView.getText().toString().trim();
            search(qs);
        }
    }

    private final class OnSearchButtonEnterHandler implements TextView.OnEditorActionListener {

        @Override
        public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
            String qs = "";
            qs = searchTextView.getText().toString().trim();
            search(qs);
            return false;
        }
    }
}
