package de.bps.asist.module.occupancy.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.bps.asist.module.poi.PoiModule;
import de.bps.asist.module.poi.model.PoiItem;

/**
 * Created by Martin Luschek on 13.12.2014.
 * Map for getting Occupancy Information based on Days of a Week.
 */
public class OccupancyMap {

    private int currentDay;

    private Context context;

    private Map<String, List<OccupancyDescription>> occuMap = new TreeMap<>();

    public OccupancyMap(Context context, Collection<OccupancyDescription> occus) {
        this.context = context;
        for (OccupancyDescription o : occus) {
            addOccupancy(o);
        }
    }

    private void addOccupancy(OccupancyDescription occupancy) {
        //was: occupancy.getReference()
        PoiItem poi = PoiModule.findItemByPoiId(context, occupancy.getReference());
        if (poi == null) {
            //Log.e(OccupancyMap.class.getSimpleName(), "poi null");
        } else {
            addOccuToMap(poi.getTitle(), occupancy);
        }

    }

    private void addOccuToMap(String buildingTitle, OccupancyDescription occu) {
        List<OccupancyDescription> list = occuMap.get(buildingTitle);
        if (list == null) {
            list = new ArrayList<>();
            occuMap.put(buildingTitle, list);
        }
        list.add(occu);
    }

    public Map<String, List<OccupancyDescription>> getOccupancyForToday(Integer today) {
        Map<String, List<OccupancyDescription>> map = new LinkedHashMap<>();
        ;
        List<OccupancyDescription> occupancyForToday;
        for (String keys : occuMap.keySet()) {
            occupancyForToday = new ArrayList<>();
            List<OccupancyDescription> occus = occuMap.get(keys);
            for (OccupancyDescription occupancy : occus) {
                if (occupancy.getTimesOfToday(today).size() > 0) {
                    occupancyForToday.add(occupancy);
                }
                map.put(keys, occupancyForToday);
            }

        }
        return map;
    }

    public List<String> getOccuMapKeySet() {
        List<String> keys = new ArrayList<>();
        for (String k : occuMap.keySet()) {
            keys.add(k);
        }
        return keys;
    }

    public List<OccupancyDescription> get(String key) {
        return occuMap.get(key);
    }

}
