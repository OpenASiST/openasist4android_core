package de.bps.asist.module.occupancy;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import de.bps.asist.R;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.occupancy.model.OccupancyDescription;
import de.bps.asist.module.occupancy.model.OccupancyMap;
import de.bps.asist.module.occupancy.model.OccupancyTimeSpan;

/**
 * Created by Martin Luschek on 13.12.2014.
 */
public class OccupancyAdapter extends CategoryListViewAdapter<OccupancyDescription> {

    private static final String TAG = OccupancyAdapter.class.getCanonicalName();
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private Context context;

    public OccupancyAdapter(final Context context, Collection<OccupancyDescription> list, Integer today) {
        this(context, createMap(context, list, today));
        this.context = context;
    }

    public OccupancyAdapter(final Context context, final Map<String, List<OccupancyDescription>> map) {
        super(context, map, new AbstractGenericListCallback<OccupancyDescription>(context, OccupancyDetailActivity.class));
    }

    private static Map<String, List<OccupancyDescription>> createMap(Context context, Collection<OccupancyDescription> occupancy, Integer today) {
        OccupancyMap occuMap = new OccupancyMap(context, occupancy);
        final Map<String, List<OccupancyDescription>> map = new LinkedHashMap<>();
        Map<String, List<OccupancyDescription>> occuToday = occuMap.getOccupancyForToday(today);
        for (String key : occuToday.keySet()) {
            List<OccupancyDescription> list = occuToday.get(key);
            if (list.size() > 0) {
                map.put(key, list);
            }
        }
        return map;
    }

    @Override
    protected LinearLayout addItemView(final int position, final LayoutInflater inflater) {
        final OccupancyDescription item = (OccupancyDescription) getItem(position);

        final LinearLayout row = (LinearLayout) inflater.inflate(R.layout.occupancy_time_layout, null);
        addRoom(item, row);
        addFreeTime(item, row);
        addCallback(item, row, getCallback());
        return row;
    }

    private void addRoom(OccupancyDescription occu, LinearLayout row) {
        final String room = occu.getTitle();
        final TextView roomView = (TextView) row.findViewById(R.id.roomNameTextView);
        roomView.setText(room);
    }

    private void addFreeTime(OccupancyDescription occu, LinearLayout row) {
        String time = getTime(occu);
        SpannableStringBuilder sb = new SpannableStringBuilder();
        final ForegroundColorSpan red_fcs = new ForegroundColorSpan(context.getResources().getColor(R.color.red));
        final ForegroundColorSpan green_fcs = new ForegroundColorSpan(context.getResources().getColor(R.color.green));
        final TextView timeView = (TextView) row.findViewById(R.id.remainingTimeTextView);
        if (time.contains("frei")) {
            sb.append(time);
            sb.setSpan(green_fcs, 0, time.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        } else {
            sb.append(time);
            sb.setSpan(red_fcs, 0, time.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        timeView.setText(sb.toString());
    }

    private String getTime(OccupancyDescription occu) {
        final StringBuilder timeSpan = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        final Collection<OccupancyTimeSpan> times = occu.getTimes();
        String curTime = timeFormat.format(cal.getTime());
        for (OccupancyTimeSpan time : times) {
            String start = timeFormat.format(time.getStart());
            String end = timeFormat.format(time.getEnd());
            if (start.compareTo(curTime) < 0 && end.compareTo(curTime) < 0) { //start und end time kleiner als curtime
                continue;
            } else if (start.compareTo(curTime) > 0) {
                if (calculateDifferenceOfTimespan(time.getStart(), time.getEnd()) <= 90) {
                    long remaining = calculateDifferenceFromCurrentTime(time.getStart());
                    timeSpan.append(remaining);
                    timeSpan.append(" min frei");
                    break;
                } else {
                    timeSpan.append("frei");
                }
            } else {
                long remaining = calculateDifferenceFromCurrentTime(time.getEnd());
                if (remaining <= 180) {
                    timeSpan.append(remaining);
                    timeSpan.append(" min besetzt");
                    break;
                } else {
                    timeSpan.append("besetzt");
                }
            }
        }
        return timeSpan.toString();
    }

    private long calculateDifferenceFromCurrentTime(Date time) {
        Calendar cal = Calendar.getInstance();
        Calendar currentCal = Calendar.getInstance();
        String hh = timeFormat.format(time).substring(0, 2);
        String mm = timeFormat.format(time).substring(3, 5);
        cal.set(currentCal.get(Calendar.YEAR), currentCal.get(Calendar.MONTH), currentCal.get(Calendar.DAY_OF_MONTH), Integer.parseInt(hh), Integer.parseInt(mm));
        long currentTimeMilli = currentCal.getTime().getTime();
        long timeEndMilli = cal.getTime().getTime();
        long diff = timeEndMilli - currentTimeMilli;
        return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
    }

    private long calculateDifferenceOfTimespan(Date start, Date end) {
        long diff = end.getTime() - start.getTime();
//        //Log.i(TAG,"Timespan diff: "+ TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS));
        return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
    }
}
