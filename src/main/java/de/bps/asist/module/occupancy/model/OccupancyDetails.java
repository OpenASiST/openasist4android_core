package de.bps.asist.module.occupancy.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.annotation.AsistList;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.poi.PoiModule;
import de.bps.asist.module.poi.model.PoiItem;

/**
 * Created by Martin Luschek on 12.12.2014.
 */
public class OccupancyDetails implements IListItem, IGenericListCallback<OccupancyDescription> {

    @ASiSTDetail(i18nKey = "module_occupancy_detail_room", position = 1)
    private String title;

    @ASiSTDetail(i18nKey = "module_occupancy_detail_building", position = 2)
    private String buildingTitle;

    private String reference;

    @AsistList
    private List<TimeSpanDetails> timeSpan = new ArrayList<>();

    public OccupancyDetails(OccupancyDescription occu, Context context) {
        this.title = occu.getTitle();

        PoiItem building = PoiModule.findItemByPoiId(context, occu.getReference());
        this.buildingTitle = building.getTitle();

        this.reference = String.valueOf(occu.getReference());

        for (OccupancyTimeSpan times : occu.getTimes()) {
            TimeSpanDetails tsd = new TimeSpanDetails(times);
            timeSpan.add(tsd);
        }
    }

    public OccupancyDetails() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBuildingTitle() {
        return buildingTitle;
    }

    public void setBuildingTitle(String buildingTitle) {
        this.buildingTitle = buildingTitle;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<TimeSpanDetails> getTimes() {
        return timeSpan;
    }

    public void setTimes(List<TimeSpanDetails> times) {
        this.timeSpan = times;
    }

    @Override
    public Long getId() {
        return 0l;
    }

    @Override
    public void doAction(OccupancyDescription o) {

    }
}
