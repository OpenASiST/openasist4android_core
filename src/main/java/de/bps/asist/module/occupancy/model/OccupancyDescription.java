package de.bps.asist.module.occupancy.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistList;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IList;
import de.bps.asist.core.model.IListItem;

/**
 * Created by Martin Luschek on 27.10.2014.
 */

@AsistItemConfiguration(showDescription = true)
@DatabaseTable
public class OccupancyDescription extends AbstractDatabaseObject implements IList, IListItem {


    private Room room;

    @DatabaseField
    @AsistTitle
    private String title;

    @DatabaseField
    private Integer reference;

    @DatabaseField
    private int poiid;

    @ForeignCollectionField(eager = true)
    @AsistList
    private Collection<OccupancyTimeSpan> times;

    public Room getRoom() {
        return room;
    }

    public void setRoom(final Room room) {
        this.room = room;
        this.reference = room.getReference();
        this.title = room.getTitle();
        this.poiid = room.getPoiid();
    }

    public int getPoiid() {
        return poiid;
    }

    public void setPoiid(final int poiid) {
        this.poiid = poiid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }

    public Collection<OccupancyTimeSpan> getTimes() {
        return times;
    }

    public void setTimes(final Collection<OccupancyTimeSpan> times) {
        this.times = times;
    }

    @Override
    public List<OccupancyTimeSpan> getItems() {
        return (List<OccupancyTimeSpan>) times;
    }

    public List<OccupancyTimeSpan> getTimesOfToday(Integer today) {
        List<OccupancyTimeSpan> allOfToday = new ArrayList<>();
        for (OccupancyTimeSpan time : times) {
            if (time.getDayOfWeek() == today) {
                allOfToday.add(time);
            }
        }
        return allOfToday;
    }
}
