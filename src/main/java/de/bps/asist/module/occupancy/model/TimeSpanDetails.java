package de.bps.asist.module.occupancy.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.model.IListItem;

/**
 * Created by Martin Luschek on 04.12.2014.
 */
@AsistItemConfiguration(showDescription = true)
public class TimeSpanDetails implements IListItem {

    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

    @ASiSTDetail(i18nKey = "module_officehours_timespandetail_timespan", position = 2)
    @AsistDescription
    private String timeSpan;

    @ASiSTDetail(i18nKey = "module_officehours_timespandetail_dayofweek", position = 1)
    @AsistTitle
    private String dayOfWeek;

    @ASiSTDetail(i18nKey = "module_officehours_timespandetail_kw", position = 3)
    private int weekNumber;

    @ASiSTDetail(i18nKey = "module_officehours_timespandetail_mode", position = 4)
    private String mode;

    public TimeSpanDetails(OccupancyTimeSpan time) {
        setDayOfWeek(time.getDayOfWeek());
        setTimeSpan(cleanDate(time.getStart()), cleanDate(time.getEnd()));
        this.weekNumber = time.getWeekNumber();
        setMode(time.getMode());
    }

    @Override
    public Long getId() {
        return 0L;
    }

    public String getTimeSpan() {
        return timeSpan;
    }

    public void setTimeSpan(final String start, final String end) {
        this.timeSpan = start + "Uhr bis " + end + " Uhr";
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(final int dayOfWeek) {
        switch (dayOfWeek) {
            case 1: {
                this.dayOfWeek = "Montag";
                break;
            }
            case 2: {
                this.dayOfWeek = "Dienstag";
                break;
            }
            case 3: {
                this.dayOfWeek = "Mittwoch";
                break;
            }
            case 4: {
                this.dayOfWeek = "Donnerstag";
                break;
            }
            case 5: {
                this.dayOfWeek = "Freitag";
                break;
            }
            case 6: {
                this.dayOfWeek = "Samstag";
                break;
            }
            case 7: {
                this.dayOfWeek = "Sonntag";
                break;
            }
        }
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(int mode) {
        switch (mode) {
            case 1: {
                this.mode = "Sondertermin";
                break;
            }
            case 10: {
                this.mode = "wöchentlich";
                break;
            }
            case 20: {
                this.mode = "Vorlesungsfreie Zeit";
                break;
            }
            case 30: {
                this.mode = "ungerade Wochen";
                break;
            }
            case 40: {
                this.mode = "gerade Wochen";
                break;
            }
        }
    }

    private String cleanDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return timeFormat.format(cal.getTime());
    }

}
