package de.bps.asist.module.occupancy;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import androidx.fragment.app.Fragment;
import de.bps.asist.BuildConfig;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.occupancy.model.OccupancyDescription;
import de.bps.asist.module.occupancy.model.OccupancyList;
import de.bps.asist.module.occupancy.model.OccupancyTimeSpan;
import de.bps.asist.module.occupancy.model.Room;

/**
 * Created by Martin Luschek on 09.12.2014.
 */
public class OccupancyModule extends AbstractAsistModule {

    private static final String TAG = OccupancyModule.class.getSimpleName();
    private static final Lock lock = new ReentrantLock();

    public OccupancyModule() {
        //TODO: Connect this with CustomerSettings
        setEnabled(true);
    }

    @Override
    public int getName() {
        return R.string.module_occupancy_name;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_pool;
    }

    @Override
    public Fragment getInitialFragment() {
        return new OccupancyFragment();
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        final List<Class<? extends AbstractDatabaseObject>> result = new ArrayList<>();
        result.add(OccupancyDescription.class);
        result.add(OccupancyTimeSpan.class);
        return result;
    }

    @Override
    public void updateData(final Context context) {
        super.updateData(context);
        lock.lock();
        //Log.i(TAG, "Locking db for updating occupancy");
        try {
            final DatabaseManager<OccupancyDescription> db = new DatabaseManager<>(context, OccupancyDescription.class);
            final DatabaseManager<OccupancyTimeSpan> dbTime = new DatabaseManager<>(context, OccupancyTimeSpan.class);

            db.deleteAll();
            dbTime.deleteAll();

            final String url = context.getResources().getString(R.string.rootUrl) + "/occupancy/" + context.getResources().getString(R.string.institutionName) + "/all";
            final OccupancyCallback cb = new OccupancyCallback(context);
            ASiSTParser.getInstance().parse(url, OccupancyList.class, cb);
        } catch (Exception e) {
            //Log.e(TAG, "Could not load occupancy" + e);
        } finally {
            lock.unlock();
            //Log.i(TAG, "unlocking DB after updating occupancy successfully");
        }
    }

    private void sortTimeSpan(Collection<OccupancyTimeSpan> timeSpans) {
        List<OccupancyTimeSpan> sortedList = new ArrayList<>();
        sortedList.addAll(timeSpans);
        try{
        Collections.sort(sortedList, new Comparator<OccupancyTimeSpan>() {
            @Override
            public int compare(OccupancyTimeSpan time1, OccupancyTimeSpan time2) {
                return time1.getTime().compareTo(time2.getTime());
            }
        });} catch (Exception e){

        }
    }

    private class OccupancyCallback implements ParserCallback<OccupancyList> {

        private final Context context;

        public OccupancyCallback(final Context context) {
            this.context = context;
        }

        @Override
        public void afterParse(OccupancyList parsed) {
            List<OccupancyDescription> items = new ArrayList<>();
            if (parsed != null) {
                items.addAll(parsed.getItems());
                //Log.i(TAG, "items.size: " + items.size());

                final DatabaseManager<OccupancyDescription> db = new DatabaseManager<>(context, OccupancyDescription.class);
                final DatabaseManager<OccupancyTimeSpan> dbTime = new DatabaseManager<>(context, OccupancyTimeSpan.class);

                Collection<OccupancyTimeSpan> timeSpans;
                for (final OccupancyDescription occupancy : items) {
                    timeSpans = new ArrayList<>();
                    db.saveObject(occupancy);
                    for (final OccupancyTimeSpan time : occupancy.getTimes()) {
                        time.setOccupancies(occupancy);
                        dbTime.saveObject(time);
                        timeSpans.add(time);
                    }
                    sortTimeSpan(timeSpans);
                    occupancy.setTimes(timeSpans);
                    Room room = occupancy.getRoom();
                    occupancy.setRoom(room);
                }

                db.release();
                dbTime.release();
            } else {
                //Log.e(TAG, "parsed is null, cannot save any values");
            }
        }

        @Override
        public void onError(Exception e) {

        }

        @Override
        public void onManagedError(OccupancyList parsed) {
            //TODO
        }
    }

}
