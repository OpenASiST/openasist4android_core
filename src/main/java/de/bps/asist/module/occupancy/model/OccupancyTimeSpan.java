package de.bps.asist.module.occupancy.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.core.model.parser.OnlyTimeParser;

/**
 * Created by Martin Luschek on 27.10.2014.
 */
@DatabaseTable
@AsistItemConfiguration
public class OccupancyTimeSpan extends AbstractDatabaseObject implements IListItem {

    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

    private static final long serialVersionUID = 3564822394323543978L;

    private long id;

    @JsonDeserialize(using = OnlyTimeParser.class)
    @DatabaseField
    private Date start;

    @JsonDeserialize(using = OnlyTimeParser.class)
    @DatabaseField
    private Date end;

    @DatabaseField
    private int mode;

    @DatabaseField
    private int dayOfWeek;

    @DatabaseField
    private int weekNumber;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private OccupancyDescription occupancies;

    private String timeSpan;

    public Date getStart() {
        return start;
    }

    public void setStart(final Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(final Date end) {
        this.end = end;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(final int mode) {
        this.mode = mode;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(final int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(final int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public OccupancyDescription getOccupancies() {
        return occupancies;
    }

    public void setOccupancies(final OccupancyDescription occupancies) {
        this.occupancies = occupancies;
    }

    public String getTime() {
        final StringBuilder time = new StringBuilder();
        if (timeSpan == null) {
            time.append(timeFormat.format(getStart())).append(" - ").append(timeFormat.format(getEnd()));
            this.timeSpan = time.toString();
        }
        return timeSpan;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
