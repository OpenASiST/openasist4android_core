package de.bps.asist.module.occupancy.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

import de.bps.asist.core.database.AbstractDatabaseObject;

/**
 * Created by Martin Luschek on 11.12.2014.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Room extends AbstractDatabaseObject implements Serializable {

    private static final long serialVersionUID = 86535439520653505L;

    private String title;

    private Integer poiid;

    private Integer reference;

    private OccupancyDescription occupancy;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Integer getPoiid() {
        return poiid;
    }

    public void setPoiid(final Integer poiid) {
        this.poiid = poiid;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(final Integer reference) {
        this.reference = reference;
    }

    public OccupancyDescription getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(final OccupancyDescription occupancy) {
        this.occupancy = occupancy;
    }
}
