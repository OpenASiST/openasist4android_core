package de.bps.asist.module.occupancy.model;

import java.util.List;

import de.bps.asist.core.model.IList;

/**
 * Created by Martin Luschek on 27.10.2014.
 */
public class OccupancyList implements IList {

    private List<OccupancyDescription> occupancies;

    public List<OccupancyDescription> getOccupancies() {
        return occupancies;
    }

    public void setOccupancies(final List<OccupancyDescription> occupancies) {
        this.occupancies = occupancies;
    }

    @Override
    public List<OccupancyDescription> getItems() {
        return getOccupancies();
    }

}
