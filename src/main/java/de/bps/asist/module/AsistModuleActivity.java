package de.bps.asist.module;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import java.io.Serializable;

import de.bps.asist.AbstractAsistActivity;
import de.bps.asist.R;

import static de.bps.asist.gui.list.AbstractGenericListCallback.EXTRA_ITEM_SELECTED;

public abstract class AsistModuleActivity<T extends Serializable> extends AbstractAsistActivity {

    private T selectedItem;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        final Intent intent = getIntent();
        if (intent != null) {
            final Bundle extras = intent.getExtras();
            if (extras != null) {
                selectedItem = (T) extras.getSerializable(EXTRA_ITEM_SELECTED);
            }
        }
    }

    public T getSelectedItem() {
        return selectedItem;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected String translate(final int id) {
        return getResources().getString(id);
    }

    /**
     * get View of Type <T> with identifier id
     * @param id
     * @param <T>
     * @return
     */

    protected <T extends View> T getView(final int id) {
        @SuppressWarnings("unchecked")
        final T view = (T) findViewById(id);
        return view;
    }

    protected String translate(final int id, final Object... param) {
        return getResources().getString(id, param);
    }

    protected void startActivity(Class<? extends Activity> activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }
}
