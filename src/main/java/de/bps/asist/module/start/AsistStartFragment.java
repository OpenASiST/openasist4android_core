package de.bps.asist.module.start;

import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.bps.asist.AbstractAsistActivity;
import de.bps.asist.R;
import de.bps.asist.core.manager.ASiSTModuleManager;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.canteen.CanteenModule;
import de.bps.asist.module.start.tree.ASiSTTreeItem;
import de.bps.asist.module.start.tree.ExpandableListAdapter;

public class AsistStartFragment extends AbstractASiSTFragment {

    private ExpandableListView listView;
    private LinearLayout linearLayout;
    private CanteenModule canteenModule;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.module_start_fragment, container, false);

        listView = getView(view, R.id.start_tree_view);
        List<ASiSTTreeItem> treeItems = new ArrayList<>(AsistStartModule.getTreeItems());

        ExpandableListAdapter adapter = new ExpandableListAdapter(getActivity(), treeItems);
        listView.setAdapter(adapter);

        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayoutStartFragmentCanteenButton);

        boolean isModuleRegistered = false;
        List<AbstractAsistModule> modules = ASiSTModuleManager.getInstance().getModules();
        for(AbstractAsistModule module : modules){
            if(module instanceof CanteenModule) {
                isModuleRegistered = true;
                canteenModule = (CanteenModule) module;
                break;
            }
        }

        if(!isModuleRegistered) linearLayout.setVisibility(View.GONE);

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsistStartActivity asistStartActivity =(AsistStartActivity)getContext();
                if(asistStartActivity instanceof AbstractAsistActivity && asistStartActivity != null) {
                    asistStartActivity.startModule(canteenModule);
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateTree();
    }

    private void updateTree() {
        List<ASiSTTreeItem> treeItems = new ArrayList<>(AsistStartModule.getTreeItems());
        List<ASiSTTreeItem> activatedItems = new ArrayList<>();
        for (ASiSTTreeItem item : treeItems) {
            if (item != null && item.isEnabled()) {
                activatedItems.add(item);
            }
        }
        ExpandableListAdapter adapter = new ExpandableListAdapter(getActivity(), activatedItems);
        listView.setAdapter(adapter);

    }

    public void updateTreeView(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateTree();
            }
        });
    }
}
