package de.bps.asist.module.start.tree;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import de.bps.asist.AbstractAsistActivity;
import de.bps.asist.R;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.canteen.CanteenModule;
import de.bps.asist.module.start.AsistStartActivity;

/**
 * Created by litho on 22.08.14.
 * Adapter for TreeView with ASiSTTreeItems
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<ASiSTTreeItem> model;

    public ExpandableListAdapter(Context context, List<ASiSTTreeItem> model) {
        this.context = context;
        this.model = model;
    }


    /**
     * Gets the number of groups.
     *
     * @return the number of groups
     */
    @Override
    public int getGroupCount() {
        return model.size();
    }

    /**
     * Gets the number of children in a specified group.
     *
     * @param groupPosition the position of the group for which the children
     *                      count should be returned
     * @return the children count in the specified group
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        ASiSTTreeItem item = model.get(groupPosition);
        if (item == null || item.getChildren() == null) {
            return 0;
        }
        return item.getChildren().size();
    }

    /**
     * Gets the data associated with the given group.
     *
     * @param groupPosition the position of the group
     * @return the data child for the specified group
     */
    @Override
    public ASiSTTreeItem getGroup(int groupPosition) {
        return model.get(groupPosition);
    }

    /**
     * Gets the data associated with the given child within the given group.
     *
     * @param groupPosition the position of the group that the child resides in
     * @param childPosition the position of the child with respect to other
     *                      children in the group
     * @return the data of the child
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ASiSTTreeItem item = model.get(groupPosition);
        return item.getChildren().get(childPosition);
    }

    /**
     * Gets the ID for the group at the given position. This group ID must be
     * unique across groups. The combined ID (see
     * {@link #getCombinedGroupId(long)}) must be unique across ALL items
     * (groups and all children).
     *
     * @param groupPosition the position of the group for which the ID is wanted
     * @return the ID associated with the group
     */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /**
     * Gets the ID for the given child within the given group. This ID must be
     * unique across all children within the group. The combined ID (see
     * {@link #getCombinedChildId(long, long)}) must be unique across ALL items
     * (groups and all children).
     *
     * @param groupPosition the position of the group that contains the child
     * @param childPosition the position of the child within the group for which
     *                      the ID is wanted
     * @return the ID associated with the child
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition * model.size() + childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    /**
     * Gets a View that displays the given group. This View is only for the
     * group--the Views for the group's children will be fetched using
     * {@link #getChildView(int, int, boolean, android.view.View, android.view.ViewGroup)}.
     *
     * @param groupPosition the position of the group for which the View is
     *                      returned
     * @param isExpanded    whether the group is expanded or collapsed
     * @param convertView   the old view to reuse, if possible. You should check
     *                      that this view is non-null and of an appropriate type before
     *                      using. If it is not possible to convert this view to display
     *                      the correct data, this method can create a new view. It is not
     *                      guaranteed that the convertView will have been previously
     *                      created by
     * @param parent        the parent that this view will eventually be attached to
     * @return the View corresponding to the group at the specified position
     */
    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, final View convertView, final ViewGroup parent) {
        ASiSTTreeItem item = getGroup(groupPosition);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.generic_treeview, null);
        TextView textview = (TextView) view.findViewById(R.id.treeViewTitle);
        textview.setText(item.getTitle());
        String description = item.getDescription();
        TextView descView = (TextView) view.findViewById(R.id.generic_treeDescription);
        if (description == null) {
            descView.setVisibility(View.GONE);
        } else {
            descView.setText(description);
        }
        if(textview.getText().equals(context.getString(R.string.module_canteen_name))) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //TODO No good Code but maybe need complete rewrite of AsistStartActivity
                    AbstractAsistModule module = new CanteenModule();
                    AsistStartActivity asistStartActivity =(AsistStartActivity)context;
                    if(asistStartActivity instanceof AbstractAsistActivity && asistStartActivity != null) {
                    asistStartActivity.startModule(module);
                    }
                }
            });
        }
        return view;
    }

    /**
     * Gets a View that displays the data for the given child within the given
     * group.
     *
     * @param groupPosition the position of the group that contains the child
     * @param childPosition the position of the child (for which the View is
     *                      returned) within the group
     * @param isLastChild   Whether the child is the last child within the group
     * @param convertView   the old view to reuse, if possible. You should check
     *                      that this view is non-null and of an appropriate type before
     *                      using. If it is not possible to convert this view to display
     *                      the correct data, this method can create a new view. It is not
     *                      guaranteed that the convertView will have been previously
     *                      created by
     * @param parent        the parent that this view will eventually be attached to
     * @return the View corresponding to the child at the specified position
     */
    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ASiSTTreeItem item = model.get(groupPosition);
        List<ASiSTTreeItem<?>> children = item.getChildren();
        final View view;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (children == null || children.size() <= childPosition) {
            view = inflater.inflate(R.layout.generic_treeview, null);
            TextView textview = (TextView) view.findViewById(R.id.treeViewTitle);
        } else {
            ASiSTTreeItem<?> child = children.get(childPosition);


            if (child.getLayouter() != null) {
                view = child.getLayouter().getView(childPosition, convertView, parent);
            } else {
                view = inflater.inflate(R.layout.generic_treeview, null);
                TextView textview = (TextView) view.findViewById(R.id.treeViewTitle);
                textview.setText(child.getTitle());
            }
        }
        view.setBackgroundColor(context.getResources().getColor(R.color.white));
        view.setPadding(20, 5, 5, 5);
        return view;
    }

    /**
     * Whether the child at the specified position is selectable.
     *
     * @param groupPosition the position of the group that contains the child
     * @param childPosition the position of the child within the group
     * @return whether the child is selectable.
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
