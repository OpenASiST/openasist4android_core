package de.bps.asist.module.start;

import android.content.SharedPreferences;

import androidx.fragment.app.FragmentActivity;

/**
 * Created by litho on 30.10.14.
 */
public interface SettingsChangedCallback {

    public void onValueChanged(FragmentActivity context, SharedPreferences preferences, String key);

}
