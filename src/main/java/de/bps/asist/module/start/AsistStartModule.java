package de.bps.asist.module.start;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.start.tree.ASiSTTreeItem;

public class AsistStartModule extends AbstractAsistModule {

    private static AsistStartFragment startFragment;
    private static final StartSettingsConfig startSettingsConfig = new StartSettingsConfig();

    private static List<ASiSTTreeItem> treeItems = new ArrayList<>();

	public AsistStartModule() {
		super(false);
        setSettingsConfig(startSettingsConfig);
	}

	@Override
	public Fragment getInitialFragment() {
        if(startFragment == null){
            startFragment = new AsistStartFragment();
        }
		return startFragment;
	}

	@Override
	public int getName() {
		return R.string.app_header;
	}

	@Override
	public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        return new ArrayList<>();
	}

	@Override
	public int getIcon() {
		return R.drawable.ic_menu_home;
	}

    public static void registerForPush(int title, String key){
        startSettingsConfig.addPushViewConfig(title, key);
    }

    public static void registerForTree(ASiSTTreeItem item){
        treeItems.add(item);

        startSettingsConfig.addStartViewConfig(item.getTitle(), item.isEnabled());
    }

    public static void update(ASiSTTreeItem toUpdate){
        String title = toUpdate.getTitle();
        if (title != null) {
            for(ASiSTTreeItem item: new ArrayList<>(treeItems)){
                if(item != null && title.equals(item.getTitle())){
                    int index = treeItems.indexOf(item);
                    treeItems.remove(item);
                    treeItems.add(index, toUpdate);
                }
            }
        }
        if(startFragment != null){
            startFragment.updateTreeView();
        }
    }

    public static void deregisterForTree(String title){
        for(ASiSTTreeItem item: treeItems){
            if(title.equals(item.getTitle())){
                item.setEnabled(false);
            }
        }
    }

    public static void activateTreeItem(String title){
        for(ASiSTTreeItem item: treeItems){
            if(title.equals(item.getTitle())){
                item.setEnabled(true);
            }
        }
    }

    /**
     *
     * @return the tree Items
     */
    public static List<ASiSTTreeItem> getTreeItems() {
        return treeItems;
    }

    @Override
    public void setShown(boolean shown) {
        if (!shown) {
            setSettingsConfig(new StartSettingsConfig(false, startSettingsConfig.getPushSection()));
        }
        super.setShown(shown);
    }
}
