package de.bps.asist.module.start.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by litho on 13.10.14.
 * Weather model class for parsing
 */
public class WeatherInformation implements Serializable {

    private String title;
    private Date lastBuildDate;
    private int condition;
    private int temp;
    private String imageUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getLastBuildDate() {
        return lastBuildDate;
    }

    public void setLastBuildDate(Date lastBuildDate) {
        this.lastBuildDate = lastBuildDate;
    }

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    public int getTemp() {
        // conversion from Fahrenheit to Celsius
        // needed because Yahoo now provides Temperature data in Fahrenheit.
        return (int)Math.round((temp - 32) * (5.0 / 9));
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
