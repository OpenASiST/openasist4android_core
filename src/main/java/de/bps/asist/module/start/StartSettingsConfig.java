package de.bps.asist.module.start;

import de.bps.asist.R;
import de.bps.asist.module.settings.model.CheckboxSettingsItem;
import de.bps.asist.module.settings.model.SettingsConfig;
import de.bps.asist.module.settings.model.SettingsSection;

/**
 * Created by litho on 30.10.14.
 * Configuration for StartView Settings
 */
public class StartSettingsConfig extends SettingsConfig {

    public static final String PREFERENCE_PREFIX = "startView_tree_";

    private SettingsSection startSection;

    private SettingsSection pushSection;

    public StartSettingsConfig(boolean withStartSection, SettingsSection _pushSection) {
        if (withStartSection) {
            startSection = new SettingsSection(R.string.module_start_settings_header);
            addSection(startSection);
        }
        //if (_pushSection == null) {
        //    pushSection = new SettingsSection(R.string.module_settings_push_header);
        //    addSection(pushSection);
        //} else {
        //    this.pushSection = _pushSection;
        //    addSection(pushSection);
        //}

    }

    public StartSettingsConfig(){
        startSection = new SettingsSection(R.string.module_start_settings_header);
        addSection(startSection);
        //pushSection = new SettingsSection(R.string.module_settings_push_header);
        //addSection(pushSection);
    }

    public SettingsSection getPushSection() {
        //return pushSection;
        return null;
    }

    public void setPushSection(SettingsSection pushSection) {
        this.pushSection = pushSection;
    }

    public void addStartViewConfig(String title, boolean enabled){
        CheckboxSettingsItem item = new CheckboxSettingsItem(title);
        item.setKey(buildKey(title));
        item.setCallback(new StartViewSettingsCallback());
        item.setDefaultValue(enabled);
        startSection.addItem(item);
    }

    public void addPushViewConfig(int title, String key){
        //CheckboxServerSettingsItem item = new CheckboxServerSettingsItem(title);
        //item.setKey(key);
        //item.setDefaultValue(true);
        //pushSection.addItem(item);

    }

    public static String buildKey(String title){
        return PREFERENCE_PREFIX + title;
    }

}
