package de.bps.asist.module.start;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.legacy.app.ActionBarDrawerToggle;
import de.bps.asist.AbstractAsistActivity;
import de.bps.asist.R;
import de.bps.asist.core.ASiSTApplication;
import de.bps.asist.core.manager.ASiSTModuleManager;
import de.bps.asist.core.manager.preferences.ASiSTPreferenceManager;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.settings.SettingsModule;
import de.bps.asist.module.start.tree.ASiSTTreeItem;
import de.bps.asist.module.start.tree.StartTreeConfig;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */

public class AsistStartActivity extends AbstractAsistActivity implements ActionBar.OnNavigationListener {

    private static final String TAG = AsistStartActivity.class.getName();
    public static final String START_VIEW_TREE = "startView_tree_";

    private ListView mDrawerList;

    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private DrawerItemClickListener drawerItemClickListener;

    private List<AbstractAsistModule> modules;
    private final List<String> moduleNames = new ArrayList<>();

    private Integer menuLayout;
    private AbstractASiSTFragment currentFragment;

    private static boolean isShown = false;
    boolean doubleBackToExitPressedOnce = false;
    ASiSTPreferenceManager apm = new ASiSTPreferenceManager();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ASiSTApplication.startASiST(this);

        mTitle = mDrawerTitle = getTitle();

        setContentView(R.layout.menu_layout);

        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

            modules = ASiSTModuleManager.getInstance().getModules();

            for (final AbstractAsistModule module : modules) {
                if (module.isShown()) {
                    String moduleName = getResources().getString(module.getName());
                    if(!isShown) {
                        if (moduleName.equals(getString(R.string.app_header))){
                            moduleName = getString(R.string.mainmodule_name);
                        }
                        moduleNames.add(moduleName);
                        StartTreeConfig startTreeConfig = module.getStartTreeConfig();
                        if (startTreeConfig != null) {
                            boolean enabled = apm.getSetting(this, START_VIEW_TREE + moduleName);
                            ASiSTTreeItem rootItem = startTreeConfig.getRootNode(this);
                            rootItem.setEnabled(enabled);
                            AsistStartModule.registerForTree(rootItem);
                        }
                    } else {
                        moduleNames.add(moduleName);
                    }
                }
            }
            isShown = true;

            final MyAdapter<String> adapter = new MyAdapter<>(this, R.layout.drawer_item, moduleNames);
            // Set the adapter for the list view
            mDrawerList.setAdapter(adapter);
            // Set the list's click listener
            drawerItemClickListener = new DrawerItemClickListener();
            mDrawerList.setOnItemClickListener(drawerItemClickListener);
            mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                    mDrawerLayout, /* DrawerLayout object */
                    R.drawable.ic_navigation_drawer, /* nav drawer icon to replace 'Up' caret */
                    R.string.open_menu, /* "open drawer" description */
                    R.string.close_menu /* "close drawer" description */
            ) {

                /**
                 * Called when a drawer has settled in a completely closed state.
                 */
                @Override
                public void onDrawerClosed(final View view) {
                    super.onDrawerClosed(view);
                    getSupportActionBar().setTitle(mTitle);
                }

                /**
                 * Called when a drawer has settled in a completely open state.
                 */
                @Override
                public void onDrawerOpened(final View drawerView) {
                    super.onDrawerOpened(drawerView);
                    getSupportActionBar().setTitle(getString(R.string.app_header));
                }
            };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);

    }



    @Override
    protected void onPostCreate(final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
        drawerItemClickListener.onItemClick(mDrawerList, mDrawerLayout, 0, 0);
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (currentFragment != null) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (currentFragment != null && currentFragment.handleMenuItemClicked(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
        }
        if (!(this.currentFragment instanceof  AsistStartFragment)) {
            selectItem(modules.get(0));
            return;
        }
        if (mTitle.equals(getResources().getString(SettingsModule.getTitle()))) {
            final Fragment fragment = modules.get(0).getInitialFragment();
            if (fragment == null) {
                return;
            }
            if (fragment instanceof AbstractASiSTFragment) {
                currentFragment = (AbstractASiSTFragment) fragment;
                menuLayout = currentFragment.getMenuLayout();
            }
            selectItem(modules.get(0));
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tippen Sie \"Zurück\" nochmal um die App zu schließen.", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onNavigationItemSelected(final int itemPosition, final long itemId) {
        return false;
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
            if (modules.size() > position) {
                final AbstractAsistModule module = modules.get(position);
                selectItem(module);
                mDrawerList.setItemChecked(position, true);
            } else {
                //Log.e(TAG, "Wanted " + position + " but modules.size() is only " + modules.size());
            }
        }
    }

    //Just used to start CanteenModule from StartTree in ExpandableListAdapter
    public void startModule(AbstractAsistModule module){
        selectItem(module);
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(final AbstractAsistModule module) {
        //Log.d(TAG, "selecting item " + getResources().getString(module.getName()));
        // Create a new fragment and specify the planet to show based on
        // position
        final Fragment fragment = module.getInitialFragment();
        if (fragment == null) {
            return;
        }
        if (fragment instanceof AbstractASiSTFragment) {
            currentFragment = (AbstractASiSTFragment) fragment;
            menuLayout = currentFragment.getMenuLayout();
        }

        // Insert the fragment by replacing any existing fragment
        final FragmentManager fm = getSupportFragmentManager();
        final String name = getResources().getString(module.getName());
        fm.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(name).commit();

        // Highlight the selected item, update the title, and close the drawer
        setTitle(module.getName());
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(final CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        if (menuLayout != null) {
            getMenuInflater().inflate(menuLayout, menu);
        }
        return true;
    }

    private class MyAdapter<T> extends ArrayAdapter<T> {

        public MyAdapter(final Context context, final int resource, final List<T> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            getItem(position);
            final TextView textView = (TextView) super.getView(position, convertView, parent);
            int moduleIcon = modules.get(position).getIcon();
            final Drawable myIcon = getResources().getDrawable(moduleIcon);
            //setting icon color
            //myIcon.setColorFilter(R.color.defaultColor, PorterDuff.Mode.MULTIPLY);
            textView.setCompoundDrawablesWithIntrinsicBounds(modules.get(position).getIcon(), 0, 0, 0);
            Drawable[] testarray = textView.getCompoundDrawables();
            textView.getCompoundDrawables()[0].setColorFilter(getResources().getColor(R.color.defaultColor), PorterDuff.Mode.MULTIPLY);



            return textView;
        }
    }
}
