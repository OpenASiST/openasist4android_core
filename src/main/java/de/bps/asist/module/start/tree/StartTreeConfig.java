package de.bps.asist.module.start.tree;

import android.content.Context;

import java.io.Serializable;

import de.bps.asist.module.AbstractAsistModule;

/**
 * Created by litho on 29.10.14.
 * Abstract tree configuration class
 */
public abstract class StartTreeConfig<T extends AbstractAsistModule> implements Serializable {

    private T module;

    public StartTreeConfig(T module){
        this.module = module;
    }

    public abstract ASiSTTreeItem getRootNode(Context context);

    public T getModule() {
        return module;
    }

    public int getName(){
        return getModule().getName();
    }

    public boolean isEnabledPerDefault() {
        return true;
    }
}
