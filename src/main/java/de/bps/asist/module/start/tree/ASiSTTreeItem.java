package de.bps.asist.module.start.tree;

import android.widget.BaseAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by litho on 22.08.14.
 * Tree Item for tree structure, e.g. for Start view
 */
public class ASiSTTreeItem<T extends Serializable> {

    private final String title;
    private final String description;
    private List<ASiSTTreeItem<?>> children = new ArrayList<>();
    private boolean enabled = true;
    private BaseAdapter layouter;
    private T modelItem;

    public ASiSTTreeItem(String title, final String description){
        this.title = title;
        this.description = description;
    }

    public void addChild(ASiSTTreeItem item){
        children.add(item);
    }

    public List<ASiSTTreeItem<?>> getChildren() {
        return children;
    }

    public void setChildren(List<ASiSTTreeItem<?>> children) {
        this.children = children;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public BaseAdapter getLayouter() {
        return layouter;
    }

    public void setLayouter(BaseAdapter layouter) {
        this.layouter = layouter;
    }

    public T getModelItem() {
        return modelItem;
    }

    public void setModelItem(T modelItem) {
        this.modelItem = modelItem;
    }
}
