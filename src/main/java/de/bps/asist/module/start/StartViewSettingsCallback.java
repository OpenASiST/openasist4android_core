package de.bps.asist.module.start;

import android.content.SharedPreferences;

import androidx.fragment.app.FragmentActivity;

/**
 * Created by litho on 30.10.14.
 * Settings callback for startView
 */
public class StartViewSettingsCallback implements SettingsChangedCallback {

    @Override
    public void onValueChanged(FragmentActivity context, SharedPreferences preferences, String key) {
        if (key.startsWith(StartSettingsConfig.PREFERENCE_PREFIX)) {
            Boolean value = preferences.getBoolean(key, true);
            String subString = key.substring(StartSettingsConfig.PREFERENCE_PREFIX.length());
            if (value) {
                AsistStartModule.activateTreeItem(subString);
            } else {
                AsistStartModule.deregisterForTree(subString);
            }

        }
    }
}
