package de.bps.asist.module.lostandfound.model;

import java.util.List;

/**
 * Created by litho on 25.08.14.
 * Model class for root elements of Lost Items
 */
public class LostList implements ILostFoundList{

    private List<LostFoundItem> losts;

    public List<LostFoundItem> getLosts() {
        return losts;
    }

    public void setLosts(List<LostFoundItem> losts) {
        this.losts = losts;
    }

    @Override
    public List<LostFoundItem> getItems() {
        return losts;
    }
}
