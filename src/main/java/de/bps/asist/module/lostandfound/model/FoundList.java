package de.bps.asist.module.lostandfound.model;

import java.util.List;

/**
 * Created by litho on 25.08.14.
 * Lost + Found root model class
 */
public class FoundList implements ILostFoundList{

    private List<LostFoundItem> found;

    public List<LostFoundItem> getFound() {
        return found;
    }

    public void setFound(List<LostFoundItem> found) {
        this.found = found;
    }

    @Override
    public List<LostFoundItem> getItems() {
        return found;
    }
}
