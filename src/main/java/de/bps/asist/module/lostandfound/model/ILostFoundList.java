package de.bps.asist.module.lostandfound.model;

import java.util.List;

import de.bps.asist.core.model.IList;

/**
 * Created by litho on 26.08.14.
 * Interface for Lost and Found items
 */
public interface ILostFoundList extends IList{

    @Override
    List<LostFoundItem> getItems();
}
