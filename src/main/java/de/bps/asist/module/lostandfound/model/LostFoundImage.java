package de.bps.asist.module.lostandfound.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import de.bps.asist.core.database.AbstractDatabaseObject;

/**
 * Created by litho on 25.08.14.
 */

@DatabaseTable
public class LostFoundImage extends AbstractDatabaseObject {

    @DatabaseField
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
