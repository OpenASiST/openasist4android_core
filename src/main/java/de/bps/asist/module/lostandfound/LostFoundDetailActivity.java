package de.bps.asist.module.lostandfound;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.gui.detail.DetailsActivity;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.lostandfound.model.LostFoundItem;
import de.bps.asist.module.poi.PoiModule;
import de.bps.asist.module.poi.model.PoiItem;

/**
 * Created by litho on 26.08.14.
 * Special Detail activity for displaying Lost + found items
 */
public class LostFoundDetailActivity extends DetailsActivity<LostFoundItem> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDetailContentView(R.layout.lostfound_details);
        super.onCreate(savedInstanceState);
        ListView listView = getView(R.id.lostfound_details_list);
        LostFoundItem selectedItem = getSelectedItem();
        Map<String, List<ASiSTDetailDescription>> map = new HashMap<>();
        List<ASiSTDetailDescription> list = new ArrayList<>();
        int pos = 0;
        if(selectedItem.getPoiid() != null){
            PoiItem poi = PoiModule.findItemByPoiId(this, selectedItem.getPoiid());
            ASiSTDetailDescription poiDesc = new ASiSTDetailDescription(R.string.module_lostfound_add_position, poi.getTitle(), pos++, "");
            list.add(poiDesc);
        }
        if(selectedItem.getLocation() != null){
            ASiSTDetailDescription poiDesc = new ASiSTDetailDescription(R.string.module_lostfound_add_position, translate(R.string.lostfound_detail_position_custom), pos++, "");
            list.add(poiDesc);
        }
        if(selectedItem.getEmail() != null){
            ASiSTDetailDescription poiDesc = new ASiSTDetailDescription(R.string.module_lostfound_add_email, selectedItem.getEmail(), pos++, "");
            list.add(poiDesc);
        }
        if(selectedItem.getTelephone() != null){
            ASiSTDetailDescription poiDesc = new ASiSTDetailDescription(R.string.module_lostfound_add_telephone, selectedItem.getTelephone(), pos++, "");
            list.add(poiDesc);
        }
        map.put("", list);

        CategoryListViewAdapter<ASiSTDetailDescription> adapter = new CategoryListViewAdapter<>(this, map, null);
        listView.setAdapter(adapter);
    }
}
