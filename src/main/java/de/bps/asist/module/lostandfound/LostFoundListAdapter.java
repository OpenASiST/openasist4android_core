package de.bps.asist.module.lostandfound;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collection;

import de.bps.asist.R;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.lostandfound.model.LostFoundItem;

/**
 * Created by litho on 20.10.14.
 */
public class LostFoundListAdapter extends GenericListViewAdapter<LostFoundItem> {


    public LostFoundListAdapter(Context context, Collection<LostFoundItem> objects, IGenericListCallback callback) {
        super(context, objects, callback);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View result = super.getView(position, convertView, parent);
        result.findViewById(R.id.listItemMainLogoWrapper).setVisibility(View.VISIBLE);
        return result;
    }
}
