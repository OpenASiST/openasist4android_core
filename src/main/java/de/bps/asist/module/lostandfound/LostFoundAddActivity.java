package de.bps.asist.module.lostandfound;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import de.bps.asist.R;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.util.ASiSTPostSender;
import de.bps.asist.core.util.AsistRequestParams;
import de.bps.asist.gui.dialog.ASiSTDialog;
import de.bps.asist.gui.dialog.ProgressFinishedCallback;
import de.bps.asist.gui.dialog.ResponseHandler;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.lostandfound.model.LOSTFOUNDTYPE;
import de.bps.asist.module.poi.PoiItemArrayAdapter;
import de.bps.asist.module.poi.model.PoiItem;

import static de.bps.asist.core.util.StringHelper.isNullOrEmpty;

/**
 * Created by litho on 27.10.14.
 * Activity for creating a new lost + found entry
 */
public class LostFoundAddActivity extends AsistModuleActivity<LOSTFOUNDTYPE> implements AdapterView.OnItemSelectedListener, ProgressFinishedCallback {

    private static final String TAG = LostFoundAddActivity.class.getSimpleName();

    private static final int SELECT_PICTURE = 101;

    private String takePhoto;
    private String chooseLibrary;
    private String cancel;

    private EditText titleEdit;
    private EditText descEdit;
    private EditText emailEdit;
    private EditText phoneEdit;

    private PoiItemArrayAdapter adapter;

    private PoiItem poi;

    private ImageView imageView;

    private Bitmap imageBm;
    private Uri mCurrentPhotoPath;

    private boolean lost;

    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lostfound_add);
        titleEdit = getView(R.id.lostfound_add_title);
        descEdit = getView(R.id.lostfound_add_description);
        emailEdit = getView(R.id.lostfound_add_email);
        phoneEdit = getView(R.id.lostfound_add_telephone);


        Button imageButton = getView(R.id.lostfound_add_image);
        imageButton.setOnClickListener(new ImageButtonHandler());
        imageView = getView(R.id.lostfound_add_imageview);
        imageView.setOnClickListener(new ImageButtonHandler());

        takePhoto = translate(R.string.module_lostfound_image_camera);
        chooseLibrary = translate(R.string.module_lostfound_image_gallery);
        cancel = translate(R.string.cancel);

        LOSTFOUNDTYPE selected = getSelectedItem();
        lost = selected == LOSTFOUNDTYPE.LOST;

        Spinner spinner = getView(R.id.lostfound_add_position);
        adapter = new PoiItemArrayAdapter(this);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        spinner.setSelected(false);

        if (lost) {
            getSupportActionBar().setTitle(R.string.module_lostfound_losts);
        } else {
            getSupportActionBar().setTitle(R.string.module_lostfound_found);
        }
        if (savedInstanceState != null) {
            mCurrentPhotoPath = savedInstanceState.getParcelable("fileName");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("fileName", mCurrentPhotoPath);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.lostfound_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.action_lostfound_send == item.getItemId()) {
            if (validate()) {
                send();
            } else {
                ASiSTDialog dialog = new ASiSTDialog(translate(R.string.lostfound_add_error_title), getError());
                dialog.show(getSupportFragmentManager(), TAG);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SELECT_PICTURE:
                if (resultCode == RESULT_OK) {
                    try {
                        Uri selectedImage = data.getData();
                        imageBm = ASiSTCameraUtil.findBitMap(selectedImage, this, imageView);
                        imageView.setImageBitmap(imageBm);
                    } catch (Exception e) {
                        //Log.e(TAG, "could not open selected image", e);
                    }
                }
                break;
            case ASiSTCameraUtil.REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {
                    try {
                        imageBm = ASiSTCameraUtil.findBitMap(data, mCurrentPhotoPath, imageView);
                        imageView.setImageBitmap(imageBm);
                    } catch (Exception e) {
                        //Log.e(TAG, "could not save image from camera", e);
                    }
                } else {
                    //Log.w(TAG, "not-ok status: " + resultCode);
                }
                break;
        }
    }

    private Bitmap getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String filePath = cursor.getString(column_index);
        cursor.close();
        // Convert file path into bitmap image using below line.
        return BitmapFactory.decodeFile(filePath);
    }

    private boolean validate() {
        Editable title = titleEdit.getText();
        if (isNullOrEmpty(title)) {
            return false;
        }
        Editable desc = descEdit.getText();
        if (isNullOrEmpty(desc)) {
            return false;
        }
        Editable email = emailEdit.getText();
        Editable phone = phoneEdit.getText();
        return !(isNullOrEmpty(email) && isNullOrEmpty(phone));
    }

    private String getError() {
        Editable title = titleEdit.getText();
        if (isNullOrEmpty(title)) {
            return translate(R.string.lostfound_add_error_notitle);
        }
        Editable desc = descEdit.getText();
        if (isNullOrEmpty(desc)) {
            return translate(R.string.lostfound_add_error_nodescription);
        }
        Editable email = emailEdit.getText();
        Editable phone = phoneEdit.getText();
        if (isNullOrEmpty(email) && isNullOrEmpty(phone)) {
            return translate(R.string.lostfound_add_error_noemail);
        }
        return "";
    }

    private void send() {
        AsistRequestParams params = new AsistRequestParams();
        params.put("title", titleEdit.getText());
        params.put("desc", descEdit.getText());
        if (poi != null) {
            params.put("poiid", poi.getPoiid());
        }
        if (imageBm != null) {
            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                imageBm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
                byte[] data = bos.toByteArray();
                ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
                params.put("photo", inputStream);
            } catch (Exception e) {
                ////Log.e(TAG, "could not add image ", e);
            }
        } else {
        }

        Editable email = emailEdit.getText();
        Editable phone = phoneEdit.getText();
        if (!isNullOrEmpty(email)) {
            params.put("email", email);
        }
        if (!isNullOrEmpty(phone)) {
            params.put("telephone", phone);
        }

        StringBuilder url = new StringBuilder();
        url.append(translate(R.string.rootUrl)).append("/lostfound/");
        url.append(translate(R.string.institutionName));
        if (lost) {
            url.append("/lostObject/");
        } else {
            url.append("/foundObject/");
        }
        url.append(EnvironmentManager.getInstance().getDeviceToken(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.show();

        AsyncHttpResponseHandler responseHandler = new ResponseHandler(progressDialog, this);
        ASiSTPostSender.post(url.toString(), params, responseHandler);
    }

    /*
        OnSelected methods
     */

    /**
     * @param parent   the parent adapter view
     * @param view     the view
     * @param position position selected
     * @param id       the id
     */

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.poi = adapter.getItem(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class ImageButtonHandler implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final CharSequence[] items = {takePhoto, chooseLibrary, cancel};

            AlertDialog.Builder builder = new AlertDialog.Builder(LostFoundAddActivity.this);
            builder.setTitle(translate(R.string.module_lostfound_image_title));
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (takePhoto.equals(items[item])) {
                        mCurrentPhotoPath = ASiSTCameraUtil.dispatchTakePictureIntent(LostFoundAddActivity.this);
                    } else if (chooseLibrary.equals(items[item])) {
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, ""), SELECT_PICTURE);
                    } else if (cancel.equals(items[item])) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();

        }

    }

    @Override
    public void onFinish() {
        finish();
    }

    @Override
    public void onError(Throwable e) {
        
    }
}
