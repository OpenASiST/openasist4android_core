package de.bps.asist.module.lostandfound.model;

/**
 * Created by litho on 25.08.14.
 */
public enum LOSTFOUNDTYPE {

       LOST, FOUND
}
