package de.bps.asist.module.lostandfound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistImage;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.core.model.common.Position;
import de.bps.asist.core.model.parser.PubdateDeserializer;

/**
 * Created by litho on 25.08.14.
 */

@DatabaseTable
@AsistItemConfiguration(showDescription = true, showMainLogo = true)
public class LostFoundItem extends AbstractDatabaseObject implements IListItem, Comparable<LostFoundItem> {

    @DatabaseField
    @AsistTitle
    private String title;

    @DatabaseField
    @AsistDescription
    @JsonProperty(value = "desc")
    private String description;

    @DatabaseField
    @JsonDeserialize(using = PubdateDeserializer.class)
    private Date creationTime;

    @DatabaseField(foreign = true)
    private Position location;

    @DatabaseField
    private LOSTFOUNDTYPE type;

    @DatabaseField
    private String telephone;

    @DatabaseField
    private String email;

    @DatabaseField
    private Integer poiid;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    @AsistImage
    private LostFoundImage image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Position getLocation() {
        return location;
    }

    public void setLocation(Position location) {
        this.location = location;
    }

    public LOSTFOUNDTYPE getType() {
        return type;
    }

    public void setType(LOSTFOUNDTYPE type) {
        this.type = type;
    }

    public LostFoundImage getImage() {
        return image;
    }

    public void setImage(LostFoundImage image) {
        this.image = image;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPoiid() {
        return poiid;
    }

    public void setPoiid(Integer poiid) {
        this.poiid = poiid;
    }

    @Override
    public int compareTo(LostFoundItem another) {
        int result = 0;
        if(creationTime != null && another.creationTime != null){
            result = creationTime.compareTo(another.creationTime);
        }else if(creationTime != null){
            result = 1;
        } else if(another.creationTime != null){
            result = -1;
        }
        return result * -1;
    }
}
