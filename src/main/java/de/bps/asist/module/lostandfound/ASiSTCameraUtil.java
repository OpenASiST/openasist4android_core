package de.bps.asist.module.lostandfound;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by litho on 21.11.14.
 * Util for Camera
 */
public class ASiSTCameraUtil {

    private static final String TAG = ASiSTCameraUtil.class.getSimpleName();

    public static final int REQUEST_CAMERA = 14792;

    public static Uri dispatchTakePictureIntent(Activity activity) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                //Log.e(TAG, "could not create image", ex);
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri mCurrentPhotoPath = Uri.fromFile(photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,mCurrentPhotoPath);
                activity.startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                return mCurrentPhotoPath;
            }
        }
        return null;
    }

    private static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap findBitMap(Intent data, Uri mCurrentPhotoPath, ImageView imageView) {
        final Bitmap imageBm;
        if (data != null) {
            Bundle extras = data.getExtras();
            imageBm = (Bitmap) extras.get("data");
        } else {
            if (mCurrentPhotoPath == null) {
                throw new IllegalArgumentException("mCurrentPhotoPath is null!");
            }
            String filePath = mCurrentPhotoPath.getPath();

            imageBm = loadBitmap(imageView, filePath);
        }
        return imageBm;
    }

    private static Bitmap loadBitmap(ImageView imageView, String filePath) {
        Bitmap imageBm;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.outHeight = imageView.getHeight();
        options.outWidth = imageView.getWidth();
        Bitmap decoded = BitmapFactory.decodeFile(filePath, options);
        options.inSampleSize = calculateInSampleSize(options, imageView.getWidth(), imageView.getHeight());

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageBm = BitmapFactory.decodeFile(filePath, options);
        return imageBm;
    }

    public static Bitmap findBitMap(Uri uri, Activity activity, ImageView imageView){
        String filePath = getPath(uri, activity);
        Bitmap result = loadBitmap(imageView, filePath);
        return result;
    }

    private static String getPath(Uri uri, Activity activity) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String filePath = cursor.getString(column_index);
        cursor.close();
        // Convert file path into bitmap image using below line.
        return filePath;
    }

    public static Bitmap setBitMap(byte[] data, ImageView imageView){
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        options.inSampleSize = calculateInSampleSize(options, imageView.getWidth(), imageView.getHeight());

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap imageBm = BitmapFactory.decodeByteArray(data, 0, data.length, options);
        imageView.setImageBitmap(imageBm);
        return imageBm;
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
            if(c == null){
                int numberOfCameras = Camera.getNumberOfCameras();
                if(numberOfCameras > 0){
                    c = Camera.open(0);
                }
            }
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            //Log.e(TAG, "could not get camera", e);
        }
        return c; // returns null if camera is unavailable
    }
}
