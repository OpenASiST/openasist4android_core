package de.bps.asist.module.lostandfound;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.Collections;
import java.util.List;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.bps.asist.R;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.OnRefreshFinishedListener;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.lostandfound.model.LOSTFOUNDTYPE;
import de.bps.asist.module.lostandfound.model.LostFoundItem;

import static de.bps.asist.gui.list.AbstractGenericListCallback.EXTRA_ITEM_SELECTED;

/**
 * Created by litho on 26.08.14.
 */
public class LostFoundListActivity extends AsistModuleActivity<LOSTFOUNDTYPE> implements SwipeRefreshLayout.OnRefreshListener, OnRefreshFinishedListener{

    private ListView listView;
    private LostFoundListAdapter adapter;
    private int parseCounter = 0;
    private SwipeRefreshLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generic_refresh_listview);

        layout = getView(R.id.generic_list_swipe);
        layout.setOnRefreshListener(this);

        layout.setColorSchemeResources(R.color.defaultColor,
                R.color.tintColor,
                R.color.defaultColor,
                R.color.tintColor);

        listView = (ListView) findViewById(R.id.generic_list);

        updateListView();
    }

    private void updateListView() {
        LOSTFOUNDTYPE selected = getSelectedItem();
        AbstractGenericListCallback cb = new AbstractGenericListCallback(this, LostFoundDetailActivity.class);
        List<LostFoundItem> items = LostAndFoundModule.getItems(this, selected);
        Collections.sort(items);
        adapter = new LostFoundListAdapter(this, items, cb);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.lostfound, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (R.id.action_lostfound_found == item.getItemId()) {
            Intent intent = new Intent(this, LostFoundAddActivity.class);

            final Bundle bundle = new Bundle();
            LOSTFOUNDTYPE o = getSelectedItem();
            bundle.putSerializable(EXTRA_ITEM_SELECTED, o);
            intent.putExtra(EXTRA_ITEM_SELECTED, o);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        parseCounter = 2;
        LostAndFoundModule.readLostFound(this, this);
    }

    @Override
    public void refreshFinished() {
        parseCounter--;
        if(parseCounter == 0){
            updateListView();
            layout.setRefreshing(false);
        }
    }
}
