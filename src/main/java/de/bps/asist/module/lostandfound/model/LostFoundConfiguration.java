package de.bps.asist.module.lostandfound.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by litho on 26.08.14.
 * Special configuration class for Lost + Found display
 */
public class LostFoundConfiguration {

    private final int maxSize;
    private Map<LostFoundItem, LOSTFOUNDTYPE> map = new HashMap<>();

    public LostFoundConfiguration(int maxSize){
        this.maxSize = maxSize;
    }

    public void addLastItem(LostFoundItem item, LOSTFOUNDTYPE type){
        map.put(item, type);
    }

    public int getMaxSize() {
        return maxSize;
    }

    public LOSTFOUNDTYPE getType(LostFoundItem item){
        return map.get(item);
    }

}
