package de.bps.asist.module.lostandfound;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.Category;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.lostandfound.model.LostFoundConfiguration;
import de.bps.asist.module.lostandfound.model.LostFoundItem;

/**
 * Created by litho on 26.08.14.
 * Adapter for LostFound View
 */
public class LostFoundAdapter extends CategoryListViewAdapter<LostFoundItem>{

    private int ITEM_TYPE_MAX_COUNT = 2;

    private LostFoundConfiguration config;
    private int maxSize;

    public LostFoundAdapter(Context context, Map<String, List<LostFoundItem>> objects, IGenericListCallback callback, LostFoundConfiguration config) {
        super(context, objects, callback);
        this.config = config;
        this.maxSize = config.getMaxSize();
    }

    @Override
    public int getItemViewType(final int position) {
        for(Category<LostFoundItem> category: getCategories()){
            if(position == category.getEndIndex() && category.getIndexSize() == maxSize){
                return ITEM_TYPE_MAX_COUNT;
            }
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
            // item object
        final int viewType = getItemViewType(position);
        if(viewType == ITEM_TYPE_MAX_COUNT){
            LinearLayout row = (LinearLayout) getInflater().inflate(R.layout.generic_list_item, parent);
            final TextView titleView = (TextView) row.findViewById(R.id.listItemTitle);
            String text = getContext().getResources().getString(R.string.generic_list_showmore);
            titleView.setText(text);
            row.setBackgroundColor(getContext().getResources().getColor(R.color.focusColor));
            row.findViewById(R.id.listItemMainLogoWrapper).setVisibility(View.GONE);
            row.findViewById(R.id.listItemDescription).setVisibility(View.GONE);
            final AbstractGenericListCallback cb = new AbstractGenericListCallback(getContext(), LostFoundListActivity.class);
            row.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View v) {
                    LostFoundItem item = (LostFoundItem) getItem(position);
                    cb.doAction(config.getType(item));
                }
            });
            return row;
        }else {
            View result = super.getView(position, convertView, parent);
            View image = result.findViewById(R.id.listItemMainLogoWrapper);
            if(image != null) {
                image.setVisibility(View.VISIBLE);
            }
            return result;
        }
    }

}
