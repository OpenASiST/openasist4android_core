/**
 *
 */
package de.bps.asist.module.lostandfound;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.gui.list.OnRefreshFinishedListener;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.lostandfound.model.FoundList;
import de.bps.asist.module.lostandfound.model.LOSTFOUNDTYPE;
import de.bps.asist.module.lostandfound.model.LostFoundImage;
import de.bps.asist.module.lostandfound.model.LostFoundItem;
import de.bps.asist.module.lostandfound.model.LostList;
import de.bps.asist.module.start.AsistStartModule;

/**
 * @author thomasw
 */
public class LostAndFoundModule extends AbstractAsistModule {

    private static final String TAG = LostAndFoundModule.class.getSimpleName();

    private static final Lock lock = new ReentrantLock();

    public LostAndFoundModule(){
        AsistStartModule.registerForPush(getName(), "LostFoundModule");
    }

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getName()
     */
    @Override
    public int getName() {
        return R.string.module_lostandfound_name;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_lost;
    }

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getInitialFragment()
     */
    @Override
    public Fragment getInitialFragment() {
        return new LostAndFoundFragment();
    }

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getDatabaseClasses()
     */
    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        List<Class<? extends AbstractDatabaseObject>> result = new ArrayList<>();
        result.add(LostFoundItem.class);
        result.add(LostFoundImage.class);
        return result;
    }

    @Override
    public void updateData(Context context) {
        super.updateData(context);
        readLostFound(context);
    }



    public void readLostFound(Context context) {
        StringBuilder sb = buildUrl(context);
        LostCallback lcb = new LostCallback(context, null);
        FoundCallback fcb = new FoundCallback(context, null);
        ASiSTParser.getInstance().parse(sb + "/lost", LostList.class, lcb);
        ASiSTParser.getInstance().parse(sb + "/found", FoundList.class, fcb);
    }

    public static void readLostFound(Context context, final OnRefreshFinishedListener listener) {
        StringBuilder sb = buildUrl(context);
        LostCallback lcb = new LostCallback(context, listener);
        FoundCallback fcb = new FoundCallback(context, listener);
        ASiSTParser.getInstance().parseWithTask(sb + "/lost", LostList.class, lcb);
        ASiSTParser.getInstance().parseWithTask(sb + "/found", FoundList.class, fcb);
    }

    private static StringBuilder buildUrl(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getResources().getString(R.string.rootUrl));
        sb.append("/lostfound/");
        sb.append(context.getResources().getString(R.string.institutionName));
        return sb;
    }

    public static List<LostFoundItem> getItems(Context context, LOSTFOUNDTYPE type) {
        DatabaseManager<LostFoundItem> db = new DatabaseManager<>(context, LostFoundItem.class);
        return db.getObjects("type", type);
    }

    private static class LostCallback implements ParserCallback<LostList> {

        private Context context;
        private OnRefreshFinishedListener listener;

        public LostCallback(Context context, final OnRefreshFinishedListener listener) {
            this.context = context;
            this.listener = listener;
        }

        @Override
        public void onError(Exception e) {
            //TODO
        }

        @Override
        public void onManagedError(LostList parsed) {
            //TODO
        }

        @Override
        public void afterParse(LostList parsed) {
            try {
                lock.lock();
                DatabaseManager<LostFoundItem> db = new DatabaseManager<>(context, LostFoundItem.class);
                DatabaseManager<LostFoundImage> imageDB = new DatabaseManager<>(context, LostFoundImage.class);
                List<LostFoundItem> items = parsed.getLosts();
                for (LostFoundItem item : items) {
                    String title = item.getTitle();
                    Date date = item.getCreationTime();
                    Map<String, Object> queryMap = new HashMap<>();
                    queryMap.put("title", title);
                    queryMap.put("creationTime", date);
                    LostFoundItem dbItem = db.getObject(queryMap);
                    if (dbItem == null) {
                        LostFoundImage image = item.getImage();
                        if (image != null) {
                            imageDB.saveObject(image);
                        }
                        item.setType(LOSTFOUNDTYPE.LOST);
                        db.saveObject(item);
                    }
                }
                if (listener != null) {
                    listener.refreshFinished();
                }
            } catch (Exception e) {
                ////Log.e(TAG, "Error parsing lostlist", e);
            } finally {
                lock.unlock();
            }

        }
    }

    private static class FoundCallback implements ParserCallback<FoundList> {

        private Context context;
        private OnRefreshFinishedListener listener;

        public FoundCallback(Context context, final OnRefreshFinishedListener listener) {
            this.context = context;
            this.listener = listener;
        }

        @Override
        public void onError(Exception e) {
            //TODO
        }

        @Override
        public void onManagedError(FoundList parsed) {
            //TODO
        }

        @Override
        public void afterParse(FoundList parsed) {
            try {
                lock.lock();
                DatabaseManager<LostFoundItem> db = new DatabaseManager<>(context, LostFoundItem.class);
                DatabaseManager<LostFoundImage> imageDB = new DatabaseManager<>(context, LostFoundImage.class);

                List<LostFoundItem> items = parsed.getFound();
                for (LostFoundItem item : items) {
                    String title = item.getTitle();
                    Date date = item.getCreationTime();
                    Map<String, Object> queryMap = new HashMap<>();
                    queryMap.put("title", title);
                    queryMap.put("creationTime", date);
                    LostFoundItem dbItem = db.getObject(queryMap);
                    if (dbItem == null) {
                        LostFoundImage image = item.getImage();
                        imageDB.saveObject(image);
                        item.setType(LOSTFOUNDTYPE.FOUND);
                        db.saveObject(item);
                    }
                }
                if (listener != null) {
                    listener.refreshFinished();
                }
            } catch (Exception e) {
                //Log.e(TAG, "Error parsing foundlist", e);
            } finally {
                lock.unlock();
            }
        }
    }

}
