/**
 *
 */
package de.bps.asist.module.lostandfound;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.OnRefreshFinishedListener;
import de.bps.asist.module.lostandfound.model.LOSTFOUNDTYPE;
import de.bps.asist.module.lostandfound.model.LostFoundConfiguration;
import de.bps.asist.module.lostandfound.model.LostFoundItem;

import static de.bps.asist.gui.list.AbstractGenericListCallback.EXTRA_ITEM_SELECTED;

/**
 * @author thomasw
 */
public class LostAndFoundFragment extends AbstractASiSTFragment implements SwipeRefreshLayout.OnRefreshListener, OnRefreshFinishedListener {

    private static final int MAX_SIZE = 10;

    private int parseCounter;

    private ListView listView;

    private Menu menuLF;

    private SwipeRefreshLayout layout;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.generic_refresh_listview, container, false);
        listView = (ListView) view.findViewById(R.id.generic_list);

        layout = getView(view, R.id.generic_list_swipe);
        layout.setOnRefreshListener(this);

        layout.setColorSchemeResources(R.color.defaultColor,
                R.color.tintColor,
                R.color.defaultColor,
                R.color.tintColor);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        refreshListView();
    }

    private void refreshListView() {
        DatabaseManager<LostFoundItem> db = new DatabaseManager<>(getActivity(), LostFoundItem.class);

        Map<String, List<LostFoundItem>> map = new LinkedHashMap<>();
        List<LostFoundItem> lost = new ArrayList<>();
        List<LostFoundItem> found = new ArrayList<>();

        map.put(translate(R.string.module_lostfound_losts), lost);
        map.put(translate(R.string.module_lostfound_found), found);

        List<LostFoundItem> dbLost = db.getObjects("type", LOSTFOUNDTYPE.LOST);
        Collections.sort(dbLost);
        int maxLost = Math.min(MAX_SIZE, dbLost.size());
        if (maxLost > 0) {
            lost.addAll(dbLost.subList(0, maxLost));
        }

        List<LostFoundItem> dbFound = db.getObjects("type", LOSTFOUNDTYPE.FOUND);
        Collections.sort(dbFound);
        int maxFound = Math.min(MAX_SIZE, dbFound.size());
        if (maxFound > 0) {
            found.addAll(dbFound.subList(0, maxFound));
        }


        AbstractGenericListCallback cb = new AbstractGenericListCallback(getActivity(), LostFoundDetailActivity.class);

        LostFoundConfiguration config = new LostFoundConfiguration(MAX_SIZE);
        if (maxLost > 0) {
            config.addLastItem(lost.get(lost.size() - 1), LOSTFOUNDTYPE.LOST);
        }
        if (maxFound > 0) {
            config.addLastItem(found.get(found.size() - 1), LOSTFOUNDTYPE.FOUND);
        }

        LostFoundAdapter adapter = new LostFoundAdapter(getActivity(), map, cb, config);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (R.id.action_lostfound_found == item.getItemId()) {
            Intent intent = new Intent(getActivity(), LostFoundAddActivity.class);

            final Bundle bundle = new Bundle();
            bundle.putSerializable(EXTRA_ITEM_SELECTED, LOSTFOUNDTYPE.FOUND);
            intent.putExtra(EXTRA_ITEM_SELECTED, LOSTFOUNDTYPE.FOUND);
            startActivity(intent);
            return true;
        } else if (R.id.action_lostfound_lost == item.getItemId()) {
            Intent intent = new Intent(getActivity(), LostFoundAddActivity.class);

            final Bundle bundle = new Bundle();
            bundle.putSerializable(EXTRA_ITEM_SELECTED, LOSTFOUNDTYPE.LOST);
            intent.putExtra(EXTRA_ITEM_SELECTED, LOSTFOUNDTYPE.LOST);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public Integer getMenuLayout() {
        return R.menu.lostfound;
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menuLF, MenuInflater inflater){
        //Inflate the menu items for use on the action bar.
        inflater.inflate(R.menu.lostfound, menuLF);
        super.onCreateOptionsMenu(menuLF, inflater);
    }

    @Override
    public void onRefresh() {
        parseCounter = 2;
        LostAndFoundModule.readLostFound(getActivity(), this);
    }

    @Override
    public void refreshFinished() {
        parseCounter--;
        if (parseCounter == 0) {
            refreshListView();
            layout.setRefreshing(false);
        }
    }
}
