package de.bps.asist.module;

import android.content.Context;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.settings.model.SettingsConfig;
import de.bps.asist.module.start.tree.StartTreeConfig;

public abstract class AbstractAsistModule {

    private SettingsConfig settingsConfig;
    private StartTreeConfig startTreeConfig;
    private boolean enabled = true;
    private boolean loaded = false;
    private boolean shown = false;

	public AbstractAsistModule() {
		this(true);
	}

	protected AbstractAsistModule(final boolean register) {

	}
	public abstract int getName();

	/**
	 * 
	 * @return Returns the resource id to the icon of this module
	 */
	public abstract int getIcon();

	@Override
	public final String toString() {
		return "" + getName(); // FIXME
	}

	public abstract Fragment getInitialFragment();

	public void updateData(final Context context) {
		// do nothing by default
	}

	public abstract List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses();

    public SettingsConfig getSettingsConfig() {
        return settingsConfig;
    }

    public void setSettingsConfig(SettingsConfig settingsConfig) {
        this.settingsConfig = settingsConfig;
    }

    public StartTreeConfig getStartTreeConfig() {
        return startTreeConfig;
    }

    public void setStartTreeConfig(StartTreeConfig startTreeConfig) {
        this.startTreeConfig = startTreeConfig;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public boolean isShown(){
        return shown;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setShown(boolean shown) {this.shown = shown;}
}
