package de.bps.asist.module.feeds.model;

import java.util.List;

import de.bps.asist.core.model.IList;

public class FeedItemList implements IList {

	private List<FeedItem> items;

	@Override
	public List<FeedItem> getItems() {
		return items;
	}

	public void setItems(final List<FeedItem> items) {
		this.items = items;
	}
}
