package de.bps.asist.module.feeds;

import android.content.Context;

import java.util.Collection;

import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.feeds.model.FeedItem;

/**
 * Created by litho on 03.11.14.
 * Special layouter for Feed in tree view
 */
public class FeedTreeViewLayouter extends GenericListViewAdapter<FeedItem> {

    public FeedTreeViewLayouter(Context context, Collection<FeedItem> objects, IGenericListCallback callback) {
        super(context, objects, callback);
    }
}
