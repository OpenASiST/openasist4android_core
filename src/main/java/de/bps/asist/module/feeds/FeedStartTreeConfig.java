package de.bps.asist.module.feeds;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.bps.asist.R;
import de.bps.asist.core.ASiSTApplication;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.gui.detail.DetailsActivity;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.IGenericListCallback;
import de.bps.asist.module.feeds.model.FeedItem;
import de.bps.asist.module.start.tree.ASiSTTreeItem;
import de.bps.asist.module.start.tree.StartTreeConfig;

/**
 * Created by litho on 29.10.14.
 * Start tree view configuration file
 */
public class FeedStartTreeConfig extends StartTreeConfig<FeedModule> {

    private List<String> blackListedFeeds = Arrays.asList(ASiSTApplication.getContext().getResources().getStringArray(R.array.module_start_blacklisted_feeds));

    public FeedStartTreeConfig(FeedModule module){
        super(module);
    }

    @Override
    public ASiSTTreeItem<FeedItem> getRootNode(Context context) {
        String title = context.getResources().getString(getModule().getName());
        String description = context.getResources().getString(getModule().getDescription());
        ASiSTTreeItem rootItem = new ASiSTTreeItem(title, description);

        final DatabaseManager<FeedItem> db = new DatabaseManager<>(context, FeedItem.class);
        List<FeedItem> allFeedItems = filterNumberOfFeeds(db.getAll(), blackListedFeeds);
        try {
            Collections.sort(allFeedItems, new Comparator<FeedItem>() {
                public int compare(FeedItem o1, FeedItem o2) {
                    /*if (o1.getPubdate() == null || o2.getPubdate() == null) {
                        return 0;
                    } else if (o1.getPubdate() == null) {
                        return -1;
                    } else if (o2.getPubdate() == null) {
                        return 1;
                    }
                    return o2.getPubdate().compareTo(o1.getPubdate());*/
                    if (o1.getPubdate() == null) {
                        return -1;
                    }
                    if (o2.getPubdate() == null) {
                        return 1;
                    }
                    if (o1.getPubdate().equals(o2.getPubdate())) {
                        return 0;
                    }
                    if (o1.getPubdate() == o2.getPubdate()) {
                        return 0;
                    }
                    return o2.getPubdate().compareTo(o1.getPubdate());
                }
            });
        } catch (Exception e){
            // nothing to do
        }
        IGenericListCallback<FeedItem> callback = new AbstractGenericListCallback<>(context, DetailsActivity.class);
        FeedTreeViewLayouter layouter = new FeedTreeViewLayouter(context, allFeedItems, callback);
        for(FeedItem item: allFeedItems){
            Date pubDate = item.getPubdate();
            String descriptionValue = "";
            if (pubDate != null) {
                descriptionValue = String.valueOf(new SimpleDateFormat(context.getResources().getString(R.string.datetime_format), Locale.getDefault()).format(pubDate));
            }
            ASiSTTreeItem<FeedItem> treeItem = new ASiSTTreeItem<>(item.getTitle(), descriptionValue);
            treeItem.setModelItem(item);
            treeItem.setLayouter(layouter);
            rootItem.addChild(treeItem);
        }
        return rootItem;
    }

    /**
     * Filters blacklisted feed items.
     * @param feedItems
     * @param blackListedFeeds
     * @return
     */
    private List<FeedItem> filterNumberOfFeeds(List<FeedItem> feedItems, List<String> blackListedFeeds) {
        List<FeedItem> filteredFeedItems = new ArrayList<>();
        for (FeedItem feedItem : feedItems) {
            String feedIdString = Long.toString(feedItem.getFeedId());
            if (!blackListedFeeds.contains(feedIdString)) {
                // feedId is not on blacklist
                filteredFeedItems.add(feedItem);
            }
        }
        return filteredFeedItems;
    }
}
