package de.bps.asist.module.feeds.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

import de.bps.asist.core.annotation.AsistColumn;
import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistImage;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IListItem;

@DatabaseTable
@AsistItemConfiguration(showDescription = true)
public class FeedDescription extends AbstractDatabaseObject implements IListItem {
	private static final long serialVersionUID = 4109532230685022319L;

	@DatabaseField
	@AsistTitle
	@AsistColumn(position = 1, header = "feed_title")
	private String title;

	@DatabaseField
	@AsistColumn(position = 2)
	@AsistImage
	private String image;

	@DatabaseField
	@AsistColumn(position = 0, header = "feed_id")
	private Long feedid;

	@DatabaseField
	@JsonProperty(value = "desc")
	@AsistDescription
	private String description;

	@JsonIgnore
	private List<FeedItem> feeditems;

	FeedDescription() {
		//
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Long getFeedid() {
		return feedid;
	}

	public void setFeedid(final Long feedid) {
		this.feedid = feedid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(final String image) {
		this.image = image;
	}

	public List<FeedItem> getFeeditems() {
		return feeditems;
	}

	public void setFeeditems(final List<FeedItem> feeditems) {
		this.feeditems = feeditems;
	}

}
