package de.bps.asist.module.feeds;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.gui.detail.DetailsActivity;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.gui.list.OnRefreshFinishedListener;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.feeds.model.FeedDescription;
import de.bps.asist.module.feeds.model.FeedItem;
import de.bps.asist.module.start.AsistStartActivity;

public class SingleFeedActivity extends AsistModuleActivity<FeedDescription> implements SwipeRefreshLayout.OnRefreshListener, OnRefreshFinishedListener {

    private ListView listView;
    private SwipeRefreshLayout swipeLayout;
    private boolean initializedItems = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        initSingleFeed(false);
	}

    private void initSingleFeed(boolean refreshItems) {
        setContentView(R.layout.generic_refresh_listview);
        swipeLayout = getView(R.id.generic_list_swipe);
        swipeLayout.setOnRefreshListener(this);
        listView = (ListView) findViewById(R.id.generic_list);
        try{
            updateListView(refreshItems);
        } catch (Exception e){
            AlertDialog.Builder builder = new AlertDialog.Builder(SingleFeedActivity.this);
            builder.setMessage(R.string.serverErrorText)
                    .setTitle(R.string.serverError);
            builder.setPositiveButton("OK",null);
            builder.create();
            builder.show();
        }
    }

    private void updateListView(boolean refreshItems) {
        FeedDescription feedDescription = getSelectedItem();
        getSupportActionBar().setTitle(feedDescription.getTitle());
        List<FeedItem> items = feedDescription.getFeeditems();
        if (refreshItems || items == null || items.isEmpty()) {
            if (!refreshItems) {
                this.initializedItems = true;
                onRefresh();
            }
            final DatabaseManager<FeedDescription> db = new DatabaseManager<>(
				this, FeedDescription.class);
            final DatabaseManager<FeedItem> dbi = new DatabaseManager<>(this, FeedItem.class);
            final List<FeedItem> feedItems = dbi.getObjects("feedId", feedDescription.getFeedid());
            //Sort feed by date
            try {
                Collections.sort(feedItems, new Comparator<FeedItem>() {
                    public int compare(FeedItem o1, FeedItem o2) {
                        if (o1.getPubdate() == null || o2.getPubdate() == null)
                            return 0;
                        return o2.getPubdate().compareTo(o1.getPubdate());
                    }
                });
                feedDescription.setFeeditems(feedItems);
                items = feedItems;
            } catch (Exception e) {
                // nothing to do
            }
            db.release();
            dbi.release();

        }
        AbstractGenericListCallback cb = new AbstractGenericListCallback(this, DetailsActivity.class);
        GenericListViewAdapter<FeedItem> adapter = new GenericListViewAdapter<>(this, items, cb);
        listView.setAdapter(adapter);
        getSupportActionBar().setTitle(feedDescription.getTitle());
    }

    @Override
    public void refreshFinished() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initSingleFeed(true);
                swipeLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        FeedModule.refreshData(this, this);
    }

    @Override
    public void onBackPressed() {
        if (this.initializedItems) {
            Intent intent = new Intent(this, AsistStartActivity.class);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }
}
