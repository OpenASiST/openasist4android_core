package de.bps.asist.module.feeds;

import android.content.Context;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.core.manager.preferences.ASiSTPreferenceManager;
import de.bps.asist.gui.list.OnRefreshFinishedListener;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.feeds.model.FeedDescription;
import de.bps.asist.module.feeds.model.FeedDescriptionList;
import de.bps.asist.module.feeds.model.FeedItem;
import de.bps.asist.module.feeds.model.FeedItemList;
import de.bps.asist.module.start.AsistStartActivity;
import de.bps.asist.module.start.AsistStartModule;
import de.bps.asist.module.start.tree.ASiSTTreeItem;

public class FeedModule extends AbstractAsistModule {

	private static final String TAG = FeedModule.class.getCanonicalName();

    private static FeedStartTreeConfig startTreeConfig;

    private static FeedModule INSTANCE;

    public FeedModule(){
        INSTANCE = this;
        startTreeConfig = new FeedStartTreeConfig(this);
        setStartTreeConfig(startTreeConfig);
        AsistStartModule.registerForPush(getName(), "FeedModule");
    }

	@Override
	public int getName() {
		return R.string.module_feeds_name;
	}

	public int getDescription() {
		return R.string.module_feeds_description;
	}

	@Override
	public int getIcon() {
		return R.drawable.ic_menu_news;
	}

	@Override
	public Fragment getInitialFragment() {
		return new FeedFragment();
	}

	@Override
	public void updateData(final Context context) {
		super.updateData(context);

        refreshData(context, null);
	}

    public static void refreshData(Context context, OnRefreshFinishedListener listener) {
		UriComponentsBuilder getFeedDescriptionUriBuilder = UriComponentsBuilder.fromUriString(context.getString(R.string.rootUrl));

		String feedApiVersion = context.getString(R.string.rest_feed_api_version);
		if (feedApiVersion != null) {
			if (!feedApiVersion.isEmpty()) {
				getFeedDescriptionUriBuilder.pathSegment(feedApiVersion);
			}
		}

		getFeedDescriptionUriBuilder.pathSegment("feed", context.getResources().getString(R.string.institutionName), "list");

		UriComponents getFeedDescriptionUri = getFeedDescriptionUriBuilder.build();

        final FeedCallBack cb = new FeedCallBack(context, listener);
        if(listener == null) {
			ASiSTParser.getInstance().parse(getFeedDescriptionUri.toUriString(), FeedDescriptionList.class, cb);
        } else {
			ASiSTParser.getInstance().parseWithTask(getFeedDescriptionUri.toUriString(), FeedDescriptionList.class, cb);
        }
    }

	/***
	 * Updates the feed entries in the start screen
	 *
	 * @param context Associated content from the application. This is needed to open the database.
	 */
	private static void updateStartFeedView(Context context) {
		startTreeConfig = new FeedStartTreeConfig(INSTANCE);
		ASiSTTreeItem<FeedItem> root = startTreeConfig.getRootNode(context);
		String moduleName = context.getString(R.string.module_feeds_name);
		ASiSTPreferenceManager apm = new ASiSTPreferenceManager();
		boolean enabled = apm.getSetting(context, AsistStartActivity.START_VIEW_TREE + moduleName);
		root.setEnabled(enabled);
		AsistStartModule.update(root);
	}

    private static class FeedCallBack implements ParserCallback<FeedDescriptionList> {

		private final Context context;
        private OnRefreshFinishedListener listener;

		public FeedCallBack(final Context context, final OnRefreshFinishedListener listener) {
			this.context = context;
            this.listener = listener;
		}

        @Override
        public void onError(Exception e) {
        }

		@Override
		public void onManagedError(FeedDescriptionList parsed) {
			//TODO
		}

		@Override
		public void afterParse(final FeedDescriptionList parsedFeedDescriptions) {
			if (parsedFeedDescriptions != null) {
				final DatabaseManager<FeedDescription> feedDescriptionDatabaseManager = new DatabaseManager<>(context, FeedDescription.class);
				final Map<Long, FeedDescription> currentCachedIdRelatedFeedDescriptions = buildIdRelatedFeedDescriptionMap(feedDescriptionDatabaseManager.getAll());

				for (final FeedDescription feedDescription : parsedFeedDescriptions.getItems()) {
					// update existing feed description
					if (currentCachedIdRelatedFeedDescriptions.containsKey(feedDescription.getFeedid())) {
						FeedDescription updatingFeedDescription = currentCachedIdRelatedFeedDescriptions.remove(feedDescription.getFeedid());
						updatingFeedDescription.setTitle(feedDescription.getTitle());
						updatingFeedDescription.setDescription(feedDescription.getDescription());
						updatingFeedDescription.setImage(feedDescription.getImage());
						feedDescriptionDatabaseManager.saveObject(updatingFeedDescription);
					} else {
						// save new feed description
						feedDescriptionDatabaseManager.saveObject(feedDescription);
					}
					parseFeed(feedDescription, context, listener);
				}

				// remove unneeded feed discriptions and associated feed items
				if (!currentCachedIdRelatedFeedDescriptions.isEmpty()) {
					DatabaseManager<FeedItem> feedItemDatabaseManager = new DatabaseManager<>(context, FeedItem.class);
					for (Long deletingFeedDescriptionId : currentCachedIdRelatedFeedDescriptions.keySet()) {
						feedItemDatabaseManager.delete("feedId", deletingFeedDescriptionId);
						feedDescriptionDatabaseManager.delete("feedid", deletingFeedDescriptionId);
					}
					feedItemDatabaseManager.release();

					updateStartFeedView(context);
				}
				feedDescriptionDatabaseManager.release();
			}
			if (listener != null) {
				listener.refreshFinished();
			}
		}

		private static Map<Long, FeedDescription> buildIdRelatedFeedDescriptionMap(final Collection<FeedDescription> feedDescriptions) {
			final HashMap<Long, FeedDescription> idRelatedFeedDescriptions = new HashMap<>(feedDescriptions.size());
			for (final FeedDescription feedDescription : feedDescriptions) {
				idRelatedFeedDescriptions.put(feedDescription.getFeedid(), feedDescription);
			}
			return idRelatedFeedDescriptions;
		}
	}

	private static void parseFeed(final FeedDescription feedDescription, final Context context, OnRefreshFinishedListener listener) {
		try {
			UriComponentsBuilder getFeedItemsUri = UriComponentsBuilder.fromUriString(context.getString(R.string.rootUrl));

			String feedApiVersion = context.getString(R.string.rest_feed_api_version);
			if (feedApiVersion != null) {
				if (!feedApiVersion.isEmpty()) {
					getFeedItemsUri.pathSegment(feedApiVersion);
				}
			}

			getFeedItemsUri
					.pathSegment("feed")
					.pathSegment(context.getResources().getString(R.string.institutionName))
					.pathSegment("items")
					.pathSegment(String.valueOf(feedDescription.getFeedid()));

			final FeedItemCallback cb = new FeedItemCallback(feedDescription.getFeedid(), context);
			ASiSTParser.getInstance().parseWithThread(getFeedItemsUri.build().toUriString(), FeedItemList.class, cb);
		} catch (final Exception e) {
			////Log.e(TAG, "could not parseWithThread " + fd.getFeedid(), e);
		}
	}

	private static class FeedItemCallback implements ParserCallback<FeedItemList> {

		private final Context context;
		private final Long fdId;

		public FeedItemCallback(final Long fdId, final Context context) {
			this.context = context;
			this.fdId = fdId;
		}

        @Override
        public void onError(Exception e) {
        }

		@Override
		public void onManagedError(FeedItemList parsed) {
			//TODO
		}

		@Override
		public void afterParse(final FeedItemList parsedFeedItem) {
			if (parsedFeedItem != null) { // list is null if it could not be parsed
				final DatabaseManager<FeedItem> feedItemDatabaseManager = new DatabaseManager<>(context, FeedItem.class);
				feedItemDatabaseManager.delete("feedId", this.fdId);
				for (final FeedItem feedItem : parsedFeedItem.getItems()) {
					feedItem.setFeedId(this.fdId);
					feedItemDatabaseManager.saveObject(feedItem);
				}
				updateStartFeedView(context);
			}
		}
	}

	@Override
	public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
		final List<Class<? extends AbstractDatabaseObject>> result = new ArrayList<>();
		result.add(FeedDescription.class);
		result.add(FeedItem.class);
		return result;
	}

}
