package de.bps.asist.module.feeds.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import de.bps.asist.core.annotation.AsistAuthor;
import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistImage;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistLink;
import de.bps.asist.core.annotation.AsistPubDate;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.parser.PubdateDeserializer;

@AsistItemConfiguration(showDescription = true, useCallback = true, showMainLogo = true)
@DatabaseTable
public class FeedItem extends AbstractDatabaseObject {

	private static final long serialVersionUID = 3577795508009911141L;

	@DatabaseField
	@AsistTitle
	private String title;

	@DatabaseField(canBeNull = true)
	@JsonProperty(value = "desc")
	@AsistDescription
	private String description;

	@DatabaseField
    @AsistImage
	private String imageUrl;

	@DatabaseField
	@AsistLink
	private String link;

	@DatabaseField
	@AsistAuthor
	private String author;

	@DatabaseField
	private String guid;

	@DatabaseField
	@JsonIgnore
	private long feedId;

	@DatabaseField
	@JsonDeserialize(using = PubdateDeserializer.class)
    @AsistPubDate
	private Date pubdate;

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getDescription() {
		if(description != null)
			return description;
		else
			return "";
	}

	public void setDescription(final String description) {
        this.description = description;
	}

	private boolean invalideURL(String url) {
		if ("".equals(url)) {
			return true;
		}
		if (url == null) {
			return true;
		}
		if (url.length() > 150) {
			return true;
		}
		if (url.contains("<")) {
			return true;
		}
		return false;
	}

	public String getLink() {
		return link;
	}

	public void setLink(final String link) {
		this.link = link;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(final String author) {
		this.author = author;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(final String guid) {
		this.guid = guid;
	}

	public long getFeedId() {
		return this.feedId;
	}

	public void setFeedId(final Long feedId) {
		this.feedId = feedId;
	}

	public Date getPubdate() {
		return pubdate;
	}

	public void setPubdate(final Date pubdate) {
		this.pubdate = pubdate;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(final String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public void setFeedId(final long feedId) {
		this.feedId = feedId;
	}

}
