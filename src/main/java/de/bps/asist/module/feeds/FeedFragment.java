package de.bps.asist.module.feeds;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.module.feeds.model.FeedDescription;
import de.bps.asist.module.feeds.model.FeedItem;

public class FeedFragment extends AbstractASiSTFragment {

	private ListView listView;

	/*
	 * @see androidx.core.app.Fragment#onCreateView(android.view.LayoutInflater,
	 *      android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
			final Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.feed_activity, container, false);
        listView = (ListView) view.findViewById(R.id.feedListView);
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		// feeds
		final DatabaseManager<FeedDescription> db = new DatabaseManager<>(
				getActivity(), FeedDescription.class);
		final List<FeedDescription> items = db.getAll();
		final DatabaseManager<FeedItem> dbi = new DatabaseManager<>(getActivity(),
				FeedItem.class);
		for (final FeedDescription feedDescription : items) {
			final List<FeedItem> feedItems = dbi.getObjects("feedId", feedDescription.getFeedid());
			//Sort feed by date
			try {
				Collections.sort(feedItems, new Comparator<FeedItem>() {
					public int compare(FeedItem o1, FeedItem o2) {
						if (o1.getPubdate() == null || o2.getPubdate() == null)
							return 0;
						return o2.getPubdate().compareTo(o1.getPubdate());
					}
				});
				feedDescription.setFeeditems(feedItems);
			} catch (Exception e) {

			}
			db.release();
			dbi.release();

			final GenericListViewAdapter<FeedDescription> adapter = new GenericListViewAdapter<>(
					getActivity(), items, new AbstractGenericListCallback(getActivity(),
					SingleFeedActivity.class));
			listView.setAdapter(adapter);
		}
	}

}
