package de.bps.asist.module.feeds.model;

import java.util.List;

import de.bps.asist.core.model.IList;

public class FeedDescriptionList implements IList {

	private List<FeedDescription> feeds;

	@Override
	public List<FeedDescription> getItems() {
		return getFeeds();
	}

	public List<FeedDescription> getFeeds() {
		return feeds;
	}

	public void setFeeds(final List<FeedDescription> feeds) {
		this.feeds = feeds;
	}

}
