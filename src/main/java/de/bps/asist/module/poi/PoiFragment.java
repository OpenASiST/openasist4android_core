/**
 *
 */
package de.bps.asist.module.poi;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.j256.ormlite.stmt.query.In;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import de.bps.asist.R;
import de.bps.asist.core.model.common.Position;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.module.poi.model.PoiItem;
import de.bps.asist.module.poi.model.poiRefreshEvent;


/**
 * @author thomasw
 *         Initial Fragment for POI view
 */
public class PoiFragment extends AbstractASiSTFragment implements AdapterView.OnItemSelectedListener, OnMapReadyCallback {

    private static final String TAG = PoiFragment.class.getSimpleName();

    private SupportMapFragment fragment;
    private GoogleMap map;
    private PoiItemArrayAdapter adapter;
    private Marker currentMarker;
    public PoiItem preselectedPoi = null;

    private View view;
    private TextView textView;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 10;
    private LocationRequest locationRequest;

    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int REQUEST_CODE = 9000;
    private static final long REQUEST_INTERVAL = 10000l;
    private static final float REQUEST_SMALLEST_DISPLACEMENT = 200.0f;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.module_poi_start, container, false);
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int status = googleAPI.isGooglePlayServicesAvailable(getContext());
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = googleAPI.getErrorDialog(getActivity(), status, requestCode);
            dialog.show();
        }

        //Check if GPS is active and ask user to do if not
        RequestCheckSettings();
        //Check if app has Location Permissions and if not, ask for permissions
        checkLocationPermission();

        //Check if permissions and set text for the result
        textView = (TextView) view.findViewById(R.id.poi_textview);
        if(ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            textView.setVisibility(View.GONE);
        }
        else{
            textView.setVisibility(View.VISIBLE);
            textView.setText(R.string.module_pts_location_permission_request_failed);
        }


        Spinner spinner = getView(view, R.id.buildings_map_spinner);
        adapter = new PoiItemArrayAdapter(getActivity());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        try {
            if(preselectedPoi != null){
                try {
                    int posStartItem = getSpinnerIndex(spinner, preselectedPoi.getTitle());
                    spinner.setSelection(posStartItem, false);
                } catch (Exception e){
                    int posStartItem = getSpinnerIndex(spinner, getString(R.string.startBuildingName));
                    spinner.setSelection(posStartItem, false);
                }
            } else {
                int posStartItem = getSpinnerIndex(spinner, getString(R.string.startBuildingName));
                spinner.setSelection(posStartItem, false);
            }
        } catch (Exception e){
            //Log.e("POI: ", "Couldn't select startup POI: "+e.getMessage());
        }
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        return view;
    }

    @Subscribe
    public void onEvent(poiRefreshEvent event){
        getFragmentManager()
                .beginTransaction()
                .detach(this)
                .attach(this)
                .commit();
    }

    private int getSpinnerIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            PoiItem curItem = (PoiItem) spinner.getItemAtPosition(i);
            if (curItem.getTitle().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            FragmentManager fm = getChildFragmentManager();
            fragment = (SupportMapFragment) fm.findFragmentById(R.id.buildings_map_mapview);
            if (fragment == null) {
                ////Log.d(TAG, "creating fragment since it does not exist");
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.buildings_map_mapview, fragment).commit();
                fragment.onActivityCreated(savedInstanceState);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this);
            }
            //textView.setVisibility(View.GONE);
            FragmentManager fm = getChildFragmentManager();
            fragment = (SupportMapFragment) fm.findFragmentById(R.id.buildings_map_mapview);
            if (fragment == null) {
                ////Log.d(TAG, "creating fragment since it does not exist");
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.buildings_map_mapview, fragment).commit();
            }
            if (map == null) {
                fragment.getMapAsync(this);
            }
            PoiItem startItem = null;
            for (PoiItem i : adapter.getItems()) {
                if (i.getTitle().equals(getString(R.string.startBuildingName))) {
                    startItem = i;
                    break;
                }
            }
            if (startItem != null) {
                selectPoiItem(startItem);
            }
        }
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
        if(fragment != null){
            fragment.onPause();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        PoiItem item = adapter.getItem(position);
        selectPoiItem(item);
    }

    public void selectPoiItem(PoiItem item) {
        if(map != null) {
            Position pos = item.getPosition();

            ////Log.g.v(TAG, "setting latlong " + pos.getLatitude() + "," + pos.getLongitude());
            LatLng point = new LatLng(pos.getLatitude(), pos.getLongitude());

            CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(point, 15f);
            map.moveCamera(camUpdate);
            if (currentMarker != null) {
                currentMarker.remove();
            }
            MarkerOptions marker = new MarkerOptions().position(point).title(item.getTitle());
            currentMarker = map.addMarker(marker);
        }
    }

    public void setPreselectedPoi(PoiItem poi){
        preselectedPoi = poi;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if(ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if(map != null) {
                map.setMyLocationEnabled(true);
            }
        }
        else{
            checkLocationPermission();
        }
    }

    //Checks if permission is enabled and if not tells user why permission is needed
    private void checkLocationPermission(){
        if(ContextCompat.checkSelfPermission(this.getContext(),Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this.getActivity(),Manifest.permission.ACCESS_FINE_LOCATION)){
                new AlertDialog.Builder(this.getContext())
                        .setTitle(R.string.module_pts_location_permission_request_dialog_titel)
                        .setMessage(R.string.module_pts_location_permission_request_dialog_content)
                        .setNeutralButton(R.string.module_pts_location_permission_request_dialog_ok_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                            }
                        })
                        .show();
            } else{
                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }
    }

    //Asks to activate GPS
    private void RequestCheckSettings(){
        this.locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(REQUEST_INTERVAL)
                .setSmallestDisplacement(REQUEST_SMALLEST_DISPLACEMENT);

        LocationSettingsRequest locationSettingsRequest = new LocationSettingsRequest.Builder()
                .setAlwaysShow(true)
                .addLocationRequest(this.locationRequest)
                .build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(getContext());
        settingsClient.checkLocationSettings(locationSettingsRequest)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        if (exception instanceof ResolvableApiException) {
                            ResolvableApiException resolvableApiException = (ResolvableApiException) exception;
                            try {
                                resolvableApiException.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                //do nothing, because exception is thrown if problem is not solvable
                            }
                        }
                    }
                });
    }

    //Look if user gave permission or not
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                if(requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION){
                    if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        try {
                            textView.setVisibility(View.GONE);
                        } catch (SecurityException exeption){
                            textView.setText(R.string.module_pts_location_permission_request_failed);
                        }
                    } else {
                        textView.setText(R.string.module_pts_location_permission_request_failed);
                    }
                    break;
                }
        }
    }
}
