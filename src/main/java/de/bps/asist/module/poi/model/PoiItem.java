package de.bps.asist.module.poi.model;

import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.common.Position;

@DatabaseTable
public class PoiItem extends AbstractDatabaseObject implements Serializable {

	private static final long serialVersionUID = 8161643674912001343L;

	@DatabaseField
	private int poiid;

	@DatabaseField
	private String title;

	@DatabaseField
	private String shortName;

	@DatabaseField
	private String imageUrl;

	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Position position;

	@JsonProperty(value = "desc")
    @DatabaseField
	private String description;

    @DatabaseField
    private int type;

    @DatabaseField
    private Integer reference;

	public int getPoiid() {
		return poiid;
	}

	public void setPoiid(final int poiid) {
		this.poiid = poiid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(final String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(final Position position) {
		this.position = position;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }
}
