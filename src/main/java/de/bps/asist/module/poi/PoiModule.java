/**
 *
 */
package de.bps.asist.module.poi;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.environment.ASiSTCoreDataManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.core.model.common.Position;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.poi.model.PoiItem;
import de.bps.asist.module.poi.model.PoiList;
import de.bps.asist.module.poi.model.poiRefreshEvent;

/**
 * @author thomasw
 */
public class PoiModule extends AbstractAsistModule {

    private static final Lock lock = new ReentrantLock();
    private static final String TAG = PoiModule.class.getSimpleName();

    public PoiModule() {
    }

    private static List<PoiItem> poiItems;


    public static List<PoiItem> getPoiItems(Context context) {
        if (poiItems == null) {
            DatabaseManager<PoiItem> poiDB = new DatabaseManager<>(context, PoiItem.class);
            poiItems = poiDB.getAll();
        }
        return poiItems;
    }

    public static PoiItem findItemByPoiId(final Context context, Integer poiid) {
        if (poiid == null) {
            return null;
        }
        lock.lock();
        try {
            DatabaseManager<PoiItem> poiDB = new DatabaseManager<>(context, PoiItem.class);
            return poiDB.getObject("poiid", poiid);
        } catch (Exception e) {
            //Log.e(TAG, "could not find poi with poiid", e);
            throw new IllegalArgumentException("could not find poi with poiid " + poiid, e);
        } finally {
            lock.unlock();
        }
    }

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getName()
     */
    @Override
    public int getName() {
        return R.string.module_locationplan_name;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_location;
    }

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getInitialFragment()
     */
    @Override
    public Fragment getInitialFragment() {
        return new PoiFragment();
    }

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getDatabaseClasses()
     */
    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        final List<Class<? extends AbstractDatabaseObject>> result = new ArrayList<>();
        result.add(PoiItem.class);
        result.add(Position.class);
        return result;
    }

    @Override
    public void updateData(final Context context) {
        super.updateData(context);
        final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
        final StringBuilder url = new StringBuilder(rootUrl);
        url.append("/poi/");
        url.append(ASiSTCoreDataManager.getInstance().getInstitutionName());
        url.append("/all");
        ASiSTParser.getInstance().parse(url.toString(), PoiList.class, new PoiCallback(context));
    }

    public static List<PoiItem> getAllPois(Context context) {
        lock.lock();
        try {
            final DatabaseManager<PoiItem> poiDB = new DatabaseManager<>(context, PoiItem.class);
            List<PoiItem> result = poiDB.getAll();
            poiDB.release();
            return result;
        }catch(Exception e){
            //Log.e(TAG, "could not get all items", e);
            return new ArrayList<>();
        } finally {
            lock.unlock();
        }
    }

    public static PoiItem findNearestItem(Context context, Location currentLocation) {
        double currentLat = currentLocation.getLatitude();
        double currentLong = currentLocation.getLongitude();
        float minDist = Float.MAX_VALUE;
        PoiItem result = null;
        for (PoiItem item : getAllPois(context)) {
            Position pos = item.getPosition();
            if (pos == null) {
                continue;
            }
            float[] resultDist = new float[1];
            double itemLat = pos.getLatitude();
            double itemLong = pos.getLongitude();
            Location.distanceBetween(currentLat, currentLong, itemLat, itemLong, resultDist);
            if (resultDist[0] < minDist) {
                minDist = resultDist[0];
                result = item;
            }
        }
        return result;
    }

    private class PoiCallback implements ParserCallback<PoiList> {

        private final Context context;

        public PoiCallback(final Context context) {
            this.context = context;
        }

        @Override
        public void onError(Exception e) {
            //TODO
        }

        @Override
        public void onManagedError(PoiList parsed) {
            //TODO
        }

        @Override
        public void afterParse(final PoiList parsed) {
            final DatabaseManager<PoiItem> poiDB = new DatabaseManager<>(context, PoiItem.class);
            final DatabaseManager<Position> posDB = new DatabaseManager<>(context, Position.class);

            lock.lock();
            try {
                poiDB.deleteAll();
                posDB.deleteAll();

                final List<PoiItem> pois = parsed.getPois();
                for (final PoiItem item : pois) {
                    final Position pos = item.getPosition();
                    posDB.saveObject(pos);
                    poiDB.saveObject(item);
                }
                EventBus.getDefault().post(new poiRefreshEvent());

            }catch(Exception e){
                //Log.e(TAG, "could not update pois", e);
            } finally {
                lock.unlock();
            }

        }

    }

}
