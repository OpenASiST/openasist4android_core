package de.bps.asist.module.poi.model;

import java.util.List;

import de.bps.asist.core.model.IList;
import de.bps.asist.core.model.IListItem;

public class PoiList implements IList {

	private List<PoiItem> pois;

	public List<PoiItem> getPois() {
		return pois;
	}

	public void setPois(final List<PoiItem> pois) {
		this.pois = pois;
	}

	@Override
	public List<? extends IListItem> getItems() {
		return pois;
	}

}
