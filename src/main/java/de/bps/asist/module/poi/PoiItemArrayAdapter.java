package de.bps.asist.module.poi;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.module.poi.model.PoiItem;

/**
 * Created by litho on 22.08.14.
 * Adapter for POI items. Only PoiItems with position are displayed
 */
public class PoiItemArrayAdapter extends ArrayAdapter<PoiItem> {

    private List<PoiItem> items;
    private Context context;

    public PoiItemArrayAdapter(Context context){
        super(context, android.R.layout.simple_spinner_item);
        this.context = context;
        DatabaseManager<PoiItem> db = new DatabaseManager<>(context, PoiItem.class);
        List<PoiItem> temp = db.getAll();
        if(temp.size() > 0)
        Collections.sort(temp, new Comparator<PoiItem>()
        {
            @Override
            public int compare(final PoiItem poiItem1, final PoiItem PoiItem2)
            {
                return poiItem1.getTitle().compareTo(PoiItem2.getTitle());
            }
        });
        this.items = new ArrayList<>();
        for(PoiItem item: temp){
            if(item.getPosition() != null){
                items.add(item);
            }
        }
    }

    public PoiItem getFirstItem(){
        if(items.size() > 0){
            return items.get(0);
        }
        return null;
    }

    public List<PoiItem> getItems(){
        return items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public PoiItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        PoiItem item = getItem(position);
        return item.getPoiid();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        PoiItem item = getItem(position);
        view.setText(item.getTitle());
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        PoiItem item = getItem(position);
        view.setText(item.getTitle());
        return view;
    }
}
