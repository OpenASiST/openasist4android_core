package de.bps.asist.module.poi;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.fragment.app.FragmentManager;
import de.bps.asist.R;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.poi.model.PoiItem;
import de.bps.asist.module.timetable.model.CourseDescription;

public class PoiActivity extends AsistModuleActivity<CourseDescription> {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.generic_fragment_activity);


        Intent i = getIntent();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("poi")) {
                getSupportFragmentManager().executePendingTransactions();
                PoiItem poi = (PoiItem) extras.get("poi");
                FragmentManager fm = getSupportFragmentManager();
                PoiFragment fragment = new PoiFragment();
                fragment.preselectedPoi = poi;
                fm.beginTransaction().add(R.id.generic_fragment_container, fragment, "CurrPoiFrag").commit();
            }
            else {
                FragmentManager fm = getSupportFragmentManager();
                PoiFragment fragment = new PoiFragment();
                fm.beginTransaction().add(R.id.generic_fragment_container, fragment, "CurrPoiFrag").commit();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
