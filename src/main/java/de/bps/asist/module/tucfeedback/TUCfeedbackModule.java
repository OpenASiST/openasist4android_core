package de.bps.asist.module.tucfeedback;

import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;

/**
 * Created by epereira on 13.09.2016.
 */
public class TUCfeedbackModule extends AbstractAsistModule {
    @Override
    public int getName() {
        return R.string.module_tucfeedback_name;
    }

    @Override
    public int getIcon() {
        //TODO: Set proper icon
        return R.drawable.module_feedbacktuc_icon;
    }

    @Override
    public Fragment getInitialFragment() {
        return new FeedbackFragment();
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        return null;
    }
}
