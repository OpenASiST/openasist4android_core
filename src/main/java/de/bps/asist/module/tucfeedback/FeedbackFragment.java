package de.bps.asist.module.tucfeedback;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import java.util.Locale;

import de.bps.asist.R;
import de.bps.asist.gui.AbstractASiSTFragment;

/**
 * Created by epereira on 13.09.2016.
 */
public class FeedbackFragment extends AbstractASiSTFragment {
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.feedback_webview_fragment, container, false);
        WebView webView = (WebView) view.findViewById(R.id.feedback_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        String lang = Locale.getDefault().getLanguage();
        webView.loadUrl(getString(R.string.feedback_url));
        return view;
    }
}
