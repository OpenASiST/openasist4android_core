/**
 * 
 */
package de.bps.asist.module.publictransport;

import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;

/**
 * @author thomasw
 * 
 */
public class PublicTransportModule extends AbstractAsistModule {

	/**
	 * @see de.bps.asist.module.AbstractAsistModule#getName()
	 */
	@Override
	public int getName() {
		return R.string.module_publictransport_name;
	}

	@Override
	public int getIcon() {
		return R.drawable.ic_menu_transport;
	}

	/**
	 * @see de.bps.asist.module.AbstractAsistModule#getInitialFragment()
	 */
	@Override
	public Fragment getInitialFragment() {
		return new PublicTransportFragment();
	}

	/**
	 * @see de.bps.asist.module.AbstractAsistModule#getDatabaseClasses()
	 */
	@Override
	public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
		return null;
	}

}
