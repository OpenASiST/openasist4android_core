/**
 * 
 */
package de.bps.asist.module.publictransport;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.bps.asist.R;
import de.bps.asist.gui.AbstractASiSTFragment;

/**
 * @author thomasw
 * 
 */
public class PublicTransportFragment extends AbstractASiSTFragment {

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.publictransport_fragment, container, false);
		return view;
	}

}
