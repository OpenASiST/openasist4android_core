package de.bps.asist.module.settings.model;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

import de.bps.asist.R;

public class NumberPickerPreference extends DialogPreference {

	NumberPicker picker;
	Integer initialValue;

    public NumberPickerPreference(Context context) {
        this(context, null);
    }
	
	public NumberPickerPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
        setDialogLayoutResource(R.layout.number_pref);
	}
	
	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);
		this.picker = (NumberPicker)view.findViewById(R.id.numberPicker1);
		picker.setMinValue(1);
        picker.setMaxValue(7);
        picker.setWrapSelectorWheel(false);
		if ( this.initialValue != null ){
            picker.setValue(initialValue);
        } else {
            picker.setValue(4);
        }
	}
	
	@Override
	public void onClick(DialogInterface dialog, int which) {
		super.onClick(dialog, which);
		if ( which == DialogInterface.BUTTON_POSITIVE ) {
			this.initialValue = picker.getValue();
			persistInt( initialValue );
			callChangeListener( initialValue );
		}
	}
	
	@Override
	protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
		int def = ( defaultValue instanceof Number ) ? (Integer)defaultValue
				: ( defaultValue != null ) ? Integer.parseInt(defaultValue.toString()) : 1;
		if ( restorePersistedValue ) {
			this.initialValue = getPersistedInt(def);
		} else {
            this.initialValue = (Integer)defaultValue;
        }
	}
		
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return a.getInt(index, 1);
	}

    public Integer getInitialValue() {
        return initialValue;
    }
}