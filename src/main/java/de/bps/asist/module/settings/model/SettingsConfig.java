package de.bps.asist.module.settings.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by litho on 29.10.14.
 */
public class SettingsConfig implements Serializable {

    private List<SettingsSection> settings = new ArrayList<>();

    public SettingsConfig(){

    }

    public void addSection(SettingsSection section){
        settings.add(section);
    }

    public List<SettingsSection> getSettings() {
        return settings;
    }
}
