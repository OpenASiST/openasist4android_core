package de.bps.asist.module.settings.model;

import de.bps.asist.module.start.SettingsChangedCallback;

/**
 * Created by litho on 30.10.14.
 * Settings Item
 */
public abstract class SettingsItem<T> {

    public enum SETTINGS_TYPE {
        CHECKBOX, LIST, SERVER_CHECKBOX, NUMBER
    }

    private String title;
    private int description;
    private int i18nTitle;
    private String key;
    private SETTINGS_TYPE type;
    private SettingsChangedCallback callback;
    private T defaultValue;
    private boolean enabled;

    public SettingsItem(String title){
        this.title = title;
        type = SETTINGS_TYPE.CHECKBOX;
    }

    public SettingsItem(int title){
        this.i18nTitle = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDescription() {
        return description;
    }

    public void setDescription(int description) {
        this.description = description;
    }

    public SETTINGS_TYPE getType() {
        return type;
    }

    public void setType(SETTINGS_TYPE type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public SettingsChangedCallback getCallback() {
        return callback;
    }

    public void setCallback(SettingsChangedCallback callback) {
        this.callback = callback;
    }

    public int getI18nTitle() {
        return i18nTitle;
    }

    public void setI18nTitle(int i18nTitle) {
        this.i18nTitle = i18nTitle;
    }

    public T getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(T defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
