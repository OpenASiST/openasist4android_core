package de.bps.asist.module.settings;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.machinarius.preferencefragment.PreferenceFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.manager.ASiSTModuleManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.manager.preferences.ASiSTPreferenceManager;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.canteen.CanteenModule;
import de.bps.asist.module.settings.model.ListSettingsItem;
import de.bps.asist.module.settings.model.NumberPickerPreference;
import de.bps.asist.module.settings.model.SettingsConfig;
import de.bps.asist.module.settings.model.SettingsItem;
import de.bps.asist.module.settings.model.SettingsSection;

/**
 * Created by litho on 09.10.14.
 * Initial Fragment for Settings view
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private Map<String, SettingsItem> settingsMap = new HashMap<>();

    private String preferencesName = EnvironmentManager.SHAREDPREFERENCE_ENVIRONMENT;

    private List<SettingsConfig> settingsConfigList;

    private Map<String, PreferenceCategory> childMap = new HashMap<>();

    ASiSTPreferenceManager APM = new ASiSTPreferenceManager();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(preferencesName);
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        setHasOptionsMenu(true);
        findSettingsConfigs();
        addPreferencesFromResource(R.xml.preferences);
        PreferenceScreen ps = getPreferenceScreen();
        ps.removeAll();
        buildPreferences();
        addPreferencesFromResource(R.xml.preferences);
        Preference pref1 = findPreference( "app_version" );
        try {
            pref1.setSummary(appVersion());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.settings_layout, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater){
        //Inflate the menu items for use on the action bar. Currently, only the "show impressum" button is here.
        //inflater.inflate(R.menu.settings_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        //Handle the selection of options on the action bar
        int id = item.getItemId();

        if(id == R.id.action_showImpressum) {
            openImpressum();
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }

    }

    protected void openImpressum(){
        //Generating a popup view with an HTML text containing the Impressum
        //The impressum text can be found under customersettins/values/strings.
        AlertDialog.Builder dImpBuilder = new AlertDialog.Builder(this.getActivity());
        dImpBuilder.setPositiveButton("OK", null);
        LayoutInflater inflater = this.getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.impressum_dialog, null);
        dImpBuilder.setView(dialogView);

        TextView impressumTextView = (TextView) dialogView.findViewById(R.id.impressumText);
        impressumTextView.setText(Html.fromHtml(getResources().getString(R.string.impressumText)));

        AlertDialog ImpDialog = dImpBuilder.create();
        ImpDialog.show();
    }

    protected void findSettingsConfigs() {
        if (settingsConfigList == null) {
            settingsConfigList = new ArrayList<>();
            List<AbstractAsistModule> modules = ASiSTModuleManager.getInstance().getModules();
            for (AbstractAsistModule module : modules) {
                SettingsConfig config = module.getSettingsConfig();
                //TODO unsauber, aber einfacher als auf die schnelle die SettingsConfig aus dem CanteenModule zu holen
                if(!(module instanceof CanteenModule)){
                    if (config != null) {
                        settingsConfigList.add(config);
                    }
                }
            }
        }
    }

    protected void buildPreferences() {

        for (SettingsConfig config : settingsConfigList) {
            List<SettingsSection> sections = config.getSettings();
            if (!sections.isEmpty()) {
                for (SettingsSection section : sections) {
                    addCategory(section, getPreferenceScreen());
                }
            }
        }
    }

    public String appVersion() throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        String version = pInfo.versionName;
        return version;
    }

    protected void addCategory(SettingsSection section, PreferenceGroup parentCategory) {
        final PreferenceCategory category = new PreferenceCategory(getActivity());
        parentCategory.addPreference(category);
        category.setTitle(section.getTitle());
        List<SettingsItem> items = section.getItems();
        for (SettingsItem item : items) {
            if(item.getKey() == null){
                throw new IllegalArgumentException("item.key cannot be null!");
            }
            Preference preference = getPreferenceByType(item);
            settingsMap.put(item.getKey(), item);
            childMap.put(item.getKey(), category);
            String title = item.getTitle();
            if (title == null) {
                title = getResources().getString(item.getI18nTitle());
            }
            preference.setTitle(title);
            preference.setKey(item.getKey());
            preference.setDefaultValue(item.getDefaultValue());
            category.addPreference(preference);
            resetPreference(preference, getPreferenceManager().getSharedPreferences());
        }
    }

    protected Preference getPreferenceByType(SettingsItem item) {
        switch (item.getType()) {
            case CHECKBOX:
            case SERVER_CHECKBOX:
                return new CheckBoxPreference(getActivity());
            case LIST:
                ListPreference result = new ListPreference(getActivity());
                if (item instanceof ListSettingsItem) {
                    ListSettingsItem lItem = (ListSettingsItem) item;
                    result.setEntryValues(lItem.getKeys());
                    int[] keys = lItem.getEntryValues();
                    String[] values = new String[keys.length];
                    for (int i = 0; i < keys.length; i++) {
                        values[i] = getResources().getString(keys[i]);
                    }
                    String selected = getPreferenceManager().getSharedPreferences().getString(lItem.getKey(), null);
                    result.setEntries(values);
                    if (selected != null) {
                        result.setValue(selected);
                    } else {
                        SharedPreferences.Editor editor = getPreferenceManager().getSharedPreferences().edit();
                        editor.putString(item.getKey(), lItem.getDefaultValue()).apply();
                        result.setValue(lItem.getDefaultValue());
                    }
                    resetPreference(result, getPreferenceManager().getSharedPreferences());
                }
                return result;
            case NUMBER:
                return new NumberPickerPreference(getActivity());
            default:
                throw new IllegalArgumentException("cannot create a Preference for item " + item);
        }
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        SettingsItem item = settingsMap.get(key);
        if (item != null && item.getCallback() != null) {
            item.getCallback().onValueChanged(getActivity(), sharedPreferences, key);
        }
        Preference pref = findPreference(key);
        resetPreference(pref, sharedPreferences);
    }

    private void resetPreference(Preference pref, SharedPreferences sharedPreferences) {
        if (pref instanceof CheckBoxPreference) {
            // key example "startView_tree_Mensa" or for push settings "FeedModule"
            Boolean value = sharedPreferences.getBoolean(pref.getKey(), false);
            APM.setSetting(this.getActivity(), pref.getKey(), value.booleanValue());
            ((CheckBoxPreference) pref).setChecked(value);
        } else if (pref instanceof ListPreference) {

            String v = ((ListPreference) pref).getValue();
            if (v != null) {
                //Recovering Mensa price configuration value
                String key = pref.getKey();
                if(key != null) {
                    if (key.equals("module_canteen_price")) {
                        APM.setCanteenPriceSetting(this.getActivity(), v);
                    }
                }
                //TODO: set default student price value.
                int index = ((ListPreference) pref).findIndexOfValue(v);
                CharSequence value = ((ListPreference) pref).getEntries()[index];
                pref.setSummary(value);
            }
            else{
                v = "STUDENT";
                //Recovering Mensa price configuration value
                String key = pref.getKey();
                if(key != null) {
                    if (key.equals("module_canteen_price")) {
                        APM.setCanteenPriceSetting(this.getActivity(), v);
                    }
                }
                //TODO: set default student price value.
                int index = ((ListPreference) pref).findIndexOfValue(v);
                CharSequence value = ((ListPreference) pref).getEntries()[index];
                pref.setSummary(value);
            }
        } else if(pref instanceof NumberPickerPreference){
            int currentValue = ((NumberPickerPreference) pref).getInitialValue();
            pref.setSummary(String.valueOf(currentValue));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public void setPreferencesName(String preferencesName) {
        this.preferencesName = preferencesName;
    }

    public void setSettingsConfigList(List<SettingsConfig> settingsConfigList) {
        this.settingsConfigList = settingsConfigList;
    }

    public void hideSetting(String key){
        Preference child = findPreference(key);
        if(child != null){
            child.setEnabled(false);
        }
    }

    public void showSetting(String key){
        Preference child = findPreference(key);
        if(child != null){
            child.setEnabled(true);
        }
    }

}
