package de.bps.asist.module.settings.model;

/**
 * Created by litho on 30.10.14.
 * Settings Item for Boolean values, triggered by a checkbox
 */
public class CheckboxSettingsItem extends SettingsItem<Boolean> {


    public CheckboxSettingsItem(String title) {
        super(title);
        setType(SETTINGS_TYPE.CHECKBOX);
    }

    public CheckboxSettingsItem(int title){
        super(title);
        setType(SETTINGS_TYPE.CHECKBOX);
    }

}
