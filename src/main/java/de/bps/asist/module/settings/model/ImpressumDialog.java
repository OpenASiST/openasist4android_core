package de.bps.asist.module.settings.model;

import android.content.Context;
import android.preference.DialogPreference;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import de.bps.asist.R;

/**
 * Created by epereira on 17.08.2016.
 */
public class ImpressumDialog extends DialogPreference {

    private Context context;
    public ImpressumDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        this.context = context;
        setPersistent(false);
        setDialogLayoutResource(R.layout.impressum_dialog);
    }

    @Override
    public void onBindDialogView(View view){
        TextView impressumTextView = (TextView)view.findViewById(R.id.impressumText);;
        impressumTextView.setText(Html.fromHtml(context.getResources().getString(R.string.impressumText)));
        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        persistBoolean(positiveResult);
    }
}