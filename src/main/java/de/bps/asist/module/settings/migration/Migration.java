package de.bps.asist.module.settings.migration;

import android.content.Context;

/**
 * Created by litho on 16.12.14.
 * Interface for Migration
 */
public interface Migration {

    public void doMigration(Context context);
}
