package de.bps.asist.module.settings.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by litho on 30.10.14.
 * A section for the settings view
 */
public class SettingsSection {

    private int title;
    private Integer description;
    private int position;

    private List<SettingsItem> items = new ArrayList<>();

    public SettingsSection(int title){
        this.title = title;
    }

    public void addItem(SettingsItem item){
        items.add(item);
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public Integer getDescription() {
        return description;
    }

    public void setDescription(int description) {
        this.description = description;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<SettingsItem> getItems() {
        return items;
    }
}
