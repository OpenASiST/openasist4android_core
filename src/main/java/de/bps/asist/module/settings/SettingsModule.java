package de.bps.asist.module.settings;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.module.AbstractAsistModule;

/**
 * Created by litho on 09.10.14.
 * Settings Module
 */
public class SettingsModule extends AbstractAsistModule{

    @Override
    public int getName() {
        return getTitle();
    }

    /**
     * @return Returns the resource id to the icon of this module
     */
    @Override
    public int getIcon() {
        return R.drawable.ic_menu_settings;
    }

    @Override
    public Fragment getInitialFragment() {
        return new SettingsFragment();
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        return new ArrayList<>();
    }

    public static int getTitle() {
        return R.string.module_settings_name;
    }
}
