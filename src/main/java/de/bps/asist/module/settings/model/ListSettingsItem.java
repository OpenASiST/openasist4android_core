package de.bps.asist.module.settings.model;

/**
 * Created by litho on 30.10.14.
 * Settings model class for multiple select settings
 */
public class ListSettingsItem extends SettingsItem<String>{

    private String[] keys;
    private int[] entryValues;



    public ListSettingsItem(String title) {
        super(title);
        setType(SETTINGS_TYPE.LIST);
    }

    public ListSettingsItem(int title){
        super(title);
        setType(SETTINGS_TYPE.LIST);
    }


    public String[] getKeys() {
        return keys;
    }

    public void setKeys(String[] keys) {
        this.keys = keys;
    }

    public int[] getEntryValues() {
        return entryValues;
    }

    public void setEntryValues(int[] entryValues) {
        this.entryValues = entryValues;
    }

    @Override
    public String getDefaultValue() {
        return super.getDefaultValue();
    }
}
