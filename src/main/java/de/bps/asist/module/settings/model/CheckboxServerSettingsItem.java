package de.bps.asist.module.settings.model;

/**
 * Created by litho on 30.10.14.
 * Checkbox settings that interact with the server. If settings setting on server fails, checkbox is toggled
 */
public class CheckboxServerSettingsItem extends CheckboxSettingsItem{

    public CheckboxServerSettingsItem(int title) {
        super(title);
        setType(SETTINGS_TYPE.SERVER_CHECKBOX);
        setCallback(new ServerSettingsCallback());
    }

}
