package de.bps.asist.module.settings.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.loopj.android.http.RequestParams;

import org.apache.http.HttpStatus;

import androidx.fragment.app.FragmentActivity;
import de.bps.asist.R;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.util.ASiSTPostSender;
import de.bps.asist.core.util.ResultHandler;
import de.bps.asist.gui.dialog.ASiSTDialog;
import de.bps.asist.module.start.SettingsChangedCallback;

/**
 * Created by litho on 04.11.14.
 * Callback for changing settings but sending to server first
 */
public class ServerSettingsCallback implements SettingsChangedCallback {

    private static final String TAG = ServerSettingsCallback.class.getSimpleName();

    private boolean inSync = false;

    @Override
    public void onValueChanged(FragmentActivity context, SharedPreferences preferences, String key) {
        if (inSync) {
            return;
        }
        inSync = true;
        StringBuilder sb = new StringBuilder(translate(context, R.string.rootUrl));
        sb.append("/device/").append(translate(context, R.string.institutionName));
        Boolean prefValue = preferences.getBoolean(key, false);
        int value = prefValue ? 1 : 0;
        sb.append("/settings/").append(key).append("/").append(String.valueOf(value));
        RequestParams params = new RequestParams();
        params.put("token", EnvironmentManager.getInstance().getDeviceToken(context));
        ResultHandler result = ASiSTPostSender.postSync(sb.toString(), params);
        if (result.getResponseCode() != HttpStatus.SC_OK) {
            ////Log.e(TAG, "could not set value for key " + key + " on server (statusCode = " + result + ", reverting setting");
            preferences.edit().putBoolean(key, !prefValue).apply();
            ASiSTDialog dialog = new ASiSTDialog(translate(context, R.string.settings_push_notpossible_title), translate(context, R.string.settings_push_notpossible_text));
            dialog.setCancelable(false);
            dialog.show(context.getSupportFragmentManager(), TAG);
        }
        inSync = false;

    }


    private String translate(Context context, int id) {
        return context.getResources().getString(id);
    }
}
