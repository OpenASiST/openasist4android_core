package de.bps.asist.module.settings.model;

/**
 * Created by litho on 02.12.14.
 * Settings Item for number values
 */
public class NumberSettingsItem extends SettingsItem<Integer> {

    public NumberSettingsItem(int title) {
        super(title);
        setType(SETTINGS_TYPE.NUMBER);
    }
}
