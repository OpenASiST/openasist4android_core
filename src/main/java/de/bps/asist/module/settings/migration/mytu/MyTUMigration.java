package de.bps.asist.module.settings.migration.mytu;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.module.feedback.FeedbackModule;
import de.bps.asist.module.settings.migration.Migration;

/**
 * Created by litho on 16.12.14.
 * migration class for myTU
 */
public class MyTUMigration implements Migration{

    private static final String MY_TU_PREFERENCES = "de.tufreiberg.mytu_preferences";

    private static final List<MigrationItem> migrationMap = createMigrationMap();

    public void migrate(Context context){
        SharedPreferences old = context.getSharedPreferences(MY_TU_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences newPreferences = context.getSharedPreferences(EnvironmentManager.SHAREDPREFERENCE_ENVIRONMENT, Context.MODE_PRIVATE);
        for(final MigrationItem item: migrationMap){
            Object value = get(old, item.getOldKey(), item.getOldClass());
            set(newPreferences, item.getNewKey(), item.getNewClass(), value);
        }
    }

    private Object get(SharedPreferences old, String key, Class type){
        if(String.class.equals(type)){
                return old.getString(key, null);
        } else if(Boolean.class.equals(type)){
            return old.getBoolean(key, false);
        } else if(Integer.class.equals(type)){
            return old.getInt(key, 0);
        }
        return null;
    }

    private void set(SharedPreferences preferences, String key, Class type, Object value){
        if(String.class.equals(type) && value instanceof String){
            preferences.edit().putString(key, (String) value).apply();
        } else if(Boolean.class.equals(type) && value instanceof Boolean){
            preferences.edit().putBoolean(key, (Boolean) value).apply();
        } else if(Integer.class.equals(type) && value instanceof Integer){
            preferences.edit().putInt(key, (Integer) value).apply();
        } else if(Integer.class.equals(type) && value instanceof String){
            String stringValue = (String) value;
            Integer intValue = Integer.parseInt(stringValue);
            preferences.edit().putInt(key, intValue).apply();
        }
    }



    private static List<MigrationItem> createMigrationMap(){
        List<MigrationItem> result = new ArrayList<>();
        result.add(new MigrationItem("feedback_lecturer_mode", FeedbackModule.SHARED_PREFERENCES_FEEDBACK, String.class, String.class));
        result.add(new MigrationItem("update_on_start", EnvironmentManager.KEY_UPDATE_ON_STARTUP, Boolean.class, Boolean.class));
        result.add(new MigrationItem("feed_limit", EnvironmentManager.KEY_NOTIFICATION_LIMIT, String.class, Integer.class));
        result.add(new MigrationItem("notification_limit", EnvironmentManager.KEY_FEED_LIMIT, String.class, Integer.class));
        result.add(new MigrationItem("participants_filter", EnvironmentManager.KEY_PARTICIPANTS_FILTER, String.class, Integer.class));
        return result;
    }

    @Override
    public void doMigration(Context context) {
        migrate(context);
    }

    private static class MigrationItem{
        private String oldKey;
        private String newKey;
        private Class oldClass;
        private Class newClass;

        public MigrationItem(String oldKey, String newKey, Class oldClass, Class newClass){
            this.oldKey = oldKey;
            this.newKey = newKey;
            this.oldClass = oldClass;
            this.newClass = newClass;
        }


        public String getOldKey() {
            return oldKey;
        }

        public String getNewKey() {
            return newKey;
        }

        public Class getOldClass() {
            return oldClass;
        }

        public Class getNewClass() {
            return newClass;
        }
    }
}
