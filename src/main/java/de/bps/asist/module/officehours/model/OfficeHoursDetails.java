package de.bps.asist.module.officehours.model;

import java.util.Collection;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.module.poi.model.PoiItem;

/**
 * Created by Martin Luschek on 02.12.2014.
 */
public class OfficeHoursDetails implements IListItem {

    @ASiSTDetail(i18nKey = "module_officehours_building", position = 1)
    @AsistTitle
    private String title;

    @ASiSTDetail(i18nKey = "module_officehours_description", position = 2)
    private String description;

    @ASiSTDetail(i18nKey = "module_officehours_email", position = 3)
    private String email;

    @ASiSTDetail(i18nKey = "module_officehours_telephone", position = 4)
    private String telephone;

    @ASiSTDetail(i18nKey = "module_officehours_times", position = 5)
    private Collection<String> times;

    public OfficeHoursDetails(OfficehoursDescription off, PoiItem poi) {
        title = poi.getTitle();     //gebäude
        description = poi.getDescription();//raum
        email = off.getEmail();
        telephone = off.getTelephone();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(final String telephone) {
        this.telephone = telephone;
    }

    public Collection<String> getTimes() {
        return times;
    }

    public void setTimes(final Collection<String> times) {
        this.times = times;
    }

    @Override
    public Long getId() {
        return 0L;
    }
}
