package de.bps.asist.module.officehours.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IListItem;

/**
 * Created by Martin Luschek on 27.10.2014.
 */
@DatabaseTable
public class OfficehoursDescription extends AbstractDatabaseObject implements IListItem {

    private static final long serialVersionUID = 965874354323543978L;

    @DatabaseField
    @AsistTitle
    private String title;

    @DatabaseField
    @AsistDescription
    private String description;

    @DatabaseField
    private String email;

    @DatabaseField
    private String telephone;

    @DatabaseField
    private int poiid;

    @ForeignCollectionField(eager = true)
    private Collection<TimeSpanDescription> times;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(final String telephone) {
        this.telephone = telephone;
    }

    public Collection<TimeSpanDescription> getTimes() {
        return times;
    }

    public void setTimes(final Collection<TimeSpanDescription> times) {
        this.times = times;
    }

    public long getPoiid() {
        return poiid;
    }

    public void setPoiid(final int poiid) {
        this.poiid = poiid;
    }
}
