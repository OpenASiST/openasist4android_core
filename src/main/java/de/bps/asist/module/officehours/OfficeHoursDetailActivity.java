package de.bps.asist.module.officehours;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.gui.detail.DetailedTableActivity;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.officehours.model.OfficehoursDescription;
import de.bps.asist.module.officehours.model.TimeSpanDescription;
import de.bps.asist.module.officehours.model.TimeSpanDetails;

/**
 * Created by Martin Luschek.
 */
public class OfficeHoursDetailActivity extends DetailedTableActivity<OfficehoursDescription> {

    private static String TAG = OfficeHoursDetailActivity.class.getSimpleName();

    private ListView timeSpanListView;

    private Map<String, List<TimeSpanDetails>> timeMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void doCreate() {
        setContentView(R.layout.office_hours_detail_activity);

        timeSpanListView = getView(R.id.officehoursTimeSpanListView);

        OfficehoursDescription selected = getSelectedItem();

        //Log.i(TAG, "selected.poiid: " + selected.getPoiid());
        DatabaseManager<OfficehoursDescription> dbOffice = new DatabaseManager<>(this, OfficehoursDescription.class);
        final OfficehoursDescription reloadedOffice = dbOffice.refresh(selected);

        Collection<TimeSpanDescription> timeSpan = reloadedOffice.getTimes();
        for (final TimeSpanDescription tsd : timeSpan) {
            TimeSpanDetails details = new TimeSpanDetails(tsd);
            List<TimeSpanDetails> list = timeMap.get(details.getMode());
            if (list == null) {
                list = new ArrayList<>();
                timeMap.put(details.getMode(), list);
            }
            list.add(details);
        }
        setAdapter();
    }

    private void setAdapter() {
        final CategoryListViewAdapter<TimeSpanDetails> timeAdapter = new CategoryListViewAdapter<>(this, sortMap(), null);
        timeSpanListView.setAdapter(timeAdapter);
    }

    /**
     * Methode for Sorting the Map. "Wöchentlich" will be the first entry.
     *
     * @return sortedMap
     */
    private Map<String, List<TimeSpanDetails>> sortMap() {
        Map<String, List<TimeSpanDetails>> sortedMap = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.compareTo(s1);
            }
        });
        sortedMap.putAll(timeMap);
        return sortedMap;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


}
