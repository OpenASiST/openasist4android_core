package de.bps.asist.module.officehours;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import androidx.fragment.app.Fragment;
import de.bps.asist.BuildConfig;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.officehours.model.OfficehoursDescription;
import de.bps.asist.module.officehours.model.OfficehoursList;
import de.bps.asist.module.officehours.model.TimeSpanDescription;

/**
 * Created by Martin Luschek on 27.10.2014.
 * Module for displaying office hours
 */
public class OfficeHoursModule extends AbstractAsistModule {

    private static final String TAG = OfficeHoursModule.class.getSimpleName();
    private static final Lock lock = new ReentrantLock();

    public OfficeHoursModule() {
        //setEnabled(BuildConfig.DEBUG);
    }

    public static void getOfficehours(Context context) {
        lock.lock();
        //Log.i(TAG, "locking db for updating office hours");
        try {
            final DatabaseManager<OfficehoursDescription> db = new DatabaseManager<>(context, OfficehoursDescription.class);
            final DatabaseManager<TimeSpanDescription> dbTime = new DatabaseManager<>(context, TimeSpanDescription.class);
            if (db.hasItems()) {
                db.deleteAll();
                dbTime.deleteAll();
            }
            final String url = context.getResources().getString(R.string.rootUrl) + "/officehours/" + context.getResources().getString(R.string.institutionName) + "/all";
            final OfficeHoursCallBack cb = new OfficeHoursCallBack(context);
            ASiSTParser.getInstance().parse(url, OfficehoursList.class, cb);
            db.release();
            dbTime.release();
        } catch (Exception e) {
            //Log.e(TAG, "could not load office hours", e);
        } finally {
            lock.unlock();
            //Log.i(TAG, "unlocking db after updating office hours successfully");
        }
    }

    @Override
    public int getName() {
        return R.string.module_officehours_name;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_time;
    }

    /**
     * @see de.bps.asist.module.AbstractAsistModule#getInitialFragment()
     */
    @Override
    public Fragment getInitialFragment() {
        return new OfficeHoursFragment();
    }

    @Override
    public void updateData(final Context context) {
        super.updateData(context);

        getOfficehours(context);
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        final List<Class<? extends AbstractDatabaseObject>> result = new ArrayList<>();
        result.add(OfficehoursDescription.class);
        result.add(TimeSpanDescription.class);
        return result;
    }

    private static class OfficeHoursCallBack implements ParserCallback<OfficehoursList> {

        private final Context context;

        public OfficeHoursCallBack(final Context context) {
            this.context = context;
        }

        @Override
        public void afterParse(OfficehoursList parsed) {
            List<OfficehoursDescription> items = new ArrayList<>();
            if (parsed != null) {
                items.addAll(parsed.getItems());

                final DatabaseManager<OfficehoursDescription> db = new DatabaseManager<>(context, OfficehoursDescription.class);
                final DatabaseManager<TimeSpanDescription> dbTime = new DatabaseManager<>(context, TimeSpanDescription.class);

                Collection<TimeSpanDescription> timeSpans;
                for (final OfficehoursDescription officehours : items) {
                    timeSpans = new ArrayList<>();
                    db.saveObject(officehours);
                    for (final TimeSpanDescription time : officehours.getTimes()) {
                        time.setOfficehours(officehours);
                        dbTime.saveObject(time);
                        timeSpans.add(time);
                    }
                    officehours.setTimes(timeSpans);
                }

                db.release();
                dbTime.release();
            } else {
                //Log.e(TAG, "parsed is null, cannot save any value");
            }
        }

        @Override
        public void onError(Exception e) {
            //Log.e(TAG, "error in updating office hours", e);
        }

        @Override
        public void onManagedError(OfficehoursList parsed) {
            //TODO
        }
    }
}
