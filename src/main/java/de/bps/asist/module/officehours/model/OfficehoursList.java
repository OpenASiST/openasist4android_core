package de.bps.asist.module.officehours.model;

import java.util.List;

import de.bps.asist.core.model.IList;

/**
 * Created by pc38766 on 27.10.2014.
 */
public class OfficehoursList implements IList {

    private List<OfficehoursDescription> officehours;

    public List<OfficehoursDescription> getOfficehours() {
        return officehours;
    }

    public void setOfficehours(final List<OfficehoursDescription> officehours) {
        this.officehours = officehours;
    }

    @Override
    public List<OfficehoursDescription> getItems() {
        return getOfficehours();
    }

}
