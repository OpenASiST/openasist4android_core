package de.bps.asist.module.officehours;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.module.officehours.model.OfficehoursDescription;
import de.bps.asist.module.poi.PoiModule;
import de.bps.asist.module.poi.model.PoiItem;

public class OfficeHoursFragment extends AbstractASiSTFragment {

    private ListView officeList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.officehours_activity, container, false);
        officeList = (ListView) view.findViewById(R.id.officehoursListView);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        final DatabaseManager<OfficehoursDescription> db = new DatabaseManager<>(
                getActivity(), OfficehoursDescription.class);

        final List<OfficehoursDescription> items = db.getAll();
        if (items.isEmpty()) {
            OfficeHoursModule.getOfficehours(getActivity());
        } else {
            for (OfficehoursDescription officeHours : items) {
                PoiItem poi = PoiModule.findItemByPoiId(getActivity(), (int) officeHours.getPoiid());
                officeHours.setTitle(poi.getTitle());
            }
            db.release();
            sortList(items);
        }
    }

    /**
     * Methode for sorting a List based on officeHoursDescription.getTitle()
     *
     * @param list unsorted List<OfficehoursDescription>
     */
    private void sortList(List<OfficehoursDescription> list) {
        try{
        Collections.sort(list, new Comparator<OfficehoursDescription>() {
            @Override
            public int compare(OfficehoursDescription officeHoursDesc1, OfficehoursDescription officeHoursDesc2) {
                return officeHoursDesc1.getTitle().compareTo(officeHoursDesc2.getTitle());
            }
        });} catch (Exception e) {

        }

        addAdapter(list);
    }

    private void addAdapter(List<OfficehoursDescription> items) {
        final GenericListViewAdapter<OfficehoursDescription> adapter = new GenericListViewAdapter<>(
                getActivity(), items, new AbstractGenericListCallback(
                getActivity(), OfficeHoursDetailActivity.class));

        officeList.setAdapter(adapter);
    }
}
