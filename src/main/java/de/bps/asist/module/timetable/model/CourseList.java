/**
 * 
 */
package de.bps.asist.module.timetable.model;

import java.util.List;

import de.bps.asist.core.model.IList;

/**
 * @author thomasw
 * 
 */
public class CourseList implements IList {

	private String message;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	private String code;

	private String uri;

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(final String uri) {
		this.uri = uri;
	}

	private String status;

	private List<Course> courses;

	public List<Course> getCourse() {
		return courses;
	}

	public void setCourses(final List<Course> courses) {
		this.courses = courses;
	}

	/**
	 * @see de.bps.asist.core.model.IList#getItems()
	 */
	@Override
	public List<Course> getItems() {
		return courses;
	}

}
