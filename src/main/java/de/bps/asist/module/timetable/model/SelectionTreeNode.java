package de.bps.asist.module.timetable.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;
import java.util.List;

import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;

/**
 * Created by litho on 20.11.14.
 * A node for course selection tree
 */

@DatabaseTable
public class SelectionTreeNode extends AbstractDatabaseObject {

    @DatabaseField
    private String name;

    @AsistTitle
    @DatabaseField
    private String value;

    @DatabaseField
    private String relPath;

    @ForeignCollectionField
    private Collection<SelectionTreeNode> children;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private SelectionTreeNode parent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRelPath() {
        return relPath;
    }

    public void setRelPath(String relPath) {
        this.relPath = relPath;
    }

    public Collection<SelectionTreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<SelectionTreeNode> children) {
        this.children = children;
    }

    public SelectionTreeNode getParent() {
        return parent;
    }

    public void setParent(SelectionTreeNode parent) {
        this.parent = parent;
    }
}
