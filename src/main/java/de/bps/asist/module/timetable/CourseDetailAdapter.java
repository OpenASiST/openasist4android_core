package de.bps.asist.module.timetable;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import de.bps.asist.BuildConfig;
import de.bps.asist.R;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.gui.detail.DetailTableAdapter;
import de.bps.asist.gui.list.Category;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.feedback.FeedbackActivity;
import de.bps.asist.module.survey.SurveyOverviewActivity;
import de.bps.asist.module.timetable.model.CourseDescription;

import static de.bps.asist.context.ASiSTConstants.TABLE_ITEM_TYPE_CELL;
import static de.bps.asist.context.ASiSTConstants.TABLE_ITEM_TYPE_FEEDBACK;
import static de.bps.asist.context.ASiSTConstants.TABLE_ITEM_TYPE_HEADER;

/**
 * Created by litho on 13.10.14.
 * Adapter for details of courses
 */
public class CourseDetailAdapter extends DetailTableAdapter {

    private static final String TAG = CourseDetailAdapter.class.getSimpleName();

    private final AsistModuleActivity activity;
    private CourseDescription course;

    public CourseDetailAdapter(AsistModuleActivity context, Map<String, List<ASiSTDetailDescription>> objects, CourseDescription course) {
        super(context, objects, null);
        this.activity = context;
        this.course = course;
    }

    @Override
    public int getItemViewType(int position) {
        Category<ASiSTDetailDescription> first = getCategories().get(0);
        if (first.hasIndex(position)) {
            return super.getItemViewType(position);
        }
        Category<ASiSTDetailDescription> feedback = getCategories().get(1);
        if (feedback.hasIndex(position)) {
            return TABLE_ITEM_TYPE_FEEDBACK;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        // item object
        final int viewType = getItemViewType(position);
        if (viewType == TABLE_ITEM_TYPE_CELL || viewType == TABLE_ITEM_TYPE_HEADER) {
            return super.getView(position, convertView, parent);
        }
        if (viewType == TABLE_ITEM_TYPE_FEEDBACK) {
            return addFeedbackCell(position);
        }
        return null;
    }

    private LinearLayout addFeedbackCell(final int position) {
        final LinearLayout row = (LinearLayout) getInflater().inflate(R.layout.course_actions_item, null);
        ASiSTDetailDescription description = (ASiSTDetailDescription) getItem(position);
        final TextView text = (TextView) row.findViewById(R.id.detailItemTitle);
        text.setText(description.getValue());
        Category<ASiSTDetailDescription> feedback = getCategories().get(1);
        final int realPos = position - feedback.getStartIndex();
        if (BuildConfig.DEBUG || course.getTimeSpan().isNow()) {
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Class<? extends Activity> target;
                    switch (realPos){
                        case 0:
                            target = FeedbackActivity.class;
                            break;
                        case 1:
                            target = SurveyOverviewActivity.class;
                            break;
                        default:
                            throw new IllegalArgumentException("cannot find a target class for position "+position);
                    }
                    forwardWithObject(activity, target, course);
                }
            });
        } else {
            text.setEnabled(false);
        }
        return row;
    }

}
