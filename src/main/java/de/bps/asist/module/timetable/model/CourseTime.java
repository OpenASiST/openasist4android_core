/**
 * 
 */
package de.bps.asist.module.timetable.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.bps.asist.R;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.annotation.AsistTitlePrefixKey;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.parser.OnlyTimeParser;

/**
 * @author thomasw
 * 
 */
@DatabaseTable
@AsistItemConfiguration(useCallback = false)
public class CourseTime extends AbstractDatabaseObject {
	private static final long serialVersionUID = -3364423408257692581L;

    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

	// the start time as formatted String
	@JsonDeserialize(using = OnlyTimeParser.class)
	@DatabaseField
	private Date start;

	// the end time as formatted String
	@JsonDeserialize(using = OnlyTimeParser.class)
	@DatabaseField
	private Date end;

	//
	@DatabaseField
	private Integer mode;

	// day of the week as int value; 1 is Monday
	@DatabaseField
	private Integer dayOfWeek;

	@DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
	private Room room;

	@DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
	private Lecturer lecturer;

	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Course course;

	@AsistTitlePrefixKey
	private int titlePrefix;

    @DatabaseField
    private int weekNumber;

	public int getTitlePrefix() {
		final int key = R.string.timetable_time_title;
		return key;
	}

	public void setTitlePrefix(final int titlePrefix) {
		this.titlePrefix = titlePrefix;
	}

	@AsistTitle
	private String title;

	public String getTitle() {
		return start + " - " + end;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(final Course course) {
		this.course = course;
	}

	public Integer getMode() {
		return mode;
	}

	public void setMode(final Integer mode) {
		this.mode = mode;
	}

	public Integer getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(final Integer dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(final Room room) {
		this.room = room;
	}

	public Lecturer getLecturer() {
		return lecturer;
	}

	public void setLecturer(final Lecturer lecturer) {
		this.lecturer = lecturer;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(final Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(final Date end) {
		this.end = end;
	}

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }
}
