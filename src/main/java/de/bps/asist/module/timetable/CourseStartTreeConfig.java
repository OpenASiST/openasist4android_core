package de.bps.asist.module.timetable;

import android.content.Context;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.bps.asist.R;
import de.bps.asist.module.start.tree.ASiSTTreeItem;
import de.bps.asist.module.start.tree.StartTreeConfig;
import de.bps.asist.module.timetable.model.Course;
import de.bps.asist.module.timetable.model.CourseMap;

/**
 * Created by litho on 29.10.14.
 * Configuration for Start Tree View of CourseModule
 */
public class CourseStartTreeConfig extends StartTreeConfig<TimetableModule>{

    public CourseStartTreeConfig(TimetableModule module){
        super(module);
    }

    @Override
    public ASiSTTreeItem getRootNode(Context context) {
        DateFormat df = DateFormat.getDateInstance();
        String title = context.getResources().getString(getModule().getName());
        String description = context.getResources().getString(R.string.course_tree_description) + " " +  df.format(new Date());
        ASiSTTreeItem rootItem = new ASiSTTreeItem(title, description);
        List<ASiSTTreeItem<?>> treeItems = new ArrayList<>();
        List<Course> allCourses = TimetableModule.getCoursesToday(context);
        CourseMap courseMap = new CourseMap(allCourses);
        List<Course> modeCourses = courseMap.getCoursesForMode(TimetableModule.getCurrentMode());
        if(modeCourses != null) {
            CourseTreeItemLayouter layouter = new CourseTreeItemLayouter(context, modeCourses);
            ASiSTTreeItem<Course> header = new ASiSTTreeItem<>("", "");
            header.setLayouter(layouter);
            //treeItems.add(header);
            if(modeCourses.size() != 0) {
                for (Course course : modeCourses) {
                    ASiSTTreeItem<Course> item = new ASiSTTreeItem<>(course.getTitle(), course.getDesc());
                    item.setModelItem(course);
                    item.setLayouter(layouter);
                    treeItems.add(item);
                }
            } else {
                //TODO: Add empty course with warning no courses are available
                String lang = Locale.getDefault().getLanguage();
                if(lang.equals("de")){
                    String noCoursemessage = context.getResources().getString(R.string.goto_timetable_message);
                    ASiSTTreeItem<Course> newItem = new ASiSTTreeItem<>(noCoursemessage,"");
                    treeItems.add(newItem);
                } else {
                    String noCoursemessage = context.getResources().getString(R.string.goto_timetable_messageEN);
                    ASiSTTreeItem<Course> newItem = new ASiSTTreeItem<>(noCoursemessage,"");
                    treeItems.add(newItem);
                }

            }
            rootItem.setChildren(treeItems);
        }
        return rootItem;
    }
}
