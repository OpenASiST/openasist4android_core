/**
 * 
 */
package de.bps.asist.module.timetable.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import de.bps.asist.R;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.annotation.AsistTitlePrefixKey;
import de.bps.asist.core.database.AbstractDatabaseObject;

/**
 * @author thomasw
 * 
 */
@DatabaseTable
@AsistItemConfiguration(useCallback = false)
public class Room extends AbstractDatabaseObject {
	private static final long serialVersionUID = -7540329075871327502L;

	@DatabaseField
	@AsistTitle
	private String title;

	@AsistTitlePrefixKey
	private final int titlePrefixKey = R.string.timetable_room_title;

	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Course course;

    @DatabaseField
    private Integer poiid;

    @DatabaseField
    private Integer reference;

	public Course getCourse() {
		return course;
	}

	public void setCourse(final Course course) {
		this.course = course;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public int getTitlePrefixKey() {
		return titlePrefixKey;
	}

    public Integer getPoiid() {
        return poiid;
    }

    public void setPoiid(Integer poiid) {
        this.poiid = poiid;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }
}
