package de.bps.asist.module.timetable;

import android.app.ProgressDialog;
import android.content.Context;

import java.util.List;

import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.module.start.AsistStartModule;
import de.bps.asist.module.start.tree.ASiSTTreeItem;
import de.bps.asist.module.timetable.model.Course;
import de.bps.asist.module.timetable.model.CourseList;
import de.bps.asist.module.timetable.model.CourseTime;
import de.bps.asist.module.timetable.model.Lecturer;
import de.bps.asist.module.timetable.model.Room;

/**
 * Created by bja on 26.06.17.
 */
class TimetableCallback implements ParserCallback<CourseList> {

    private final Context context;
    private final CourseParseCallback pc;
    private final ProgressDialog pg;
    private final TimetableModule timetableModule;

    public TimetableCallback(final Context context, CourseParseCallback pc, ProgressDialog pg, TimetableModule timetableModule) {
        this.context = context;
        this.pc = pc;
        this.pg = pg;
        this.timetableModule = timetableModule;
    }

    @Override
    public void onError(Exception e) {
        if (pc != null) {
            pc.onError(e);
        } else if (pg != null) {
            pg.dismiss();
        }
    }

    @Override
    public void onManagedError(CourseList parsedCourses) {
        if (pc != null) {
            pc.onCoursesParsed(parsedCourses);
        } else if (pg != null) {
            pg.dismiss();
        }
    }

    @Override
    public void afterParse(final CourseList parsed) {
        if (parsed == null || parsed.getItems() == null || parsed.getItems().isEmpty()) {
            // list could not be parsed
            ////Log.w(TAG, "could not parse courseList");
            return;
        }
        final List<Course> items = parsed.getItems();
        final DatabaseManager<Course> db = new DatabaseManager<>(context, Course.class);
        final DatabaseManager<Lecturer> dbLecturer = new DatabaseManager<>(context, Lecturer.class);
        final DatabaseManager<Room> dbRoom = new DatabaseManager<>(context, Room.class);
        final DatabaseManager<CourseTime> dbTime = new DatabaseManager<>(context, CourseTime.class);

        // clean the db
        db.deleteAll();
        dbLecturer.deleteAll();
        dbRoom.deleteAll();
        dbTime.deleteAll();

        ////Log.i("TimetableFragment", "Courses after delete: " + (courses != null ? courses.size() : " null"));

        for (final Course course : items) {
            db.saveObject(course);
            if (course.getLecturer() != null) {
                for (final Lecturer lec : course.getLecturer()) {
                    lec.setCourse(course);
                    dbLecturer.saveObject(lec);
                }
            }
            if (course.getRooms() != null) {
                for (final Room room : course.getRooms()) {
                    room.setCourse(course);
                    dbRoom.saveObject(room);
                }
            }
            if (course.getTimes() != null) {
                for (final CourseTime time : course.getTimes()) {
                    time.setCourse(course);
                    dbTime.saveObject(time);
                    if (time.getRoom() != null) {
                        dbRoom.saveObject(time.getRoom());
                    }
                    if (time.getLecturer() != null) {
                        dbLecturer.saveObject(time.getLecturer());
                    }

                }
            }
        }
        db.release();
        if (pc != null) {
            pc.onSuccess();
        }
        this.timetableModule.startTreeConfig = new CourseStartTreeConfig(this.timetableModule);
        ASiSTTreeItem root = this.timetableModule.startTreeConfig.getRootNode(context);
        AsistStartModule.update(root);
    }
}
