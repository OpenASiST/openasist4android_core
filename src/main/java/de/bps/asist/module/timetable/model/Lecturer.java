/**
 * 
 */
package de.bps.asist.module.timetable.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import de.bps.asist.R;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.annotation.AsistTitlePrefixKey;
import de.bps.asist.core.database.AbstractDatabaseObject;

/**
 * @author thomasw
 * 
 */
@DatabaseTable
@AsistItemConfiguration
public class Lecturer extends AbstractDatabaseObject {
	private static final long serialVersionUID = -8994117367680907302L;

	@DatabaseField
	@AsistTitle
	private String name;

	@AsistTitlePrefixKey
	private final int titleKey = R.string.timetable_lecturer_title;

	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Course course;

	public Course getCourse() {
		return course;
	}

	public void setCourse(final Course course) {
		this.course = course;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getTitleKey() {
		return titleKey;
	}
}
