package de.bps.asist.module.timetable;

import android.content.DialogInterface;

import de.bps.asist.gui.code.GenericAddActivity;
import de.bps.asist.gui.dialog.ASiSTDialog;

/**
 * Created by bja on 26.06.17.
 */

public class TimeTableInputCallbackDialog extends ASiSTDialog {

    private GenericAddActivity activity;
    private String code;

    public TimeTableInputCallbackDialog(CharSequence title, CharSequence message, int okText, int cancelText) {
        super(title, message, okText, cancelText);
    }

    @Override
    public void onOkClicked(DialogInterface dialog, int which) {
        UICallback cb = new UICallback(activity, code);
        cb.execute();
    }

    @Override
    public void onCancelClicked(DialogInterface dialog, int which) {
        this.dismiss();
    }

    public void setActivity(GenericAddActivity activity) {
        this.activity = activity;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
