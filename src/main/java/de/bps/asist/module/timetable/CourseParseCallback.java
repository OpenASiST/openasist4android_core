package de.bps.asist.module.timetable;

import de.bps.asist.module.timetable.model.CourseList;

/**
 * Created by litho on 20.11.14.
 * Callback for Course parsing
 */
public interface CourseParseCallback {

    public void onError(Exception e);

    public void onSuccess();

    public void onCoursesParsed(CourseList parsedCourses);
}
