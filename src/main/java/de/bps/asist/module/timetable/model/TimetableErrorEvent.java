package de.bps.asist.module.timetable.model;

/**
 * Created by epereira on 27.04.2016.
 */
public class TimetableErrorEvent {
    private final String message;

    public TimetableErrorEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
