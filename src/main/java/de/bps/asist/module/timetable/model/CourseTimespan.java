package de.bps.asist.module.timetable.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 * Created by litho on 28.08.14.
 * time span of course
 */
public class CourseTimespan implements Serializable {

    private Date start;
    private Date end;
    private int dayOfWeek;

    public CourseTimespan(Course course){
        Collection<CourseTime> times = course.getTimes();
        if(times != null) {
            for (CourseTime time : times) {
                start = time.getStart();
                end = time.getEnd();
                dayOfWeek = time.getDayOfWeek();
            }
        }
    }

    public boolean isNow(){
        if(start == null || end == null){
            return false;
        }
        SimpleDateFormat df = new SimpleDateFormat("HHmm");
        String now = df.format(new Date());
        String startFormatted = df.format(start);
        String endFormatted = df.format(end);

        Calendar cal = Calendar.getInstance();
        boolean sameDay = cal.get(Calendar.DAY_OF_WEEK) - 1 == dayOfWeek;
        return now.compareTo(startFormatted) >= 0 && now.compareTo(endFormatted) <= 0 && sameDay;
    }



}
