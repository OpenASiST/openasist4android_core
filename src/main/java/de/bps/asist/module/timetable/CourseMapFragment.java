package de.bps.asist.module.timetable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.SupportMapFragment;

import androidx.annotation.Nullable;
import de.bps.asist.module.poi.PoiActivity;
import de.bps.asist.module.poi.model.PoiItem;

/**
 * Created by litho on 22.10.14.
 * Small fragment for preview of location of a course
 */
@SuppressLint("ValidFragment")
public class CourseMapFragment extends SupportMapFragment{

    public CourseMapFragment(){
        // empty constructor needed
    }

    public CourseMapFragment(PoiItem course){
        this.course = course;
    }
    private View original;

    private PoiItem course;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        original = super.onCreateView(inflater, container, savedInstanceState);
        TouchableWrapper wrapper = new TouchableWrapper(getActivity());
        wrapper.addView(original);
        return wrapper;
    }

    @Nullable
    @Override
    public View getView() {
        return original;
    }

    private class TouchableWrapper extends FrameLayout {

        private boolean mMapIsTouched;

        public TouchableWrapper(Context context) {
            super(context);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    final Intent intent = new Intent(getActivity(), PoiActivity.class);
                    intent.putExtra("poi",course);
                    startActivity(intent);
                    break;

                case MotionEvent.ACTION_UP:
                    mMapIsTouched = false;
                    break;
            }

            return super.dispatchTouchEvent(ev);
        }
    }
}
