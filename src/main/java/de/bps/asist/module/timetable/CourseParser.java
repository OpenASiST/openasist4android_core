package de.bps.asist.module.timetable;

import android.content.Context;

import de.bps.asist.R;

public class CourseParser {

    public static String parseLectureType(Context context, int type){
        String key = "n/a";
        switch (type){
            case 1:
                key = context.getResources().getString(R.string.module_timetable_course_type_lecture);
                break;
            case 2:
                key = context.getResources().getString(R.string.module_timetable_course_type_tutorial);
                break;
            case 3:
                key = context.getResources().getString(R.string.module_timetable_course_type_intership);
                break;
            case 4:
                key = context.getResources().getString(R.string.module_timetable_course_type_seminar);
                break;
            case 5:
                key = context.getResources().getString(R.string.module_timetable_course_type_exercise);
                break;
            case 6:
                key = context.getResources().getString(R.string.module_timetable_course_type_advanced_seminar);
                break;
            case 7:
                key = context.getResources().getString(R.string.module_timetable_course_type_proseminar);
                break;
            case 8:
                key = context.getResources().getString(R.string.module_timetable_course_type_oberseminar);
                break;
            case 9:
                key = context.getResources().getString(R.string.module_timetable_course_type_excursion);
                break;
            case 10:
                key = context.getResources().getString(R.string.module_timetable_course_type_colloquium);
                break;
            case 11:
                key = context.getResources().getString(R.string.module_timetable_course_type_project);
                break;
            case 12:
                key = context.getResources().getString(R.string.module_timetable_course_type_exam);
                break;
            case 0:
                key = context.getResources().getString(R.string.module_timetable_course_type_other);
            default:
                break;
        }
        return key;
    }

    public static String parseDayOfWeek(Context context, int dayOfWeek) {
        String key = "n/a";
        switch (dayOfWeek) {
            case 1:
                key = context.getResources().getString(R.string.monday);
                break;
            case 2:
                key = context.getResources().getString(R.string.tuesday);
                break;
            case 3:
                key = context.getResources().getString(R.string.wednesday);
                break;
            case 4:
                key = context.getResources().getString(R.string.thursday);
                break;
            case 5:
                key = context.getResources().getString(R.string.friday);
                break;
            case 0:
                key = context.getResources().getString(R.string.sonstige_veranstaltung);
            default:
                break;
        }
        return key;
    }

}
