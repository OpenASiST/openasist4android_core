package de.bps.asist.module.timetable;

import android.content.Context;
import android.content.DialogInterface;

import de.bps.asist.R;
import de.bps.asist.gui.code.CodeInputCallback;
import de.bps.asist.gui.code.GenericAddActivity;
import de.bps.asist.gui.dialog.ASiSTDialog;

/**
 * Created by litho on 10.12.14.
 * Callback for when timetable code input is finished
 */
public class TimeTableInputCallback implements CodeInputCallback {

    public boolean timetableSaved;
    //private Context context;

    //public TimeTableInputCallback(Context context, boolean timetableSaved){
    //    this.timetableSaved = timetableSaved;
    //    this.context = context;
    //}

    @Override
    public void onCodeInputFinished(GenericAddActivity activity, String code){

        if (timetableSaved) {
            showDownloadCoursesDialog(activity, code);
        } else {
            importTimetable(activity, code);
        }
    }

    private void importTimetable(GenericAddActivity activity, String code) {
        UICallback cb = new UICallback(activity, code);
        cb.execute();
    }

    private void showDownloadCoursesDialog(final GenericAddActivity activity, final String code) {
        TimeTableInputCallbackDialog dialog = new TimeTableInputCallbackDialog("Stundenplan importieren", "Möchten sie den alten Stundenplan verwerfen und den neuen speichern?", R.string.dialog_ok, R.string.dialog_cancel);
        dialog.setActivity(activity);
        dialog.setCode(code);
        dialog.show(activity.getSupportFragmentManager(), "");
    }

//    private String translate(int id){
//        return context.getResources().getString(id);
//    }
}
