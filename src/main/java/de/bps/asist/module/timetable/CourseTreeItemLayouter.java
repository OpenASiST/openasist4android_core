package de.bps.asist.module.timetable;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.module.timetable.model.Course;

/**
 * Created by litho on 03.11.14.
 * Layouter of Tree in StartView for CourseModule
 */
public class CourseTreeItemLayouter extends TimetableAdapter {

    public CourseTreeItemLayouter(Context context, Collection<Course> objects) {
        super(context, parseMap(objects));
    }

    private static Map<String, List<Course>> parseMap(Collection<Course> courses){
        Map<String, List<Course>> result = new HashMap<>();
        if(courses == null){
            courses = new ArrayList<>();
        }
        result.put("", new ArrayList<>(courses));
        return result;

    }

}
