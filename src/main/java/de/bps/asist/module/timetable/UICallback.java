package de.bps.asist.module.timetable;

import android.app.ProgressDialog;
import android.content.DialogInterface;

import androidx.fragment.app.FragmentManager;
import de.bps.asist.BuildConfig;
import de.bps.asist.R;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.gui.dialog.ASiSTDialog;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.timetable.model.CourseList;

/**
 * Created by litho on 20.11.14.
 * Callback for import course action
 */
public class UICallback implements CourseParseCallback {

    private ProgressDialog progressDialog;
    private AsistModuleActivity context;
    private String shortCode;

    public UICallback(final AsistModuleActivity context, String shortCode){
        this.context = context;
        this.shortCode = shortCode;
    }

    public void execute(){
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.module_course_add_wait_title));
        progressDialog.setMessage(context.getResources().getString(R.string.module_course_add_wait_desc));
        progressDialog.show();
        EnvironmentManager.getInstance().setCourseShortCode(context, shortCode);
        TimetableModule.updateByShortCode(context, shortCode, this, progressDialog);
    }

    private void showErrorDialog(Exception e) {
        FragmentManager fm = context.getSupportFragmentManager();
        String errorReason;
        if(BuildConfig.DEBUG)
            errorReason = e.getMessage();
        else
            errorReason = translate(R.string.error);
        ASiSTDialog dialog = new ASiSTDialog(errorReason, translate(R.string.timetable_import_error_message), true, false);
        progressDialog.dismiss();
        dialog.show(fm, "");
    }

    @Override
    public void onSuccess() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        context.setResult(TimetableModule.IMPORT_FINISHED);
        context.finish();
    }

    private String translate(int id){
        return context.getResources().getString(id);
    }

    @Override
    public void onError(Exception e) {
        showErrorDialog(e);
    }

    @Override
    public void onCoursesParsed(CourseList parsedCourses){
        if(parsedCourses.getCode() == null){
            return;
        }
        FragmentManager fm = context.getSupportFragmentManager();
        String errorReason = parsedCourses.getMessage();
        String code = "Code:" + parsedCourses.getCode();
        String uri = "Uri:" + parsedCourses.getUri();
        String status = "Status:" + parsedCourses.getStatus();
        ASiSTDialog dialog;
        if(BuildConfig.DEBUG) {
            dialog = new ASiSTDialog(translate(R.string.timetable_import_error_message), errorReason + "\n\n" + code + "\n\n" + uri + "\n\n" + status, true, false);
        } else {
            dialog = new ASiSTDialog(translate(R.string.timetable_import_error_message), errorReason, true, false);
        }
        progressDialog.dismiss();
        dialog.show(fm, "");
    }
}
