package de.bps.asist.module.timetable.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.core.annotation.AsistColumn;
import de.bps.asist.core.annotation.AsistColumnDescription;
import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistList;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.model.IListItem;

/**
 * @author thomasw
 * 
 */
@DatabaseTable
@AsistItemConfiguration(showDescription = true)
public class Course extends AbstractDatabaseObject implements IListItem {
	private static final long serialVersionUID = 3255131380451681854L;

	/**
	 * Time formatted for showing in Table
	 */
	@AsistColumn(position = 0)
    @ASiSTDetail(i18nKey = "timetable_time_title", position = 1)
	private String time;

    private CourseTimespan timespan;

    @DatabaseField
	private Long courseid;

	@DatabaseField
	@AsistTitle
	@AsistColumn(position = 1)
	private String title;

	@DatabaseField
	private String desc;

	@ForeignCollectionField(eager = true)
	private Collection<Lecturer> lecturer;

	@ForeignCollectionField(eager = true)
	private Collection<Room> rooms;

	@ForeignCollectionField(eager = true)
	private Collection<CourseTime> times;

	private Integer dayOfWeekWanted;

	@AsistList
	private List<Object> detailsViewList;

	@AsistColumnDescription(position = 1)
	@AsistDescription
    @ASiSTDetail(i18nKey = "timetable_room_title", position = 2)
	private String room;

	private String titleAndRoom;

    @AsistColumnDescription(position = 2)
    @AsistDescription
    @ASiSTDetail(i18nKey = "timetable_lecturer_title", position = 6)
    @DatabaseField
	private Integer type;

	public Course() {
		//
	}

	public List<Object> getDetailsViewList() {
		final List<Object> list = new ArrayList<>();
		if (times != null && !times.isEmpty()) {
			list.add(times.toArray()[0]);
		}
		if (rooms != null && !rooms.isEmpty()) {
			list.add(rooms.toArray()[0]);
		}
		if (lecturer != null && !lecturer.isEmpty()) {
			list.add(lecturer.toArray()[0]);
		}
		return list;
	}

	public void setDetailsViewList(final List<Object> detailsViewList) {
		this.detailsViewList = detailsViewList;
	}

    public CourseTimespan getCourseTime(){
        if(timespan == null){
            timespan = new CourseTimespan(this);
        }
        return timespan;
    }

	public String getTime() {
		if (time == null) {
			final StringBuilder tableTime = new StringBuilder();
			if (times != null && !times.isEmpty()) {
				for (final CourseTime time : times) {
					tableTime.append(CourseTime.timeFormat.format(time.getStart())).append(" - ").append(CourseTime.timeFormat.format(time.getEnd()));
					break;
				}
			}
			time = tableTime.toString();
		}
		return time;
	}

	public void setTime(final String time) {
		this.time = time;
	}

	public String getTitleAndRoom() {
		if (titleAndRoom == null) {
			final StringBuilder sb = new StringBuilder();
			sb.append(title);
			if (rooms != null && !rooms.isEmpty()) {
				for (final Room room : rooms) {
					sb.append("\n");
					sb.append(room.getTitle());
					break;
				}
			}
			titleAndRoom = sb.toString();
		}

		return titleAndRoom;
	}

	public String getRoom() {
		if (rooms != null && !rooms.isEmpty()) {
			for (final Room room : rooms) {
				return room.getTitle();
			}
		}

		return room;
	}

	public void setRoom(final String room) {
		this.room = room;
	}

	public Collection<CourseTime> getTimes() {
		return times;
	}

	public void setTimes(final Collection<CourseTime> times) {
		this.times = times;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(final String desc) {
		this.desc = desc;
	}

	public Collection<Lecturer> getLecturer() {
		return lecturer;
	}

	public void setLecturer(final Collection<Lecturer> lecturer) {
		this.lecturer = lecturer;
	}

	public Collection<Room> getRooms() {
		return rooms;
	}

	public void setRooms(final Collection<Room> rooms) {
		this.rooms = rooms;
	}

	public Integer getDayOfWeekWanted() {
		return dayOfWeekWanted;
	}

	public void setDayOfWeekWanted(final Integer dayOfWeekWanted) {
		this.dayOfWeekWanted = dayOfWeekWanted;
	}

	public Long getCourseid() {
		return courseid;
	}

	public void setCourseid(final Long courseid) {
		this.courseid = courseid;
	}

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
