package de.bps.asist.module.timetable;

import android.content.Context;

import java.util.List;

import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.module.timetable.model.FacultyList;
import de.bps.asist.module.timetable.model.SelectionTreeNode;

/**
 * Created by bja on 26.06.17.
 */

public class SelectionTreeCallback implements ParserCallback<FacultyList> {

    private final Context context;

    public SelectionTreeCallback(final Context context) {
        this.context = context;
    }

    @Override
    public void afterParse(FacultyList parsed) {

        if (parsed != null) {
            DatabaseManager<SelectionTreeNode> db = new DatabaseManager<>(context, SelectionTreeNode.class);
            List<SelectionTreeNode> nodes = parsed.getItems();
            for (SelectionTreeNode node : nodes) {
                parseAndSave(db, node);
            }
        }
    }

    @Override
    public void onError(Exception e) {

    }

    @Override
    public void onManagedError(FacultyList parsed) {
        //TODO
    }

    private void parseAndSave(DatabaseManager<SelectionTreeNode> db, SelectionTreeNode node) {
        db.saveObject(node);
        for (SelectionTreeNode child : node.getChildren()) {
            child.setParent(node);
            parseAndSave(db, child);
        }
    }
}
