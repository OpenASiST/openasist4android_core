package de.bps.asist.module.timetable;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.module.timetable.model.Course;
import de.bps.asist.module.timetable.model.CourseMap;
import de.bps.asist.module.timetable.model.CourseTime;

import static de.bps.asist.module.timetable.CourseParser.parseDayOfWeek;
import static de.bps.asist.module.timetable.CourseParser.parseLectureType;

public class TimetableAdapter extends CategoryListViewAdapter<Course> {

    private static final int[] keys = new int[]{R.string.monday, R.string.tuesday, R.string.wednesday, R.string.thursday,
            R.string.friday, R.string.sonstige_veranstaltung};

    public TimetableAdapter(final Context context, final Collection<Course> courses, Integer currentMode) {
        this(context, createMap(context, courses, currentMode));
    }

    public TimetableAdapter(final Context context, final Map<String, List<Course>> map) {
        super(context, map, new AbstractGenericListCallback(context, TimetableDetailActivity.class));
    }

    private static Map<String, List<Course>> createMap(Context context, Collection<Course> courses, Integer currentMode) {
        CourseMap courseMap = new CourseMap(courses);
        final Map<String, List<Course>> map = new LinkedHashMap<>();
        List<Course> modeCourses = courseMap.getCoursesForMode(currentMode);
        if (modeCourses == null) {
            return map;
        }
        for (final int key : keys) {
            final String value = context.getResources().getString(key);
            map.put(value, new ArrayList<Course>());
        }
        for (final Course course : modeCourses) {
            for (final CourseTime time : course.getTimes()) {
                int dayOfWeek = time.getDayOfWeek();
                String key = parseDayOfWeek(context, dayOfWeek);
                List<Course> list = map.get(key);
                if (list == null) {
                    list = new ArrayList<>();
                    map.put(key, list);
                }
                list.add(course);
            }
        }
        for (final List<Course> list : map.values()) {
            Collections.sort(list, new CourseComparator());
        }
        return map;
    }

    @Override
    protected LinearLayout addItemView(final int position, final LayoutInflater inflater) {
        final Course item = (Course) getItem(position);

        final LinearLayout row = (LinearLayout) inflater.inflate(R.layout.timetable_list_item, null);
        addTitle(item, row);
        addDescription(item, row);
        addTime(item, row);
        addType(item, row);
        addCallback(item, row, getCallback());
        return row;
    }

    private void addType(Course item, LinearLayout row) {
        final String type = parseLectureType(getContext(),item.getType());
        final TextView typeView = (TextView) row.findViewById(R.id.listItemType);
        typeView.setText(type);
    }

    private void addTime(final Course course, final LinearLayout row) {
        final String time = getTime(course);
        final TextView timeView = (TextView) row.findViewById(R.id.timeTableTimeView);
        timeView.setText(time);
    }

    public String getTime(final Course course) {
        final Collection<CourseTime> times = course.getTimes();
        final StringBuilder tableTime = new StringBuilder();
        if (times != null && !times.isEmpty()) {
            for (final CourseTime time : times) {
                final String start = CourseTime.timeFormat.format(time.getStart());
                final String end = CourseTime.timeFormat.format(time.getEnd());
                tableTime.append(start).append("\n").append(end);
                break;
            }
        }
        return tableTime.toString();
    }

    /**
     * Compares Course Objects and sorts them after the time
     *
     * @author thomasw
     */
    private static class CourseComparator implements Comparator<Course> {
        @Override
        public int compare(final Course course1, final Course course2) {
            return course1.getTime().compareTo(course2.getTime());
        }
    }

}
