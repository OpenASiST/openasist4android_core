package de.bps.asist.module.timetable.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by litho on 04.11.14.
 * Map for getting Course Information based on Modes
 */
public class CourseMap {

    private static final String TAG = CourseMap.class.getSimpleName();


    private int currentWeekNo;

    private Map<Integer, List<Course>> courseMap = new TreeMap<>();

    public CourseMap(Collection<Course> courses) {
        Calendar cal = Calendar.getInstance();
        currentWeekNo = cal.get(Calendar.WEEK_OF_YEAR);

        for (Course course : courses) {
            addCourse(course);
        }
    }

    private void addCourse(Course course) {
        List<Integer> modes = getModes(course);
        for (Integer mode : modes) {
            addCourseToMap(mode, course);
        }
    }

    private void addCourseToMap(Integer mode, Course course) {
        List<Course> list = courseMap.get(mode);
        if (list == null) {
            list = new ArrayList<>();
            courseMap.put(mode, list);
        }
        list.add(course);
        ////Log.v(TAG, "adding course " + course.getTitle() + " for mode " + mode);
    }

    private List<Integer> getModes(Course course) {
        Collection<CourseTime> times = course.getTimes();
        List<Integer> result = new ArrayList<>();
        for (CourseTime time : times) {
            Integer mode = time.getMode();
            if (isModeValid(mode, time)) {
                result.add(mode);
            }
        }
        return result;
    }

    private boolean isModeValid(Integer mode, CourseTime time) {
        if (mode % 10 == 0) {
            return true;
        }
        if (mode % 10 == 5) {
            int weekno = time.getWeekNumber();
            return weekno >= currentWeekNo;
        }

        if (mode % 10 == 6) {
            int weekno = time.getWeekNumber();
            return weekno >= currentWeekNo;
        }

        return mode == 1;

    }

    public List<Course> getCoursesForMode(Integer mode) {
        List<Course> weekly = courseMap.get(10);
        if (weekly == null) {
            weekly = new ArrayList<>();
        }
        List<Course> modeCourses = courseMap.get(mode);
        if (modeCourses != null) {
            weekly.addAll(modeCourses);
        }

        List<Course> singleEvents = courseMap.get(1);
        if (singleEvents != null) {
            weekly.addAll(singleEvents);
        }

        return weekly;
    }
}
