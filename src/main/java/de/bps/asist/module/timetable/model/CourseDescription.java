package de.bps.asist.module.timetable.model;

import android.content.Context;

import java.io.Serializable;

import de.bps.asist.core.annotation.ASiSTDetail;
import de.bps.asist.module.poi.PoiModule;
import de.bps.asist.module.poi.model.PoiItem;

import static de.bps.asist.module.timetable.CourseParser.parseLectureType;

/**
 * Created by litho on 13.10.14.
 * Convenience class for Course information
 */
public class CourseDescription implements Serializable {

    @ASiSTDetail(i18nKey = "timetable_time_title", position = 0)
    private String time;

    @ASiSTDetail(i18nKey = "timetable_lecturer_title", position = 1)
    private String lecturer;

    @ASiSTDetail(i18nKey = "timetable_room_title", position = 2)
    private String room;

    @ASiSTDetail(i18nKey = "module_course_detail_building", position = 3)
    private String building;

    @ASiSTDetail(i18nKey = "module_course_detail_address", position = 4)
    private String address;

    @ASiSTDetail(i18nKey = "module_course_detail_description", position = 5)
    private String desc;

    @ASiSTDetail(i18nKey = "timetable_lecture_type", position =  6)
    private String types;

    private PoiItem poi;

    private CourseTimespan timeSpan;

    private Course course;

    private Integer type;


    public CourseDescription(Context context, Course course) {
        this.course = course;
        final StringBuilder tableTime = new StringBuilder();
        if (course.getTimes() != null && !course.getTimes().isEmpty()) {
            for (final CourseTime time : course.getTimes()) {
                tableTime.append(CourseTime.timeFormat.format(time.getStart())).append(" - ").append(CourseTime.timeFormat.format(time.getEnd()));
                break;
            }
        }
        time = tableTime.toString();
        this.desc = course.getDesc();

        if (course.getLecturer() != null && course.getLecturer().size() > 0) {
            Lecturer lect = course.getLecturer().iterator().next();
            this.lecturer = lect.getName();
        }

        if (course.getRooms() != null && course.getRooms().size() > 0) {
            Room room = course.getRooms().iterator().next();
            this.room = room.getTitle();
            Integer poiid = room.getReference();
            this.poi = PoiModule.findItemByPoiId(context, poiid);
            if (poi != null) {
                this.building = poi.getTitle();
                this.address = poi.getDescription();
            }
        }
        timeSpan = course.getCourseTime();
        type = course.getType();
        types = parseLectureType(context,type);
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public PoiItem getPoi() {
        return poi;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPoi(PoiItem poi) {
        this.poi = poi;
    }

    public CourseTimespan getTimeSpan() {
        return timeSpan;
    }

    public void setTimeSpan(CourseTimespan timeSpan) {
        this.timeSpan = timeSpan;
    }

    public Course getCourse() {
        return course;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
