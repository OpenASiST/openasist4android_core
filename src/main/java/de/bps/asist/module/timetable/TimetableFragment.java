/**
 *
 */
package de.bps.asist.module.timetable;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.gui.AbstractASiSTFragment;
import de.bps.asist.gui.code.GenericAddActivity;
import de.bps.asist.gui.dialog.ASiSTDialog;
import de.bps.asist.module.timetable.model.Course;
import de.bps.asist.module.timetable.model.CourseList;
import de.bps.asist.module.timetable.model.SelectionTreeNode;

/**
 * @author thomasw
 */
public class TimetableFragment extends AbstractASiSTFragment {

    /**
     * Extra varible to indicate if a timetable is already present; used in GenericAddActivity
     */
    public static final String TIMETABLE_SAVED = "timetablesaved";

    private boolean timetableSaved = false;

    private ListView courses;
    private ImageButton prevButton;
    private ImageButton nextButton;
    private TextView weekTitle;
    private TextView tipNoPlanImported;
    private int currentMode;
    private LinearLayout weekSwitch;
    private boolean hasSelectionTree = false;


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.timetable_start, container, false);
        courses = (ListView) view.findViewById(R.id.courses_overview);
        tipNoPlanImported = (TextView) view.findViewById(R.id.tip_noplanimported);

        prevButton = getView(view, R.id.button_back);
        prevButton.setEnabled(false);
        prevButton.setOnClickListener(new OnButtonClicked());

        nextButton = getView(view, R.id.button_next);
        nextButton.setOnClickListener(new OnButtonClicked());

        weekTitle = getView(view, R.id.week_title);
        weekSwitch = getView(view, R.id.week_switch);

        Calendar cal = Calendar.getInstance();
        int weekNo = cal.get(Calendar.WEEK_OF_YEAR);
        if (weekNo % 2 == 0) {
            currentMode = 40;
        } else {
            currentMode = 30;
        }
        setModeTitle();

        DatabaseManager<SelectionTreeNode> selectionDB = new DatabaseManager<>(getActivity(), SelectionTreeNode.class);
        hasSelectionTree = selectionDB.hasItems();

        final SwipeRefreshLayout refreshTimeTableSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_timetable_swipe_layout);
        refreshTimeTableSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        if (EnvironmentManager.getInstance().containsCourseShortCode(getContext())) {
                            TimetableModule.updateByShortCode(getContext(), EnvironmentManager.getInstance().getCourseShortCode(getContext()), new CourseParseCallback() {
                                @Override
                                public void onError(Exception e) {
                                    Log.d("timetable_update", "Error while Downloading/Parsing timetable", e);
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getContext(), R.string.module_timetable_course_update_failure, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                    refreshTimeTableSwipeLayout.setRefreshing(false);
                                }

                                @Override
                                public void onSuccess() {
                                    refreshTimeTableSwipeLayout.setRefreshing(false);
                                }

                                @Override
                                public void onCoursesParsed(final CourseList parsedCourses) {
                                    // Refresh UI in onCoursesParsed Method, because in Success run after Database update.
                                    // So UI and Database are updating "parallel".
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            drawTable(parsedCourses.getCourse());
                                        }
                                    });
                                    refreshTimeTableSwipeLayout.setRefreshing(false);
                                }
                            }, null);
                        } else {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    TextView toastContent = new TextView(getContext());
                                    toastContent.setText(R.string.module_timetable_course_code_not_found);
                                    toastContent.setGravity(Gravity.CENTER);

                                    Toast courseCodeNotFoundToast = new Toast(getContext());
                                    courseCodeNotFoundToast.setView(toastContent);
                                    courseCodeNotFoundToast.setDuration(Toast.LENGTH_LONG);
                                    courseCodeNotFoundToast.show();
                                }
                            });
                            refreshTimeTableSwipeLayout.setRefreshing(false);
                        }
                    }
                });
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        int menuConfig = R.menu.timetable_config;
        if(hasSelectionTree){
            menuConfig = R.menu.course_selection_tree;
        }
        inflater.inflate(menuConfig, menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_timetable_menu) {
            startGetTimetableActivity();
            return true;
        }
        if (i == R.id.action_timetable_selectiontree_menu) {
            startGetSelectionTreeActivity();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        DatabaseManager<SelectionTreeNode> selectionDB = new DatabaseManager<>(getActivity(), SelectionTreeNode.class);
        hasSelectionTree = selectionDB.hasItems();

        super.onStart();

        String shortCode = EnvironmentManager.getInstance().getCourseShortCode(getActivity());

        final DatabaseManager<Course> db = new DatabaseManager<>(getActivity(), Course.class);
        final List<Course> courses = db.getAll();


        if (shortCode != null) {
            timetableSaved = true;
            tipNoPlanImported.setVisibility(View.GONE);
            drawTable(courses);
        } else {
            weekSwitch.setVisibility(View.GONE);
            tipNoPlanImported.setVisibility(View.VISIBLE);
            ASiSTDialog dialog = new ASiSTDialog(translate(R.string.timetable_no_data), translate(R.string.timetable_no_data_message), true, false);
            dialog.show(getFragmentManager(), "");
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (resultCode == TimetableModule.IMPORT_FINISHED) {
            ////Log.i("TimetableFragment", "imported courses and show in table.");
            final DatabaseManager<Course> db = new DatabaseManager<>(getActivity(), Course.class);
            final List<Course> courses = db.getAll();
            drawTable(courses);
        }
    }

    private void drawTable(final List<Course> courses) {
        weekSwitch.setVisibility(View.VISIBLE);

        final TimetableAdapter adapter = new TimetableAdapter(getActivity(), courses, currentMode);

        this.courses.setAdapter(adapter);
        Calendar defaultCalender = Calendar.getInstance();
        int dayOfWeek = defaultCalender.get(Calendar.DAY_OF_WEEK);
        String header = parseDayOfWeek(dayOfWeek);
        int index = adapter.getIndexForHeader(header);
        if (index > 0) {
            adapter.notifyDataSetChanged();
            this.courses.smoothScrollToPositionFromTop(index, 0);
        }
    }

    private String parseDayOfWeek(int dayOfWeek) {
        String key = "n/a";
        switch (dayOfWeek) {
            case Calendar.MONDAY:
                key = translate(R.string.monday);
                break;
            case Calendar.TUESDAY:
                key = translate(R.string.tuesday);
                break;
            case Calendar.WEDNESDAY:
                key = translate(R.string.wednesday);
                break;
            case Calendar.THURSDAY:
                key = translate(R.string.thursday);
                break;
            case Calendar.FRIDAY:
                key = translate(R.string.friday);
            default:
                break;
        }
        return key;
    }

    private void startGetTimetableActivity() {
        TimeTableInputCallback callback = new TimeTableInputCallback();
        callback.timetableSaved = timetableSaved;
        forwardWithObject(GenericAddActivity.class, callback);
    }

    private void startGetSelectionTreeActivity() {
        final Intent showSpinner = new Intent(getActivity(), TimetableSelectioncodeAddActivity.class);
        startActivity(showSpinner);
    }

    private void setModeTitle() {
        if (currentMode == 30) {
            weekTitle.setText(translate(R.string.course_week_odd));
        } else {
            weekTitle.setText(translate(R.string.course_week_even));
        }
    }


    private class OnButtonClicked implements View.OnClickListener {

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            boolean toggleMode = false;
            if (v == prevButton) {
                prevButton.setEnabled(false);
                nextButton.setEnabled(true);
                toggleMode = true;
            } else if (nextButton == v) {
                nextButton.setEnabled(false);
                prevButton.setEnabled(true);
                toggleMode = true;
            }
            if (toggleMode) {
                toggleMode();
            }
        }

        private void toggleMode() {
            if (currentMode == 30) {
                currentMode = 40;
            } else {
                currentMode = 30;
            }
            setModeTitle();
        }
    }


}
