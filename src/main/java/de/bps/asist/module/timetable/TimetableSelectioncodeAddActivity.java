package de.bps.asist.module.timetable;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.gui.list.GenericListViewAdapter;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.timetable.model.SelectionTreeNode;

/**
 * Created by Martin Luschek.
 */
public class TimetableSelectioncodeAddActivity extends AsistModuleActivity {

    private Spinner fachrichtung_Spinner;
    private Spinner studiengang_Spinner;
    private Spinner matrikel_Spinner;

    private List<SelectionTreeNode> faculties;
    private Map<String, Collection<SelectionTreeNode>> facs = new HashMap<>();
    private Map<String, Collection<SelectionTreeNode>> studies = new HashMap<>();

    private String shortCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.module_timetable_selectioncode_add_activity);

        fachrichtung_Spinner = (Spinner) findViewById(R.id.fachrichtung_Spinner);
        fachrichtung_Spinner.setSelection(-1);
        fachrichtung_Spinner.setOnItemSelectedListener(new onItemClickListener());

        studiengang_Spinner = (Spinner) findViewById(R.id.studiengang_Spinner);
        studiengang_Spinner.setOnItemSelectedListener(new onItemClickListener());
        studiengang_Spinner.setSelection(-1);
        matrikel_Spinner = (Spinner) findViewById(R.id.matrikel_Spinner);

        matrikel_Spinner.setOnItemSelectedListener(new onItemClickListener());
        matrikel_Spinner.setSelection(-1);
        getSelectionTree(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.course_add_selection_tree, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.action_add_selection_tree == item.getItemId()) {
            if (shortCode != null) {
                UICallback cb = new UICallback(this, shortCode);
                cb.execute();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getSelectionTree(Context context) {
        DatabaseManager<SelectionTreeNode> db = new DatabaseManager<>(context, SelectionTreeNode.class);
        List<SelectionTreeNode> rootNodes = db.getObjects("parent_id", null);
        faculties = new ArrayList<>();
        faculties.addAll(rootNodes);
        fillLists();

        setAdapter(fachrichtung_Spinner, faculties, context);
        fachrichtung_Spinner.setSelection(-1);
    }

    private void fillLists() {
        facs = new HashMap<>();
        studies = new HashMap<>();

        for (SelectionTreeNode fac : faculties) {
            facs.put(fac.getValue(), fac.getChildren());
            for (SelectionTreeNode stud : fac.getChildren()) {
                studies.put(stud.getValue(), stud.getChildren());
            }
        }
    }

    private void fillNextSpinner(Spinner currentSpinner, String selected) {
        if (currentSpinner == fachrichtung_Spinner) {
            Collection<SelectionTreeNode> stud = facs.get(selected);
            setAdapter(studiengang_Spinner, stud, this);
        }
        if (currentSpinner == studiengang_Spinner) {
            Collection<SelectionTreeNode> mat = studies.get(selected);
            setAdapter(matrikel_Spinner, mat, this);
        }
    }

    private void setAdapter(Spinner spinner, Collection<? extends IListItem> list, Context context) {
        GenericListViewAdapter adapter = new GenericListViewAdapter<>(context, list, null);
        spinner.setAdapter(adapter);
    }

    private class onItemClickListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Object obj = adapterView.getItemAtPosition(i);

            if (adapterView == fachrichtung_Spinner) {
                SelectionTreeNode fac = (SelectionTreeNode) obj;
                fillNextSpinner(fachrichtung_Spinner, fac.getValue());
            }
            if (adapterView == studiengang_Spinner) {
                SelectionTreeNode stud = (SelectionTreeNode) obj;
                fillNextSpinner(studiengang_Spinner, stud.getValue());
            }
            if (adapterView == matrikel_Spinner) {
                SelectionTreeNode mat = (SelectionTreeNode) obj;
                shortCode = mat.getRelPath();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }
}
