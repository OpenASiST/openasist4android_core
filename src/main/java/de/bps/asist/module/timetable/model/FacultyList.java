package de.bps.asist.module.timetable.model;

import java.util.List;

import de.bps.asist.core.model.IList;

/**
 * Created by pc38766 on 10.11.2014.
 */
public class FacultyList implements IList {

    private List<SelectionTreeNode> shortCodes;

    public List<SelectionTreeNode> getShortCodes() {
        return shortCodes;
    }

    public void setShortCodes(final List<SelectionTreeNode> shortcodes) {
        this.shortCodes = shortcodes;
    }

    @Override
    public List<SelectionTreeNode> getItems() {
        return shortCodes;
    }
}
