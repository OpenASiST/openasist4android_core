package de.bps.asist.module.timetable.model;

import android.util.Log;

import org.apache.http.auth.AuthenticationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

import de.bps.asist.core.manager.parser.BackgroundParserTask;

/**
 * Created by epereira on 24.05.2016.
 */
public class TimetableResponseErrorHandler implements ResponseErrorHandler {

    private static final String TAG = BackgroundParserTask.class.getCanonicalName();

    @Override
    public void handleError(ClientHttpResponse clienthttpresponse) throws IOException {

        if (clienthttpresponse.getStatusCode() == HttpStatus.FORBIDDEN) {
            //Log.d(TAG, "FORBIDDEN response. Throwing authentication exception");
            try {
                throw new AuthenticationException();
            } catch (AuthenticationException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean hasError(ClientHttpResponse clienthttpresponse) throws IOException {

        if (clienthttpresponse.getStatusCode() != HttpStatus.OK) {
            //Log.d(TAG,"Status code: " + clienthttpresponse.getStatusCode());
            //Log.d(TAG,"Response" + clienthttpresponse.getStatusText());
            //Log.d(TAG,clienthttpresponse.getBody().toString());

            if (clienthttpresponse.getStatusCode() == HttpStatus.FORBIDDEN) {
                //Log.d(TAG,"Call returned a error 403 forbidden resposne ");
                return true;
            }
        }
        return false;
    }
}