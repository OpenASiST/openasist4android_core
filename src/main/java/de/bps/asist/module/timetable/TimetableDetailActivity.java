package de.bps.asist.module.timetable;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import androidx.fragment.app.FragmentManager;
import de.bps.asist.R;
import de.bps.asist.core.annotation.AnnotationHelper;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.core.model.common.Position;
import de.bps.asist.gui.list.AbstractGenericListCallback;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.poi.PoiActivity;
import de.bps.asist.module.timetable.model.Course;
import de.bps.asist.module.timetable.model.CourseDescription;

/**
 * Created by thomasw on 28.08.14.
 * Detail activity for course
 */
public class TimetableDetailActivity extends AsistModuleActivity<Course> implements OnMapReadyCallback {

    private static final String TAG = TimetableDetailActivity.class.getSimpleName();

    private CourseDetailAdapter adapter;

    private CourseDescription courseDesc;

    private SupportMapFragment fragment;

    private LinearLayout mapLayout;

    private TextView mapHeaderView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.timetable_details);
        ListView listView = getView(R.id.course_details_list);
        mapLayout = getView(R.id.course_details_map_wrapper);
        mapHeaderView = getView(R.id.course_details_map_header);

        Course course = getSelectedItem();

        Map<String, List<ASiSTDetailDescription>> categories = new LinkedHashMap<>();

        String courseTitle = course.getTitle();
        List<ASiSTDetailDescription> descList = new ArrayList<>();
        categories.put(courseTitle, descList);

        courseDesc = new CourseDescription(this, course);
        final Map<Integer, ASiSTDetailDescription> map = AnnotationHelper.readASiSTDetails(courseDesc);
        for (final Integer pos : map.keySet()) {
            final ASiSTDetailDescription desc = map.get(pos);
            descList.add(desc);
        }

        List<ASiSTDetailDescription> actionList = new ArrayList<>();

        //Depending on Flag in Config-File, activate or deactivate the actions here.
        final boolean EnableActions = getResources().getBoolean(R.bool.enableActionsInTimetable);

        if(EnableActions == true) {
            final String actionHeader = translate(R.string.course_detail_actions);

            String feedbackHeader = translate(R.string.module_feedback_name);
            actionList.add(new ASiSTDetailDescription(R.string.module_feedback_name, feedbackHeader, 1, feedbackHeader));

            String surveyHeader = translate(R.string.survey_name);
            actionList.add(new ASiSTDetailDescription(R.string.survey_name, surveyHeader, 1, surveyHeader));

            categories.put(actionHeader, actionList);
            String mapHeader = translate(R.string.module_locationplan_name);
            List<ASiSTDetailDescription> mapList = new ArrayList<>();
            categories.put(mapHeader, mapList);
        }
        else {
            String mapHeader = "";
            List<ASiSTDetailDescription> mapList = new ArrayList<>();
            categories.put(mapHeader, mapList);

        }

        adapter = new CourseDetailAdapter(this, categories, courseDesc);
        listView.setAdapter(adapter);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        onActivityCreated(null);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        FragmentManager fm = getSupportFragmentManager();
        fragment = new CourseMapFragment(courseDesc.getPoi());
        fm.beginTransaction().replace(R.id.course_details_map, fragment).commit();
        fragment.onActivityCreated(savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState){
        fragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        GoogleMap map = googleMap;
        if(courseDesc.getPoi() != null && map != null) {
            mapLayout.setVisibility(View.VISIBLE);
            mapHeaderView.setVisibility(View.VISIBLE);
            Position pos = courseDesc.getPoi().getPosition();

            map.setMyLocationEnabled(true);
            LatLng point = new LatLng(pos.getLatitude(), pos.getLongitude());

            CameraUpdate camUpdate = CameraUpdateFactory.newLatLng(point);
            map.moveCamera(camUpdate);
            map.animateCamera(CameraUpdateFactory.zoomTo(15f));

            map.addMarker(new MarkerOptions().position(point).title(courseDesc.getPoi().getTitle()));
            View mapView = getView(R.id.course_details_map_wrapper);
            mapView.setOnTouchListener(new MapTouchedListener());
        }else{
            //Log.e(TAG, courseDesc == null ? "The courseDescription is null" : "The Map from the fragment is null");
            mapLayout.setVisibility(View.GONE);
            mapHeaderView.setVisibility(View.GONE);
        }
    }

    private class MapTouchedListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(final View v, final MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                AbstractGenericListCallback cb = new AbstractGenericListCallback(TimetableDetailActivity.this, PoiActivity.class);
                cb.doAction(courseDesc.getPoi());
            }
            return false;
        }
    }

}
