/**
 *
 */
package de.bps.asist.module.timetable;

import android.app.ProgressDialog;
import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.fragment.app.Fragment;
import de.bps.asist.BuildConfig;
import de.bps.asist.R;
import de.bps.asist.core.database.AbstractDatabaseObject;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.manager.environment.ASiSTCoreDataManager;
import de.bps.asist.core.manager.environment.EnvironmentManager;
import de.bps.asist.core.manager.parser.ASiSTParser;
import de.bps.asist.core.manager.parser.ParserCallback;
import de.bps.asist.module.AbstractAsistModule;
import de.bps.asist.module.start.AsistStartModule;
import de.bps.asist.module.start.tree.ASiSTTreeItem;
import de.bps.asist.module.timetable.model.Course;
import de.bps.asist.module.timetable.model.CourseList;
import de.bps.asist.module.timetable.model.CourseMap;
import de.bps.asist.module.timetable.model.CourseTime;
import de.bps.asist.module.timetable.model.Lecturer;
import de.bps.asist.module.timetable.model.Room;
import de.bps.asist.module.timetable.model.SelectionTreeNode;

/**
 * @author thomasw
 */
public class TimetableModule extends AbstractAsistModule {

    public static final int IMPORT_FINISHED = 0;
    public static final int MODE_ODD = 30;
    public static final int MODE_EVEN = 40;

    public static final String PREF_KEY_SHORTCODE = "course_selection_tree";

    private static final String TAG = TimetableModule.class.getSimpleName();

    public static CourseStartTreeConfig startTreeConfig;
    private static TimetableModule INSTANCE;
    private static ProgressDialog ProgressDialog;

    public TimetableModule() {
        INSTANCE = this;
        startTreeConfig = new CourseStartTreeConfig(this);
        setStartTreeConfig(startTreeConfig);
        AsistStartModule.registerForPush(getName(), "CourseModule");
    }

    public static List<Course> findCurrentCourses(Context context) {
        final DatabaseManager<Course> db = new DatabaseManager<>(context, Course.class);
        final List<Course> courses = db.getAll();

        CourseMap map = new CourseMap(courses);


        final List<Course> result = new ArrayList<>();

        for (Course course : map.getCoursesForMode(TimetableModule.getCurrentMode())) {
            if (BuildConfig.DEBUG || course.getCourseTime().isNow()) {
                result.add(course);
            }
        }
        return result;
    }

    public static List<Course> getCoursesToday(Context context) {
        Calendar cal = Calendar.getInstance();
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
        final DatabaseManager<Course> db = new DatabaseManager<>(context, Course.class);
        final List<Course> courses = db.getAll();
        final List<Course> result = new ArrayList<>();
        for (Course course : courses) {
            if (isOnDayOfWeek(course, dayOfWeek)) {
                result.add(course);
            }
        }
        return result;
    }

    public static Integer getCurrentMode() {
        Calendar cal = Calendar.getInstance();
        int weekNo = cal.get(Calendar.WEEK_OF_YEAR);
        if (weekNo % 2 == 0) {
            return MODE_EVEN;
        } else {
            return MODE_ODD;
        }
    }

    private static boolean isOnDayOfWeek(Course course, int dayOfWeek) {
        for (final CourseTime time : course.getTimes()) {
            if (time.getDayOfWeek() == dayOfWeek) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getName() {
        return R.string.module_timetable_name;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_menu_schedule;
    }

    @Override
    public Fragment getInitialFragment() {
        return new TimetableFragment();
    }

    @Override
    public void updateData(Context context) {
        super.updateData(context);
        String shortCode = EnvironmentManager.getInstance().getCourseShortCode(context);
        if (shortCode != null) {
            updateByShortCode(context, shortCode, null, null);
        }
        updateSelectionTree(context);
    }

    public static void updateByShortCode(Context context, String code, CourseParseCallback cb, ProgressDialog pg) {
        final String rootUrl = ASiSTCoreDataManager.getInstance().getRootUrl();
        final StringBuilder url;
        if(pg != null)
            ProgressDialog = pg;
        url = new StringBuilder(rootUrl);
        url.append("/course/");
        url.append(ASiSTCoreDataManager.getInstance().getInstitutionName());
        url.append("/shortcode/").append(code);
        url.append("/").append(EnvironmentManager.getInstance().getDeviceToken(context));
        ParserCallback<CourseList> pc = new TimetableCallback(context, cb, pg, INSTANCE);
        if (cb == null) {
            ASiSTParser.getInstance().parse(url.toString(), CourseList.class, pc);
        } else {
            ASiSTParser.getInstance().parseWithTask(url.toString(), CourseList.class, pc);
        }
    }

    public static void updateSelectionTree(Context context) {
        /*final StringBuilder sb;
        sb = new StringBuilder();
        sb.append(context.getResources().getString(R.string.rootUrl));
        sb.append("/course/").append(context.getResources().getString(R.string.institutionName));
        sb.append("/selectionTree");

        final SelectionTreeCallback cb = new SelectionTreeCallback(context);

        ASiSTParser.getInstance().parse(sb.toString(), FacultyList.class, cb);*/
        //TODO: Find out if Selectiontree is needed at all.
    }

    @Override
    public List<Class<? extends AbstractDatabaseObject>> getDatabaseClasses() {
        final List<Class<? extends AbstractDatabaseObject>> result = new ArrayList<>();
        result.add(Course.class);
        result.add(Lecturer.class);
        result.add(Room.class);
        result.add(CourseTime.class);
        result.add(SelectionTreeNode.class);
        return result;
    }

}
