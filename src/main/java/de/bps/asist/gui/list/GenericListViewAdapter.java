package de.bps.asist.gui.list;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.bps.asist.R;
import de.bps.asist.core.annotation.AnnotationHelper;
import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistImage;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistPubDate;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.annotation.AsistTitlePrefixKey;
import de.bps.asist.core.manager.image.ASiSTImageManager;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.module.feeds.SingleFeedActivity;
import de.bps.asist.module.feeds.model.FeedItem;
import de.bps.asist.module.lostandfound.model.LostFoundImage;
import de.bps.asist.module.start.AsistStartActivity;

public class GenericListViewAdapter<T extends IListItem> extends BaseAdapter {

    private final List<T> data;
    private final Context context;
    private final IGenericListCallback callback;

    public GenericListViewAdapter(final Context context, final Collection<T> objects,
                                  final IGenericListCallback callback) {
        data = new ArrayList<>(objects);
        this.context = context;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public T getItem(final int position) {
        if (data.size() < position) {
            return null;
        }
        return data.get(position);
    }

    @Override
    public long getItemId(final int position) {
        final T item = getItem(position);
        if (item == null) {
            return -1;
        }
        return item.getId();
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout row = (LinearLayout) inflater.inflate(R.layout.generic_list_item, null);
        // item object
        final T student = getItem(position);
        // item configuration
        final boolean showMainLogo = AnnotationHelper.getBooleanOf(student, "showMainLogo", AsistItemConfiguration.class);
        final boolean showDescription = AnnotationHelper.getBooleanOf(student, "showDescription", AsistItemConfiguration.class);
        final boolean useCallback = AnnotationHelper.getBooleanOf(student, "useCallback", AsistItemConfiguration.class);
        ////Log.d("GenericListView", "showMainLogo = " + showMainLogo + " ,showSubLogo = " + showSubLogo + " ,showDescription = " + showDescription + " ,useCallback = " + useCallback + " for item " + student);
        // title
        final Integer titlePrefixKey = AnnotationHelper.getValueOf(student, AsistTitlePrefixKey.class);
        String titlePrefix = "";
        if (titlePrefixKey != null) {
            titlePrefix = context.getString(titlePrefixKey.intValue());
            titlePrefix += " ";
        }
        final String title = titlePrefix
                + String.valueOf(AnnotationHelper.getValueOf(student, AsistTitle.class));
        ((TextView) row.findViewById(R.id.listItemTitle)).setText(title);
        // description
        if (showDescription) {
            String descriptionValue = "";
            if (student instanceof FeedItem) {
                Date pubDate = AnnotationHelper.getValueOf(student, AsistPubDate.class);
                if (pubDate != null) {
                    descriptionValue = String.valueOf(new SimpleDateFormat(getContext().getResources().getString(R.string.datetime_format), Locale.getDefault()).format(pubDate));
                }
            } else {
                descriptionValue = String.valueOf(AnnotationHelper.getValueOf(student, AsistDescription.class));
            }
            Spannable span = new SpannableString(descriptionValue);
            ((TextView) row.findViewById(R.id.listItemDescription)).setText(span);
        } else {
            // hide description
            row.findViewById(R.id.listItemDescription).setVisibility(View.GONE);
        }
        // image
        if (showMainLogo) {
            final Object imageUrl = AnnotationHelper.getValueOf(student, AsistImage.class);
            if (imageUrl != null) {
                final String url;
                if (imageUrl instanceof LostFoundImage) {
                    url = context.getResources().getString(R.string.imageRootPath) + ((LostFoundImage) imageUrl).getImageUrl();
                } else {
                    url = context.getString(R.string.pictureProxyUrl) + imageUrl.toString() + "&size=256";
                }
                ASiSTImageManager.getInstance().setImage(url,
                        (ImageView) row.findViewById(R.id.listItemMainLogo));
            } else {
                row.findViewById(R.id.listItemMainLogoWrapper).setVisibility(View.GONE);
            }
        } else {
            row.findViewById(R.id.listItemMainLogoWrapper).setVisibility(View.GONE);
        }

        if (useCallback) {
            row.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    callback.doAction(student);
                }
            });
        }

        return row;
    }

    public Context getContext() {
        return context;
    }

}
