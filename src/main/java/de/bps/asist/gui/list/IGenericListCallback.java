package de.bps.asist.gui.list;

import java.io.Serializable;

public interface IGenericListCallback<T extends Serializable> extends Serializable {

	void doAction(T o);

}
