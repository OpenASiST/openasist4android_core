package de.bps.asist.gui.list;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;

import java.io.Serializable;

/**
 * Created by litho on 22.10.14.
 * Default Base Adapter with some convenience methods
 */
public abstract class ASiSTBaseAdapter extends BaseAdapter {

    private final Context context;
    private final LayoutInflater inflater;

    public ASiSTBaseAdapter(Context context){
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    protected <T extends View> T getView(final View src, final int id) {
        @SuppressWarnings("unchecked")
        final T view = (T) src.findViewById(id);
        return view;
    }

    protected View inflateView(int id){
        return inflater.inflate(id, null);
    }

    public String translate(int id){
        return context.getResources().getString(id);
    }

    public Context getContext() {
        return context;
    }

    public void forwardWithObject(Context context, Class<? extends Activity> target, Serializable extra){
        AbstractGenericListCallback cb = new AbstractGenericListCallback(context, target);
        cb.doAction(extra);
    }
}
