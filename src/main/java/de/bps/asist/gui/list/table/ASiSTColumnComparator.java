package de.bps.asist.gui.list.table;

import java.util.Comparator;

import de.bps.asist.core.annotation.AsistColumn;

public class ASiSTColumnComparator implements Comparator<AsistColumn> {

	@Override
	public int compare(final AsistColumn lhs, final AsistColumn rhs) {
		if (lhs.position() < rhs.position()) {
			return -1;
		} else if (lhs.position() > rhs.position()) {
			return 1;
		} else {
			return -1;
		}
	}

}
