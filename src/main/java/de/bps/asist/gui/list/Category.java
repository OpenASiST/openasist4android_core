package de.bps.asist.gui.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class Category<T extends Serializable> {

	private final String header;
	private final List<T> items;
	private int startIndex = 0;
	private int endIndex;
	private int headerIndex;
    private int indexSize;

	public Category(final String key, final Collection<T> items) {
		this.items = new ArrayList<>(items);
		this.header = key.toUpperCase(Locale.getDefault());
        indexSize = items.size();
	}

	public List<T> getItems() {
		return items;
	}

	public boolean hasIndex(final int pos) {
		final int realIndex = pos - startIndex;
		return realIndex >= 0 && realIndex < items.size();
	}

	public T getItem(final int pos) {
		return items.get(pos - startIndex);
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(final int startIndex) {
		this.startIndex = startIndex;
		this.endIndex = startIndex + items.size() - 1;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public int size() {
		return items.size();
	}

	public int getHeaderIndex() {
		return headerIndex;
	}

	public String getHeader() {
		return header;
	}

	public void setHeaderIndex(final int headerIndex) {
		this.headerIndex = headerIndex;
	}

    public int getIndexSize() {
        return indexSize;
    }

    /**
     *
     * @return
     */
    public int getFullSize(){
        return items.size() + 1;
    }

    public boolean isEmpty(){
        return items.size() == 0;
    }
}
