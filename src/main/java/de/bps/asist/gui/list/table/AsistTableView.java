/**
 *
 */
package de.bps.asist.gui.list.table;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.bps.asist.R;
import de.bps.asist.core.annotation.AnnotationHelper;
import de.bps.asist.core.annotation.AsistColumn;
import de.bps.asist.core.annotation.AsistColumnDescription;
import de.bps.asist.core.util.StringHelper;
import de.bps.asist.gui.list.IGenericListCallback;

/**
 * @author thomasw
 * 
 */
public class AsistTableView<T extends Serializable> {

	private final List<T> elements;
	private final Context context;
	private final TableLayout layout;
	private IGenericListCallback callback;

	public AsistTableView(final Context context, final List<T> elements, final TableLayout layout) {
		this.context = context;
		this.elements = elements;
		this.layout = layout;

		layout.removeAllViews();

		init();
	}

	private void init() {

		if (elements == null || elements.isEmpty()) {
			final TableRow tR = new TableRow(context);
			final TextView noElements = new TextView(context);
			noElements.setText(R.string.no_element);
			tR.addView(noElements);
			layout.addView(tR);
		} else {
			// get fields for table, sorted by position
			final Map<AsistColumn, Field> tableFields = new TreeMap<>(new ASiSTColumnComparator());

			final Map<Integer, Field> descriptions = new HashMap<>();

			for (final Field field : elements.get(0).getClass().getDeclaredFields()) {
				final AsistColumn column = field.getAnnotation(AsistColumn.class);
				if (column != null) {
					////Log.i("AsistTableView", "added column " + column + " ,field: " + field.getName());
					tableFields.put(column, field);
				}
				final AsistColumnDescription description = field.getAnnotation(AsistColumnDescription.class);
				if (description != null) {
					descriptions.put(description.position(), field);
				}
			}

			////Log.d("AsistTableView", "tableFields size: " + tableFields.size());

			// add headers
			final TableRow tRHead = new TableRow(context);
			boolean hasHeader = false;
			for (final AsistColumn column : tableFields.keySet()) {
				final TextView headTV = new TextView(context);
				if (!"".equals(column.header())) {
					hasHeader = true;
					headTV.setText(column.header());
					headTV.setPadding(5, 5, 5, 5);
				}
				tRHead.addView(headTV);
				tRHead.setPadding(5, 5, 5, 5);
			}
			if (hasHeader) {
				// only add header row if at least one heading exists
				layout.addView(tRHead);
			}

			// add the data rows
			for (final T element : elements) {
				final TableRow tR = new TableRow(context);
				tR.setPadding(5, 5, 5, 5);
				tR.setBackgroundResource(R.drawable.abc_ab_bottom_transparent_light_holo);

				tR.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(final View v) {
						callback.doAction(element);
					}
				});

				final Map<String, String> values = new LinkedHashMap<>();
				for (final Map.Entry<AsistColumn, Field> fieldEntry : tableFields.entrySet()) {
					String value = "";
					final AsistColumn col = fieldEntry.getKey();
					final Field field = fieldEntry.getValue();
					final Object o = AnnotationHelper.getValueOf(element, field);
					if (o != null) {
						value = o.toString();
					}
					////Log.i("AsistTableView", "Add value " + value + " for field " + field.getName());

					String descr = "";
					final Field descField = descriptions.get(col.position());
					if (descField != null) {
						final Object oDesc = AnnotationHelper.getValueOf(element, descField);
						if (oDesc != null) {
							descr = oDesc.toString();
						}
					}

					values.put(value, descr);
				}
				boolean first = true;
				for (final String value : values.keySet()) {
					// add space TextView
					if (!first && !StringHelper.isNullOrEmpty(value)) {
						first = false;
						final TextView spaceTv = new TextView(context);
						spaceTv.setText("|");
						tR.addView(spaceTv);
					}

					final View tableItem = TableLayout.inflate(context, R.layout.table_item, null);
					final TextView columnTV = (TextView) tableItem.findViewById(R.id.tableItem);

					columnTV.setSingleLine(false);
					columnTV.setPadding(5, 5, 5, 5);

					final String description = values.get(value);
					if (!StringHelper.isNullOrEmpty(description)) {
						final Spannable text = new SpannableString(value + "\n" + description);
						text.setSpan(new TextAppearanceSpan(context, R.style.TableNormalText), 0, value.length(),
								Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						text.setSpan(new TextAppearanceSpan(context, R.style.TableAdditionalText), value.length() + 1,
								value.length() + description.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						columnTV.setText(text, TextView.BufferType.SPANNABLE);
					} else {
						final Spannable text = new SpannableString(value);
						text.setSpan(new TextAppearanceSpan(context, R.style.TableAdditionalText), 0, value.length(),
								Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						columnTV.setText(text, TextView.BufferType.SPANNABLE);
					}

					tR.addView(columnTV);
				}

				layout.addView(tR);
			}
		}
	}

	public void setCallback(final IGenericListCallback callback) {
		this.callback = callback;
	}
}
