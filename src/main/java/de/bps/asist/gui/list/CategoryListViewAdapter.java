package de.bps.asist.gui.list;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.annotation.ASiSTCallback;
import de.bps.asist.core.annotation.ASiSTDrawable;
import de.bps.asist.core.annotation.ASiSTViewId;
import de.bps.asist.core.annotation.AnnotationHelper;
import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistImage;
import de.bps.asist.core.annotation.AsistItemConfiguration;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.annotation.AsistTitlePrefixKey;
import de.bps.asist.core.manager.image.ASiSTImageManager;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.module.lostandfound.model.LostFoundImage;

public class CategoryListViewAdapter<T extends Serializable> extends ASiSTBaseAdapter {

    public static int VIEW_ID_ALERT = 2;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_CELL = 1;

    private int rowLayout = R.layout.generic_list_item;
    private int headerLayout = R.layout.generic_list_header;

    private final List<Category<T>> categories = new ArrayList<>();
    private final IGenericListCallback<T> callback;
    private int size;
    private final LayoutInflater inflater;
    private final Map<String, List<T>> objects;

    public CategoryListViewAdapter(final Context context, final Map<String, List<T>> objects, final IGenericListCallback<T> callback) {
        super(context);
        this.objects = objects;
        this.callback = callback;

        buildCategories();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private void buildCategories() {
        categories.clear();
        int fullSize = 0;
        for (final String key : objects.keySet()) {
            final Collection<T> col = objects.get(key);
            final Category<T> cat = new Category<>(key, col);
            cat.setHeaderIndex(fullSize);
            cat.setStartIndex(fullSize + 1);
            fullSize += cat.getFullSize();
            categories.add(cat);
        }
        this.size = fullSize;
    }

    @Override
    public int getCount() {
        return size;
    }

    @Override
    public Object getItem(final int position) {
        if (size <= position) {
            throw new IllegalArgumentException("position cannot be lower than size of objects: size " + size + ", position = " + position);
        }

        for (final Category<T> c : categories) {
            if (c.getHeaderIndex() == position) {
                return c.getHeader();
            }
            if (c.hasIndex(position)) {
                return c.getItem(position);
            }
        }
        throw new IllegalArgumentException("there has not been any item for position " + position + "; size = " + size);
    }

    @Override
    public int getItemViewType(final int position) {
        for (final Category<T> c : categories) {
            if (c.getHeaderIndex() == position) {
                return TYPE_HEADER;
            }
        }
        return TYPE_CELL;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public long getItemId(final int position) {
        final Object item = getItem(position);
        if (item == null) {
            return -1;
        }
        return item.hashCode();
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        // item object
        final int viewType = getItemViewType(position);

        final View row;
        if (viewType == TYPE_CELL) {
            row = addItemView(position, inflater);
        } else {
            row = inflater.inflate(headerLayout, null);
            final String header = (String) getItem(position);
            addHeaderView(row, header);
        }
        return row;
    }

    protected void addHeaderView(View row, String header){
        final TextView titleView = (TextView) row.findViewById(R.id.headerTitle);
        titleView.setText(header);
    }

    protected View addItemView(final int position, final LayoutInflater inflater) {
        @SuppressWarnings(value = "unchecked")
        final T item = (T) getItem(position);
        final View row;
        if (item instanceof ASiSTDetailDescription) {
            row = addDetailDescription((ASiSTDetailDescription) item);
        } else {
            row = inflater.inflate(rowLayout, null);
            // item configuration
            LinearLayout listItem = (LinearLayout) row.findViewById(R.id.listItem);
            Integer viewId = AnnotationHelper.getValueOf(item, ASiSTViewId.class);
            if (viewId != null && viewId.equals(VIEW_ID_ALERT)) {
               listItem.setBackgroundColor(Color.RED);
            }

            addTitle(item, row);
            // description
            addDescription(item, row);
            // image
            addMainLogo(item, row);

            addCallback(item, row, callback);
        }
        return row;
    }

    protected void addCallback(final T item, final View row, final IGenericListCallback<T> callback) {
        final boolean useCallback = AnnotationHelper.getBooleanOf(item, "useCallback", AsistItemConfiguration.class) && callback != null;
        if (useCallback) {
            row.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    callback.doAction(item);
                }
            });
        } else {
            final IGenericListCallback<T> customCallback = AnnotationHelper.getValueOf(item, ASiSTCallback.class, null);
            if (customCallback != null) {
                row.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(final View v) {
                        customCallback.doAction(item);
                    }
                });
            }
        }
    }

    protected void addDescription(final T item, final View row) {
        final boolean showDescription = AnnotationHelper.getBooleanOf(item,
                "showDescription", AsistItemConfiguration.class);
        if (showDescription) {
            final String description = AnnotationHelper.getValueOf(item, AsistDescription.class);
            TextView descView = getView(row, R.id.listItemDescription);
            if (description == null || description.isEmpty()) {
                descView.setText("");
            } else {
                descView.setText(description);
            }
        } else {
            // hide description
            TextView image = getView(row, R.id.listItemDescription);
            if (image != null) {
                image.setText("");
            }
        }
    }

    protected void addMainLogo(final T item, final View row) {
        final boolean showMainLogo = AnnotationHelper.getBooleanOf(item,
                "showMainLogo", AsistItemConfiguration.class);
        if (showMainLogo) {
            ImageView imageView = (ImageView) row.findViewById(R.id.listItemMainLogo);
            final Object imageUrl = AnnotationHelper.getValueOf(item, AsistImage.class);
            if (imageUrl != null) {
                final String url;
                if (imageUrl instanceof LostFoundImage) {
                    url = translate(R.string.imageRootPath) + ((LostFoundImage) imageUrl).getImageUrl();
                } else {
                    url = imageUrl.toString();
                }
                ASiSTImageManager.getInstance().setImage(url, imageView);
            } else {
                Drawable drawable = AnnotationHelper.getValueOf(item, ASiSTDrawable.class);
                if (drawable != null) {
                    imageView.setImageDrawable(drawable);
                } else {
                    row.findViewById(R.id.listItemMainLogoWrapper).setVisibility(View.GONE);
                }
            }
        } else {
            row.findViewById(R.id.listItemMainLogoWrapper).setVisibility(
                    View.GONE);
        }
    }

    protected View addDetailDescription(ASiSTDetailDescription desc) {
        View row = inflater.inflate(R.layout.detail_list_item, null);
        TextView titleView = getView(row, R.id.detailItemTitle);
        titleView.setText(desc.getI18nKey());
        TextView valueView = getView(row, R.id.detailItemValue);
        valueView.setText(desc.getValue());
        return row;
    }

    protected void addTitle(final T item, final View row) {
        // title
        final Integer titlePrefixKey = AnnotationHelper.getValueOf(item,
                AsistTitlePrefixKey.class);
        String titlePrefix = "";
        if (titlePrefixKey != null) {
            titlePrefix = translate(titlePrefixKey);
            titlePrefix += " ";
        }
        final String title = titlePrefix
                + String.valueOf(AnnotationHelper.getValueOf(item,
                AsistTitle.class));
        TextView titleView = (TextView) row.findViewById(R.id.listItemTitle);
        titleView.setText(title);
    }

    public void setRowLayout(final int rowLayout) {
        this.rowLayout = rowLayout;
    }

    public void setHeaderLayout(int headerLayout) {
        this.headerLayout = headerLayout;
    }

    public List<Category<T>> getCategories() {
        return categories;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public int getIndexForHeader(String header) {
        if (header == null) {
            return -1;
        }
        String compare = header.toUpperCase();
        for (final Category<T> c : categories) {
            if (compare.equals(c.getHeader())) {
                return c.getHeaderIndex();
            }
        }
        return -1;
    }

    public Map<String, List<T>> getObjects() {
        return objects;
    }

    public IGenericListCallback<T> getCallback() {
        return callback;
    }
}
