package de.bps.asist.gui.list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.io.Serializable;

import de.bps.asist.core.ASiSTApplication;

public class AbstractGenericListCallback<T extends Serializable> implements IGenericListCallback<T> {

	private static final long serialVersionUID = -5357832834953165977L;

	public static final String EXTRA_ITEM_SELECTED = "item.Selected";

	private final Class<? extends Activity> redirect;
	private final Context context;

	public AbstractGenericListCallback(final Context context, final Class<? extends Activity> redirect) {
		this.redirect = redirect;
		this.context = context;
	}

	@Override
	public void doAction(final Serializable o) {
		doBeforeStartActivity(o);
		doStartActivity(o);
		doAfterStartActivity(o);
	}

	protected void doBeforeStartActivity(final Serializable o) {
		// maybe used later
	}

	protected void doStartActivity(final Serializable o) {
		if(this.context instanceof ASiSTApplication){
			Context c = this.context;
			final Intent intent = new Intent(c, redirect);
			final Bundle bundle = new Bundle();
			bundle.putSerializable(EXTRA_ITEM_SELECTED, o);
			intent.putExtra(EXTRA_ITEM_SELECTED, o);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			c.startActivity(intent);
		} else {
			Context c = (Activity) this.context;
			final Intent intent = new Intent(c, redirect);
			final Bundle bundle = new Bundle();
			bundle.putSerializable(EXTRA_ITEM_SELECTED, o);
			intent.putExtra(EXTRA_ITEM_SELECTED, o);
			c.startActivity(intent);
		}
	}

	protected void doAfterStartActivity(final Serializable o) {
		// maybe used later
	}

}
