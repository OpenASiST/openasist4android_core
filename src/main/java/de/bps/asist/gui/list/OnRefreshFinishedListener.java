package de.bps.asist.gui.list;

/**
 * Created by litho on 27.10.14.
 * Simple Listener for finished refresh
 */
public interface OnRefreshFinishedListener {

    public void refreshFinished();
}
