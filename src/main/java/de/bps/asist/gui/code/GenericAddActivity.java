/**
 *
 */
package de.bps.asist.gui.code;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import de.bps.asist.R;
import de.bps.asist.module.AsistModuleActivity;

/**
 * @author thomasw
 */
public class GenericAddActivity extends AsistModuleActivity<CodeInputCallback> {

    private EditText preEditText;

    private List<EditText> codeEdits;
    private CodeInputCallback callback;

    private Toolbar toolbar;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.generic_code_input);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Code eingeben");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        ///we set a new title for the action bar to avoid confusion about where the user is
        //getSupportActionBar().setTitle("Code eingeben");

        codeEdits = new ArrayList<>();

        codeEdits.add((EditText) findViewById(R.id.timetableCode1));
        codeEdits.add((EditText) findViewById(R.id.timetableCode2));
        codeEdits.add((EditText) findViewById(R.id.timetableCode3));
        codeEdits.add((EditText) findViewById(R.id.timetableCode4));
        codeEdits.add((EditText) findViewById(R.id.timetableCode5));
        codeEdits.add((EditText) findViewById(R.id.timetableCode6));
        codeEdits.add((EditText) findViewById(R.id.timetableCode7));
        codeEdits.add((EditText) findViewById(R.id.timetableCode8));

        final InputWatcher watcher = new InputWatcher();
        for (final EditText codeEdit : codeEdits) {
            codeEdit.addTextChangedListener(watcher);
        }

        callback = getSelectedItem();


    }


    private void getToNextEditTextOrOK(final EditText edit) {
        EditText nextEdit = null;
        int i = edit.getId();
        if (i == R.id.timetableCode1) {
            nextEdit = (EditText) findViewById(R.id.timetableCode2);
            preEditText = edit;

        } else if (i == R.id.timetableCode2) {
            nextEdit = (EditText) findViewById(R.id.timetableCode3);
            preEditText = edit;

        } else if (i == R.id.timetableCode3) {
            nextEdit = (EditText) findViewById(R.id.timetableCode4);
            preEditText = edit;

        } else if (i == R.id.timetableCode4) {
            nextEdit = (EditText) findViewById(R.id.timetableCode5);
            preEditText = edit;

        } else if (i == R.id.timetableCode5) {
            nextEdit = (EditText) findViewById(R.id.timetableCode6);
            preEditText = edit;

        } else if (i == R.id.timetableCode6) {
            nextEdit = (EditText) findViewById(R.id.timetableCode7);
            preEditText = edit;

        } else if (i == R.id.timetableCode7) {
            nextEdit = (EditText) findViewById(R.id.timetableCode8);
            preEditText = edit;

        } else {
            if (preEditText != findViewById(R.id.timetableCode8)) {
                preEditText = edit;
            }

        }
        if(nextEdit != null){
            nextEdit.requestFocus();
        }
    }

    public void codeInputFinished(View view) {
        if(callback != null){
            callback.onCodeInputFinished(this, getCodeFromInput());
        }
    }

    private String getCodeFromInput() {
        final StringBuilder sb = new StringBuilder();
        for (final EditText codeEdit : codeEdits) {
            sb.append(codeEdit.getText());
        }

        return sb.toString();
    }

    private class InputWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(final Editable s) {
            final EditText edit = (EditText) getCurrentFocus();
            if (edit != null && edit.length() == 1) {
                getToNextEditTextOrOK(edit);
            }
        }

        @Override
        public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
        }

        @Override
        public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
        }

    }
}
