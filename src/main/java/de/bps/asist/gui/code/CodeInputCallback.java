package de.bps.asist.gui.code;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by litho on 10.12.14.
 * Callback for when Code input is finished
 */
public interface CodeInputCallback extends Serializable {

    public void onCodeInputFinished(GenericAddActivity activity, String code);


}
