/**
 * 
 */
package de.bps.asist.gui.layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * @author thomasw
 * 
 */
public class SquareLayout extends LinearLayout {

	public SquareLayout(final Context context) {
		super(context);
	}

	public SquareLayout(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, widthMeasureSpec);
	}

}
