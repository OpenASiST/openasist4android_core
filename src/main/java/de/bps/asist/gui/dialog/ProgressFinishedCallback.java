package de.bps.asist.gui.dialog;

/**
 * Created by litho on 28.11.14.
 * Interface for handling finished events
 */
public interface ProgressFinishedCallback {

    public void onFinish();

    public void onError(Throwable e);


}
