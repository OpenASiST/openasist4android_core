package de.bps.asist.gui.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import de.bps.asist.R;

/**
 * An Dialog with buttons.
 * Created by thomasw on 27.08.14.
 */
@SuppressLint("ValidFragment")
public class ASiSTDialog extends DialogFragment {

    public static final String KEY_TITLE = "key_title";
    public static final String KEY_MESSAGE = "key_message";
    public static final String KEY_OK_TEXT = "key_ok_text";
    public static final String KEY_CANCEL_TEXT = "key_cancel_text";
    public static final String KEY_ENABLE_OK = "key_enable_ok";
    public static final String KEY_ENABLE_CANCEL = "key_enable_cancel";

    private final CharSequence title;
    private final CharSequence message;
    private int okText = R.string.dialog_ok;
    private int cancelText = R.string.dialog_cancel;
    private boolean enableOk = true;
    private boolean enableCancel = false;

    @SuppressLint("ValidFragment")
    public ASiSTDialog(CharSequence title, CharSequence message, boolean enableOk, boolean enableCancel) {
        this.title = title;
        this.message = message;
        this.enableOk = enableOk;
        this.enableCancel = enableCancel;
    }

    @SuppressLint("ValidFragment")
    public ASiSTDialog(CharSequence title, CharSequence message, int okText, int cancelText) {
        this.title = title;
        this.message = message;
        this.okText = okText;
        this.cancelText = cancelText;
    }

    public ASiSTDialog(CharSequence title, CharSequence message){
        this.title = title;
        this.message = message;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(title);

        builder.setMessage(message);

        if (enableOk) {
            builder.setPositiveButton(okText, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onOkClicked(dialog, which);
                }
            });
        }

        if (enableCancel) {
            builder.setNegativeButton(cancelText, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                onCancelClicked(dialog, which);
            }
        });
    }

       return builder.create();
    }

    public void onOkClicked(DialogInterface dialog, int which) {
        // override this in your class
        this.dismiss();
    }

    public void onCancelClicked(DialogInterface dialog, int which) {
        // override this in your class
        this.dismiss();
    }

    private String translate(int id){
        return getResources().getString(id);
    }

    public void setEnableCancel(boolean enableCancel) {
        this.enableCancel = enableCancel;
    }

    public void setEnableOk(boolean enableOk) {
        this.enableOk = enableOk;
    }
}
