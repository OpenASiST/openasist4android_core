package de.bps.asist.gui.dialog;

import android.app.ProgressDialog;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpStatus;

/**
* Created by litho on 28.11.14.
*/
public class ResponseHandler extends AsyncHttpResponseHandler {

    private static final String TAG = ResponseHandler.class.getSimpleName();

    private ProgressDialog dialog;
    private ProgressFinishedCallback finished;

    public ResponseHandler(ProgressDialog dialog, ProgressFinishedCallback finished) {
        this.dialog = dialog;
        this.finished = finished;
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        if (statusCode != HttpStatus.SC_OK) {
            //Log.w(TAG, "could not send StatusCode = " + statusCode);
        }
        dialog.dismiss();
        finished.onFinish();
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        //Log.e(TAG, "could not send lostFound", error);
        finished.onError(error);
    }

    @Override
    public void onProgress(int bytesWritten, int totalSize) {
        super.onProgress(bytesWritten, totalSize);
        if (totalSize > 1) {
            int percent = (int) Math.round((bytesWritten * 1.0 / totalSize) * 10000);
            dialog.setProgress(percent);
        }
    }
}
