package de.bps.asist.gui.detail;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.annotation.AnnotationHelper;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.module.AsistModuleActivity;

public class DetailedTableActivity<T extends IListItem> extends AsistModuleActivity<T> {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        doCreate();
	}

    protected void doCreate(){
        setContentView(R.layout.generic_list_view);
        ListView listView = getView(R.id.generic_list);

        final IListItem item = getSelectedItem();
        final Map<String, List<ASiSTDetailDescription>> tableMap = createDetailsMap(item);
        final DetailTableAdapter adapter = new DetailTableAdapter(this, tableMap, null);
        listView.setAdapter(adapter);
    }

    protected Map<String, List<ASiSTDetailDescription>> createDetailsMap(IListItem item) {
        final Map<Integer, ASiSTDetailDescription> map = AnnotationHelper.readASiSTDetails(item);
        final Map<String, List<ASiSTDetailDescription>> tableMap = new LinkedHashMap<>();
        for (final Integer pos : map.keySet()) {
            final ASiSTDetailDescription desc = map.get(pos);
            final String category = desc.getCategory();
            List<ASiSTDetailDescription> list = tableMap.get(category);
            if (list == null) {
                list = new ArrayList<>();
                tableMap.put(category, list);
            }
            list.add(desc);
        }
        return tableMap;
    }


}
