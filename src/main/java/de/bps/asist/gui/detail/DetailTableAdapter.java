package de.bps.asist.gui.detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import de.bps.asist.R;
import de.bps.asist.core.model.ASiSTDetailDescription;
import de.bps.asist.gui.list.CategoryListViewAdapter;
import de.bps.asist.gui.list.IGenericListCallback;

public class DetailTableAdapter extends CategoryListViewAdapter<ASiSTDetailDescription> {

	public DetailTableAdapter(final Context context, final Map<String, List<ASiSTDetailDescription>> objects, final IGenericListCallback callback) {
		super(context, objects, callback);
	}

	@Override
	protected LinearLayout addItemView(final int position, final LayoutInflater inflater) {
		final ASiSTDetailDescription item = (ASiSTDetailDescription) getItem(position);

		final LinearLayout row = (LinearLayout) inflater.inflate(R.layout.detail_list_item, null);

		final String title = getContext().getString(item.getI18nKey());
		((TextView) row.findViewById(R.id.detailItemTitle)).setText(title);

		final String value = item.getValue();
		((TextView) row.findViewById(R.id.detailItemValue)).setText(value);
		addCallback(item, row, getCallback());
		return row;
	}

}
