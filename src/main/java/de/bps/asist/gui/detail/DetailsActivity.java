package de.bps.asist.gui.detail;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.bps.asist.R;
import de.bps.asist.core.annotation.AsistAuthor;
import de.bps.asist.core.annotation.AsistDescription;
import de.bps.asist.core.annotation.AsistImage;
import de.bps.asist.core.annotation.AsistLink;
import de.bps.asist.core.annotation.AsistList;
import de.bps.asist.core.annotation.AsistTitle;
import de.bps.asist.core.annotation.AsistTitlePrefixKey;
import de.bps.asist.core.manager.database.DatabaseManager;
import de.bps.asist.core.model.IListItem;
import de.bps.asist.module.AsistModuleActivity;
import de.bps.asist.module.feeds.model.FeedDescription;
import de.bps.asist.module.feeds.model.FeedItem;
import de.bps.asist.module.lostandfound.model.LostFoundImage;

import static de.bps.asist.core.annotation.AnnotationHelper.getValueOf;
import static de.bps.asist.core.util.StringHelper.isNullOrEmpty;

/**
 * Created by litho on 26.08.14.
 * Activity for displaying details of an @link IListItem
 */
public class DetailsActivity<T extends IListItem> extends AsistModuleActivity<T> {

	private TextView tvTitle;
	private WebView tvDescription;
    private int detailContentView = R.layout.generic_details;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(detailContentView);

        tvTitle = (TextView) findViewById(R.id.generic_detailTitle);
        tvDescription = (WebView) findViewById(R.id.generic_detailWebview);

		String title = "<n/a>";
		String windowTitle = "<n/a>";
		String date = "<n/a>";
		String author = "";
		long itemfeedId = -1;

		boolean isFeed = false;
		int titlePrefix = 0;
		String titleImageUrl = null;
		String description = "<n/a>";
		String link = null;
		Collection<IListItem> items = null;

		final IListItem selected = getSelectedItem();
		if (selected != null) {
			title = getValueOf(selected, AsistTitle.class, title);
			try{
				FeedItem i = (FeedItem) selected;
				date = getString(R.string.library_details_publishedon) + ": " + new SimpleDateFormat(getString(R.string.datetime_format), Locale.getDefault()).format(i.getPubdate());
				String authorName = getValueOf(selected, AsistAuthor.class, author);
				if (authorName != null && authorName.trim().length() > 0) {
					author = getString(R.string.library_details_author) + ": " + authorName;
				}
				itemfeedId = i.getFeedId();
				isFeed = true;
			} catch(Exception e){
				date = "<n/a>";
				isFeed = false;
				//TODO: Check if this works with Lost and Found
			}
			titlePrefix = getValueOf(selected, AsistTitlePrefixKey.class, 0);
			Object imageUrl = getValueOf(selected, AsistImage.class);
            if(imageUrl instanceof LostFoundImage){
                titleImageUrl = translate(R.string.imageRootPath) + ((LostFoundImage) imageUrl).getImageUrl();
            }else if(!isFeed && imageUrl != null){
                titleImageUrl = getResources().getString(R.string.pictureProxyUrl) + imageUrl.toString() + "&size=500";
            }
			if(isFeed) {
				String descrValue = getValueOf(selected, AsistDescription.class, description);
				description = "<p><ul style=\"list-style-type: none; text-align: center; margin: 0; padding: 0; font-size: 0.8rem; font-color: " + R.color.descColor + "\"><li>" + date + "</li>";
				if (author.length() > 0) {
					description += "<li>"+ author + "</li>";
				}
				description += "</ul></p>" + descrValue;
			} else {
				description = getValueOf(selected, AsistDescription.class, description);
			}
			link = getValueOf(selected, AsistLink.class);
			if (isFeed && link != null) {
				description += "<p style=\"text-align: center; font-size: 0.8rem;\"><a href =\"" + link + "\">" + getString(R.string.library_details_link) + "</a></p>";
			}
			items = getValueOf(selected, AsistList.class);
		}
		if(isFeed) {
			// feeds
			final DatabaseManager<FeedDescription> db = new DatabaseManager<>(
					this, FeedDescription.class);
			final List<FeedDescription> NewsFeedItems = db.getAll();
			final DatabaseManager<FeedItem> dbi = new DatabaseManager<>(this,
					FeedItem.class);
			for (final FeedDescription feedDescription : NewsFeedItems) {
				if(feedDescription.getFeedid().equals(itemfeedId)){
					windowTitle = feedDescription.getTitle();
					setTitle(windowTitle);
					break;
				}

			}
		} else {
			if (!isNullOrEmpty(title)) {
				setTitle(title);
			}
		}

		////Log.i("DetailsActivity", "titlePrefix: " + titlePrefix+",imageUrl: "+titleImageUrl);
		if (titlePrefix != 0) {
			title = getResources().getString(titlePrefix) + " " + title;
		}

		////Log.i("DetailsActivity", "title: " + title);
		// tvTitle.setText(title.toUpperCase());
		tvTitle.setText(title);
        String fullHTML = buildHTML(description, titleImageUrl);
		tvDescription.loadData(fullHTML, "text/html; charset=utf-8", null);
        tvDescription.setBackgroundColor(Color.TRANSPARENT);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            tvDescription.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        }
	}

    private String buildHTML(String description, String imageUrl){
        String color = String.format("#%06X", (0xFFFFFF & R.color.categoryTitleColor));
        if(description == null){
            description = "";
        }

        //Spannable span = new SpannableString(Html.fromHtml(description));
        //Linkify.addLinks(span, Linkify.EMAIL_ADDRESSES | Linkify.WEB_URLS | Linkify.MAP_ADDRESSES);
        StringBuilder html = new StringBuilder();
        html.append("<html><head><style>img{display: inline;height: auto;max-width: 100%;}</style></head><body style=\"color: ");
        html.append(color);
        html.append("; background: transparent\">");
        if(!isNullOrEmpty(imageUrl)){
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            int width = metrics.widthPixels / 4;
            html.append("<img src=\"");
            html.append(imageUrl);
            html.append("\" style=\"float:left; width: 100%; max-width: 100%; display: inline; height: auto; margin-right: 1em;\" />");
        }
        //html.append(Html.toHtml(span));
		html.append(description);
        html.append("</body></html>");
        return html.toString();
    }

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		final int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onImageClick(final View view) {
		/*
		 * TODO final Intent mIntent = new Intent(this, BigImageActivity.class);
		 * mIntent.putExtra(BigImageActivity.KEY_IMAGE_LOCATION,
		 * BigImageActivity.IMAGE_LOCATION_URL);
		 * mIntent.putExtra(BigImageActivity.KEY_HEADER, mRss.getTitle());
		 * mIntent.putExtra(BigImageActivity.KEY_IMAGE_URL, mRss.getImageUrl());
		 * startActivity(mIntent);
		 */
	}

	public void onLinkClick(final View view) {
		/*
		 * TODO open internal browser and open link String link =
		 * tvLink.getText();
		 */
	}

    public void setDetailContentView(int detailContentView) {
        this.detailContentView = detailContentView;
    }
}
