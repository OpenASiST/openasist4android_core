package de.bps.asist.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import java.io.Serializable;

import androidx.fragment.app.Fragment;
import de.bps.asist.gui.list.AbstractGenericListCallback;

public abstract class AbstractASiSTFragment extends Fragment {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    protected String translate(final int id) {
        return getActivity().getString(id);
    }

    protected <T extends View> T getView(final View src, final int id) {
        @SuppressWarnings("unchecked")
        final T view = (T) src.findViewById(id);
        return view;
    }

    protected final String translate(final int resKey, final Object... params) {
        return getResources().getString(resKey, params);
    }

    public Integer getMenuLayout(){
        return null;
    }

    public boolean handleMenuItemClicked(final MenuItem item){
        return false;
    }

    protected void startActivity(Class<? extends Activity> activity) {
        Intent intent = new Intent(getActivity(), activity);
        startActivity(intent);
    }

    public void forwardWithObject(Class<? extends Activity> target, Serializable extra){
        AbstractGenericListCallback cb = new AbstractGenericListCallback(getActivity(), target);
        cb.doAction(extra);
    }

}
