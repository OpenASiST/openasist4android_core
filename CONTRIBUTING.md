# Mitwirken an der TUC-App

Willkommen bei der TUC-App,

wir möchten uns für Ihr Interesse an der TUC-App bedanken.
Mit Ihrer Teilnahme an der TUC-App bitten wir Sie, den [Verhaltenskodex](Code of Conduct) einzuhalten.

Weiterhin möchten wir Sie bitten, den [Gitflow-Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) zu verwenden, um Ihre Änderungen zur Verfügung zu stellen.
Dies hilft uns, den Überblick über veröffentlichte Änderungen zu behalten und erleichtert eine Diskussion.
Für eine einfachere Handhabung können Sie die ["git-flow"-Erweiterung](https://danielkummer.github.io/git-flow-cheatsheet/) installieren.
Somit erhalten Sie spezielle git-Kommandos, die den Gitflow-Workflow für Sie umsetzen.

## Verwendungsablauf von git

Mit Hilfe des Gitflow Workflow können wir den Überlick über eingereichte Features und Bugfixes wahren.
Doch wie setzt man diesen Workflow um?
In diesem Repository sind 2-Hauptbranches vorhanden.
Der "master"-Branch wird nie direkt verändert und beinhaltet immer die aktuell stabile Version dieser Software.
Der "master"-Branch kann nur verändert werden, wenn er mit dem Develop-Branch gemerged wird.
Deshalb werden jegliche Push-Anfragen auf dem "master"-Branch zurückgewiesen.
Weiterhin können nur ausgewählte Maintainer einen Merge in den "develop"-Branch durchführen, wodurch auch dieser vor unbefugten Push-Anfragen geschützt ist.
Möchten Sie Änderungen an dem Code vornehmen, müssen Sie einen weiteren Branch aus dem aktuellen "develop"-Branch erstellen.
```bash
git checkout develop # checkout current develop branch
git branch feature_branch # create new branch with name feature_branch
git checkout feature_branch # checkout created branch
```
Erfahrene git-Benutzer verwenden eine kürzere Variante.

```bash
git checkout develop # checkout current develop branch
git checkout -b feature_branch # creating new branch and checkout in a single step
```

In dem angelegten Branch können Sie Ihre Änderungen vornehmen und diese commiten.

```bash
git commit -a
```

Ein Branch, der von dem "develop"-Branch abgezweigt wurde und alle Commits zu einer konkreten Änderung enthält, wird Feature-Branch genannt.
Diese Branches sollten aussagekräftige Namen besitzen, damit leicht verständlich ist, welche Funktionalität in dem Branch entwickelt wird.
Ein Beispiel ist ein Branch, der ein neues Hintergrundbild für den Startbildschirm setzt.
Diesen Branch könnte man "new_background_for_startscreen" nennen.
Somit ist sofort ersichtlich, welche Änderugen in diesem Branch vorgenommen wurden.
Um diese Änderungen in den OpenAssist-Kern einbringen zu können, muss ein Merge-Request gestellt werden.
Dazu wird der Feature-Branch in das GitLab gepusht, damit ist der Branch öffentlich zugänglich.

```bash
git push origin feature-branch
```

Mithilfe des GitLabs kann ein Merge-Request gestellt werden.
Dabei ist der "develop"-Branch als Zielbranch zu wählen.
Der Merge-Request informiert die Maintainer, dass neue Änderungen für den OpenASiST-Kern zur Verfügung stehen.
Wird der Merge-Request angenommen, sind die Änderungen des Feature-Branches auch im "develop"-Branch verfügbar.  

Beachten Sie bitte folgende Richtlinien, damit wir Ihre Änderungen schnellstmöglichst bearbeiten können.
Für eine schnellstmögliche Bearbeitung Ihrer Beiträge sollten Sie folgende Richtlinen einhalten.
Commit-Nachrichten sind in englischer Sprache zu verfassen und beschreiben kurz alle Auswirkungen des Commits.
Weiterhin ist eine passive und neutrale Beschreibung der Auswirkungen zu bevorzugen.
Eine Erklärung der Implementierung ist an dieser Stelle unnötig, weil diese Aufgabe eine Dokumentation des Codes übernimmt.
Sinnvolle Commit-Nachrichten sind:
  * 'Feed module deletes outdated news'
  * 'Canteen module supports moving between days using a swipe gesture'

Sind Ihre Commits in einer brauchbaren Form, können Sie einen Merge-Request stellen.
Der Ziel-Branch sollte immer der 'develop'-Branch sein, da dieser den derzeitigen Entwicklungsstand repräsentiert.
Weiterhin sollten Sie eine Beschreibung der Lösung zu dem Merge-Request beifügen.
Möchten Sie mit Ihren Änderungen einen Issue lösen, nennen Sie den Issue in dem request-Titel oder in der Beschreibung.
Somit wird der gelöste Issue automatisch geschlossen, wenn der Request akzeptiert wurde.
Dieses Vorgehen hilft, die Issues aktuell zu halten und mindert den Verwaltungsaufwand dieser.
Der zu mergende Branch ist mit einem sinnvollen und aussagekräftigen Namen zu benennen. 

## App-Design

Die App soll sich in das native System einfügen, somit sind UI-Änderungen entsprechend der Guidelines des nativen Systems durchzuführen.

## Coding Styles

Bitte halten Sie folgende Coding Styles ein!
Wir werden Beiträge ablehnen, die nicht den Richtlinien entsprechen.

### Vollständige Import-Anweisungen

Importieren Sie Klassen mit ihren voll qualifizierten Namen.
Die Verwendung von &ast;-Import kann zu Nebeneffekten führen, wenn Klassen mit gleichen Namen in unterschiedlichen Paketen implementiert sind.

```java
import java.util.Date;
```

Bitte keine Import-Anweisungen in dieser Form!

```java
import java.util.*;
```

### Alphabetisch sortierte Importanweisungen

Sortieren Sie die verwendeten Import-Anweisungen alphabetisch.

### Exception Kurzform

Verwenden Sie die Kurzform, um auf Exceptions zu verweisen.

```java
try
{
}
catch (IOException e)
{
}
```

Falsch:

```java
try
{
}
catch (java.io.IOException e)
{
}
```

### Leerzeichen statt Tabulatoren

Benutzen Sie Leerzeichen anstatt Tabulatoren. Die Standard Tabulatoren-Weite beträgt 4 Leerzeichen, dennoch weichen die Tabulatorenbreiten in unterschiedlichen Systemen voneinander ab.

### Initialisierung der Iteratoren

Vermeiden Sie es, Iteratoren außerhalb von Schleifen zu initialisieren.

```java
for (Iterator iter=getIterator(); iter.hasNext; )
{
}
```
Bitte nicht:

```java
Iterator iter=getIterator();
for (;iter.hasNext; )
{
}
```
### Klassendokumentation

Klassen und Methoden müssen ein verständliches JavaDoc besitzen. Bitte lesen Sie folgende [Guidelines](http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html). JavaDoc für Variablen und Konstanten sollten nicht länger als eine Zeile sein.

```java
/**
 * Ein Raum, der die Personen verwaltet die in ihm sind.
 */
class Room
{
    /**
     * Fügt eine Person zu einem Raum hinzu.
     *
     * @param person Person, die zu dem Raum hinzugefügt wird.
     */
    void add( Person person)
    {
      ...
    }

    /**
     * Berechnet das Durchschnittsalter anhand der Personen im Raum.
     *
     * @return Durchschnittsalter der aller Personen im Raum.
     */
    void averagePersonAge()
    {
      ...
    }
  ...
}
```

### Vergleiche mit "true"

Vergleiche mit true sind unnötig und sollten nicht benutzt werden.
Das folgende Beispiel verdeutlicht einen solchen Vergleich.

```java
if ( foo==true ) {
    doSomething();
}
```

Wir werden Sie darauf hinweisen, Beitrage wie diese zu ändern.

### Variablennamen und Datenfeldnamen

Bitte vergeben Sie aussagekräftige Variablennamen. Diese dürfen etwas länger ausfallen.

```java
class Room {

    /**
     * Beinhaltet die maximale Anzahl an Schülern in einem Raum
     */
    private int maxStudentNumber = 42;
}
```

Bitte nicht:

```java
class Room {

    /**
     * Beinhaltet die maximale Anzahl an Schülern in einem Raum
     */
    private int int1 = 42;
}
```

## Bibliotheken

Wir bitten Sie, folgende Aspekte zu beachten, bevor Sie eine neue Bibliothek in das Projekt einführen:

* Existiert bereits eine Bibliothek mit gleicher oder ähnlicher Funktionalität, dann ist diese zu benutzen, anstatt eine weitere Bibliothek mit ähnlichem Funktionsumfang zu verwenden.
* Die Lizenz der Bibliothek muss mit der Apache-Lizenz des Projektes kompatibel sein.
* Es werden keine experimentellen Bibliotheken in das Projekt aufgenommen.
* Die Bibliothek muss noch gepflegt werden.

Wir werden Beiträge, die Änderungen an den Bibliotheken enthalten, genauer prüfen, um die Qualität der App zu erhalten.
Bitte haben Sie Verständnis, wenn Ihr Beitrag eine längere Bearbeitungszeit in Anspruch nimmt.

### Lizenzen

Es werden keine Bibliotheken mit folgender Lizenz in das Projekt mit aufgenommen:

* LGPL
* GPL
* Proprietäre Lizenzen

## Medien

Medien, die Sie in das Projekt einbringen möchten, müssen unter einer freien Lizenz für Medien gestellt werden.
Medienlizenzen, die in diesem Projekt akzeptiert werden:

* CC0
* CC-BY
* CC-BY-SA

Vielen Dank für Ihre Kooperation und Unterstützung.